FROM alpine AS builder

COPY output/linux/ /archon/

# Pick the architecture
RUN ARCH= && dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) ARCH='amd64';; \
    aarch64) ARCH='arm64';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac && echo "ARCH is ${ARCH}" && cp -r /archon/$ARCH/* /archon/


FROM alpine:latest

ENV TZ=Asia/Shanghai

WORKDIR /archon

COPY --from=builder /archon/archon-api /archon/archon-api
COPY --from=builder /archon/archon-manager /archon/archon-manager

CMD ["/archon/archon-api"]

ENTRYPOINT ["/bin/sh", "-c", "/archon/archon-api \"$0\" \"$@\""]
