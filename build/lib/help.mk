# Copyright 2019 THL A29 Limited, a Tencent company.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# ==============================================================================
# Help

define HELPTEXT
Usage: make <OPTIONS> ... <TARGETS>

Targets:
    build              Build source code for host platform.
    build.all          Build source code for all platforms.
                       Best done in the cross build container
                       due to cross compiler dependencies.
    image              Build docker images and push to registry.
	deploy             Deploy updated components to development env.
	deploy.all         Deploy all components to development env.
    lint               Check syntax and styling of go sources.
    test               Run unit test.
    clean              Remove all files that are created by building.
    help               Show this help info.

Options:
    DEBUG        Whether to generate debug symbols. Default is 0.
    IMAGES       Backend images to make. All by default.
    PLATFORMS    The platform to build. Default is host platform and arch.
    BINS         The binaries to build. Default is all of cmd.
    VERSION      The version information compiled into binaries.
                 The default is obtained from git.
    V            Set to 1 enable verbose build. Default is 0.

endef
export HELPTEXT

