# Copyright 2019 THL A29 Limited, a Tencent company.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# ==============================================================================
# Makefile helper functions for docker image
#

DOCKER := docker
DOCKER_SUPPORTED_VERSIONS ?= 17|18|19

REGISTRY_PREFIX ?= index.alauda.cn/alaudaorg

# Determine image files by looking into hack/docker/*.Dockerfile
IMAGE_FILES=$(wildcard ${ROOT_DIR}/build/docker/*.Dockerfile)
# Determine images names by stripping out the dir names
IMAGES=$(foreach image,${IMAGE_FILES},$(subst .Dockerfile,,$(notdir ${image})))

ifeq (${IMAGES},)
  $(error Could not determine IMAGES, set ROOT_DIR or run in source dir)
endif

.PHONY: image.build.verify
image.build.verify:
	ifneq ($(shell $(DOCKER) -v | grep -q -E '\bversion ($(DOCKER_SUPPORTED_VERSIONS)).*' && echo 0 || echo 1), 0)
		$(error unsupported docker version. Please make install one of the following supported version: '$(DOCKER_SUPPORTED_VERSIONS)')
	endif
		@echo "===========> Docker version verification passed"

.PHONY: image.build
image.build: image.build.verify go.build.verify $(addprefix image.build., $(IMAGES))

.PHONY: image.build.%
image.build.%: go.build.linux_amd64.%
	@echo "===========> Building $* $(VERSION) docker image"
	@$(DOCKER) build --pull -t $(REGISTRY_PREFIX)/$*:$(VERSION) -f $(ROOT_DIR)/build/docker/$*.Dockerfile .
	#@echo "===========> Pushing $* $(VERSION) image to $(REGISTRY_PREFIX)"
	#@$(DOCKER) push $(REGISTRY_PREFIX)/$*:$(VERSION)
