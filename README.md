## Archon

Archon(['ɑrkɑn]) aims to provide powerful features based on multiple kubernetes clusters.

## Features

* Support application create/udpate/rollback with full sub resources.

## Run

```console
$ make build
$ cd output/linux/amd64 # the output directory depends on your os
$ output/linux/amd64/archon-api --enable-multi-cluster -multi-cluster-host https://erebus
```

You can run `archon-api -h` to get the usage help.

## Documents

## API

Run the `archon` with `--swagger` and `--swagger-ui` arguments to enable swagger. Then open the browser to access `http://{archon-host}:8080/swagger-ui`, you will find the swagger api doc of `archon`.
