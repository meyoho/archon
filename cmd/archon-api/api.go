package main

import (
	"math/rand"
	"os"
	"runtime"
	"time"

	"alauda.io/archon/cmd/archon-api/app"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}

	app.NewApp("archon-api").Run()
}
