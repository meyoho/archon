package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
)

// Options options for archon-api
type Options struct {
	options.Optioner
}

// NewOptions new options for archon-api
func NewOptions(_ string) *Options {
	// recommended := options.NewRecommendedOptions().Options
	return &Options{
		Optioner: options.With(
			options.NewInsecureServingOptions(),
			options.NewMetricsOptions(),
			options.NewErrorOptions(),
			options.NewDebugOptions(),
			NewAPIServerOptions(),
			options.NewOpenAPIOptions(),
			options.NewClientOptions(),
		),
	}
}
