package options

import (
	"flag"

	"alauda.io/archon/pkg/api"
	"alauda.io/archon/pkg/api/config"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	utilfeature "k8s.io/apiserver/pkg/util/feature"
	"k8s.io/klog"
)

const (
	flagLabelBaseDomain = "label-base-domain"
	flagAlaudaNamespace = "alauda-namespace"
)

const (
	configLabelBaseDomain = "api.label_base_domain"
	configAlaudaNamespace = "api.alauda_namespace"
)

var _ options.Optioner = &ServerOptions{}

// ServerOptions contains configuration items related to apiserver attributes.
type ServerOptions struct {
	*api.Server
}

// NewAPIServerOptions creates a ServerOptions object with default parameters.
func NewAPIServerOptions() *ServerOptions {
	a := api.NewServer()
	a.Config.LabelBaseDomain = "alauda.io"
	a.Config.AlaudaNamespace = "alauda-system"
	return &ServerOptions{
		Server: a,
	}
}

// AddFlags adds flags for api to the specified FlagSet object.
func (o *ServerOptions) AddFlags(fs *pflag.FlagSet) {
	fs.String(flagLabelBaseDomain, o.Config.LabelBaseDomain,
		"The based domain of the resource label.")
	_ = viper.BindPFlag(configLabelBaseDomain, fs.Lookup(flagLabelBaseDomain))
	fs.String(flagAlaudaNamespace, o.Config.AlaudaNamespace,
		"The namespace of alauda components. Default is \"alauda-system\".")
	_ = viper.BindPFlag(configAlaudaNamespace, fs.Lookup(flagAlaudaNamespace))
	klogFs := flag.NewFlagSet("klog", flag.ContinueOnError)
	klog.InitFlags(klogFs)
	fs.AddGoFlagSet(klogFs)
	utilfeature.DefaultMutableFeatureGate.AddFlag(fs)
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *ServerOptions) ApplyFlags() []error {
	var errs []error

	o.Config.LabelBaseDomain = viper.GetString(configLabelBaseDomain)
	o.Config.AlaudaNamespace = viper.GetString(configAlaudaNamespace)

	config.SetConfig(o.Config)
	return errs
}

// ApplyToServer apply options on server
func (o *ServerOptions) ApplyToServer(srv server.Server) error {
	return o.Server.ApplyToServer(srv)
}
