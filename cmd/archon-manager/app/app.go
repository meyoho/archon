package app

import (
	"os"

	"alauda.io/archon/cmd/options"
	"alauda.io/archon/pkg/api"
	"alauda.io/archon/pkg/controller"
	srvopt "bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
	"bitbucket.org/mathildetech/app"

	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
)

const commandDesc = `This application is an manager of archon.`

// NewApp creates a new archon-api app
func NewApp(name string) *app.App {
	opts := options.NewOptions(name)
	application := app.NewApp("Alauda Archon Manager",
		name,
		app.WithOptions(opts),
		app.WithDescription(commandDesc),
		app.WithRunFunc(run(opts)),
	)
	return application
}

func run(opts *options.Options) app.RunFunc {
	return func(basename string) error {
		runManager(opts)
		return nil
	}
}

// RunManager run the manager of controllers.
func runManager(opts *options.Options) {
	var srvOpt *options.ServerOptions
	for _, opt := range opts.Optioner.(*srvopt.Options).Options {
		if sOpt, ok := opt.(*options.ServerOptions); ok {
			srvOpt = sOpt
			break
		}
	}
	if srvOpt == nil {
		klog.Errorf("unable to get ServerOptions")
		os.Exit(1)
	}
	srvConfig := srvOpt.Config
	klog.Info("Setting up client for manager")
	cfg, err := config.GetConfig()
	if err != nil {
		klog.Error(err, "unable to set up client config")
		os.Exit(1)
	}

	klog.Info("Setting up manager")
	mgr, err := manager.New(cfg, manager.Options{
		MetricsBindAddress:      "0",
		LeaderElectionNamespace: srvConfig.AlaudaNamespace,
		LeaderElectionID:        "archon-controller-leader",
		LeaderElection:          true,
		HealthProbeBindAddress:  ":8080",
	})
	if err != nil {
		klog.Error(err, "unable to set up overall controller manager")
		os.Exit(1)
	}

	klog.Info("Setting up scheme")
	if err := api.AddToScheme(mgr.GetScheme()); err != nil {
		klog.Error(err, "unable add APIs to scheme")
		os.Exit(1)
	}

	// Setup all Controllers
	klog.Info("Setting up controller")
	if err := controller.AddToManager(mgr); err != nil {
		klog.Error(err, "unable to register controllers to the manager")
		os.Exit(1)
	}

	klog.Info("Starting the Cmd.")
	if err := mgr.Start(signals.SetupSignalHandler()); err != nil {
		klog.Error(err, "unable to run the manager")
		os.Exit(1)
	}
}
