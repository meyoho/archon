package main

import (
	"math/rand"
	"os"
	"runtime"
	"time"

	"alauda.io/archon/cmd/archon-manager/app"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}

	app.NewApp("archon-manager").Run()
}
