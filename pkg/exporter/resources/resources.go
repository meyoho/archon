package resources

import (
	"alauda.io/archon/pkg/util"
	"github.com/prometheus/client_golang/prometheus"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

var GVRs = make(map[string]schema.GroupVersionResource)

// http://confluence.alauda.cn/pages/viewpage.action?pageId=61914927
var Guage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
	Name: "product_metric",
}, []string{"product", "version", "metricname"})

func init() {
	prometheus.MustRegister(Guage)
}

func SetGuage(resource string, val int) {
	Guage.With(prometheus.Labels{"product": "acp", "version": util.ProductVersion(), "metricname": resource}).Set(float64(val))
}
