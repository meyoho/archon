package resources

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func init() {
	GVRs["applications"] = schema.GroupVersionResource{
		Group:    "app.k8s.io",
		Version:  "v1beta1",
		Resource: "applications",
	}
}
