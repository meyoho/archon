package exporter

import (
	"alauda.io/archon/pkg/exporter/resources"
	"alauda.io/archon/pkg/util"
	"encoding/base64"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/klog"
	"sync"
	"time"
)

const ErebusEndpointTemplate = "https://erebus/kubernetes/%s"

var Timeout, _ = time.ParseDuration("10s")
var Interval, _ = time.ParseDuration("60s")

type Collector struct{}

func New() *Collector {
	return &Collector{}
}

func (c *Collector) Start() {
	ticker := time.NewTicker(Interval)
	for {
		select {
		case <-ticker.C:
			if err := c.collect(); err != nil {
				klog.Errorf("collect metrics failed, %v", err)
			}
		}
	}
}

func (c *Collector) getClusters() (map[string]string, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	di, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	ri := di.Resource(schema.GroupVersionResource{
		Group:    "clusterregistry.k8s.io",
		Version:  "v1alpha1",
		Resource: "clusters",
	}).Namespace(util.PodNamespace())
	sri := di.Resource(schema.GroupVersionResource{
		Version:  "v1",
		Resource: "secrets",
	})

	clusters, err := ri.List(metav1.ListOptions{})
	cinfo := map[string]string{}
	for _, c := range clusters.Items {
		ts, ok, err := unstructured.NestedMap(c.Object, "spec", "authInfo", "controller")
		if err != nil {
			klog.Errorf("Get secret from cluster %q error: %v", c.GetName(), err)
			continue
		}
		if !ok {
			klog.Errorf("Secret not found in cluster %q", c.GetName())
			continue
		}
		s, err := sri.Namespace(ts["namespace"].(string)).Get(ts["name"].(string), metav1.GetOptions{})
		if err != nil {
			klog.Errorf("Get secret %q in %q error: %v", ts["name"], ts["namespace"], err)
			continue
		}
		token64, ok, err := unstructured.NestedString(s.Object, "data", "token")
		if !ok {
			klog.Errorf("Token not found in secret %q", s.GetName())
			continue
		}
		token, err := base64.StdEncoding.DecodeString(token64)
		if err != nil {
			klog.Errorf("Token decode error: %v", err)
			continue
		}
		cinfo[c.GetName()] = string(token)
	}

	return cinfo, nil
}

func (c *Collector) collect() error {
	clusters, err := c.getClusters()
	if err != nil {
		return err
	}
	for metricsName, gvr := range resources.GVRs {
		sum := 0
		wg := sync.WaitGroup{}
		sumCh := make(chan int, len(clusters))
		for c, t := range clusters {
			cfg := rest.Config{
				Host:        fmt.Sprintf(ErebusEndpointTemplate, c),
				BearerToken: t,
				Timeout:     Timeout,
			}
			cfg.Insecure = true
			cdi, err := dynamic.NewForConfig(&cfg)
			if err != nil {
				klog.Errorf("Create dynamic interface for %q error: %v", c, err)
				return err
			}
			wg.Add(1)
			go func(p dynamic.Interface, c chan int) {
				defer wg.Done()
				client := p.Resource(schema.GroupVersionResource{
					Group:    gvr.Group,
					Version:  gvr.Version,
					Resource: gvr.Resource,
				}).Namespace("")
				list, err := client.List(metav1.ListOptions{})
				if err != nil {
					klog.Errorf("failed to list resource %s, err: %v", metricsName, err)
					return
				}
				c <- len(list.Items)
			}(cdi, sumCh)
		}
		wg.Wait()
		close(sumCh)
		for c := range sumCh {
			sum += c
		}
		resources.SetGuage(metricsName, sum)
	}
	return nil
}
