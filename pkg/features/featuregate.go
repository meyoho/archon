package features

import (
	utilfeature "k8s.io/apiserver/pkg/util/feature"
	"k8s.io/component-base/featuregate"
)

const (
	// owner: @hexiaoxi
	// alpha: v2.9
	// beta: v2.10
	// GA: v2.11
	//
	// ApplicationNG is a code refactor for application lifecycle management.
	ApplicationNG featuregate.Feature = "ApplicationNG"
)

func init() {
	utilfeature.DefaultMutableFeatureGate.Add(defaultFeatureGates)
}

var defaultFeatureGates = map[featuregate.Feature]featuregate.FeatureSpec{
	ApplicationNG: {Default: false, PreRelease: featuregate.Alpha, LockToDefault: false},
}
