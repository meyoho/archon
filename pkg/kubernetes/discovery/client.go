package discovery

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/klog"
)

type KubeClient struct {
	cluster string
	config  *rest.Config
	kc      kubernetes.Interface
	ic      dynamic.Interface
}

func NewKubeClient(cfg *rest.Config, cluster string) (*KubeClient, error) {
	kc, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	ic, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	c := KubeClient{
		cluster: cluster,
		config:  cfg,
		kc:      kc,
		ic:      ic,
	}
	if err := c.syncGroupVersion(false); err != nil {
		return nil, err
	}
	if err := c.syncAPIResourceMap(false); err != nil {
		return nil, err
	}
	return &c, nil
}

// syncGroupVersion happens in client init and new resource added
// if force == false, skip sync if already have data in the map
func (c *KubeClient) syncGroupVersion(force bool) error {
	if !force {
		_, ok := allAPIResourceMap.M[c.cluster]
		if ok {
			return nil
		}
	}
	klog.V(3).Infof("force resync group version info for cluster: %s", c.cluster)
	groups, err := c.kc.Discovery().ServerGroups()
	if err != nil {
		return err
	}
	klog.V(5).Infof("resync group version info %+v for cluster: %s", groups, c.cluster)
	allAPIGroupMap.Lock()
	allAPIGroupMap.M[c.cluster] = groups
	allAPIGroupMap.Unlock()

	return nil
}

// syncKindResourceMap happens in client init and new resource added
// if force == false, skip sync if already have data in the map
func (c *KubeClient) syncAPIResourceMap(force bool) error {
	if !force {
		_, ok := allAPIResourceMap.M[c.cluster]
		if ok {
			return nil
		}
	}
	klog.V(3).Infof("force resync api resources for cluster: %s", c.cluster)
	serverResourceList, err := c.kc.Discovery().ServerResources()
	if err != nil {
		// ignore GroupDiscoveryFailedError
		if !discovery.IsGroupDiscoveryFailedError(err) {
			return err
		}
		// ensure serverResourceList is a secure var
		if serverResourceList == nil {
			return err
		}
		klog.Warningf("%v", err)
	}
	klog.V(5).Infof("resync api resource info %+v for cluster: %s", serverResourceList, c.cluster)

	// set group and version
	for _, rl := range serverResourceList {
		gv, err := schema.ParseGroupVersion(rl.GroupVersion)
		if err != nil {
			klog.Errorf("parse group version for %s error: %s", rl.GroupVersion, err)
			continue
		}
		for idx := range rl.APIResources {
			rl.APIResources[idx].Group = gv.Group
			rl.APIResources[idx].Version = gv.Version
		}
	}

	allAPIResourceMap.Lock()
	allAPIResourceMap.M[c.cluster] = serverResourceList
	allAPIResourceMap.Unlock()
	return nil
}
