package discovery

import (
	"testing"

	"k8s.io/apimachinery/pkg/runtime/schema"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
)

func TestGetGroupVersionList(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	gvl, err := kubeclient.GetGroupVersionList()
	if gvl == nil {
		t.Error("GroupVersionList is nil")
	}
	if err != nil {
		t.Error(err)
	}
}

func TestGetApiResourceList(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	grl, err := kubeclient.GetApiResourceList()
	if grl == nil {
		t.Error("ApiResourceList is nil")
	}
	if err != nil {
		t.Error(err)
	}
}

func TestGetResourceTypeByKind(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		kind string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				"Pod",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				"PP",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetResourceTypeByKind(tt.args.kind)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetResourceTypeByGroupKind(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		gk v1.GroupKind
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist GroupKind",
			args: args{
				gk: v1.GroupKind{
					Group: "",
					Kind:  "Pod",
				},
			},
			expectErr: false,
		},
		{
			name: "Not Exist GroupKind",
			args: args{
				gk: v1.GroupKind{
					Group: "xxx",
					Kind:  "Pod",
				},
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetResourceTypeByGroupKind(tt.args.gk)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestIsSubResource(t *testing.T) {
	type args struct {
		resource v1.APIResource
	}
	tests := []struct {
		name   string
		args   args
		expect bool
	}{
		{
			name: "Not SubeResource",
			args: args{
				resource: v1.APIResource{
					Name: "pods",
				},
			},
			expect: false,
		},
		{
			name: "SubeResource",
			args: args{
				resource: v1.APIResource{
					Name: "v1/pods",
				},
			},
			expect: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if IsSubResource(&tt.args.resource) == tt.expect {

			}
		})
	}
}

func TestGetApiResourceByKind(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		kind string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				kind: "Pod",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				kind: "Podd",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetApiResourceByKind(tt.args.kind)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetApiResourceByGroupKind(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		gk v1.GroupKind
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist GroupKind",
			args: args{
				gk: v1.GroupKind{
					Group: "",
					Kind:  "Pod",
				},
			},
			expectErr: false,
		},
		{
			name: "Not Exist GroupKind",
			args: args{
				gk: v1.GroupKind{
					Group: "xxx",
					Kind:  "Pod",
				},
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetApiResourceByGroupKind(tt.args.gk)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetApiResourceByKindInsensitive(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		kind string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				kind: "pod",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				kind: "Podd",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetApiResourceByKindInsensitive(tt.args.kind)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetApiResourceByName(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		resource string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				resource: "pods",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				resource: "Podd",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetApiResourceByName(tt.args.resource, "")
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetVersionByGroup(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		group string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				group: "apps",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				group: "app",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetVersionByGroup(tt.args.group)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetGroupVersionByName(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		resource string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				resource: "pods",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				resource: "pod",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetGroupVersionByName(tt.args.resource, "")
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestGetGroupVersionByGroupKind(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		gk v1.GroupKind
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				gk: v1.GroupKind{
					Group: "",
					Kind:  "Pod",
				},
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				gk: v1.GroupKind{
					Group: "v2",
					Kind:  "Pod",
				},
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.GetGroupVersionByGroupKind(tt.args.gk)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestConfigForResource(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		resource string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				resource: "pods",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				resource: "podd",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.ConfigForResource(tt.args.resource, "")
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestIsClusterScopeResource(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		kind string
	}
	type expect struct {
		result bool
	}
	tests := []struct {
		name   string
		args   args
		expect expect
	}{
		{
			name: "Namespace",
			args: args{
				kind: "Namespace",
			},
			expect: expect{result: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if kubeclient.IsClusterScopeResource(tt.args.kind) != tt.expect.result {
				t.Errorf("IsClusterScopeResource expect %v", tt.expect.result)
			}
		})
	}
}

func TestIsNamespaceScoped(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		resource string
	}
	type expect struct {
		result bool
	}
	tests := []struct {
		name   string
		args   args
		expect expect
	}{
		{
			name: "Namespace",
			args: args{
				resource: "Namespace",
			},
			expect: expect{result: false},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			res, _ := kubeclient.IsNamespaceScoped(tt.args.resource)
			if res != tt.expect.result {
				t.Errorf("IsClusterScopeResource expect %v", tt.expect.result)
			}
		})
	}
}

func TestDynamicClientForResource(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		resource string
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist Kind",
			args: args{
				resource: "pods",
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				resource: "podd",
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.DynamicClientForResource(tt.args.resource, "")
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestDynamicClientForGroupKind(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		gk v1.GroupKind
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist GroupKind",
			args: args{
				gk: v1.GroupKind{
					Group: "",
					Kind:  "Pod",
				},
			},
			expectErr: false,
		},
		{
			name: "Not Exist Kind",
			args: args{
				gk: v1.GroupKind{
					Group: "",
					Kind:  "Podd",
				},
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.DynamicClientForGroupKind(tt.args.gk)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}

func TestClientForGVK(t *testing.T) {
	objs := []runtime.Object{}
	kubeclient := NewFakeKubeClient(objs...)
	type args struct {
		gvk schema.GroupVersionKind
	}
	tests := []struct {
		name      string
		args      args
		expectErr bool
	}{
		{
			name: "Exist GroupVersionKind",
			args: args{
				gvk: schema.GroupVersionKind{
					Group:   "",
					Version: "v1",
					Kind:    "Pod",
				},
			},
			expectErr: false,
		},
		{
			name: "Not Exist GroupVersionKind",
			args: args{
				gvk: schema.GroupVersionKind{
					Group:   "",
					Version: "",
					Kind:    "Pod",
				},
			},
			expectErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := kubeclient.ClientForGVK(tt.args.gvk)
			if err != nil && !tt.expectErr {
				t.Error(err)
			}
		})
	}
}
