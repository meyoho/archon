package discovery

import "testing"

func TestNewTypeNotFoundError(t *testing.T) {
	msg := "Testing Not Found Error"
	err := NewTypeNotFoundError(msg)
	if err.message != msg {
		t.Error(err)
	}
}

func TestError(t *testing.T) {
	msg := "Testing Error()"
	err := NewTypeNotFoundError(msg)
	if err.Error() != msg {
		t.Error(err)
	}
}

func TestIsResourceTypeNotFound(t *testing.T) {
	msg := "Testing IsResourceTypeNotFound()"
	err := NewTypeNotFoundError(msg)
	if !IsResourceTypeNotFound(err) {
		t.Error(err)
	}
}
