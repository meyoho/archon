package kubernetes

import (
	"alauda.io/archon/pkg/kubernetes/discovery"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type ClientBuilderNewFunc func(cfg *rest.Config, cluster string) ClientBuilderInterface

type ClientBuilderInterface interface {
	KubeClient() (KubeClientInterface, error)
	// Deprecated: Use NamespacedDynamicClientByGroupKind instead.
	NamespacedDynamicClient(resource, version, namespace string) (ri dynamic.ResourceInterface, err error)
	// TODO: rename this function
	NamespacedDynamicClientByGroupKind(o v1.GroupKind, namespace string) (ri dynamic.ResourceInterface, err error)
	// Deprecated: Use NamespacedDynamicClientByGroupKind instead.
	ClientFromResource(o *unstructured.Unstructured) (dynamic.ResourceInterface, error)
	// TODO: refactor this function and simplify code logic
	ClientFromResourceKind(kind, namespace string) (dynamic.ResourceInterface, error)
	ClientSet() (kubernetes.Interface, error)
}

//  TODO: Remove  dependency cyborg
type KubeClientInterface interface {
	GetGroupVersionList() (*v1.APIGroupList, error)
	GetApiResourceList() ([]*v1.APIResourceList, error)
	GetResourceTypeByKind(kind string) (string, error)
	GetResourceTypeByGroupKind(gk v1.GroupKind) (string, error)
	GetApiResourceByKind(kind string) (*v1.APIResource, error)
	GetApiResourceByGroupKind(gk v1.GroupKind) (*v1.APIResource, error)
	GetApiResourceByKindInsensitive(kind string) (*v1.APIResource, error)
	GetApiResourceByName(name string, preferredVersion string) (*v1.APIResource, error)
	GetVersionByGroup(group string) (string, error)
	GetGroupVersionByName(name string, preferredVersion string) (schema.GroupVersion, error)
	ConfigForResource(name string, preferredVersion string) (rest.Config, error)
	IsClusterScopeResource(kind string) bool
	IsNamespaceScoped(resource string) (bool, error)
	DynamicClientForResource(resource string, version string) (dynamic.NamespaceableResourceInterface, error)
	DynamicClientForGroupKind(gk v1.GroupKind) (dynamic.NamespaceableResourceInterface, error)
	ClientForGVK(gvk schema.GroupVersionKind) (dynamic.NamespaceableResourceInterface, error)
}

type kubeClientBuilder struct {
	cfg        *rest.Config
	cluster    string
	kubeClient func() (KubeClientInterface, error)
}

// NamespacedDynamicClient returns a client with namespace.
// The 'namespace' arg could be empty if the resource is cluster scoped.
func (b *kubeClientBuilder) NamespacedDynamicClient(resource, version, namespace string) (ri dynamic.ResourceInterface, err error) {
	c, err := b.kubeClient()
	if err != nil {
		return
	}

	dc, err := c.DynamicClientForResource(resource, version)
	if err != nil {
		return
	}
	namespaced, err := c.IsNamespaceScoped(resource)
	if err != nil {
		return
	}

	if namespaced {
		ri = dc.Namespace(namespace)
	} else {
		ri = dc
	}
	return
}

// NamespacedDynamicClientByGroupKind returns a client with namespace.
// The 'namespace' arg could be empty if the resource is cluster scoped.
func (b *kubeClientBuilder) NamespacedDynamicClientByGroupKind(o v1.GroupKind, namespace string) (ri dynamic.ResourceInterface, err error) {
	c, err := b.kubeClient()
	if err != nil {
		return
	}

	dc, err := c.DynamicClientForGroupKind(o)
	if err != nil {
		return
	}

	resource, err := c.GetApiResourceByGroupKind(o)
	if err != nil {
		return
	}

	if resource.Namespaced {
		ri = dc.Namespace(namespace)
	} else {
		ri = dc
	}
	return
}

// ClientFromResource returns a dynamic client from resource object.
func (b *kubeClientBuilder) ClientFromResource(o *unstructured.Unstructured) (ri dynamic.ResourceInterface, err error) {
	c, err := b.kubeClient()
	if err != nil {
		return
	}
	gvk := o.GroupVersionKind()
	resourceType, err := c.GetResourceTypeByGroupKind(v1.GroupKind{
		Group: gvk.Group,
		Kind:  gvk.Kind,
	})
	if err != nil {
		return
	}

	dc, err := c.DynamicClientForResource(resourceType, o.GetAPIVersion())
	if err != nil {
		return nil, err
	}
	if o.GetNamespace() != "" {
		ri = dc.Namespace(o.GetNamespace())
	} else {
		ri = dc
	}
	return
}

// ClientFromResourceKind returns a dynamic client from Kind, Namespace.
func (b *kubeClientBuilder) ClientFromResourceKind(kind, namespace string) (ri dynamic.ResourceInterface, err error) {
	c, err := b.kubeClient()
	if err != nil {
		return
	}

	ar, err := c.GetApiResourceByKindInsensitive(kind)
	if err != nil {
		return
	}

	ri, err = b.NamespacedDynamicClient(ar.Name, "", namespace)
	if err != nil {
		return
	}
	return
}

//ClientSet will return kubernetes.Interface
func (b *kubeClientBuilder) ClientSet() (kubernetes.Interface, error) {
	return kubernetes.NewForConfig(b.cfg)
}

// DefaultClientBuilder returns a default ClientBuilder.
func DefaultClientBuilder(cfg *rest.Config, cluster string) ClientBuilderInterface {
	c := &kubeClientBuilder{
		cfg:     cfg,
		cluster: cluster,
	}
	c.kubeClient = c.KubeClient
	return c
}

// KubeClient returns a new KubeClient instance.
func (b *kubeClientBuilder) KubeClient() (KubeClientInterface, error) {
	return discovery.NewKubeClient(b.cfg, b.cluster)
}
