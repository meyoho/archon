package kubernetes

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/ghodss/yaml"
	"github.com/juju/errors"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/api/autoscaling/v2beta1"
	corev1 "k8s.io/api/core/v1"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/klog"
)

const (
	// KubernetesMetadataKey is the key of 'metadata' in k8s resource.
	KubernetesMetadataKey = "metadata"
	// KubernetesSpecKey is the key of 'spec' in k8s resource.
	KubernetesSpecKey = "spec"
	// KubernetesAnnotationsKey is the key of 'annotations' in k8s resource.
	KubernetesAnnotationsKey = "annotations"
	// KubernetesReplicasKey is the key of 'replicas' in k8s workload resource.
	KubernetesReplicasKey = "replicas"
)

// NewControllerRef creates an OwnerReference pointing to the given owner.
func NewControllerRef(owner *unstructured.Unstructured, gvk schema.GroupVersionKind) *metav1.OwnerReference {
	blockOwnerDeletion := true
	isController := true
	return &metav1.OwnerReference{
		APIVersion:         gvk.GroupVersion().String(),
		Kind:               gvk.Kind,
		Name:               owner.GetName(),
		UID:                owner.GetUID(),
		BlockOwnerDeletion: &blockOwnerDeletion,
		Controller:         &isController,
	}
}

//CreateJobFromCronJob create Job From CronJob
func CreateJobFromCronJob(cronjob *unstructured.Unstructured, jApiversion, cjApiversion *metav1.APIResource) (job *unstructured.Unstructured, err error) {

	cronjobObj := cronjob.Object
	cronTemplate, isFound, err := unstructured.NestedMap(cronjobObj, "spec", "jobTemplate")
	if !isFound {
		err = errors.New("Not found jobTemplate from cronjob")
		return
	}
	if err != nil {
		return
	}

	labels, isFound, err := unstructured.NestedStringMap(cronjobObj, "spec", "jobTemplate", "metadata", "labels")
	if !isFound || err != nil {
		klog.V(4).Infof("Not found labels from cronjob")
	}

	name := fmt.Sprintf("%s-%d", cronjob.GetName(), time.Now().Unix())
	annotations := cronjob.GetAnnotations()
	jGvk := schema.GroupVersionKind{
		Group:   jApiversion.Group,
		Version: jApiversion.Version,
		Kind:    jApiversion.Kind,
	}
	cjGvk := schema.GroupVersionKind{
		Group:   cjApiversion.Group,
		Version: cjApiversion.Version,
		Kind:    cjApiversion.Kind,
	}
	ownerReference := NewControllerRef(cronjob, cjGvk)

	job = &unstructured.Unstructured{Object: map[string]interface{}{}}
	for k, v := range cronTemplate {
		err = unstructured.SetNestedField(job.Object, v, k)
		if err != nil {
			return
		}
	}

	job.SetAPIVersion(jApiversion.Version)
	job.SetAnnotations(annotations)
	job.SetLabels(labels)
	job.SetName(name)
	job.SetOwnerReferences([]metav1.OwnerReference{*ownerReference})
	job.SetGroupVersionKind(jGvk)
	return
}

// GetKey returns an key string of k8s format by domain, resource and attribute.
func GetKey(domain, resource, attr string) string {
	return resource + "." + domain + "/" + attr
}

// CreatorKey returns the annotation key of creator.
func CreatorKey(domain string) string {
	return domain + "/creator"
}

// CreatorOfObject returns creator of the object. If not found, returns empty string.
func CreatorOfObject(domain string, o runtime.Object) string {
	om, err := meta.Accessor(o)
	if err != nil {
		return ""
	}
	anno := om.GetAnnotations()
	if anno != nil {
		return anno[CreatorKey(domain)]
	}
	return ""
}

// IsClusterScopeKind returns true if the kind is cluster scope.
func IsClusterScopeKind(kind string) bool {
	return ClusterScopeResourceKind[kind]
}

// IsPodController returns true if the kind is pod controller.
func IsPodController(kind string) bool {
	return PodControllerKind[kind]
}

// AddLabels add labels map to the k8s resource object. If an label is
// already existed, it will be overridden.
func AddLabels(o v1.Object, add map[string]string) {
	labels := o.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
	}
	for k, v := range add {
		labels[k] = v
	}
	o.SetLabels(labels)
}

// RemoveLabels removes labels map from the k8s resource object. If an label is
// not found, it will be ignored.
func RemoveLabels(o v1.Object, remove map[string]string) {
	labels := o.GetLabels()
	if labels == nil {
		return
	}
	for k := range remove {
		delete(labels, k)
	}
	o.SetLabels(labels)
}

// AddAnnotations add annotations map to the k8s resource object. If an
// annotation is already existed, it will be overridden.
func AddAnnotations(o metav1.Object, add map[string]string) {
	annos := o.GetAnnotations()
	if annos == nil {
		annos = make(map[string]string)
	}
	for k, v := range add {
		annos[k] = v
	}
	o.SetAnnotations(annos)
}

// UpdatePodTemplate updates the annotations and labels of pod template
func UpdatePodTemplate(o *unstructured.Unstructured, annotations, labels map[string]string) error {
	podTemplateSpec, ok, err := unstructured.NestedMap(o.Object, "spec", "template")
	if err != nil || !ok {
		return fmt.Errorf("update pod template error")
	}
	if annotations != nil {
		if err := updatePodTemplateAnnotations(podTemplateSpec, annotations); err != nil {
			return err
		}
	}

	if labels != nil {
		if err := updatePodTemplateLabels(podTemplateSpec, labels); err != nil {
			return err
		}
	}

	unstructured.SetNestedMap(o.Object, podTemplateSpec, "spec", "template")
	return nil
}

func updatePodTemplateAnnotations(podTemplateSpec map[string]interface{}, annotations map[string]string) error {
	podAnnos, ok, err := unstructured.NestedMap(podTemplateSpec, "metadata", "annotations")
	if err != nil {
		return fmt.Errorf("update pod template annotations error")
	}
	if !ok {
		podAnnos = make(map[string]interface{})
	}
	for k, v := range annotations {
		podAnnos[k] = v
	}
	podTemplateSpec["metadata"].(map[string]interface{})["annotations"] = podAnnos
	return nil
}

func updatePodTemplateLabels(podTemplateSpec map[string]interface{}, labels map[string]string) error {
	podLabels, ok, err := unstructured.NestedMap(podTemplateSpec, "metadata", "labels")
	if err != nil {
		return fmt.Errorf("update pod template labels error")
	}
	if !ok {
		podLabels = make(map[string]interface{})
	}
	for k, v := range labels {
		podLabels[k] = v
	}
	podTemplateSpec["metadata"].(map[string]interface{})["labels"] = podLabels
	return nil
}

// ObjectString returns the string representation of a kubernetes object.
func ObjectString(o runtime.Object) string {
	objMeta, err := meta.Accessor(o)
	if err != nil {
		return fmt.Sprintf("<invalid object>")
	}
	concatName := ""
	if objMeta.GetNamespace() != "" {
		concatName = strings.Join([]string{objMeta.GetNamespace(), objMeta.GetName()}, "/")
	} else {
		concatName = objMeta.GetName()
	}
	return fmt.Sprintf("%s, %s",
		o.GetObjectKind().GroupVersionKind().String(),
		concatName)
}

// ObjectToUnstructured converts a runtime.Object to an Unstructured object.
func ObjectToUnstructured(obj runtime.Object) (*unstructured.Unstructured, error) {
	_, ok := obj.(*unstructured.Unstructured)
	if ok {
		return obj.(*unstructured.Unstructured), nil
	}
	unstruct := &unstructured.Unstructured{}
	raw, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	err = unstruct.UnmarshalJSON(raw)
	if err != nil {
		return nil, err
	}
	return unstruct, nil
}

// CleanAutogeneratedFunc cleans auto-generated field of an k8s object.
type CleanAutogeneratedFunc func(domain string, obj *unstructured.Unstructured)

// CleanPodControllerAutogenerated cleans auto-generated fields of a pod controller object.
func CleanPodControllerAutogenerated(_ string, obj *unstructured.Unstructured) {
	// Deal with 'spec.template.metadata.annotations.updateTimestamp' in
	// pod controller added by Application udpate.
	if IsPodController(obj.GetKind()) {
		unstructured.RemoveNestedField(
			obj.Object,
			KubernetesSpecKey,
			"template",
			KubernetesAnnotationsKey,
			"updateTimestamp")
	}
}

// CleanAutogenerated cleans the auto generated fields by
// kubernetes in the object. Accept other clean functions
// for specific resource kinds.
func CleanAutogenerated(domain string, obj *unstructured.Unstructured, cleanFuncs ...CleanAutogeneratedFunc) {
	obj.SetCreationTimestamp(metav1.Time{})
	obj.SetResourceVersion("")
	obj.SetSelfLink("")
	obj.SetUID("")
	unstructured.RemoveNestedField(obj.Object, "status")

	for _, cf := range cleanFuncs {
		cf(domain, obj)
	}
}

// UnstructuredToDaemonSet converts "Unstructured" type object to "DaemonSet" type.
func UnstructuredToDaemonSet(unstructured *unstructured.Unstructured) (*appsv1.DaemonSet, error) {
	data, err := unstructured.MarshalJSON()
	if err != nil {
		return nil, err
	}

	daemonset := &appsv1.DaemonSet{}
	err = json.Unmarshal(data, daemonset)
	if err != nil {
		return nil, err
	}
	return daemonset, nil
}

// UnstructuredToDeployment converts "Unstructured" type object to "Deployment" type.
func UnstructuredToDeployment(unstructured *unstructured.Unstructured) (*appsv1.Deployment, error) {
	data, err := unstructured.MarshalJSON()
	if err != nil {
		return nil, err
	}
	deploy := &appsv1.Deployment{}
	err = json.Unmarshal(data, deploy)
	if err != nil {
		return nil, err
	}
	return deploy, nil
}

// UnstructuredToStatefulSet converts "Unstructured" type object to "StatefulSet" type.
func UnstructuredToStatefulSet(unstructured *unstructured.Unstructured) (*appsv1.StatefulSet, error) {
	data, err := unstructured.MarshalJSON()
	if err != nil {
		return nil, err
	}
	stateful := &appsv1.StatefulSet{}
	err = json.Unmarshal(data, stateful)
	if err != nil {
		return nil, err
	}
	return stateful, nil
}

// UnstructuredToService converts "Unstructured" type object to "Service" type.
func UnstructuredToService(unstructured *unstructured.Unstructured) (*corev1.Service, error) {
	data, err := unstructured.MarshalJSON()
	if err != nil {
		return nil, err
	}
	service := &corev1.Service{}
	err = json.Unmarshal(data, service)
	if err != nil {
		return nil, err
	}
	return service, nil
}

// UnstructuredToHPA converts "Unstructured" type object to "HorizontalPodAutoscaler" type.
func UnstructuredToHPA(unstructured *unstructured.Unstructured) (*v2beta1.HorizontalPodAutoscaler, error) {
	data, err := unstructured.MarshalJSON()
	if err != nil {
		return nil, err
	}
	hpa := &v2beta1.HorizontalPodAutoscaler{}
	err = json.Unmarshal(data, hpa)
	if err != nil {
		return nil, err
	}
	return hpa, nil
}

//CreateResource is create universal resource
func CreateResource(obj *unstructured.Unstructured, clientBuild ClientBuilderInterface) (*unstructured.Unstructured, error) {

	if obj == nil {
		err := errors.New("Object Unstructured is nil")
		return nil, err
	}

	dynamicClient, err := clientBuild.ClientFromResource(obj)
	if err != nil {
		return nil, err
	}

	start := time.Now()
	result, err := dynamicClient.Create(obj, v1.CreateOptions{})
	if err != nil {
		return nil, err
	}
	klog.V(5).Infof("obj %s with body %+v cost: %v", result.GetName(), result.Object, time.Since(start))
	return result, err
}

//UpdateResource is update universal resource
func UpdateResource(obj *unstructured.Unstructured, clientBuild ClientBuilderInterface) (*unstructured.Unstructured, error) {

	if obj == nil {
		err := errors.New("Object Unstructured is nil")
		return nil, err
	}
	dynamicClient, err := clientBuild.ClientFromResource(obj)
	if err != nil {
		return nil, err
	}

	start := time.Now()
	result, err := dynamicClient.Update(obj, v1.UpdateOptions{})
	if err != nil {
		return nil, err
	}
	klog.V(5).Infof("obj %s with body %+v cost: %v", result.GetName(), result.Object, time.Since(start))
	return result, err
}

//CreateOrUpdateResource will create universal resource,If it not exist, will update it.
func CreateOrUpdateResource(obj *unstructured.Unstructured, clientBuild ClientBuilderInterface) (*unstructured.Unstructured, error) {

	if obj == nil {
		err := errors.New("Object Unstructured is nil")
		return nil, err
	}

	runObject, err := CreateResource(obj, clientBuild)
	if err != nil {
		if apiErrors.IsAlreadyExists(errors.Cause(err)) {

			klog.V(5).Info("resource already exist when create, update it")
			// exist, merge and update
			dynamicClient, err := clientBuild.ClientFromResource(obj)
			if err != nil {
				return nil, err
			}
			runObject, err := dynamicClient.Get(obj.GetName(), v1.GetOptions{})
			if err = MergeOriginData(runObject, obj); err != nil {
				return nil, err
			}

			return UpdateResource(obj, clientBuild)
		}
		return nil, err
	}
	return runObject, nil
}

// MergeOriginData will merge exist data with the new one. for some resources, this is needed.
// such as PVC, when bounded, it's spec will changed(bad design).
func MergeOriginData(old *unstructured.Unstructured, new *unstructured.Unstructured) error {
	new.SetUID(old.GetUID())
	new.SetResourceVersion(old.GetResourceVersion())
	switch old.GetKind() {
	case KindPVC:
		oldSpec, ok, err := unstructured.NestedMap(old.Object, "spec")
		if err != nil {
			return err
		}
		if ok {
			return unstructured.SetNestedField(new.Object, oldSpec, "spec")
		}
	case KindService:
		oldClusterIP, ok, err := unstructured.NestedString(old.Object, "spec", "clusterIP")
		if err != nil {
			return err
		}
		if ok {
			return unstructured.SetNestedField(new.Object, oldClusterIP, "spec", "clusterIP")
		}
	}
	return nil
}

// GetPodTemplateSpecFromUnstructured will return PodTemplateSpec from Unstructured
func GetPodTemplateSpecFromUnstructured(object unstructured.Unstructured) (templateSpec *corev1.PodTemplateSpec, err error) {

	templateSpec = &corev1.PodTemplateSpec{}
	templateObject, found, err := unstructured.NestedMap(object.Object, "spec", "template")

	if !found {
		err = errors.New("Not Found template")
		return
	}
	if err != nil {
		return
	}
	data, err := json.Marshal(templateObject)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, templateSpec)
	if err != nil {
		return nil, err
	}
	return
}

// UnstructuredToObject converts an Unstructured object to the target object.
func UnstructuredToObject(obj *unstructured.Unstructured, target interface{}) error {
	raw, err := obj.MarshalJSON()
	if err != nil {
		return err
	}
	err = json.Unmarshal(raw, target)
	if err != nil {
		return err
	}
	return err
}

// AddOwnerReferenceIfKindNotExist adds an object as an owner reference in the target object if
// the specified kind of owner is not exist.
func AddOwnerReferenceIfKindNotExist(obj *unstructured.Unstructured, owner *unstructured.Unstructured, kind string) {
	refs := obj.GetOwnerReferences()
	for _, ref := range refs {
		if ref.Kind == kind {
			return
		}
		if ref.Kind == owner.GetKind() &&
			ref.Name == owner.GetName() &&
			ref.UID == owner.GetUID() {
			return
		}
	}
	controller := true
	blockOwnerDelete := true
	ref := metav1.OwnerReference{
		APIVersion:         owner.GetAPIVersion(),
		Kind:               owner.GetKind(),
		Name:               owner.GetName(),
		UID:                owner.GetUID(),
		Controller:         &controller,
		BlockOwnerDeletion: &blockOwnerDelete,
	}
	refs = append(refs, ref)
	obj.SetOwnerReferences(refs)
}

// AddOwnerReference adds an object as an owner reference in the target object.
func AddOwnerReference(obj *unstructured.Unstructured, owner *unstructured.Unstructured) {
	AddOwnerReferenceIfKindNotExist(obj, owner, "")
}

// RemoveOwnerReference removes an owner reference from an object.
func RemoveOwnerReference(obj *unstructured.Unstructured, owner *unstructured.Unstructured) {
	var newRefs []metav1.OwnerReference
	refs := obj.GetOwnerReferences()
	for _, ref := range refs {
		if ref.Kind == owner.GetKind() &&
			ref.Name == owner.GetName() &&
			ref.UID == owner.GetUID() {
			continue
		}
		newRefs = append(newRefs, ref)
	}
	obj.SetOwnerReferences(newRefs)
}

// ResourceTypeForKind returns resource type of the kind.
func ResourceTypeForKind(cb ClientBuilderInterface, kind string) (resource string, err error) {
	kc, err := cb.KubeClient()
	if err != nil {
		return
	}
	resource, err = kc.GetResourceTypeByKind(kind)
	if err != nil {
		return
	}

	return
}

func InterfaceToUnstructured(o interface{}) (*unstructured.Unstructured, error) {
	uo := &unstructured.Unstructured{}
	b, err := json.Marshal(o)
	if err != nil {
		return nil, err
	}
	err = uo.UnmarshalJSON(b)
	return uo, err
}

// RetryOnError will run fn and retry when Err
func RetryOnError(backoff wait.Backoff, fn func() error) error {
	var lastErr error
	err := wait.ExponentialBackoff(backoff, func() (bool, error) {
		err := fn()
		switch {
		case err == nil:
			return true, nil
		default:
			return false, nil
		}
	})
	if err == wait.ErrWaitTimeout {
		err = lastErr
	}
	return err
}

func ToRuntimeObjects(objs []*unstructured.Unstructured) (robjs []runtime.Object) {
	for _, o := range objs {
		o := o
		robjs = append(robjs, o)
	}
	return
}

func YamlToUnstructuredArray(yamlBytes []byte) (objects []unstructured.Unstructured, err error) {
	err = yaml.Unmarshal(yamlBytes, &objects)
	if err != nil {
		return
	}
	return
}
