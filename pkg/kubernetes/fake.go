package kubernetes

import (
	"alauda.io/archon/pkg/test"
	"fmt"
	"strings"

	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/fake"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

type FakeKubeClient struct {
	cs           *fake.FakeDynamicClient
	apiGroups    *metav1.APIGroupList
	apiResources []*metav1.APIResourceList
}

func NewFakeKubeClient(objects ...runtime.Object) KubeClientInterface {
	c := &FakeKubeClient{
		cs:        fake.NewSimpleDynamicClient(scheme.Scheme, objects...),
		apiGroups: &metav1.APIGroupList{},
	}
	test.ObjectFromFixture("api_groups.json", c.apiGroups)
	test.ObjectFromFixture("api_resources.json", &c.apiResources)
	return c
}

func (c *FakeKubeClient) FakeDynamicClient() *fake.FakeDynamicClient {
	return c.cs
}

func (c *FakeKubeClient) GetGroupVersionList() (*metav1.APIGroupList, error) {
	return c.apiGroups, nil
}

func (c *FakeKubeClient) GetApiResourceList() ([]*metav1.APIResourceList, error) {
	return c.apiResources, nil
}

func (c *FakeKubeClient) GetResourceTypeByKind(kind string) (string, error) {
	for _, rl := range c.apiResources {
		for _, r := range rl.APIResources {
			if r.Kind == kind {
				return r.Name, nil
			}
		}
	}
	return "", fmt.Errorf("kind not found")
}

func (c *FakeKubeClient) GetApiResourceByKind(kind string) (*metav1.APIResource, error) {
	for _, rl := range c.apiResources {
		for _, r := range rl.APIResources {
			if r.Kind == kind {
				gv, _ := schema.ParseGroupVersion(rl.GroupVersion)
				r.Group = gv.Group
				r.Version = gv.Version
				return &r, nil
			}
		}
	}
	return nil, fmt.Errorf("kind not found")
}

func (c *FakeKubeClient) GetApiResourceByKindInsensitive(kind string) (*metav1.APIResource, error) {
	for _, rl := range c.apiResources {
		for _, r := range rl.APIResources {
			if strings.ToUpper(r.Kind) == strings.ToUpper(kind) {
				return &r, nil
			}
		}
	}
	return nil, fmt.Errorf("kind not found")
}

func (c *FakeKubeClient) GetApiResourceByGroupKind(gk metav1.GroupKind) (*metav1.APIResource, error) {
	for _, rl := range c.apiResources {
		gv, _ := schema.ParseGroupVersion(rl.GroupVersion)
		if gv.Group != gk.Group {
			continue
		}
		for _, r := range rl.APIResources {
			if r.Kind == gk.Kind {
				r.Group = gv.Group
				r.Version = gv.Version
				return &r, nil
			}
		}
	}
	return nil, fmt.Errorf("group kind %s not found", gk)
}

func (c *FakeKubeClient) GetApiResourceByName(name string, preferredVersion string) (*metav1.APIResource, error) {
	for _, rl := range c.apiResources {
		gv, _ := schema.ParseGroupVersion(rl.GroupVersion)
		for _, r := range rl.APIResources {
			if r.Name == name && (preferredVersion == "" || rl.GroupVersion == preferredVersion) {
				r.Group = gv.Group
				r.Version = gv.Version
				return &r, nil
			}
		}
	}
	return nil, fmt.Errorf("kind for %q not found with preferredVersion %q", name, preferredVersion)
}

func (*FakeKubeClient) GetVersionByGroup(group string) (string, error) {
	return "", nil
}

func (*FakeKubeClient) GetGroupVersionByName(name string, preferredVersion string) (schema.GroupVersion, error) {
	return schema.GroupVersion{}, nil
}

func (*FakeKubeClient) ConfigForResource(name string, preferredVersion string) (rest.Config, error) {
	return rest.Config{}, nil
}

func (c *FakeKubeClient) IsClusterScopeResource(kind string) bool {
	r, _ := c.GetApiResourceByKind(kind)
	return !r.Namespaced
}

func (c *FakeKubeClient) IsNamespaceScoped(resource string) (bool, error) {
	r, err := c.GetApiResourceByName(resource, "")
	if err != nil {
		return false, err
	}
	return r.Namespaced, nil
}

func (c *FakeKubeClient) DynamicClientForResource(resource string, version string) (dynamic.NamespaceableResourceInterface, error) {
	r, err := c.GetApiResourceByName(resource, version)
	if err != nil {
		return nil, err
	}
	return c.cs.Resource(schema.GroupVersionResource{
		Group: r.Group, Version: r.Version, Resource: r.Name}), nil
}

func (c *FakeKubeClient) ClientForGVK(gvk schema.GroupVersionKind) (dynamic.NamespaceableResourceInterface, error) {
	rt, err := c.GetResourceTypeByKind(gvk.Kind)
	if err != nil {
		return nil, err
	}
	return c.cs.Resource(schema.GroupVersionResource{
		Group: gvk.Group, Version: gvk.Version, Resource: rt}), nil
}

func (c *FakeKubeClient) GetResourceTypeByGroupKind(gk metav1.GroupKind) (string, error) {
	for _, rl := range c.apiResources {
		gv, err := schema.ParseGroupVersion(rl.GroupVersion)
		if err != nil {
			return "", err
		}
		for _, r := range rl.APIResources {

			if r.Kind == gk.Kind && gv.Group == gk.Group {
				return r.Name, nil
			}
		}
	}
	return "", fmt.Errorf("kind not found")
}

func (c *FakeKubeClient) DynamicClientForGroupKind(gk metav1.GroupKind) (dynamic.NamespaceableResourceInterface, error) {
	r, err := c.GetApiResourceByKind(gk.Kind)
	if err != nil {
		return nil, err
	}
	return c.cs.Resource(schema.GroupVersionResource{
		Group: r.Group, Version: r.Version, Resource: r.Name}), nil
}

type FakeKubeClientBuilder struct {
	kubeClientBuilder
	client  *FakeKubeClient
	objects []runtime.Object
}

func NewFakeKubeClientBuilder(objects ...runtime.Object) *FakeKubeClientBuilder {
	return &FakeKubeClientBuilder{
		objects: objects,
	}
}

func (b *FakeKubeClientBuilder) FakeClientBuilder(cfg *rest.Config, cluster string) ClientBuilderInterface {
	c := &FakeKubeClientBuilder{
		kubeClientBuilder: kubeClientBuilder{
			cfg:     cfg,
			cluster: cluster,
		},
		client:  NewFakeKubeClient(b.objects...).(*FakeKubeClient),
		objects: b.objects,
	}
	c.kubeClient = c.KubeClient
	return c
}

func (b *FakeKubeClientBuilder) KubeClient() (KubeClientInterface, error) {
	return b.client, nil
}

func FakeRESTConfig(_ *restful.Request) (*rest.Config, error) {
	return new(rest.Config), nil
}
