package kubernetes

var (
	// ClusterScopeResourceKind defines the cluster scope kubernetes resource kinds.
	ClusterScopeResourceKind = map[string]bool{
		"Namespace":          true,
		"PersistentVolume":   true,
		"ServiceCatalog":     true,
		"ClusterRole":        true,
		"ClusterRoleBinding": true,
	}

	// PodControllerKind defines the pod controller kubernetes resource kinds.
	PodControllerKind = map[string]bool{
		"Deployment":  true,
		"DaemonSet":   true,
		"StatefulSet": true,
	}
)

const (
	// KindPVC is the string name of the PVC kind.
	KindPVC = "PersistentVolumeClaim"
	// KindService is the string name of the Service kind.
	KindService = "Service"
)
