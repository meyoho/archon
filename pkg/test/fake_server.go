package test

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	"github.com/spf13/pflag"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type FakeServer struct {
	server.DefaultServer
}

func NewFakeServer() server.Server {
	return &FakeServer{
		DefaultServer: *server.New("fake").(*server.DefaultServer),
	}
}

type FakeClientOptions struct{}

func (*FakeClientOptions) AddFlags(*pflag.FlagSet) {
	return
}

func (*FakeClientOptions) ApplyFlags() []error {
	return nil
}

func (*FakeClientOptions) ApplyToServer(srv server.Server) error {
	srv.SetManager(&FakeManager{})
	return nil
}

type FakeManager struct{}

func (*FakeManager) InsecureClient() (kubernetes.Interface, error) {
	return nil, nil
}

func (*FakeManager) Client(req *restful.Request) (kubernetes.Interface, error) {
	return nil, nil
}

func (*FakeManager) Config(req *restful.Request) (*rest.Config, error) {
	return &rest.Config{}, nil
}

func (*FakeManager) DynamicClient(req *restful.Request, gvk *schema.GroupVersionKind) (client dynamic.NamespaceableResourceInterface, err error) {
	return nil, nil
}

func (*FakeManager) ManagerConfig() client.Config {
	return client.Config{}
}
