package test

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"text/template"

	"github.com/emicklei/go-restful"
)

func FakeRestfulRequest() *restful.Request {
	fakeHttpReq, _ := http.NewRequest("GET", "fake", bytes.NewBufferString(""))
	return restful.NewRequest(fakeHttpReq)
}

func FakeRestfulResponse() (*restful.Response, *httptest.ResponseRecorder) {
	recorder := httptest.NewRecorder()
	return restful.NewResponse(recorder), recorder
}

type FakeRestful struct {
	Request  *restful.Request
	Response *restful.Response
	Recorder *httptest.ResponseRecorder
}

func FakeRestfulWithJSONFile(method, url, file string, args ...interface{}) (fr *FakeRestful) {
	fr = &FakeRestful{}
	body := FixtureFromFile(2, file)
	if len(args) > 0 {
		tpl := template.Must(template.New("test").Parse(body))
		var res bytes.Buffer
		for _, arg := range args {
			tpl.Execute(&res, arg)
		}
		body = res.String()
	}
	fr.Request = FakeRestfulRequestWithJSON(method, url, body)
	resp, recorder := FakeRestfulResponse()
	fr.Response = resp
	fr.Recorder = recorder
	return
}

func (f *FakeRestful) ReturnedResponse() *http.Response {
	return f.Recorder.Result()
}

func (f *FakeRestful) ReturnedBody() []byte {
	return f.Recorder.Body.Bytes()
}

func FakeRestfulRequestWithJSON(method, url, body string) *restful.Request {
	fakeHttpReq, _ := http.NewRequest(method, url, bytes.NewBufferString(body))
	fakeHttpReq.Header.Set("Content-Type", "application/json")
	return restful.NewRequest(fakeHttpReq)
}
