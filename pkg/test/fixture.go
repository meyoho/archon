package test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"runtime"
)

// FixtureFromFile returns the string content of the file by `fileName` in the `fixtures` directory of the caller.
// The `skip` param specifies the caller depth that should be skipped.
func FixtureFromFile(skip int, fileName string) string {
	_, curfile, _, _ := runtime.Caller(skip)
	f, err := os.Open(path.Join(path.Dir(curfile), "fixtures", fileName))
	if err != nil {
		return ""
	}
	raw, err := ioutil.ReadAll(f)
	if err != nil {
		return ""
	}
	return string(raw)
}

func ObjectFromFixture(fileName string, obj interface{}) {
	f := FixtureFromFile(2, fileName)
	json.Unmarshal([]byte(f), obj)
	return
}
