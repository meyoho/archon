package util

import (
	"archive/tar"
	"compress/gzip"
	"go.uber.org/zap/buffer"
	"gotest.tools/assert"
	"io"
	"testing"
)

func TestArchiveFilesToTGZ(t *testing.T) {
	tests := []struct {
		name    string
		files   []ArchiveFile
		wantErr bool
	}{
		{
			name: "ok",
			files: []ArchiveFile{
				{
					Name: "a",
					Body: "abcdef",
					Mode: 0600,
				},
				{
					Name: "b/c",
					Body: "123456",
					Mode: 0700,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotBuf, err := ArchiveFilesToTGZ(tt.files...)
			if (err != nil) != tt.wantErr {
				t.Errorf("ArchiveFilesToTGZ() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				gr, err := gzip.NewReader(gotBuf)
				assert.NilError(t, err)
				tr := tar.NewReader(gr)
				var dFiles []ArchiveFile
				for {
					h, err := tr.Next()
					if err == io.EOF {
						break
					}
					assert.NilError(t, err)
					if h.Typeflag == tar.TypeDir {
						continue
					}
					var body buffer.Buffer
					_, err = io.Copy(&body, tr)
					assert.NilError(t, err)
					dFiles = append(dFiles, ArchiveFile{
						Name: h.Name,
						Body: body.String(),
						Mode: h.Mode,
					})
				}
				assert.DeepEqual(t, dFiles, tt.files)
			}
		})
	}
}
