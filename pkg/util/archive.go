package util

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"time"
)

type ArchiveFile struct {
	Name string
	Body string
	Mode int64
}

func ArchiveFilesToTGZ(files ...ArchiveFile) (buf *bytes.Buffer, err error) {
	buf = &bytes.Buffer{}
	gw := gzip.NewWriter(buf)
	tw := tar.NewWriter(gw)
	for _, file := range files {
		hdr := &tar.Header{
			Name:    file.Name,
			Mode:    file.Mode,
			Size:    int64(len(file.Body)),
			ModTime: time.Now(),
		}
		if err = tw.WriteHeader(hdr); err != nil {
			return
		}
		if _, err = tw.Write([]byte(file.Body)); err != nil {
			return
		}
	}
	if err = tw.Close(); err != nil {
		return
	}
	err = gw.Close()
	return
}
