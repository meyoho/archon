package util

import (
	"os"
)

const (
	EnvPodNamespace   = "POD_NAMESPACE"
	EnvProductVersion = "PRODUCT_VERSION"
)

// PodNamespace returns the namespace of the archon pod.
func PodNamespace() string {
	return os.Getenv(EnvPodNamespace)
}

// ProductVersion returns the product version.
func ProductVersion() string {
	return os.Getenv(EnvProductVersion)
}
