package util

// MapStringObjToStringString converts map[string]interface{} to map[string]string.
func MapStringObjToStringString(obj map[string]interface{}) map[string]string {
	t := make(map[string]string)
	for k, v := range obj {
		t[k] = v.(string)
	}
	return t
}
