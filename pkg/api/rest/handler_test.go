package rest

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/kubernetes"
	"alauda.io/archon/pkg/test"

	"github.com/emicklei/go-restful"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/rest"
)

type invalidObjectMeta struct{}

func (*invalidObjectMeta) SetGroupVersionKind(kind schema.GroupVersionKind) {
	return
}

func (*invalidObjectMeta) GroupVersionKind() schema.GroupVersionKind {
	return schema.GroupVersionKind{
		Group:   "",
		Version: "v1",
		Kind:    "InvalidObjectMeta",
	}
}

func (o *invalidObjectMeta) GetObjectKind() schema.ObjectKind {
	return o
}

func (o *invalidObjectMeta) DeepCopyObject() runtime.Object {
	return &(*o)
}

func TestHandlerWriteError(t *testing.T) {
	tests := []struct {
		name       string
		err        error
		expectBody metav1.Status
	}{
		{
			name: "internal error",
			err:  errors.NewInternalError(fmt.Errorf("fake error")),
			expectBody: metav1.Status{
				TypeMeta: metav1.TypeMeta{
					Kind:       "Status",
					APIVersion: "v1",
				},
				Status: metav1.StatusFailure,
				Code:   http.StatusInternalServerError,
				Reason: metav1.StatusReasonInternalError,
				Details: &metav1.StatusDetails{
					Causes: []metav1.StatusCause{{Message: "fake error"}},
				},
				Message: fmt.Sprintf("Internal error occurred: fake error"),
			},
		},
		{
			name: "other error",
			err:  fmt.Errorf("fake other error"),
			expectBody: metav1.Status{
				TypeMeta: metav1.TypeMeta{
					Kind:       "Status",
					APIVersion: "v1",
				},
				Status: metav1.StatusFailure,
				Code:   http.StatusInternalServerError,
				Reason: metav1.StatusReasonInternalError,
				Details: &metav1.StatusDetails{
					Causes: []metav1.StatusCause{{Message: "fake other error"}},
				},
				Message: fmt.Sprintf("Internal error occurred: fake other error"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Handler{}
			recorder := httptest.NewRecorder()
			fakeRes := restful.NewResponse(recorder)
			r.WriteError(fakeRes, tt.err)
			fakeRes.Flush()
			raw := recorder.Body.Bytes()
			status := &metav1.Status{}
			err := json.Unmarshal(raw, status)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
				return
			}
			if !reflect.DeepEqual(status, &tt.expectBody) {
				t.Errorf("Expected %+v, got %+v", &tt.expectBody, status)
			}
		})
	}
}

func TestHandlerCreate(t *testing.T) {
	tests := []struct {
		name              string
		resourceName      string
		resourceNamespace string
		NewFunc           func() runtime.Object
		ResourceFunc      func() schema.GroupVersionResource
		NamespaceScoped   func() bool
		reqBody           string
		statusCode        int
	}{
		{
			name:              "namespaced resource",
			resourceName:      "wordpress-01",
			resourceNamespace: "default",
			NewFunc: func() runtime.Object {
				return &v1beta1.Application{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "app.k8s.io",
					Version:  "v1beta1",
					Resource: "applications",
				}
			},
			NamespaceScoped: func() bool {
				return true
			},
			reqBody: `
{
  "apiVersion": "app.k8s.io/v1beta1",
  "kind": "Application",
  "metadata": {
    "name": "wordpress-01",
    "namespace": "default",
    "labels": {
      "app.kubernetes.io/name": "wordpress-01"
    }
  },
  "spec": {
    "selector": {
      "matchLabels": {
        "app.kubernetes.io/name": "wordpress-01"
      }
    },
    "componentKinds": [
      {
        "group": "",
        "kind": "Service"
      },
      {
        "group": "apps",
        "kind": "StatefulSet"
      }
    ],
    "componentTemplates": [
        {
                  "apiVersion": "v1",
                  "kind": "Service",
                  "metadata": {
                        "name": "web1",
                        "labels": {
                          "app": "nginx"
                        }
                  },
                  "spec": {
                        "ports": [
                          {
                                "port": 8080
                          }
                        ],
                        "selector": {
                          "app": "nginx"
                        }
                  }
        },
        {
            "apiVersion": "extensions/v1beta1",
            "kind": "Deployment",
            "metadata": {
              "name": "web1",
              "namespace": "default"
            },
            "spec": {
              "replicas": 3,
              "template": {
                "metadata": {
                  "labels": {
                    "app": "nginx"
                  }
                },
                "spec": {
                  "containers": [
                    {
                      "image": "nginx:1.9.0",
                      "name": "nginx-app",
                      "ports": [
                        {
                          "containerPort": 82,
                          "name": "web1"
                        }
                      ]
                    }
                  ]
                }
              }
            }
        }
        ],
    "version": "4.9.4",
    "description": "WordPress is open source software you can use to create a beautiful website, blog, or app.",
    "maintainers": [
      {
        "name": "Kenneth Owens",
        "email": "kow3ns@github.com"
      }
    ],
    "owners": [
      "Kenneth Owens kow3ns@github.com"
    ],
    "keywords": [
      "cms",
      "blog",
      "wordpress"
    ],
    "links": [
      {
        "description": "About",
        "url": "https://wordpress.org/"
      },
      {
        "description": "Web Server Dashboard",
        "url": "https://metrics/internal/wordpress-01/web-app"
      },
      {
        "description": "Mysql Dashboard",
        "url": "https://metrics/internal/wordpress-01/mysql"
      }
    ]
  },
  "status": {
    "state": "Running"
  }
}
`,
			statusCode: http.StatusOK,
		},
		{
			name:         "cluster resource",
			resourceName: "test",
			NewFunc: func() runtime.Object {
				return &v1.Namespace{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Version:  "v1",
					Resource: "namespaces",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody: `
{
	"kind": "Namespace",
	"apiVersion": "v1",
	"metadata": {
		"annotations": {
			"a": "b"
		},
		"name": "test"
	}
}
`,
			statusCode: http.StatusOK,
		},
		{
			name:         "invalid json",
			resourceName: "test",
			NewFunc: func() runtime.Object {
				return &v1.Namespace{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Version:  "v1",
					Resource: "namespaces",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody: `
{
	"kind": "Namespace",
	"apiVersion": "v1",
		"metadata": {
			    "name": "test"
		},
}
`,
			statusCode: http.StatusBadRequest,
		},
		{
			name:         "invalid object",
			resourceName: "test",
			NewFunc: func() runtime.Object {
				return &v1.Namespace{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Version:  "v1",
					Resource: "namespaces",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody: `
{
	"kind": "Namespace",
	"apiVersion": "v1"
}
`,
			statusCode: http.StatusBadRequest,
		},
		{
			name:         "invalid object meta",
			resourceName: "test",
			NewFunc: func() runtime.Object {
				return &invalidObjectMeta{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Version:  "v1",
					Resource: "invalidobjectmetas",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody:    `{}`,
			statusCode: http.StatusBadRequest,
		},
	}
	config := config.FakeAPIConfig()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Handler{
				config:          config,
				NewFunc:         tt.NewFunc,
				ResourceFunc:    tt.ResourceFunc,
				NamespaceScoped: tt.NamespaceScoped,
				CreateStrategy:  FakeCreateStrategy{},
				UpdateStrategy:  FakeUpdateStrategy{},
			}
			httpReq, _ := http.NewRequest("POST", "fake", strings.NewReader(tt.reqBody))
			httpReq.Header.Add("Content-Type", "application/json")
			req := restful.NewRequest(httpReq)
			recorder := httptest.NewRecorder()
			res := restful.NewResponse(recorder)
			r.Create(context.TODO(), req, res)
			res.Flush()
			if recorder.Code != tt.statusCode {
				t.Errorf("Expect status code %v, got %v, body: %s", tt.statusCode, recorder.Code, recorder.Body.Bytes())
				return
			}
			if recorder.Code != http.StatusOK {
				return
			}
			client := req.Attribute(ClientAttribute)
			if client == nil {
				t.Errorf("Unexpected nil client")
				return
			}

			kc := client.(*kubernetes.FakeKubeClientBuilder)
			c, _ := kc.NamespacedDynamicClient(r.ResourceFunc().Resource, r.ResourceFunc().GroupVersion().String(), tt.resourceNamespace)
			_, err := c.Get(tt.resourceName, metav1.GetOptions{})
			if err != nil {
				t.Errorf("Unexpected error %v", err)
				return
			}
		})
	}
}

func TestHandlerUpdate(t *testing.T) {
	initObjects := []runtime.Object{
		&v1.Pod{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Pod",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "foo",
				Namespace: "default",
			},
			Spec: v1.PodSpec{
				RestartPolicy: v1.RestartPolicyAlways,
			},
		},
		&v1.Service{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Service",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "foo",
				Namespace: "default",
			},
		},
		&v1.Namespace{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Namespace",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "foo",
			},
		},
	}
	type updatedFields struct {
		fields []string
		value  string
	}
	tests := []struct {
		name              string
		resourceName      string
		resourceNamespace string
		NewFunc           func() runtime.Object
		ResourceFunc      func() schema.GroupVersionResource
		NamespaceScoped   func() bool
		reqBody           string
		statusCode        int
		updated           updatedFields
	}{
		{
			name:              "namespaced resource",
			resourceName:      "foo",
			resourceNamespace: "default",
			NewFunc: func() runtime.Object {
				return &v1.Pod{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "",
					Version:  "v1",
					Resource: "pods",
				}
			},
			NamespaceScoped: func() bool {
				return true
			},
			reqBody: `
{
	"kind": "Pod",
	"apiVersion": "v1",
	"metadata": {
		"name": "foo",
		"namespace": "default"
	},
	"spec": {
		"restartPolicy": "Never"
	}
}
`,
			statusCode: http.StatusOK,
			updated: updatedFields{
				fields: []string{"spec", "restartPolicy"},
				value:  "Never",
			},
		},
		{
			name:         "cluster resource",
			resourceName: "foo",
			NewFunc: func() runtime.Object {
				return &v1.Namespace{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "",
					Version:  "v1",
					Resource: "namespaces",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody: `
{
	"kind": "Namespace",
	"apiVersion": "v1",
	"metadata": {
		"annotations": {
			"a": "b"
		},
		"name": "foo"
	}
}
`,
			statusCode: http.StatusOK,
			updated: updatedFields{
				fields: []string{"metadata", "annotations", "a"},
				value:  "b",
			},
		},
		{
			name:         "invalid json",
			resourceName: "foo",
			NewFunc: func() runtime.Object {
				return &v1.Namespace{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "",
					Version:  "v1",
					Resource: "namespaces",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody: `
{
	"kind": "Namespace",
	"apiVersion": "v1",
		"metadata": {
			    "name": "test"
		},
}
`,
			statusCode: http.StatusBadRequest,
		},
		{
			name:         "not found",
			resourceName: "foo",
			NewFunc: func() runtime.Object {
				return &v1.Namespace{}
			},
			ResourceFunc: func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "",
					Version:  "v1",
					Resource: "namespaces",
				}
			},
			NamespaceScoped: func() bool {
				return false
			},
			reqBody: `
{
	"kind": "Namespace",
	"apiVersion": "v1",
	"metadata": {
		"name": "bar"
	}
}
`,
			statusCode: http.StatusNotFound,
		},
	}
	config := config.FakeAPIConfig(initObjects...)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Handler{
				config:          config,
				NewFunc:         tt.NewFunc,
				ResourceFunc:    tt.ResourceFunc,
				NamespaceScoped: tt.NamespaceScoped,
				CreateStrategy:  FakeCreateStrategy{},
				UpdateStrategy:  FakeUpdateStrategy{},
			}
			httpReq, _ := http.NewRequest("PUT", "fake", strings.NewReader(tt.reqBody))
			httpReq.Header.Add("Content-Type", "application/json")
			req := restful.NewRequest(httpReq)
			req.PathParameters()["name"] = tt.resourceName
			req.PathParameters()["namespace"] = tt.resourceNamespace
			recorder := httptest.NewRecorder()
			res := restful.NewResponse(recorder)
			r.Update(context.TODO(), req, res)
			res.Flush()
			if recorder.Code != tt.statusCode {
				t.Errorf("Expect status code %v, got %v, body: %s", tt.statusCode, recorder.Code, recorder.Body.Bytes())
				return
			}
			if recorder.Code != http.StatusOK {
				return
			}

			client := req.Attribute(ClientAttribute)
			if client == nil {
				t.Errorf("Unexpected nil client")
				return
			}

			kc := client.(*kubernetes.FakeKubeClientBuilder)
			c, _ := kc.NamespacedDynamicClient(r.ResourceFunc().Resource, "", tt.resourceNamespace)
			updateObj, err := c.Get(tt.resourceName, metav1.GetOptions{})
			if err != nil {
				t.Errorf("Unexpected error %v", err)
				return
			}

			value, ok, err := unstructured.NestedString(updateObj.Object, tt.updated.fields...)
			if err != nil {
				t.Errorf("Unexpected error %v", err)
				return
			}
			if !ok {
				t.Errorf("Fields %q not foud", tt.updated.fields)
				return
			}

			if value != tt.updated.value {
				t.Errorf("Expect %q, got %q", tt.updated.value, value)
			}
		})
	}
}

func TestNewHandler(t *testing.T) {
	type args struct {
		cfg             *config.APIConfig
		createStrategy  CreateStrategy
		updateStrategy  UpdateStrategy
		newFunc         func() runtime.Object
		resourceFunc    func() schema.GroupVersionResource
		namespaceScoped func() bool
	}
	handlerFoo := &Handler{
		config:         &config.APIConfig{},
		CreateStrategy: FakeCreateStrategy{},
		UpdateStrategy: FakeUpdateStrategy{},
		NewFunc: func() runtime.Object {
			return &v1.Pod{}
		},
		ResourceFunc: func() schema.GroupVersionResource {
			return schema.GroupVersionResource{
				Group:    "",
				Version:  "v1",
				Resource: "pods",
			}
		},
		NamespaceScoped: func() bool {
			return false
		},
	}
	tests := []struct {
		name string
		args args
		want *Handler
	}{
		{
			name: "basic",
			args: args{
				cfg:             handlerFoo.config,
				createStrategy:  handlerFoo.CreateStrategy,
				updateStrategy:  handlerFoo.UpdateStrategy,
				newFunc:         handlerFoo.NewFunc,
				resourceFunc:    handlerFoo.ResourceFunc,
				namespaceScoped: handlerFoo.NamespaceScoped,
			},
			want: handlerFoo,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewHandler(tt.args.cfg, tt.args.createStrategy, tt.args.updateStrategy, tt.args.newFunc, tt.args.resourceFunc, tt.args.namespaceScoped)
			if got.config != tt.want.config {
				t.Errorf("Unexpected config")
				return
			}
			newObj := got.NewFunc()
			newObjWant := tt.want.NewFunc()
			if reflect.TypeOf(newObj).Name() != reflect.TypeOf(newObjWant).Name() {
				t.Errorf("Expect returned object type of NewFunc %v, got %v", reflect.TypeOf(newObjWant).Name(), reflect.TypeOf(newObj).Name())
				return
			}
			resource := got.ResourceFunc()
			resourceWant := tt.want.ResourceFunc()
			if resource.Group != resourceWant.Group || resource.Version != resourceWant.Version || resource.Resource != resourceWant.Resource {
				t.Errorf("Expect resource %v, got %v", resourceWant, resource)
				return
			}
			namespaced := got.NamespaceScoped()
			namespacedWant := tt.want.NamespaceScoped()
			if namespaced != namespacedWant {
				t.Errorf("Expect namespacescoped %v, got %v", namespacedWant, namespaced)
			}
		})
	}
}

func TestHandlerClientFromRequest(t *testing.T) {
	type fields struct {
		config          *config.APIConfig
		NewFunc         func() runtime.Object
		ResourceFunc    func() schema.GroupVersionResource
		NamespaceScoped func() bool
		CreateStrategy  CreateStrategy
		UpdateStrategy  UpdateStrategy
	}
	type args struct {
		req       *restful.Request
		namespace string
	}
	podHandler := &Handler{
		config:         config.FakeAPIConfig(),
		CreateStrategy: FakeCreateStrategy{},
		UpdateStrategy: FakeUpdateStrategy{},
		NewFunc: func() runtime.Object {
			return &v1.Pod{}
		},
		ResourceFunc: func() schema.GroupVersionResource {
			return schema.GroupVersionResource{
				Group:    "",
				Version:  "v1",
				Resource: "pods",
			}
		},
		NamespaceScoped: func() bool {
			return false
		},
	}
	fakeHttpReq, _ := http.NewRequest("POST", "fakeurl", bytes.NewBufferString("fake"))
	tests := []struct {
		name      string
		args      args
		errConfig bool
		wantErr   bool
	}{
		{
			name: "general",
			args: args{
				req:       restful.NewRequest(fakeHttpReq),
				namespace: "default",
			},
		},
		{
			name: "error rest config",
			args: args{
				req:       restful.NewRequest(fakeHttpReq),
				namespace: "default",
			},
			errConfig: true,
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.errConfig {
				podHandler.config.RESTConfig = func(req *restful.Request) (*rest.Config, error) {
					return nil, fmt.Errorf("config error")
				}
			}
			got, err := podHandler.ClientFromRequest(tt.args.req, tt.args.namespace)
			if (err != nil) != tt.wantErr {
				t.Errorf("Handler.ClientFromRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil && !tt.wantErr {
				t.Errorf("Unexpect nil client")
				return
			}
		})
	}
}

func TestHandlerWrapError(t *testing.T) {
	tests := []struct {
		name         string
		handler      ResponsiveHandler
		expectStatus int
	}{
		{
			name: "generic",
			handler: func(req *restful.Request, res *restful.Response) (i int, e error) {
				return http.StatusOK, nil
			},
			expectStatus: http.StatusOK,
		},
		{
			name: "internal error",
			handler: func(req *restful.Request, res *restful.Response) (i int, e error) {
				return http.StatusOK, fmt.Errorf("fake error")
			},
			expectStatus: http.StatusInternalServerError,
		},
		{
			name: "k8s error",
			handler: func(req *restful.Request, res *restful.Response) (i int, e error) {
				return http.StatusOK, errors.NewBadRequest("fake error")
			},
			expectStatus: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Handler{}
			h := r.WrapError(tt.handler)
			req := test.FakeRestfulRequest()
			resp, _ := test.FakeRestfulResponse()
			h(req, resp)
			if tt.expectStatus != resp.StatusCode() {
				t.Errorf("Expect status code %d, got %d", tt.expectStatus, resp.StatusCode())
			}
		})
	}
}
