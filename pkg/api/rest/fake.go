package rest

import (
	"context"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

type FakeCreateStrategy struct{}

func (FakeCreateStrategy) PrepareForCreate(ctx context.Context, obj runtime.Object) error {
	return nil
}

func (FakeCreateStrategy) CreateFunc() CreateFunc {
	return nil
}

func (FakeCreateStrategy) Validate(ctx context.Context, obj runtime.Object) field.ErrorList {
	return field.ErrorList{}
}

func (FakeCreateStrategy) AfterCreate(ctx context.Context, obj runtime.Object) error {
	return nil
}

type FakeUpdateStrategy struct{}

func (FakeUpdateStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) error {
	return nil
}

func (FakeUpdateStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) field.ErrorList {
	return field.ErrorList{}
}

func (FakeUpdateStrategy) AfterUpdate(ctx context.Context, obj, old runtime.Object) error {
	return nil
}
