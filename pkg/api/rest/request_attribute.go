package rest

import (
	"fmt"

	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
)

const (
	// Request attributes

	// ClientAttribute stores dynamic client in the rest request.
	ClientAttribute = "DynamicClient"
	// RequestBodyHackAttribute stores hacked request body in the rest request.
	RequestBodyHackAttribute = "RequestBodyHack"
	// ForceUpdateRetryOnConflictAttribute force update retry on conflict.
	ForceUpdateRetryOnConflictAttribute = "ForceRetryOnConflict"
)

func ClientBuilderFromRequest(req *restful.Request) (k8s.ClientBuilderInterface, error) {
	clientAttr := req.Attribute(ClientAttribute)
	if clientAttr == nil {
		return nil, errors.NewInternalError(fmt.Errorf("get client from request failed"))
	}

	c, ok := clientAttr.(k8s.ClientBuilderInterface)
	if !ok {
		return nil, errors.NewInternalError(fmt.Errorf("get client from request failed"))
	}
	return c, nil
}

// IsForceUpdateRetryFromRequest returns true if ForceUpdateRetry is enabled on request.
func IsForceUpdateRetryFromRequest(req *restful.Request) bool {
	retryAttr := req.Attribute(ForceUpdateRetryOnConflictAttribute)
	if retryAttr != nil {
		return retryAttr.(bool)
	}
	return false
}

// SetForceUpdateRetry sets the ForceUpdateRetryOnConflictAttribute on request.
func SetForceUpdateRetry(req *restful.Request, retry bool) {
	req.SetAttribute(ForceUpdateRetryOnConflictAttribute, retry)
}
