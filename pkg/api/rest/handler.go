package rest

import (
	"alauda.io/archon/pkg/api/config"
	k8s "alauda.io/archon/pkg/kubernetes"
	"context"
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"net/http"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/util/retry"
	"k8s.io/klog"
)

// ResponsiveHandler returns expected statusCode and error if error occurs in the handler.
type ResponsiveHandler func(req *restful.Request, res *restful.Response) (int, error)

// Handler handles the rest request and allow strategies to add custom
// logic.
type Handler struct {
	config          *config.APIConfig
	NewFunc         func() runtime.Object
	ResourceFunc    func() schema.GroupVersionResource
	NamespaceScoped func() bool
	CreateStrategy  CreateStrategy
	UpdateStrategy  UpdateStrategy
}

// NewHandler returns a Handler object.
func NewHandler(
	cfg *config.APIConfig,
	createStrategy CreateStrategy,
	updateStrategy UpdateStrategy,
	newFunc func() runtime.Object,
	resourceFunc func() schema.GroupVersionResource,
	namespaceScoped func() bool) *Handler {
	return &Handler{
		config:          cfg,
		NewFunc:         newFunc,
		ResourceFunc:    resourceFunc,
		NamespaceScoped: namespaceScoped,
		CreateStrategy:  createStrategy,
		UpdateStrategy:  updateStrategy,
	}
}

// WrapError wraps a ResponsiveHandler as a RouteFunction.
func (r *Handler) WrapError(h ResponsiveHandler) restful.RouteFunction {
	return func(req *restful.Request, res *restful.Response) {
		statusCode, err := h(req, res)
		if statusCode == 0 {
			// Default the statusCode value
			if err == nil {
				statusCode = http.StatusOK
			} else {
				statusCode = http.StatusInternalServerError
			}
		}
		if err != nil {
			r.WriteError(res, err)
		} else {
			res.WriteHeader(statusCode)
		}
	}
}

// WriteError writes error as StatusError to the response.
func (r *Handler) WriteError(res *restful.Response, err error) {
	klog.Errorf("Handle request error: %v", err)
	statusErr, ok := err.(*errors.StatusError)
	if !ok {
		statusErr = errors.NewInternalError(err)
	}
	statusErr.ErrStatus.Kind = "Status"
	statusErr.ErrStatus.APIVersion = "v1"
	res.WriteHeaderAndJson(
		int(statusErr.Status().Code),
		statusErr.Status(),
		restful.MIME_JSON)
}

// ClientFromRequest returns a dynamic client from the request.
func (r *Handler) ClientFromRequest(req *restful.Request, namespace string) (dynamic.ResourceInterface, error) {
	c, err := r.config.ClientBuilderFromRequest(req)
	if err != nil {
		return nil, err
	}
	req.SetAttribute(ClientAttribute, c)

	gvr := r.ResourceFunc()
	return c.NamespacedDynamicClient(gvr.Resource, gvr.GroupVersion().String(), namespace)
}

// ClientFromRequestAndGVR returns a dynamic client from the request and gvr of the resource.
func (r *Handler) ClientFromRequestAndGVR(req *restful.Request, namespace string, gvr schema.GroupVersionResource) (dynamic.ResourceInterface, error) {
	c, err := r.config.ClientBuilderFromRequest(req)
	if err != nil {
		return nil, err
	}
	req.SetAttribute(ClientAttribute, c)

	return c.NamespacedDynamicClient(gvr.Resource, gvr.GroupVersion().String(), namespace)
}

// ClientToolsFromRequest generates a dynamic client and client builder from request and namespace.
func (r *Handler) ClientToolsFromRequest(req *restful.Request, namespace string) (dynamic.ResourceInterface, k8s.ClientBuilderInterface, error) {
	ri, err := r.ClientFromRequest(req, namespace)
	if err != nil {
		return nil, nil, err
	}
	client := req.Attribute(ClientAttribute).(k8s.ClientBuilderInterface)
	return ri, client, nil
}

func (r *Handler) ContextFromRequest(req *restful.Request) context.Context {
	ctx := ContextWithRequest(req.Request.Context(), req)
	ctx = ContextWithAPIConfig(ctx, r.config)
	return ctx
}

func (r *Handler) ReadEntity(obj interface{}, req *restful.Request) (err error) {
	bodyHack := req.Attribute(RequestBodyHackAttribute)
	if bodyHack != nil {
		err = json.Unmarshal(bodyHack.([]byte), obj)
	} else {
		err = req.ReadEntity(obj)
		if err != nil {
			err = errors.NewBadRequest(err.Error())
		}
	}
	return
}

// Create creates a resource object in the kubernetes cluseter from the request. This
// function is the base implementation of a resource creation. The CreateStrategy
// will be called during the creation process to allow extra logics injected.
func (r *Handler) Create(context context.Context, req *restful.Request, res *restful.Response) {
	cluster := req.PathParameter("cluster")
	log := r.config.Logger.Named(cluster)
	obj := r.NewFunc()
	err := r.ReadEntity(obj, req)

	defer func() {
		if err != nil {
			r.WriteError(res, err)
		}
	}()
	if err != nil {
		err = errors.NewBadRequest(
			fmt.Sprintf("invalid object: %v", err),
		)
		return
	}

	objStr := k8s.ObjectString(obj)

	objMeta, err := meta.Accessor(obj)
	if err != nil {
		err = errors.NewBadRequest(
			fmt.Sprintf("access object meta error: %v", err),
		)
		return
	}

	if objMeta.GetName() == "" {
		err = errors.NewBadRequest(
			fmt.Sprintf("object name is empty"),
		)
		return
	}

	log.Infof("Creating object %q", objStr)

	log.Infof("Validating object %q", objStr)
	if errs := r.CreateStrategy.Validate(context, obj); len(errs) > 0 {
		err = errors.NewInvalid(
			obj.GetObjectKind().GroupVersionKind().GroupKind(),
			objMeta.GetName(),
			errs,
		)

		return
	}

	log.Infof("Preparing object %q", objStr)
	err = r.CreateStrategy.PrepareForCreate(context, obj)
	if err != nil {
		return
	}
	c, err := r.config.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	req.SetAttribute(ClientAttribute, c)

	unstructObj, err := k8s.ObjectToUnstructured(obj)
	if err != nil {
		return
	}

	var createObj *unstructured.Unstructured
	log.Infof("Create object %q on cluster", objStr)
	createFunc := r.CreateStrategy.CreateFunc()
	if createFunc != nil {
		createObj, err = createFunc(context, unstructObj, metav1.CreateOptions{})
	} else {
		var ri dynamic.ResourceInterface
		ri, err = r.ClientFromRequest(req, unstructObj.GetNamespace())
		if err != nil {
			return
		}

		createObj, err = ri.Create(unstructObj, metav1.CreateOptions{})
		if err != nil {
			return
		}
	}

	if err != nil {
		return
	}

	log.Infof("After create object %q", objStr)
	err = r.CreateStrategy.AfterCreate(context, createObj)
	if err != nil {
		return
	}
	res.WriteAsJson(createObj)
}

func (r *Handler) CreateFunc() CreateFunc {
	return nil
}

// Deprecated:
// Update updates a resource object in the kubernetes cluster from the request. This
// function is the base implementation of a resource update. The UpdateStrategy
// will be called during the update process to allow extra logic injected.
func (r *Handler) Update(context context.Context, req *restful.Request, res *restful.Response) {
	log := r.config.Logger
	obj := r.NewFunc()
	err := r.ReadEntity(obj, req)
	defer func() {
		if err != nil {
			r.WriteError(res, err)
		}
	}()
	if err != nil {
		err = errors.NewBadRequest(
			fmt.Sprintf("invalid applicaition object: %v", err),
		)
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	objStr := k8s.ObjectString(obj)

	log.Infof("Updating object %q", objStr)

	ri, err := r.ClientFromRequest(req, namespace)
	if err != nil {
		return
	}

	old, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	log.Infof("Validating object %q", objStr)
	if errs := r.UpdateStrategy.ValidateUpdate(context, obj, old); len(errs) > 0 {
		err = errors.NewInvalid(
			obj.GetObjectKind().GroupVersionKind().GroupKind(),
			name,
			errs,
		)
		return
	}

	log.Infof("Preparing object %q", objStr)
	err = r.UpdateStrategy.PrepareForUpdate(context, obj, old)
	if err != nil {
		return
	}

	unstructObj, err := k8s.ObjectToUnstructured(obj)
	if err != nil {
		return
	}

	log.Infof("Update object %q on cluster", objStr)
	updateObj, err := ri.Update(unstructObj, metav1.UpdateOptions{})
	if err != nil {
		err, o, u := r.retryUpdateOnConflict(err, req, log, objStr, ri, name, unstructObj)
		if err != nil {
			return
		}
		old = o
		updateObj = u
	}

	log.Infof("After update object %q", objStr)
	err = r.UpdateStrategy.AfterUpdate(context, updateObj, old)
	if err != nil {
		return
	}
	res.WriteAsJson(updateObj)
}

func (r *Handler) retryUpdateOnConflict(err error, req *restful.Request, log *zap.SugaredLogger, objStr string, ri dynamic.ResourceInterface, name string, unstructObj *unstructured.Unstructured) (error, *unstructured.Unstructured, *unstructured.Unstructured) {
	var oldObj *unstructured.Unstructured
	var updateObj *unstructured.Unstructured

	if errors.IsConflict(err) && IsForceUpdateRetryFromRequest(req) {
		log.Infof("Retry update object %q on cluster", objStr)
		var errRetry error
		err = retry.RetryOnConflict(retry.DefaultRetry, func() error {
			oldObj, errRetry = ri.Get(name, metav1.GetOptions{})
			if errRetry != nil {
				return errRetry
			}
			unstructObj.SetResourceVersion(oldObj.GetResourceVersion())
			updateObj, errRetry = ri.Update(unstructObj, metav1.UpdateOptions{})
			return errRetry
		})
	}
	return err, oldObj, updateObj
}
