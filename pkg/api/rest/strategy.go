package rest

import (
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

type CreateFunc func(ctx context.Context, obj *unstructured.Unstructured, options metav1.CreateOptions, subresources ...string) (*unstructured.Unstructured, error)

// CreateStrategy provides strategies for resource creation.
type CreateStrategy interface {
	// PrepareForCreate prepares the object for creating the resource.
	PrepareForCreate(ctx context.Context, obj runtime.Object) error

	// Validate validates the fields of the object. The fields value should
	// not be changed during validation.
	Validate(ctx context.Context, obj runtime.Object) field.ErrorList

	// CreateFunc returns a CreateFunc to create the object on the backend storage.
	CreateFunc() CreateFunc

	// AfterCreate will be called after the success resource creation.
	AfterCreate(ctx context.Context, obj runtime.Object) error
}

// UpdateStrategy provides strategies for resource update.
type UpdateStrategy interface {
	// PrepareForUpdate prepares the object for updating the resource.
	PrepareForUpdate(ctx context.Context, obj, old runtime.Object) error

	// Validate validates the fields fo the object. The fields value should
	// not be changed during validation.
	ValidateUpdate(ctx context.Context, obj, old runtime.Object) field.ErrorList

	// AfterUpdate will be called after the success resource update.
	AfterUpdate(ctx context.Context, obj, old runtime.Object) error
}
