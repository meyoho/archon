package rest

import (
	"context"

	"alauda.io/archon/pkg/api/config"
	"github.com/emicklei/go-restful"
)

type key int

const (
	contextAPIConfigKey key = iota

	contextRequestKey
)

// NewContext returns a new Context.
func NewContext() context.Context {
	return context.TODO()
}

// ContextWithAPIConfig returns a Context with APIConfig.
func ContextWithAPIConfig(parent context.Context, config *config.APIConfig) context.Context {
	return context.WithValue(parent, contextAPIConfigKey, config)
}

// APIConfigFromContext returns APIConfig from the Context.
func APIConfigFromContext(ctx context.Context) (*config.APIConfig, bool) {
	config, bool := ctx.Value(contextAPIConfigKey).(*config.APIConfig)
	return config, bool
}

// ContextWithRequest returns a Context with restful Request.
func ContextWithRequest(parent context.Context, req *restful.Request) context.Context {
	return context.WithValue(parent, contextRequestKey, req)
}

// RequestFromContext returns the request from the Context.
func RequestFromContext(ctx context.Context) (*restful.Request, bool) {
	req, bool := ctx.Value(contextRequestKey).(*restful.Request)
	return req, bool
}
