package rest

import (
	"bytes"
	"context"
	"net/http"
	"testing"

	"alauda.io/archon/pkg/api/config"

	"github.com/emicklei/go-restful"
)

func TestNewContext(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "normal",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewContext(); got == nil {
				t.Errorf("Unexpected nil context")
			}
		})
	}
}

func TestContextWithAPIConfig(t *testing.T) {
	tests := []struct {
		name   string
		config *config.APIConfig
	}{
		{
			name:   "normal",
			config: &config.APIConfig{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContextWithAPIConfig(context.TODO(), tt.config); got.Value(contextAPIConfigKey).(*config.APIConfig) != tt.config {
				t.Errorf("Unexpected config value in context")
			}
		})
	}
}

func TestAPIConfigFromContext(t *testing.T) {
	tests := []struct {
		name       string
		config     *config.APIConfig
		haveConfig bool
	}{
		{
			name:       "normal",
			config:     &config.APIConfig{},
			haveConfig: true,
		},
		{
			name: "no config",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctx context.Context
			if tt.haveConfig {
				ctx = ContextWithAPIConfig(context.TODO(), tt.config)
			} else {
				ctx = context.TODO()
			}
			got, ok := APIConfigFromContext(ctx)
			if tt.haveConfig {
				if !ok {
					t.Errorf("Expect config, got nil")
					return
				}
				if got != tt.config {
					t.Errorf("Expect %v, got %v", tt.config, got)
					return
				}
			} else if ok {
				t.Errorf("Expect nil config, got %v", got)
				return
			}
		})
	}
}

func TestContextWithRequest(t *testing.T) {
	httpReq, _ := http.NewRequest("GET", "", bytes.NewBufferString(""))
	tests := []struct {
		name string
		req  *restful.Request
	}{
		{
			name: "normal",
			req:  restful.NewRequest(httpReq),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContextWithRequest(context.TODO(), tt.req); got.Value(contextRequestKey).(*restful.Request) != tt.req {
				t.Errorf("Unexpected config value in context")
			}
		})
	}
}

func TestRequestFromContext(t *testing.T) {
	httpReq, _ := http.NewRequest("GET", "", bytes.NewBufferString(""))
	tests := []struct {
		name    string
		req     *restful.Request
		haveReq bool
	}{
		{
			name:    "normal",
			req:     restful.NewRequest(httpReq),
			haveReq: true,
		},
		{
			name: "no request",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var ctx context.Context
			if tt.haveReq {
				ctx = ContextWithRequest(context.TODO(), tt.req)
			} else {
				ctx = context.TODO()
			}
			got, ok := RequestFromContext(ctx)
			if tt.haveReq {
				if !ok {
					t.Errorf("Expect request, got nil")
					return
				}
				if got != tt.req {
					t.Errorf("Expect %v, got %v", tt.req, got)
					return
				}
			} else if ok {
				t.Errorf("Expect nil request, got %v", got)
				return
			}
		})
	}
}
