package rest

import (
	"alauda.io/archon/pkg/api/config"
	"k8s.io/client-go/dynamic"
)

type Manager struct {
	config.ManagerConfig
}

func (m *Manager) Client(namespace string) (dynamic.ResourceInterface, error) {
	return m.ClientBuilder.NamespacedDynamicClientByGroupKind(
		m.GroupKind,
		namespace)
}
