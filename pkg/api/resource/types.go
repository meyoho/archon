package resource

import (
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

//ObjectList is Unstructured Array
type ObjectList []unstructured.Unstructured

type NodeLabel struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type NodeLabels []*NodeLabel

func NewNodeLabels(nodes v1.Node) *NodeLabels {
	nls := NodeLabels{}
	labels := nodes.GetLabels()
	for k, v := range labels {
		nls = append(nls, &NodeLabel{
			Key:   k,
			Value: v,
		})
	}
	return &nls
}

type ClusterNodeLabel map[string]*NodeLabels

func NewClusterNodeLabel(nodes *v1.NodeList) *ClusterNodeLabel {
	cnl := ClusterNodeLabel{}

	for _, node := range nodes.Items {
		nls := NewNodeLabels(node)

		//Get NodeInternalIP
		var internalIP string
		for _, addr := range node.Status.Addresses {
			if addr.Type == v1.NodeInternalIP {
				internalIP = addr.Address
			}
		}

		cnl[internalIP] = nls
	}

	return &cnl
}
