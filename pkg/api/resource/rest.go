package resource

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"github.com/spf13/cast"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/discovery"
	"k8s.io/klog"
)

var erebusClient = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
		MaxIdleConnsPerHost: 100,
	},
}

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// SearchParams defines the params for resource search.
type SearchParams struct {
	// Fields is the field paths to search in.
	Fields []string
	// Keyword is the keyword to search for.
	Keyword string
	// Limit is the returned limit count for every search.
	Limit int
	// Continue is the continue token for the next search.
	Continue string
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	clusterParam := restful.PathParameter("cluster", "cluster name").DataType("string")
	searchParam := restful.PathParameter("search", "search url string").DataType("string")
	ws.Route(
		ws.GET("search/{search:*}").To(r.WrapError(r.SearchResource)).
			Doc("Search resources by filters").
			Param(searchParam).
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("{cluster}/resourcetypes").To(r.WrapError(r.GetClusterResourceTypes)).
			Doc("Get Cluster Resource Types").
			Param(clusterParam).
			Returns(200, "OK", []v1.APIResourceList{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/resources").To(r.WrapError(r.BatchCreateResource)).
			Doc("Batch Create Resource Types").
			Param(clusterParam).
			Returns(204, "OK", "").
			Produces(restful.MIME_JSON),
	).Route(ws.GET("{cluster}/nodes/labels").To(r.WrapError(r.ListClusterNodeLabels)).
		Doc("List all Node labels of the cluster").
		Param(clusterParam).
		Returns(200, "OK", ClusterNodeLabel{}).
		Produces(restful.MIME_JSON))
}

func parseSearchQuery(query url.Values) (sp SearchParams) {
	sp.Fields = []string{"metadata.name"}
	if query["field"] != nil {
		sp.Fields = strings.Split(query["field"][0], ",")
		query.Del("field")
	}
	if query["keyword"] != nil {
		sp.Keyword = query["keyword"][0]
		query.Del("keyword")
	}
	if query["limit"] != nil {
		sp.Limit = cast.ToInt(query["limit"][0])
	}

	if query["continue"] != nil {
		sp.Continue = query["continue"][0]
	}
	return
}

// SearchResource searches the resource list by field and keyword.
// The query parameters accept:
// - kubernetes list params
// - field: field paths split by "," used to search the keyword
// - keyword: the keyword to inexact search in the field paths
//
// The count of items returned should not be less than the "limit" param
// unless there is no more items.
//
// e.g. The search URL:
// "/acp/v1/resources/search/kubernetes/api/v1/namespaces/default/pods?limit=5&field=metadata.name&keyword=foo"
// means to search the pods in "metadata.name" field which contains
// string "foo", and returns at least 5 items as the result.
func (r *REST) SearchResource(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	filtered := &unstructured.UnstructuredList{}
	search := req.PathParameter("search")
	query := req.Request.URL.Query()
	sp := parseSearchQuery(query)

	cfg := r.cfg.Server.GetManager().ManagerConfig()
	baseURL := fmt.Sprintf("%s/%s", cfg.MultiClusterHost, search)

	klog.V(1).Infof("Search with URL %q, Param %+v", search, sp)
	for {
		var end bool
		err, end = r.filterEachList(&sp, query, baseURL, req, res, filtered)
		if err != nil {
			return
		}
		if end {
			break
		}
	}

	if res.StatusCode() != http.StatusOK {
		// Returns error response.
		return
	}

	filtered.SetContinue(sp.Continue)
	body, err := filtered.MarshalJSON()
	if err != nil {
		return
	}

	res.Header().Set("Content-Type", restful.MIME_JSON)
	res.Write(body)
	return
}

func (r *REST) filterEachList(
	sp *SearchParams,
	query url.Values,
	baseURL string,
	req *restful.Request,
	res *restful.Response,
	filtered *unstructured.UnstructuredList) (err error, end bool) {
	end = true
	if sp.Continue != "" {
		query["continue"] = []string{sp.Continue}
	}
	erebusURL := baseURL
	if len(query) > 0 {
		erebusURL = fmt.Sprintf("%s?%s", baseURL, query.Encode())
	}
	klog.V(1).Infof("Forward the search to %q", erebusURL)
	var qErr error
	list, status, body, qErr := queryErebusList(erebusURL, req.Request.Header)
	if qErr != nil {
		err = qErr
		return
	}
	if status != http.StatusOK {
		// Directly returns error response from erebus.
		res.WriteErrorString(status, string(body))
		return
	}
	filtered.SetAPIVersion(list.GetAPIVersion())
	filtered.SetKind(list.GetKind())
	filtered.SetResourceVersion(list.GetResourceVersion())
	sp.Continue = list.GetContinue()
	if sp.Keyword == "" {
		// No search, just returns all items.
		filtered = list
		return
	}
	filterResourceList(list, *sp, filtered)
	if len(filtered.Items) >= sp.Limit || sp.Continue == "" {
		// If count of filtered items is greater than or equal limit, or all items have
		// been returned, the search is done.
		return
	}
	end = false
	return
}

// filterResourceList filters the items of list with every specified fields to search the keyword.
// The keyword match is case insensitive.
func filterResourceList(list *unstructured.UnstructuredList, sp SearchParams, filtered *unstructured.UnstructuredList) {
	for _, o := range list.Items {
		for _, f := range sp.Fields {
			v, found, _ := unstructured.NestedString(o.Object, strings.Split(f, ".")...)
			if found && strings.Contains(strings.ToLower(v), strings.ToLower(sp.Keyword)) {
				filtered.Items = append(filtered.Items, o)
				break
			}
		}
	}
}

func queryErebusList(erebusURL string, header http.Header) (
	list *unstructured.UnstructuredList,
	statusCode int,
	body []byte,
	err error) {
	req, err := http.NewRequest(http.MethodGet, erebusURL, nil)
	if err != nil {
		return
	}

	for k, v := range header {
		for _, vv := range v {
			if k == "Accept-Encoding" {
				// Content-encoding with gzip, use built-in http transport gzip.
				break
			}
			req.Header.Add(k, vv)
		}
	}

	resp, err := erebusClient.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	statusCode = resp.StatusCode
	if statusCode != http.StatusOK {
		return
	}

	list = &unstructured.UnstructuredList{}
	err = list.UnmarshalJSON(body)
	return
}

// search resource list
func (r *REST) GetClusterResourceTypes(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	client, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	ks, err := client.ClientSet()
	if err != nil {
		return
	}

	resourceList, err := ks.Discovery().ServerResources()
	if err != nil {
		// ignore GroupDiscoveryFailedError
		if !discovery.IsGroupDiscoveryFailedError(err) {
			return
		}
		// ensure serverResourceList is a secure var
		if resourceList == nil {
			return
		}
		klog.Warningf("%v", err)
		err = nil
	}

	res.WriteAsJson(resourceList)
	return
}

//BatchCreateResource is Batch Crate Resources
func (r *REST) BatchCreateResource(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusNoContent

	list := ObjectList{}
	err = req.ReadEntity(&list)
	if err != nil {
		return
	}

	clientBuild, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	for _, obj := range list {
		if _, err = kubernetes.CreateOrUpdateResource(&obj, clientBuild); err != nil {
			return
		}
	}
	return
}

//ListClusterNodeLabels is  return all Node labels of a cluster

func (r *REST) ListClusterNodeLabels(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	clientBuild, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	client, err := clientBuild.ClientSet()
	if err != nil {
		return
	}
	nodes, err := client.CoreV1().Nodes().List(v1.ListOptions{
		ResourceVersion: "0",
	})
	if err != nil {
		return
	}
	res.WriteAsJson(NewClusterNodeLabel(nodes))
	return
}
