package resource

import (
	"net/http"
	"testing"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/test"

	"github.com/emicklei/go-restful"
	"github.com/magiconair/properties/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

func TestNewREST(t *testing.T) {
	type args struct {
		cfg *config.APIConfig
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "generic",
			args: args{
				cfg: &config.APIConfig{
					LabelBaseDomain: "fake",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewREST(tt.args.cfg)
			if got.cfg.LabelBaseDomain != tt.args.cfg.LabelBaseDomain {
				t.Errorf("Expect LabelBaseDomain %s, got %s", tt.args.cfg.LabelBaseDomain, got.cfg.LabelBaseDomain)
				return
			}
		})
	}
}

func TestRESTInstallRoutes(t *testing.T) {
	type fields struct {
		Handler rest.Handler
		config  *config.APIConfig
	}
	type args struct {
		version core.APIVersion
		ws      *restful.WebService
	}
	rest := NewREST(config.FakeAPIConfig())
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "generic",
			fields: fields{
				Handler: rest.Handler,
				config:  rest.cfg,
			},
			args: args{
				version: core.APIVersionV1,
				ws:      new(restful.WebService),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &REST{
				Handler: tt.fields.Handler,
				cfg:     tt.fields.config,
			}
			a.InstallRoutes(tt.args.version, tt.args.ws)
			if len(tt.args.ws.Routes()) == 0 {
				t.Errorf("Unexpected installed routes is empty")
				return
			}
		})
	}
}

func TestREST_BatchCreateResource(t *testing.T) {
	appREST := NewREST(config.FakeAPIConfig())
	tests := []struct {
		name    string
		request string
	}{
		{
			name: "namespace",
			request: `
			{
				"namespace": {
					"apiVersion": "v1",
					"kind": "Namespace",
					"metadata": {
						"name": "system-test-alauda",
						"labels": {
						"alauda.io/project": "system"
					},
						"annotations": {}
					}
				},
				"resourcequota": {
					"apiVersion": "v1",
					"kind": "ResourceQuota",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"hard": {
							"requests.cpu": "2",
							"requests.memory": "2G",
							"requests.storage": "100G",
							"pods": "100",
							"persistentvolumeclaims": "100",
							"limits.cpu": "4",
							"limits.memory": "8G"
						}
					}
				},
				"limitrange": {
					"apiVersion": "v1",
					"kind": "LimitRange",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"limits": [{
							"default": {
								"memory": "512M",
								"cpu": "500m"
							},
							"defaultRequest": {
								"memory": "512M",
								"cpu": "500m"
							},
							"max": {
								"cpu": "4",
								"memory": "4G"
							},
							"type": "Container"
						}]
					}
				}
			}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := test.FakeRestfulRequestWithJSON(http.MethodPost, "fake", tt.request)
			resp, recorder := test.FakeRestfulResponse()
			statuscode, err := appREST.BatchCreateResource(req, resp)
			httpResp := recorder.Result()
			body := recorder.Body.Bytes()
			if statuscode != http.StatusNoContent && err == nil {
				t.Errorf("Expect status %d, got %d, resp: %s", http.StatusNoContent, httpResp.StatusCode, body)
				return
			}
		})
	}
}

func TestREST_GetClusterResourceTypes(t *testing.T) {
	appREST := NewREST(config.FakeAPIConfig())
	tests := []struct {
		name    string
		request string
	}{
		{
			name:    "GetResourcesList",
			request: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := test.FakeRestfulRequestWithJSON(http.MethodGet, "fake", tt.request)
			resp, recorder := test.FakeRestfulResponse()
			appREST.GetClusterResourceTypes(req, resp)
			httpResp := recorder.Result()
			body := recorder.Body.Bytes()
			if httpResp.StatusCode != http.StatusOK {
				t.Errorf("Expect status %d, got %d, resp: %s", http.StatusNoContent, httpResp.StatusCode, body)
				return
			}
		})
	}
}

func TestREST_ListClusterNodeLabels(t *testing.T) {
	initObjects := []runtime.Object{
		&v1.Node{
			TypeMeta: metav1.TypeMeta{},
			ObjectMeta: metav1.ObjectMeta{
				Name: "global",
				Labels: map[string]string{
					"Node": "Node",
				},
			},
			Spec: v1.NodeSpec{},
			Status: v1.NodeStatus{
				Addresses: []v1.NodeAddress{
					{
						Type:    v1.NodeInternalIP,
						Address: "192.168.1.1",
					},
				},
			},
		},
	}

	appREST := NewREST(config.FakeAPIConfig(initObjects...))
	request := test.FakeRestfulRequestWithJSON(http.MethodGet, "fake", "")
	request.PathParameters()["cluster"] = ""
	response, _ := test.FakeRestfulResponse()
	statuscode, _ := appREST.ListClusterNodeLabels(request, response)
	assert.Equal(t, statuscode, http.StatusOK)
}
