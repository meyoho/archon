package topology

import (
	"strings"

	"alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

//TopologyRequest is easy to save params of request
type TopologyRequest struct {
	Request   *restful.Request
	Client    kubernetes.ClientBuilderInterface
	Name      string
	Namespace string
	Kind      string
	Depth     int16
}

// EdgeHelper is a helper struct that helps representing an edge
type EdgeHelper struct {
	unstructured.Unstructured
	EdgeType string
}

// Topology represents the relation of node and their edges
type Topology map[string][]*EdgeHelper

// TopologyHelper carries information of all nodes and their informations
type TopologyHelper struct {
	Indexes map[string]*unstructured.Unstructured
	Topo    Topology
}

// TopologyResponse represents the whole topology graph
type TopologyResponse struct {
	TopologyHelper
	Graph *Graph
	Refer *Refer
}

// Graph represents topological relation for kubernetes resources within one namespace
type Graph struct {
	// Nodes represent kubernetes resources
	Nodes map[string]*Node `json:"nodes"`
	// Edges represent relations between these resources
	Edges []*Edge `json:"edges"`
}

// Refer represents topological relation for one kubernetes resource
type Refer struct {
	// Reference represents resources referenced by this resource
	Reference []*ReferNode `json:"reference"`
	// ReferencedBy represents resources which referenced to this resource
	ReferencedBy []*ReferNode `json:"referenced_by"`
}

type ReferNode struct {
	Type string `json:"type"`
	Node *Node  `json:"node"`
}

// Node represents a kubernetes resource with meta data and type data
type Node struct {
	//metaV1.TypeMeta   `json:",inline"`
	//metaV1.ObjectMeta `json:",inline"`
	unstructured.Unstructured
}

// Edge represents relations between two kubernetes resources
type Edge struct {
	Type string `json:"type"`
	From string `json:"from"`
	To   string `json:"to"`
}

// getIndex return index of current nsode
func getIndex(kind, name string) string {
	index := strings.Join([]string{kind, name}, "/")
	return strings.ToLower(index)
}

// getIndex return index of two nodes
func getEdgeIndex(src, dst *unstructured.Unstructured) string {
	s := getIndex(src.GetKind(), src.GetName())
	d := getIndex(dst.GetKind(), dst.GetName())
	index := strings.Join([]string{s, d}, "->")
	return strings.ToLower(index)
}

// traverse will traverse edges of root, return the subset and filtered edges
func traverse(root *unstructured.Unstructured, visitTbl map[string]bool,
	edges []*EdgeHelper) (subset []*unstructured.Unstructured, filtered []*EdgeHelper, visited map[string]bool, reflect Topology) {

	if nil == visitTbl {
		visited = make(map[string]bool)
	} else {
		visited = visitTbl
	}

	subset = make([]*unstructured.Unstructured, 0)

	reflect = make(Topology)

	filtered = make([]*EdgeHelper, 0)

	for _, e := range edges {
		idx := getIndex(e.GetKind(), e.GetName())

		if visited[idx] {
			continue
		}
		subset = append(subset, &e.Unstructured)

		// filter edge
		var edType string
		var edIdx string

		switch e.EdgeType {
		case Referenced:
			edType = Reference
			edIdx = getEdgeIndex(&e.Unstructured, root)
		case Selected:
			edType = Selector
			edIdx = getEdgeIndex(&e.Unstructured, root)
		default:
			edIdx = getEdgeIndex(root, &e.Unstructured)

		}

		if "" == edType && !visited[edIdx] {
			filtered = append(filtered, e)
			visited[edIdx] = true
			continue
		}

		if "" != edType && !visited[edIdx] {
			reflect[idx] = append(reflect[idx], &EdgeHelper{*root, edType})
			visited[edIdx] = true
		}
	}

	return
}

// Merge will merge provided topology
func (t Topology) Merge(result Topology) {

	if nil == t || nil == result {
		return
	}

	for idx, edges := range result {
		if 0 == len(edges) {
			continue
		}
		if nil == result[idx] {
			t[idx] = edges
		} else {
			t[idx] = append(t[idx], edges...)
		}
	}

}

// Walk will traverse the topology graph until certain depth
func (t Topology) Walk(roots []*unstructured.Unstructured, visited map[string]bool, depth int16) (result Topology) {
	if nil == t {
		return
	}
	result = make(Topology)

	if nil == visited {
		visited = map[string]bool{}
	}

	var subset []*unstructured.Unstructured

	if 0 > depth-1 {
		return
	}

	for _, root := range roots {

		index := getIndex(root.GetKind(), root.GetName())
		if visited[index] {
			continue
		}

		edges, ok := t[index]

		if !ok {
			continue
		}

		sub, filtered, visited, reflect := traverse(root, visited, edges)

		subset = append(subset, sub...)

		result.Merge(reflect)
		if nil == result[index] {
			result[index] = filtered
		} else {
			result[index] = append(result[index], filtered...)
		}
		visited[index] = true
	}

	if 0 == len(subset) {
		return
	}

	t1 := t.Walk(subset, visited, depth-1)

	result.Merge(t1)

	return
}

// ParseGraph parse TopologyHelper to Graph
func (t TopologyHelper) ParseGraph() *Graph {
	graph := &Graph{
		Nodes: make(map[string]*Node),
		Edges: []*Edge{},
	}

	for k, edges := range t.Topo {
		obj, ok := t.Indexes[k]
		if !ok || nil == obj {
			continue
		}
		graph.Nodes[string(obj.GetUID())] = &Node{Unstructured: *obj}
		for _, e := range edges {
			to := e.Unstructured
			toUID := string(to.GetUID())

			graph.Edges = append(graph.Edges, &Edge{
				From: string(obj.GetUID()),
				To:   toUID,
				Type: e.EdgeType,
			})
			graph.Nodes[toUID] = &Node{Unstructured: to}
		}
	}
	return graph
}
