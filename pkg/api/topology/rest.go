package topology

import (
	"net/http"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"

	"github.com/emicklei/go-restful"
	"github.com/spf13/cast"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

func NewREST(config *config.APIConfig) *REST {
	return &REST{
		cfg: config,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.GET("{cluster}/topology/{namespace}/{kind}/{name}").To(r.WrapError(r.Topology)).
			Doc("Get the topology of a resources for example: resources--ame:[application,service,deployment]").
			Param(restful.PathParameter("cluster", "cluster").DataType("string")).
			Param(restful.PathParameter("namespace", "namespace").DataType("string")).
			Param(restful.PathParameter("kind", "kind").DataType("string")).
			Param(restful.PathParameter("name", "name").DataType("string")).
			Param(restful.QueryParameter("depth", "depth").DataType("integer")).
			Returns(200, "OK", TopologyResponse{}.Graph).
			Produces(restful.MIME_JSON),
	)
}

// NameSpaceTopology is get all of kind topology
func (r *REST) Topology(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	namespace := req.PathParameter("namespace")
	kind := req.PathParameter("kind")
	name := req.PathParameter("name")

	depth, err := cast.ToInt16E(req.QueryParameter("depth"))
	if err != nil || depth < 0 {
		depth = 2
	}

	kc, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	ts := TopologyStore{
		Request: &TopologyRequest{
			Request:   req,
			Name:      name,
			Kind:      kind,
			Depth:     depth,
			Namespace: namespace,
			Client:    kc,
		},
		Logger: r.cfg.Logger,
	}

	ts.LoadObjects()
	ts.ParseTopology()
	ts.ParseResourceTopology(kind, name, depth)
	res.WriteAsJson(ts.Response.Graph)
	return
}
