package topology

import (
	"strings"
	"sync"

	"alauda.io/archon/pkg/api/application"
	"alauda.io/archon/pkg/kubernetes"

	"github.com/thoas/go-funk"
	"go.uber.org/zap"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/labels"
)

var WorkloadKind = []string{
	KubernetesKindDeployment,
	KubernetesKindDaemonSet,
	KubernetesKindStatefulSet,
}

var IgnoreKinds = []string{
	KubernetesKindPod,
	KubernetesKindEndpoints,
	KubernetesKindReplicaSet,
}

// graph path
var Topologies = map[string][]string{
	strings.ToLower(KubernetesKindApplication): {
		// workload
		strings.ToLower(KubernetesKindDeployment),
		strings.ToLower(KubernetesKindDaemonSet),
		strings.ToLower(KubernetesKindStatefulSet),
		// volume
		strings.ToLower(KubernetesKindSecret),
		strings.ToLower(KubernetesKindConfigmap),
		strings.ToLower(KubernetesKindPVC),
		// network
		strings.ToLower(KubernetesKindService),
		strings.ToLower(KubernetesKindRoute),
		strings.ToLower(KubernetesKindIngress),
	},
	strings.ToLower(KubernetesKindDeployment): {
		strings.ToLower(KubernetesKindApplication),
		strings.ToLower(KubernetesKindSecret),
		strings.ToLower(KubernetesKindConfigmap),
		strings.ToLower(KubernetesKindPVC),
		strings.ToLower(KubernetesKindService),
	},
	strings.ToLower(KubernetesKindDaemonSet): {
		strings.ToLower(KubernetesKindApplication),
		strings.ToLower(KubernetesKindSecret),
		strings.ToLower(KubernetesKindConfigmap),
		strings.ToLower(KubernetesKindPVC),
		strings.ToLower(KubernetesKindService),
	},
	strings.ToLower(KubernetesKindStatefulSet): {
		strings.ToLower(KubernetesKindApplication),
		strings.ToLower(KubernetesKindSecret),
		strings.ToLower(KubernetesKindConfigmap),
		strings.ToLower(KubernetesKindPVC),
		strings.ToLower(KubernetesKindService),
	},
	strings.ToLower(KubernetesKindSecret): {
		strings.ToLower(KubernetesKindDeployment),
		strings.ToLower(KubernetesKindDaemonSet),
		strings.ToLower(KubernetesKindStatefulSet),
		strings.ToLower(KubernetesKindApplication),
	},
	strings.ToLower(KubernetesKindConfigmap): {
		strings.ToLower(KubernetesKindDeployment),
		strings.ToLower(KubernetesKindDaemonSet),
		strings.ToLower(KubernetesKindStatefulSet),
		strings.ToLower(KubernetesKindApplication),
	},
	strings.ToLower(KubernetesKindPVC): {
		strings.ToLower(KubernetesKindDeployment),
		strings.ToLower(KubernetesKindDaemonSet),
		strings.ToLower(KubernetesKindStatefulSet),
		strings.ToLower(KubernetesKindApplication),
	},
	strings.ToLower(KubernetesKindRoute): {
		strings.ToLower(KubernetesKindService),
		strings.ToLower(KubernetesKindApplication),
	},
	strings.ToLower(KubernetesKindService): {
		strings.ToLower(KubernetesKindIngress),
		strings.ToLower(KubernetesKindApplication),
		strings.ToLower(KubernetesKindRoute),
		strings.ToLower(KubernetesKindDeployment),
		strings.ToLower(KubernetesKindDaemonSet),
		strings.ToLower(KubernetesKindStatefulSet),
	},
	strings.ToLower(KubernetesKindIngress): {
		strings.ToLower(KubernetesKindService),
		strings.ToLower(KubernetesKindApplication),
	},
}

// TopologyStore is easy to transfer  for providing data
type TopologyStore struct {
	Request  *TopologyRequest
	Objects  []*unstructured.Unstructured
	Response *TopologyResponse
	Logger   *zap.SugaredLogger
}

//ListObjByKind is list all object of kind in one namespace
func (ts *TopologyStore) ListObjByKind(kind string, namespace string) (uns []unstructured.Unstructured, err error) {
	c, err := ts.Request.Client.ClientFromResourceKind(kind, namespace)
	if err != nil {
		return
	}
	res, err := c.List(v1.ListOptions{})
	if err != nil {
		return
	}
	uns = res.Items
	return uns, nil
}

//LoadObjects is load all object of Relevant resource
func (ts *TopologyStore) LoadObjects() {

	refs := GetRef(ts.Request.Kind, ts.Request.Depth)
	ts.Logger.Debugf("LoadObjects: refs=%+v", refs)
	var objects []unstructured.Unstructured
	c, err := ts.Request.Client.ClientFromResourceKind(ts.Request.Kind, ts.Request.Namespace)
	if err != nil {
		ts.Logger.Warnf("Client From Resource Kind, err=%v", err)
		return
	}
	mainRes, err := c.Get(ts.Request.Name, v1.GetOptions{})
	if err != nil {
		ts.Logger.Warnf("Get kubernetes data error, err=%v", err)
		return
	} else {
		ts.Logger.Warnf("Get Main Res: %v", mainRes.GetName())
	}
	objects = append(objects, *mainRes)
	errChan := make(chan error, len(refs))

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}
	for _, ref := range refs {
		wg.Add(1)
		go func(kind string) {
			defer wg.Done()
			objs, err := ts.ListObjByKind(kind, ts.Request.Namespace)
			if nil != err {
				errChan <- err
				return
			}
			mutex.Lock()
			objects = append(objects, objs...)
			mutex.Unlock()
		}(ref)
	}

	go func() {
		wg.Wait()
		close(errChan)
	}()

	for err := range errChan {
		ts.Logger.Warnf("Get kubernetes data error, err=%s, skip", err.Error())
	}

	for _, obj := range objects {
		if funk.Contains(obj.GetKind(), IgnoreKinds) {
			continue
		}
		uus := &unstructured.Unstructured{Object: obj.Object}
		ts.Objects = append(ts.Objects, uus)
	}
	ts.Logger.Debugf("Get topology objects: len=%v", len(ts.Objects))
}

func getRef(sources []string, depth int16, visited map[string]bool) []string {

	result := []string{}
	if 0 >= depth {
		return result
	}
	var sub []string

	for _, source := range sources {
		source = strings.ToLower(source)

		refs, ok := Topologies[source]

		if !ok {
			continue
		}

		visited[source] = true

		for _, item := range refs {
			if !visited[item] {
				sub = append(sub, item)
				visited[item] = true
			}
		}
	}

	if 0 == len(sub) {
		return result
	}

	result = append(result, sub...)

	depth--
	result = append(result, getRef(sub, depth, visited)...)

	return result
}

// GetRef get ref from the sub ref
func GetRef(source string, depth int16) (result []string) {
	if 0 >= depth {
		return
	}

	source = strings.ToLower(source)

	visited := make(map[string]bool)

	refs, ok := Topologies[source]

	if !ok {
		return
	}

	visited[source] = true

	for _, item := range refs {
		visited[item] = true
	}

	result = append(result, refs...)

	depth--

	result = append(result, getRef(refs, depth, visited)...)
	return
}

//ParseNSTopology will parse Edge if kind is namespace
func (ts *TopologyStore) ParseNSTopology() {
	nodes := make(map[string]*Node, len(ts.Objects))
	for _, object := range ts.Objects {
		nodes[string(object.GetUID())] = &Node{Unstructured: *object}
	}
	ts.Response = &TopologyResponse{
		Graph: &Graph{Nodes: nodes, Edges: []*Edge{}},
		Refer: &Refer{Reference: []*ReferNode{}, ReferencedBy: []*ReferNode{}},
	}

	for _, object := range ts.Objects {
		switch object.GetKind() {
		case KubernetesKindApplication:
			ts.parseReferenceForApplication(object)
		case KubernetesKindDeployment:
			ts.parseReferenceForPodController(object)
		case KubernetesKindStatefulSet:
			ts.parseReferenceForPodController(object)
		case KubernetesKindDaemonSet:
			ts.parseReferenceForPodController(object)
		case KubernetesKindService:
			ts.parseReferenceForService(object)
		case KubernetesKindHPA:
			ts.parseReferenceForHPA(object)
		case KubernetesKindRoute:
			ts.parseReferenceForRoute(object)
		}
	}
}

//ParseTopology is parse all object's relation
func (ts *TopologyStore) ParseTopology() {
	indexes := make(map[string]*unstructured.Unstructured, len(ts.Objects))
	for _, object := range ts.Objects {
		indexes[getIndex(object.GetKind(), object.GetName())] = object
	}

	ts.Response = &TopologyResponse{
		TopologyHelper: TopologyHelper{Indexes: indexes},
		Graph:          &Graph{Nodes: make(map[string]*Node), Edges: []*Edge{}},
	}

	for _, object := range ts.Objects {
		switch object.GetKind() {
		case KubernetesKindApplication:
			ts.parseReferenceForApplication(object)
		case KubernetesKindDeployment:
			ts.parseReferenceForPodController(object)
		case KubernetesKindStatefulSet:
			ts.parseReferenceForPodController(object)
		case KubernetesKindDaemonSet:
			ts.parseReferenceForPodController(object)
		case KubernetesKindService:
			ts.parseReferenceForService(object)
		case KubernetesKindHPA:
			ts.parseReferenceForHPA(object)
		case KubernetesKindRoute:
			ts.parseReferenceForRoute(object)
		}
	}
}

//ParseReference is object reference
func (ts *TopologyStore) ParseReference() {
	name, kind := ts.Request.Name, ts.Request.Kind
	if name == "" && kind == "" {
		return
	}

	target, ok := ts.Response.Indexes[getIndex(kind, name)]
	if !ok || target == nil {
		ts.Logger.Infof("Can not find kubernetes object %s/%s", kind, name)
		return
	}

	graph, refer := ts.Response.Graph, ts.Response.Refer
	for _, edge := range ts.Response.Graph.Edges {
		if edge.From == string(target.GetUID()) {
			refer.Reference = append(refer.Reference, &ReferNode{Type: edge.Type, Node: graph.Nodes[edge.To]})
		}
		if edge.To == string(target.GetUID()) {
			refer.ReferencedBy = append(refer.ReferencedBy, &ReferNode{Type: edge.Type, Node: graph.Nodes[edge.From]})
		}
	}
}

// ParseResourceTopology only return the topology of a resource.
// ParseNSTopology() Must be called BEFORE it.
func (ts *TopologyStore) ParseResourceTopology(kind, name string, depth int16) {
	target, ok := ts.Response.Indexes[getIndex(kind, name)]
	if !ok || target == nil {
		ts.Logger.Infof("Can not find kubernetes object %s/%s", kind, name)
		return
	}

	uid := string(target.GetUID())

	ts.Response.Graph.Nodes[uid] = &Node{Unstructured: *target}

	ts.Response.Topo = ts.Response.Topo.Walk([]*unstructured.Unstructured{target}, nil, depth)

	ts.Response.Graph = ts.Response.TopologyHelper.ParseGraph()
}

func (ts *TopologyStore) parseReferenceForPodController(from *unstructured.Unstructured) {
	template := corev1.PodTemplateSpec{}
	switch from.GetKind() {
	case KubernetesKindDeployment:
		deploy, err := kubernetes.UnstructuredToDeployment(from)
		if err != nil {
			return
		}
		template = deploy.Spec.Template
	case KubernetesKindStatefulSet:
		stateful, err := kubernetes.UnstructuredToStatefulSet(from)
		if err != nil {
			return
		}
		template = stateful.Spec.Template
	case KubernetesKindDaemonSet:
		daemon, err := kubernetes.UnstructuredToDaemonSet(from)
		if err != nil {
			return
		}
		template = daemon.Spec.Template
	}
	containers := template.Spec.Containers
	for _, container := range containers {
		var kind, name string
		for _, env := range container.EnvFrom {
			if env.ConfigMapRef != nil {
				kind = KubernetesKindConfigmap
				name = env.ConfigMapRef.Name
			}
			if env.SecretRef != nil {
				kind = KubernetesKindSecret
				name = env.SecretRef.Name
			}
			ts.addEdge(from, kind, name, Reference)
		}
		for _, env := range container.Env {
			valueFrom := env.ValueFrom
			if valueFrom == nil {
				continue
			}
			if valueFrom.ConfigMapKeyRef != nil {
				kind = KubernetesKindConfigmap
				name = valueFrom.ConfigMapKeyRef.Name
			}
			if valueFrom.SecretKeyRef != nil {
				kind = KubernetesKindSecret
				name = valueFrom.SecretKeyRef.Name
			}
			ts.addEdge(from, kind, name, Reference)
		}
	}

	for _, volume := range template.Spec.Volumes {
		var kind, name string
		if volume.PersistentVolumeClaim != nil {
			kind = KubernetesKindPVC
			name = volume.PersistentVolumeClaim.ClaimName
		}
		if volume.ConfigMap != nil {
			kind = KubernetesKindConfigmap
			name = volume.ConfigMap.Name
		}
		if volume.Secret != nil {
			kind = KubernetesKindSecret
			name = volume.Secret.SecretName
		}
		ts.addEdge(from, kind, name, Reference)
	}

	accountName := template.Spec.ServiceAccountName
	if accountName != "" {
		ts.addEdge(from, KubernetesKindServiceAccount, accountName, Reference)
	}
}

func (ts *TopologyStore) parseReferenceForService(from *unstructured.Unstructured) {
	service, err := kubernetes.UnstructuredToService(from)
	if err != nil {
		ts.Logger.Errorf("UnstructuredToService error, %s", err.Error())
		return
	}
	selector := labels.SelectorFromSet(service.Spec.Selector)

	if selector.Empty() {
		ts.Logger.Infof("Service %s has no selector, skip", from.GetName())
		return
	}

	for _, object := range ts.Objects {
		if !IsPodController(object.GetKind()) {
			continue
		}

		templateSpec, err := kubernetes.GetPodTemplateSpecFromUnstructured(*object)
		if err != nil {
			ts.Logger.Warnf("%v", err)
			continue
		}

		ls := templateSpec.GetLabels()
		if len(ls) == 0 {
			continue
		}

		if selector.Matches(labels.Set(ls)) {
			ts.addEdge(from, object.GetKind(), object.GetName(), Selector)
		}
	}
}

func (ts *TopologyStore) parseReferenceForHPA(from *unstructured.Unstructured) {

	hpa, err := kubernetes.UnstructuredToHPA(from)
	if err != nil {
		ts.Logger.Errorf("UnstructuredToHPA error, %s", err.Error())
		return
	}
	reference := hpa.Spec.ScaleTargetRef

	ts.addEdge(from, reference.Kind, reference.Name, Reference)
}

func (ts *TopologyStore) parseReferenceForRoute(from *unstructured.Unstructured) {
	reference, found, err := unstructured.NestedMap(from.Object, "spec", "to")
	if !found {
		ts.Logger.Errorf("Can not find to field for Route")
		return
	}
	if err != nil {
		ts.Logger.Errorf("Can not find to field for Route: v%", err)
		return
	}
	ts.addEdge(from, reference["kind"].(string), reference["name"].(string), Reference)
}

func (ts *TopologyStore) parseReferenceForApplication(from *unstructured.Unstructured) {
	app, err := application.UnstructuredToApplication(from)

	if err != nil {
		ts.Logger.Errorf("UnstructuredToApplication Err,%s", err.Error())
		return
	}
	selector := labels.SelectorFromSet(app.Spec.Selector.MatchLabels)
	if selector.Empty() {
		return
	}
	for _, object := range ts.Objects {
		ls := object.GetLabels()
		if len(ls) == 0 {
			continue
		}

		if selector.Matches(labels.Set(ls)) {
			ts.addEdge(from, object.GetKind(), object.GetName(), Selector)
		}
	}
}

func (ts *TopologyStore) addHelperEdge(from, to *unstructured.Unstructured, edgeType string) {
	if nil == from || nil == to {
		return
	}
	tKey := getIndex(to.GetKind(), to.GetName())

	if nil == ts.Response.Indexes {
		ts.Logger.Errorf("Indexes not initialized")
		return
	}

	fKey := getIndex(from.GetKind(), from.GetName())

	if nil == ts.Response.Indexes[fKey] || nil == ts.Response.Indexes[tKey] {
		ts.Logger.Errorf("Index not in nodes: index=%s", tKey)
		return
	}

	node := &EdgeHelper{*to, edgeType}

	if nil == ts.Response.Topo {
		ts.Response.Topo = make(Topology)
	}

	nodes, ok := ts.Response.Topo[fKey]

	if !ok {
		nodes = []*EdgeHelper{{*to, edgeType}}
	} else {
		nodes = append(nodes, node)
	}
	ts.Response.Topo[fKey] = nodes

}

func (ts *TopologyStore) addEdge(from *unstructured.Unstructured, kind string, name string, edgeType string) {
	index := getIndex(kind, name)
	to, ok := ts.Response.Indexes[index]
	if !ok || to == nil {
		ts.Logger.Errorf("Object %s/%s not found", kind, name)
		return
	}

	ts.addHelperEdge(from, to, edgeType)

	switch edgeType {
	case Reference:
		ts.addHelperEdge(to, from, Referenced)
	case Selector:
		ts.addHelperEdge(to, from, Selected)
	}
}
