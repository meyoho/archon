package topology

// Kubernetes topology reference kinds
const (
	Reference  = "Reference"
	Referenced = "Referenced"
	Selector   = "Selector"
	Selected   = "Selected"
)

// Kubernetes Resources
const (
	KubernetesKindNamespace   = "Namespace"
	KubernetesKindApplication = "Application"
	KubernetesKindService     = "Service"
	KubernetesKindDeployment  = "Deployment"
	KubernetesKindDaemonSet   = "DaemonSet"
	KubernetesKindStatefulSet = "StatefulSet"
	KubernetesKindConfigmap   = "ConfigMap"
	KubernetesKindReplicaSet  = "ReplicaSet"
	KubernetesKindHPA         = "HorizontalPodAutoscaler"
	KubernetesKindPod         = "Pod"
	KubernetesKindSecret      = "Secret"

	KubernetesKindPV             = "PersistentVolume"
	KubernetesKindPVC            = "PersistentVolumeClaim"
	KubernetesKindSC             = "StorageClass"
	KubernetesKindRoute          = "Route"
	KubernetesKindIngress        = "Ingress"
	KubernetesKindEvent          = "Event"
	KubernetesKindEndpoints      = "Endpoints"
	KubernetesKindServiceAccount = "ServiceAccount"
)

// Kubernetes Deploy Mode. (Pod controllers)
var (
	KubernetesDeployModeKind = map[string]bool{
		KubernetesKindDeployment:  true,
		KubernetesKindDaemonSet:   true,
		KubernetesKindStatefulSet: true,
	}
)

// IsPodController check whether a kubernetes resource kind is a pod controller.
// current support: daemonset/deployment/statefulset
func IsPodController(kind string) bool {
	return KubernetesDeployModeKind[kind]
}
