package psp

import (
	"alauda.io/archon/pkg/api/config"
	"testing"
)

func TestPSPDeletedLabelKey(t *testing.T) {
	tests := []struct {
		name   string
		domain string
		want   string
	}{
		{
			name:   "ok",
			domain: "fake",
			want:   "psp.fake/deleted",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			config.SetConfig(config.APIConfig{
				LabelBaseDomain: tt.domain,
			})
			if got := PSPDeletedLabelKey(); got != tt.want {
				t.Errorf("PSPDeletedLabelKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
