package psp

import (
	"context"
	"reflect"
	"testing"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"
	"alauda.io/archon/pkg/test"

	"k8s.io/api/policy/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func fakeManager(cb kubernetes.ClientBuilderInterface) rest.Manager {
	return rest.Manager{
		ManagerConfig: config.ManagerConfig{
			GroupKind:       metav1.GroupKind{"policy", "PodSecurityPolicy"},
			Cluster:         "fake",
			LabelBaseDomain: "fake.io",
			Context:         context.TODO(),
			ClientBuilder:   cb,
		},
	}
}

func SetupClusterPSPManager(pspsFile string) ClusterPSPManager {
	config.SetConfig(config.APIConfig{
		LabelBaseDomain: "fake",
	})
	var psps []*unstructured.Unstructured
	if pspsFile != "" {
		test.ObjectFromFixture(pspsFile, &psps)
	}
	fakecb := kubernetes.NewFakeKubeClientBuilder(kubernetes.ToRuntimeObjects(psps)...)
	restcfg, _ := kubernetes.FakeRESTConfig(nil)
	return &defaultClusterPSPManager{
		Manager: fakeManager(fakecb.FakeClientBuilder(restcfg, "fake")),
	}
}

func Test_defaultClusterPSPManager_Create(t *testing.T) {
	type args struct {
		policyFile  string
		objectsFile string
	}
	tests := []struct {
		name    string
		args    args
		wantPsp string
		wantErr bool
	}{
		{
			name: "ok not exist",
			args: args{
				policyFile:  "create_psp.json",
				objectsFile: "",
			},
			wantPsp: "created_psp.json",
		},
		{
			name: "ok existed",
			args: args{
				policyFile:  "create_psp.json",
				objectsFile: "init_psps.json",
			},
			wantPsp: "created_psp.json",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := SetupClusterPSPManager(tt.args.objectsFile)
			psp := &v1beta1.PodSecurityPolicy{}
			test.ObjectFromFixture(tt.args.policyFile, psp)
			gotPsp, err := m.Create(psp)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			gotPsp.UID = ""
			gotPsp.ResourceVersion = ""
			wantPspObj := &v1beta1.PodSecurityPolicy{}
			test.ObjectFromFixture(tt.wantPsp, wantPspObj)
			if !reflect.DeepEqual(gotPsp, wantPspObj) {
				t.Errorf("Create() gotPsp = %v, want %v", gotPsp, wantPspObj)
			}
		})
	}
}

func Test_defaultClusterPSPManager_Delete(t *testing.T) {
	tests := []struct {
		name        string
		objectsFile string
		wantErr     bool
	}{
		{
			name: "not exist",
		},
		{
			name:        "exist",
			objectsFile: "init_psps.json",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := SetupClusterPSPManager(tt.objectsFile)
			if err := m.Delete(); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_defaultClusterPSPManager_Get(t *testing.T) {
	tests := []struct {
		name        string
		objectsFile string
		wantErr     bool
	}{
		{
			name:        "ok",
			objectsFile: "init_psps.json",
		},
		{
			name:    "not found",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := SetupClusterPSPManager(tt.objectsFile)
			gotPsp, err := m.Get()
			if err == nil && gotPsp.Name != UserRestrictedPSPName {
				t.Errorf("Expect psp name %s, got %s", UserRestrictedPSPName, gotPsp.Name)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_defaultClusterPSPManager_Update(t *testing.T) {
	type args struct {
		policyFile string
	}
	tests := []struct {
		name    string
		args    args
		wantPsp string
		wantErr bool
	}{
		{
			name: "ok",
			args: args{
				policyFile: "update_psp.json",
			},
			wantPsp: "updated_psp.json",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := SetupClusterPSPManager("init_psps.json")
			psp := &v1beta1.PodSecurityPolicy{}
			test.ObjectFromFixture(tt.args.policyFile, psp)
			gotPsp, err := m.Update(psp)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			wantPspObj := &v1beta1.PodSecurityPolicy{}
			test.ObjectFromFixture(tt.wantPsp, wantPspObj)
			if !reflect.DeepEqual(gotPsp, wantPspObj) {
				t.Errorf("Update() gotPsp = %v, want %v", gotPsp, wantPspObj)
			}
		})
	}
}

func Test_defaultClusterPSPManager_defaultHiddenPSPFields(t *testing.T) {
	type args struct {
		pspFile string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{
				pspFile: "create_psp.json",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := SetupClusterPSPManager("").(*defaultClusterPSPManager)
			psp := &v1beta1.PodSecurityPolicy{}
			test.ObjectFromFixture(tt.args.pspFile, psp)
			m.defaultHiddenPSPFields(psp)
			if psp.Name != UserRestrictedPSPName ||
				psp.Spec.FSGroup.Rule != v1beta1.FSGroupStrategyRunAsAny ||
				psp.Spec.RunAsGroup.Rule != v1beta1.RunAsGroupStrategyRunAsAny ||
				psp.Spec.SELinux.Rule != v1beta1.SELinuxStrategyRunAsAny ||
				psp.Spec.SupplementalGroups.Rule != v1beta1.SupplementalGroupsStrategyRunAsAny ||
				psp.Annotations[AllowedProfilesAnnotationKey] != AllowAny {
				t.Error("Unexpected default psp fields value")
			}
		})
	}
}

func Test_newDefaultClusterPSPManager(t *testing.T) {
	type args struct {
		config config.ManagerConfig
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{config: config.ManagerConfig{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newDefaultClusterPSPManager(tt.args.config); got == nil {
				t.Errorf("newDefaultClusterPSPManager() is nil")
			}
		})
	}
}
