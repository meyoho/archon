package psp

import (
	"alauda.io/archon/pkg/api/config"
	"testing"
)

func Test_defaultLifecycleManagerFactory_LifecycleManager(t *testing.T) {
	type args struct {
		config config.ManagerConfig
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{config: config.ManagerConfig{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lf := &defaultLifecycleManagerFactory{}
			if got := lf.LifecycleManager(tt.args.config); got == nil {
				t.Errorf("LifecycleManager() = nil")
			}
		})
	}
}
