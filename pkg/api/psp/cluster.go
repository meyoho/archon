package psp

import (
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"

	"k8s.io/api/core/v1"
	"k8s.io/api/policy/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ClusterPSPManager interface {
	Get() (*v1beta1.PodSecurityPolicy, error)
	Create(policy *v1beta1.PodSecurityPolicy) (*v1beta1.PodSecurityPolicy, error)
	Update(policy *v1beta1.PodSecurityPolicy) (*v1beta1.PodSecurityPolicy, error)
	Delete() error
}

type defaultClusterPSPManager struct {
	rest.Manager
}

func (m *defaultClusterPSPManager) Get() (psp *v1beta1.PodSecurityPolicy, err error) {
	client, err := m.Client("")
	if err != nil {
		return
	}
	unstructPSP, err := client.Get(UserRestrictedPSPName, metav1.GetOptions{})
	if err != nil {
		return
	}
	// If the deleted label is existed, return NotFound
	labels := unstructPSP.GetLabels()
	if labels != nil && labels[PSPDeletedLabelKey()] == "true" {
		return nil, errors.NewNotFound(
			schema.GroupResource{
				Group:    "policy",
				Resource: "podsecuritypolices",
			},
			"cluster",
		)
	}
	psp = &v1beta1.PodSecurityPolicy{}
	err = kubernetes.UnstructuredToObject(unstructPSP, psp)
	if err != nil {
		return
	}

	return
}

func (m *defaultClusterPSPManager) defaultHiddenPSPFields(psp *v1beta1.PodSecurityPolicy) {
	psp.Name = UserRestrictedPSPName
	psp.Spec.AllowedCapabilities = []v1.Capability{"*"}
	psp.Spec.FSGroup = v1beta1.FSGroupStrategyOptions{
		Rule: v1beta1.FSGroupStrategyRunAsAny,
	}
	psp.Spec.RunAsGroup = &v1beta1.RunAsGroupStrategyOptions{
		Rule: v1beta1.RunAsGroupStrategyRunAsAny,
	}
	psp.Spec.SELinux = v1beta1.SELinuxStrategyOptions{
		Rule: v1beta1.SELinuxStrategyRunAsAny,
	}
	psp.Spec.SupplementalGroups = v1beta1.SupplementalGroupsStrategyOptions{
		Rule: v1beta1.SupplementalGroupsStrategyRunAsAny,
	}
	if psp.Annotations == nil {
		psp.Annotations = map[string]string{}
	}
	psp.Annotations[AllowedProfilesAnnotationKey] = AllowAny
	psp.Annotations[SkipSyncKey] = "true"
	psp.Spec.Volumes = []v1beta1.FSType{v1beta1.All}
}

func (m *defaultClusterPSPManager) Create(policy *v1beta1.PodSecurityPolicy) (psp *v1beta1.PodSecurityPolicy, err error) {
	client, err := m.Client("")
	if err != nil {
		return
	}
	// Create the psp
	m.defaultHiddenPSPFields(policy)
	upsp, err := kubernetes.ObjectToUnstructured(policy)
	if err != nil {
		return
	}
	upsp, err = client.Create(upsp, metav1.CreateOptions{})
	if err != nil {
		if errors.IsAlreadyExists(err) {
			err = nil
			upspo, errI := client.Get(UserRestrictedPSPName, metav1.GetOptions{})
			if errI != nil {
				err = errI
				return
			}
			policy.ResourceVersion = upspo.GetResourceVersion()
			policy.UID = upspo.GetUID()
			upsp, errI = kubernetes.ObjectToUnstructured(policy)
			if errI != nil {
				err = errI
				return
			}
			upsp, errI = client.Update(upsp, metav1.UpdateOptions{})
			if errI != nil {
				err = errI
				return
			}
		} else {
			return
		}
	}
	psp = &v1beta1.PodSecurityPolicy{}
	err = kubernetes.UnstructuredToObject(upsp, psp)
	return
}

func (m *defaultClusterPSPManager) Update(policy *v1beta1.PodSecurityPolicy) (psp *v1beta1.PodSecurityPolicy, err error) {
	client, err := m.Client("")
	if err != nil {
		return
	}
	m.defaultHiddenPSPFields(policy)
	upsp, err := kubernetes.ObjectToUnstructured(policy)
	if err != nil {
		return
	}
	upsp, err = client.Update(upsp, metav1.UpdateOptions{})
	if err != nil {
		return
	}
	psp = &v1beta1.PodSecurityPolicy{}
	err = kubernetes.UnstructuredToObject(upsp, psp)
	return
}

func (m *defaultClusterPSPManager) Delete() (err error) {
	client, err := m.Client("")
	if err != nil {
		return
	}
	upsp, err := client.Get(UserRestrictedPSPName, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			err = nil
			return
		}
		return
	}
	deletedPSP := &DefaultUserRestrictedPSP
	deletedPSP.UID = upsp.GetUID()
	deletedPSP.ResourceVersion = upsp.GetResourceVersion()
	kubernetes.AddLabels(deletedPSP, map[string]string{
		PSPDeletedLabelKey(): "true",
	})
	upsp, err = kubernetes.ObjectToUnstructured(deletedPSP)
	if err != nil {
		return
	}
	_, err = client.Update(upsp, metav1.UpdateOptions{})
	return
}

func newDefaultClusterPSPManager(config config.ManagerConfig) ClusterPSPManager {
	return &defaultClusterPSPManager{
		Manager: rest.Manager{ManagerConfig: config},
	}
}
