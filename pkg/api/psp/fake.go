package psp

import (
	"alauda.io/archon/pkg/api/config"
	"k8s.io/api/policy/v1beta1"
)

type FakeLifeLifecycleManagerFactory struct{}

func (f *FakeLifeLifecycleManagerFactory) LifecycleManager(config config.ManagerConfig) ClusterPSPManager {
	return &FakeClusterPSPManager{}
}

type FakeClusterPSPManager struct{}

func (f *FakeClusterPSPManager) Get() (*v1beta1.PodSecurityPolicy, error) {
	return &DefaultUserRestrictedPSP, nil
}

func (f *FakeClusterPSPManager) Create(policy *v1beta1.PodSecurityPolicy) (*v1beta1.PodSecurityPolicy, error) {
	return &DefaultUserRestrictedPSP, nil
}

func (f *FakeClusterPSPManager) Update(policy *v1beta1.PodSecurityPolicy) (*v1beta1.PodSecurityPolicy, error) {
	return &DefaultUserRestrictedPSP, nil
}

func (f *FakeClusterPSPManager) Delete() error {
	return nil
}
