package psp

import "alauda.io/archon/pkg/api/config"

type LifecycleManagerFactory interface {
	LifecycleManager(config config.ManagerConfig) ClusterPSPManager
}

type defaultLifecycleManagerFactory struct{}

func (lf *defaultLifecycleManagerFactory) LifecycleManager(config config.ManagerConfig) ClusterPSPManager {
	return newDefaultClusterPSPManager(config)
}
