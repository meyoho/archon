package psp

import (
	"context"
	"net/http"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"

	"github.com/emicklei/go-restful"
	"k8s.io/api/policy/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg     *config.APIConfig
	factory LifecycleManagerFactory
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg:     cfg,
		factory: &defaultLifecycleManagerFactory{},
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	clusterParam := ws.PathParameter("cluster", "The name of the kubernetes cluster")
	ws.Route(
		ws.GET("{cluster}/podsecuritypolicies/cluster").To(r.WrapError(r.GetClusterPSP)).
			Doc("Get the cluster pod security policy.").
			Returns(http.StatusOK, "success", v1beta1.PodSecurityPolicy{}).
			Param(clusterParam).
			Produces(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/podsecuritypolicies/cluster").To(r.WrapError(r.CreateClusterPSP)).
			Doc("Create the cluster pod security policy.").
			Returns(http.StatusOK, "success", v1beta1.PodSecurityPolicy{}).
			Param(clusterParam).
			Consumes(restful.MIME_JSON).
			Produces(restful.MIME_JSON),
	).Route(
		ws.PUT("{cluster}/podsecuritypolicies/cluster").To(r.WrapError(r.UpdateClusterPSP)).
			Doc("Update the cluster pod security policy.").
			Returns(http.StatusOK, "success", v1beta1.PodSecurityPolicy{}).
			Param(clusterParam).
			Consumes(restful.MIME_JSON).
			Produces(restful.MIME_JSON),
	).Route(
		ws.DELETE("{cluster}/podsecuritypolicies/cluster").To(r.WrapError(r.DeleteClusterPSP)).
			Doc("Delete the cluster pod security policy.").
			Returns(http.StatusNoContent, "success", nil),
	)
}

func (r *REST) GetClusterPSP(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	m, err := r.getPSPManagerFromRequest(req)
	if err != nil {
		return
	}
	psp, err := m.Get()
	if err != nil {
		return
	}
	res.WriteAsJson(psp)
	return
}

func (r *REST) CreateClusterPSP(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	psp := &v1beta1.PodSecurityPolicy{}
	err = r.ReadEntity(psp, req)
	if err != nil {
		return
	}
	m, err := r.getPSPManagerFromRequest(req)
	if err != nil {
		return
	}
	pspCreated, err := m.Create(psp)
	if err != nil {
		return
	}
	res.WriteAsJson(pspCreated)
	return
}

func (r *REST) UpdateClusterPSP(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	psp := &v1beta1.PodSecurityPolicy{}
	err = r.ReadEntity(psp, req)
	if err != nil {
		return
	}
	m, err := r.getPSPManagerFromRequest(req)
	if err != nil {
		return
	}
	pspUpdated, err := m.Update(psp)
	if err != nil {
		return
	}
	res.WriteAsJson(pspUpdated)
	return
}
func (r *REST) DeleteClusterPSP(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusNoContent
	m, err := r.getPSPManagerFromRequest(req)
	if err != nil {
		return
	}
	err = m.Delete()
	return
}

func (r *REST) getPSPManagerFromRequest(req *restful.Request) (m ClusterPSPManager, err error) {
	c, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	m = r.factory.LifecycleManager(config.ManagerConfig{
		GroupKind: metav1.GroupKind{
			Group: "policy",
			Kind:  "PodSecurityPolicy",
		},
		Cluster:       req.PathParameter("cluster"),
		Context:       context.TODO(),
		ClientBuilder: c,
	})
	return
}
