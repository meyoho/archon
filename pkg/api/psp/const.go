package psp

import (
	"alauda.io/archon/pkg/api/config"
	"fmt"
	"k8s.io/api/core/v1"
	"k8s.io/api/policy/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	UserRestrictedPSPName = "20-user-restricted"
	// AllowAny is the wildcard used to allow any profile.
	AllowAny = "*"
	// The annotation key specifying the allowed seccomp profiles.
	AllowedProfilesAnnotationKey = "seccomp.security.alpha.kubernetes.io/allowedProfileNames"
	// SkipSyncKey used to skip sync operation by sentry.
	SkipSyncKey = "skip-sync"
)

var (
	DefaultUserRestrictedPSP = v1beta1.PodSecurityPolicy{
		TypeMeta: metav1.TypeMeta{
			Kind:       "PodSecurityPolicy",
			APIVersion: "policy/v1beta1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: UserRestrictedPSPName,
			Annotations: map[string]string{
				AllowedProfilesAnnotationKey: AllowAny,
			},
		},
		Spec: v1beta1.PodSecurityPolicySpec{
			Privileged:          true,
			AllowedCapabilities: []v1.Capability{"*"},
			FSGroup: v1beta1.FSGroupStrategyOptions{
				Rule: v1beta1.FSGroupStrategyRunAsAny,
			},
			HostIPC:     true,
			HostNetwork: true,
			HostPID:     true,
			HostPorts: []v1beta1.HostPortRange{
				{
					Max: 65535,
					Min: 0,
				},
			},
			RunAsUser: v1beta1.RunAsUserStrategyOptions{
				Rule: v1beta1.RunAsUserStrategyRunAsAny,
			},
			RunAsGroup: &v1beta1.RunAsGroupStrategyOptions{
				Rule: v1beta1.RunAsGroupStrategyRunAsAny,
			},
			SELinux: v1beta1.SELinuxStrategyOptions{
				Rule: v1beta1.SELinuxStrategyRunAsAny,
			},
			SupplementalGroups: v1beta1.SupplementalGroupsStrategyOptions{
				Rule: v1beta1.SupplementalGroupsStrategyRunAsAny,
			},
			Volumes: []v1beta1.FSType{v1beta1.All},
		},
	}
)

func init() {
	allow := true
	DefaultUserRestrictedPSP.Spec.AllowPrivilegeEscalation = &allow
}

func PSPDeletedLabelKey() string {
	return fmt.Sprintf("psp.%s/deleted", config.GetConfig().LabelBaseDomain)
}
