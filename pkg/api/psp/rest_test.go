package psp

import (
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/test"
	"github.com/emicklei/go-restful"
	"net/http"
	"testing"
)

func TestNewREST(t *testing.T) {
	type args struct {
		cfg *config.APIConfig
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{cfg: config.FakeAPIConfig()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewREST(tt.args.cfg); got == nil {
				t.Errorf("NewREST() is nil")
			}
		})
	}
}

func SetupREST(method, url, file string) (*REST, *test.FakeRestful) {
	cfg := config.FakeAPIConfig()
	config.SetConfig(*cfg)
	return &REST{
		cfg:     cfg,
		factory: &FakeLifeLifecycleManagerFactory{},
	}, test.FakeRestfulWithJSONFile(method, url, file)
}

func TestREST_CreateClusterPSP(t *testing.T) {
	tests := []struct {
		name           string
		createPsp      string
		wantStatusCode int
	}{
		{
			name:           "ok",
			createPsp:      "create_psp.json",
			wantStatusCode: http.StatusOK,
		},
		{
			name:           "invalid",
			createPsp:      "invalid_psp.json",
			wantStatusCode: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodPost, "fake", tt.createPsp)
			r.WrapError(r.CreateClusterPSP)(fr.Request, fr.Response)
			gotStatusCode := fr.ReturnedResponse().StatusCode
			if gotStatusCode != tt.wantStatusCode {
				t.Errorf("CreateClusterPSP() gotStatusCode = %v, want %v", gotStatusCode, tt.wantStatusCode)
			}
		})
	}
}

func TestREST_DeleteClusterPSP(t *testing.T) {
	tests := []struct {
		name           string
		wantStatusCode int
		wantErr        bool
	}{
		{
			name:           "ok",
			wantStatusCode: http.StatusNoContent,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodDelete, "fake", "")
			r.WrapError(r.DeleteClusterPSP)(fr.Request, fr.Response)
			gotStatusCode := fr.ReturnedResponse().StatusCode
			if gotStatusCode != tt.wantStatusCode {
				t.Errorf("DeleteClusterPSP() gotStatusCode = %v, want %v", gotStatusCode, tt.wantStatusCode)
			}
		})
	}
}

func TestREST_GetClusterPSP(t *testing.T) {
	tests := []struct {
		name           string
		wantStatusCode int
		wantErr        bool
	}{
		{
			name:           "ok",
			wantStatusCode: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodGet, "fake", "")
			r.WrapError(r.GetClusterPSP)(fr.Request, fr.Response)
			gotStatusCode := fr.ReturnedResponse().StatusCode
			if gotStatusCode != tt.wantStatusCode {
				t.Errorf("GetClusterPSP() gotStatusCode = %v, want %v", gotStatusCode, tt.wantStatusCode)
			}
		})
	}
}

func TestREST_InstallRoutes(t *testing.T) {
	type fields struct {
		Handler rest.Handler
		cfg     *config.APIConfig
		factory LifecycleManagerFactory
	}
	type args struct {
		version core.APIVersion
		ws      *restful.WebService
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "ok",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &REST{}
			ws := &restful.WebService{}
			r.InstallRoutes(core.APIVersionV1, ws)
			if len(ws.Routes()) <= 0 {
				t.Errorf("Unexpected empty routes")
			}
		})
	}
}

func TestREST_UpdateClusterPSP(t *testing.T) {
	tests := []struct {
		name           string
		updatePsp      string
		wantStatusCode int
	}{
		{
			name:           "ok",
			updatePsp:      "update_psp.json",
			wantStatusCode: http.StatusOK,
		},
		{
			name:           "invalid",
			updatePsp:      "invalid_psp.json",
			wantStatusCode: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodPut, "fake", tt.updatePsp)
			r.WrapError(r.UpdateClusterPSP)(fr.Request, fr.Response)
			gotStatusCode := fr.ReturnedResponse().StatusCode
			if gotStatusCode != tt.wantStatusCode {
				t.Errorf("UpdateClusterPSP() gotStatusCode = %v, want %v", gotStatusCode, tt.wantStatusCode)
			}
		})
	}
}

func TestREST_getPSPManagerFromRequest(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name: "ok",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodGet, "", "")
			gotM, err := r.getPSPManagerFromRequest(fr.Request)
			if (err != nil) != tt.wantErr {
				t.Errorf("getPSPManagerFromRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotM == nil {
				t.Errorf("getPSPManagerFromRequest() gotM is nil")
			}
		})
	}
}
