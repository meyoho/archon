package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// ALB2Kind ALB2
	ALB2Kind = "ALB2"
	// FrontendKind Frontend
	FrontendKind = "Frontend"
	// RuleKind Rule
	RuleKind = "Rule"
)

// ALB2 alb2 type
type ALB2 struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec ALB2Spec `json:"spec"`
}

// ALB2Spec alb2 spec
type ALB2Spec struct {
	Address     string   `json:"address"`
	BindAddress string   `json:"bind_address"`
	Domains     []string `json:"domains"`
	IaasID      string   `json:"iaas_id"`
	Type        string   `json:"type"`
}

// ALB2List alb2 list
type ALB2List struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []ALB2 `json:"items"`
}

// Frontend frontend type
type Frontend struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec FrontendSpec `json:"spec"`
}

// FrontendSpec frontend spec
type FrontendSpec struct {
	Port            int           `json:"port"`
	Protocol        string        `json:"protocol"`
	ServiceGroup    *ServiceGroup `json:"serviceGroup,omitempty"`
	Source          *Source       `json:"source,omitempty"`
	CertificateName string        `json:"certificate_name"`
}

// FrontendList frontend list
type FrontendList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Frontend `json:"items"`
}

// Rule rule type
type Rule struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec RuleSpec `json:"spec"`
}

// Service service info
type Service struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
	Port      int    `json:"port"`
	Weight    int    `json:"weight"`
}

// ServiceGroup serivces
type ServiceGroup struct {
	SessionAffinityPolicy    string    `json:"session_affinity_policy,omitempty"`
	SessionAffinityAttribute string    `json:"session_affinity_attribute,omitempty"`
	Services                 []Service `json:"services"`
}

// Source who create the resource
type Source struct {
	Name      string `json:"name"`
	Namespace string `json:"namespace"`
	Type      string `json:"type"`
}

// RuleSpec rule spec
type RuleSpec struct {
	Description     string        `json:"description"`
	Domain          string        `json:"domain"`
	DSL             string        `json:"dsl"`
	Priority        int           `json:"priority"`
	ServiceGroup    *ServiceGroup `json:"serviceGroup,omitempty"`
	Source          *Source       `json:"source,omitempty"`
	Type            string        `json:"type"`
	URL             string        `json:"url"`
	CertificateName string        `json:"certificate_name"`
	// +optional
	RewriteTarget string `json:"rewrite_target,omitempty"`
}

// RuleList rule list
type RuleList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Rule `json:"items"`
}
