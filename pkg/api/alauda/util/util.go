package util

import (
	"encoding/json"

	alb2v1 "alauda.io/archon/pkg/api/alauda/v1"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// UnstructuredToALB2 converts "Unstructured" type object to "ALB2" type.
func UnstructuredToALB2(obj *unstructured.Unstructured) (*alb2v1.ALB2, error) {
	o := &alb2v1.ALB2{}
	raw, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(raw, o)
	if err != nil {
		return nil, err
	}
	return o, err
}

// UnstructuredToFrontend converts "Unstructured" type object to "Frontend" type.
func UnstructuredToFrontend(obj *unstructured.Unstructured) (*alb2v1.Frontend, error) {
	o := &alb2v1.Frontend{}
	raw, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(raw, o)
	if err != nil {
		return nil, err
	}
	return o, err
}

// UnstructuredToRule converts "Unstructured" type object to "Rule" type.
func UnstructuredToRule(obj *unstructured.Unstructured) (*alb2v1.Rule, error) {
	o := &alb2v1.Rule{}
	raw, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(raw, o)
	if err != nil {
		return nil, err
	}
	return o, err
}
