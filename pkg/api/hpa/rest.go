package hpa

import (
	"net/http"

	"alauda.io/archon/pkg/api/feature_gate"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"

	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/klog"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.PUT("{cluster}/hpa/{namespace}/{kind}/{name}").To(r.WrapError(r.CreateHPA)).
			Doc("Create a HPA and old HPA will be covered or delete").
			Param(restful.PathParameter("cluster", "cluster name")).
			Param(restful.PathParameter("namespace", "namespace name")).
			Param(restful.PathParameter("kind", "workload kind")).
			Param(restful.PathParameter("name", "workload kind")),
	).Route(
		ws.GET("{cluster}/hpa/{namespace}/{kind}/{name}").To(r.WrapError(r.GetAutoScaleSpec)).
			Doc("GetActiveAutoScale  cronhpa or hpa of workload").
			Param(restful.PathParameter("cluster", "cluster name")).
			Param(restful.PathParameter("namespace", "namespace name")).
			Param(restful.PathParameter("kind", "workload kind")).
			Param(restful.PathParameter("name", "workload kind")),
	)
}

//CreateHPA will create HPA resource and old hpa will be cleared
func (r *REST) CreateHPA(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	namespace := req.PathParameter("namespace")
	kind := req.PathParameter("kind")
	workloadName := req.PathParameter("name")
	hpar := AutoScaleSpec{}
	err = req.ReadEntity(&hpar)
	if err != nil {
		return
	}

	cb, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	//Find workload
	ri, err := cb.ClientFromResourceKind(kind, namespace)
	if err != nil {
		return
	}
	workload, err := ri.Get(workloadName, metav1.GetOptions{})
	if err != nil {
		return
	}
	//clear all hpa
	if err = removeAllAutoScale(workload, cb); err != nil {
		return
	}

	// create hpa
	kc, err := cb.KubeClient()
	if err != nil {
		return
	}
	newHpa, err := CreateAutoScale(&hpar, workload, kc)
	if err != nil {
		return
	}
	res.WriteAsJson(newHpa)
	return
}

//GetActiveAutoScale will return hpa of workload
func (r *REST) GetAutoScaleSpec(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	namespace := req.PathParameter("namespace")
	cluster := req.PathParameter("cluster")
	kind := req.PathParameter("kind")
	workloadName := req.PathParameter("name")
	hpaName := autoScaleName(kind, workloadName)
	hpar := &AutoScaleSpec{}

	cb, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	kc, err := cb.KubeClient()
	if err != nil {
		return
	}
	var retGVK = []schema.GroupVersionKind{
		CronHPAGVK,
	}

	fg, err := feature_gate.GetClusterFeatureGateAPI(cluster, "hpav2", req, r.cfg)
	if err != nil {
		return statusCode, err
	}

	if fg.Status.Enabled {
		retGVK = append(retGVK, HPAv2beta2)
	} else {
		retGVK = append(retGVK, HPAv1)
	}

	for _, gvk := range retGVK {
		client, err := kc.ClientForGVK(gvk)
		if err != nil {
			klog.Warningf("Get hpa with workload %v: %v", workloadName, err)
			err = nil
			continue
		}
		hpa, err := client.Namespace(namespace).Get(hpaName, metav1.GetOptions{})
		if err != nil {
			klog.Warningf("Get hpa with workload %v: %v", workloadName, err)
			err = nil
			continue
		}
		hpar.SetAutoScale(hpa)
		break
	}

	_ = res.WriteAsJson(hpar)
	return
}
