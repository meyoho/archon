package hpa

import (
	"net/http"
	"testing"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/test"
	"github.com/emicklei/go-restful"

	"github.com/gsamokovarov/assert"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

func newRest() *REST {
	var initObjects = []runtime.Object{
		&v1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Deployment",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "nginx",
				Namespace: "default",
			},
		},
		&v1beta1.CustomResourceDefinition{
			ObjectMeta: metav1.ObjectMeta{
				Name: "cronhpas.tkestack.io",
			},
			TypeMeta: metav1.TypeMeta{
				Kind:       "CustomResourceDefinition",
				APIVersion: "apiextensions.k8s.io/v1beta1",
			},
			Spec: v1beta1.CustomResourceDefinitionSpec{
				Group:   "tkestack.io",
				Version: "v1",
				Scope:   v1beta1.ResourceScope("Namespaced"),
				Names: v1beta1.CustomResourceDefinitionNames{
					Plural:   "cronhpas",
					Singular: "cronhpa",
					Kind:     "CronHPA",
					ListKind: "CronHPAList",
				},
			},
		},
	}

	return NewREST(config.FakeAPIConfig(initObjects...))
}

func TestNewREST(t *testing.T) {
	type args struct {
		cfg *config.APIConfig
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "generic",
			args: args{
				cfg: &config.APIConfig{
					LabelBaseDomain: "fake",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewREST(tt.args.cfg)
			if got.cfg.LabelBaseDomain != tt.args.cfg.LabelBaseDomain {
				t.Errorf("Expect LabelBaseDomain %s, got %s", tt.args.cfg.LabelBaseDomain, got.cfg.LabelBaseDomain)
				return
			}
		})
	}
}

func TestRESTInstallRoutes(t *testing.T) {
	type fields struct {
		Handler rest.Handler
		config  *config.APIConfig
	}
	type args struct {
		version core.APIVersion
		ws      *restful.WebService
	}
	testRest := newRest()
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "generic",
			fields: fields{
				Handler: testRest.Handler,
				config:  testRest.cfg,
			},
			args: args{
				version: core.APIVersionV1,
				ws:      new(restful.WebService),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &REST{
				Handler: tt.fields.Handler,
				cfg:     tt.fields.config,
			}
			a.InstallRoutes(tt.args.version, tt.args.ws)
			assert.NotEqual(t, len(tt.args.ws.Routes()), 0)
		})
	}
}

func TestREST_CreateHPA(t *testing.T) {

	rest := newRest()
	tests := []struct {
		name    string
		url     string
		request string
	}{
		{
			name: "cronHpa",
			url:  "http://localhost:8080/acp/v1/kubernetes/nice/hpa/default/deployment/nginx",
			request: `{
   			"cronHpa":{
				"apiVersion": "tkestack.io/v1",
				"kind": "CronHPA",
				"metadata": {
					"name": "nginx",
					"namespace": "default",
				},
				"spec": {
					"crons": [
						{
							"schedule": "*/5 * * * *",
							"targetReplicas": 3
						},
						{
							"schedule": "*/10 * * * *",
							"targetReplicas": 6
						}
					],
					"scaleTargetRef": {
						"apiVersion": "apps/v1",
						"kind": "Deployment",
						"name": "nginx"
					}
				}
			}
		}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := test.FakeRestfulRequestWithJSON(http.MethodPut, tt.url, tt.request)
			resp, recorder := test.FakeRestfulResponse()
			rest.CreateHPA(req, resp)
			httpResp := recorder.Result()
			assert.Equal(t, httpResp.StatusCode, http.StatusOK)
		})
	}

}
