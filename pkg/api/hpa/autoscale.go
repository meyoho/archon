package hpa

import (
	"fmt"
	"strings"

	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/thoas/go-funk"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/klog"
)

//CreateHPA will create hpa resource and delete other hpa resources
func CreateAutoScale(as *AutoScaleSpec, workload *unstructured.Unstructured, kc k8s.KubeClientInterface) (*AutoScaleSpec, error) {

	autoScaleSpec := &AutoScaleSpec{}
	namespaces := workload.GetNamespace()
	workloadName := workload.GetName()
	workloadKind := workload.GetKind()
	hpa := as.GetActiveAutoScale()

	if hpa == nil {
		klog.Warningf("%v's hpa not need update", workloadName)
		return autoScaleSpec, nil
	}

	if hpa.GetName() != autoScaleName(workloadKind, workloadName) {
		klog.Warningf("HPA name format error: %s", hpa.GetName())
		hpa.SetName(autoScaleName(workloadKind, workloadName))
	}

	//Set OwnerReference
	ownerReference := k8s.NewControllerRef(workload, workload.GroupVersionKind())
	hpa.SetOwnerReferences([]metav1.OwnerReference{*ownerReference})

	//hpa and workload must be at same namespace
	gvk := hpa.GroupVersionKind()
	client, err := kc.ClientForGVK(gvk)
	if err != nil {
		return nil, err
	}

	newHpa, err := client.Namespace(namespaces).Create(hpa, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}
	autoScaleSpec.SetAutoScale(newHpa)
	return autoScaleSpec, nil
}

// removeAllAutoScale remove all AutoScale rules
func removeAllAutoScale(workload *unstructured.Unstructured, builder k8s.ClientBuilderInterface) error {
	workloadName := workload.GetName()
	namespace := workload.GetNamespace()
	kind := workload.GetKind()
	client, err := builder.KubeClient()
	if err != nil {
		return err
	}
	// Clear all HPA
	for _, gvk := range ALLAutoScaleGVK {
		client, err := client.ClientForGVK(schema.GroupVersionKind(gvk))
		if err != nil {
			klog.Warningf("clear autoscale with workload %v: %v", workloadName, err)
			continue
		}
		err = client.Namespace(namespace).Delete(autoScaleName(kind, workloadName), &metav1.DeleteOptions{})
		if err != nil {
			klog.Warningf("clear autoscale with workload %v: %v", workloadName, err)
			if !apiErrors.IsNotFound(err) {
				return err
			}
		}
	}
	return nil
}

// autoScaleName return hpa name by workload
func autoScaleName(kind, workloadName string) string {
	if funk.Contains(CompatibleOldNameRules, strings.ToLower(kind)) {
		return fmt.Sprintf("%v", workloadName)
	}
	return fmt.Sprintf("%v-%v", strings.ToLower(kind), workloadName)
}
