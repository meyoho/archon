package hpa

import (
	"strings"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

var CompatibleOldNameRules = []string{
	strings.ToLower("Deployment"),
}

var FeatureGateGroupKind = v1.GroupKind{
	Group: "alauda.io",
	Kind:  "AlaudaFeatureGate",
}

type AutoScaleSpec struct {
	// CronHpa  CronHpa resources
	// optional
	CronHPA *unstructured.Unstructured `json:"cronHpa"`
	// HPA  HorizontalPodAutoscaler resources
	// optional
	HPA *unstructured.Unstructured `json:"hpa"`
}

// GetActiveAutoScale will return cronhpa or hpa when one of them is not empty
func (as *AutoScaleSpec) GetActiveAutoScale() *unstructured.Unstructured {
	if as.HPA != nil {
		return as.HPA
	}
	return as.CronHPA
}

//SetAutoScale will set hpa by kind
func (as *AutoScaleSpec) SetAutoScale(hpa *unstructured.Unstructured) {
	if hpa == nil {
		return
	}
	switch hpa.GetKind() {
	case CronHPA:
		as.CronHPA = hpa
		break
	case HorizontalPodAutoscaler:
		as.HPA = hpa
		break
	default:
		break

	}
}
