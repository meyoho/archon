package hpa

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	CronHPA                 = "CronHPA"
	HorizontalPodAutoscaler = "HorizontalPodAutoscaler"
)

var CronHPAGVK = schema.GroupVersionKind{
	Group:   "tkestack.io",
	Kind:    CronHPA,
	Version: "v1",
}

var HPAv1 = schema.GroupVersionKind{
	Group:   "autoscaling",
	Kind:    HorizontalPodAutoscaler,
	Version: "v1",
}

var HPAv2beta2 = schema.GroupVersionKind{
	Group:   "autoscaling",
	Kind:    HorizontalPodAutoscaler,
	Version: "v2beta2",
}

var ALLAutoScaleGVK = []schema.GroupVersionKind{
	CronHPAGVK,
	HPAv1,
	HPAv2beta2,
}
