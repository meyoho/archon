package namespace

import (
	"encoding/json"
	"net/http"
	"testing"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/test"
)

func TestREST_UpdateGeneralNamespaces(t *testing.T) {
	initObjects := []runtime.Object{
		&v1.LimitRange{
			TypeMeta: metav1.TypeMeta{
				Kind:       "LimitRange",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "default",
				Namespace: "system-test-alauda",
			},
		},
		&v1.ResourceQuota{
			TypeMeta: metav1.TypeMeta{
				Kind:       "ResourceQuota",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "default",
				Namespace: "system-test-alauda",
			},
		},
		&v1.Namespace{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Namespace",
				APIVersion: "v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name: "system-test-alauda",
			},
		},
	}
	appREST := NewREST(config.FakeAPIConfig(initObjects...))
	tests := []struct {
		name    string
		request string
	}{
		{
			name: "full",
			request: `
			{
				"namespace": {
					"apiVersion": "v1",
					"kind": "Namespace",
					"metadata": {
						"name": "system-test-alauda",
						"labels": {
						"alauda.io/project": "system"
					},
						"annotations": {}
					}
				},
				"resourcequota": {
					"apiVersion": "v1",
					"kind": "ResourceQuota",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"hard": {
							"requests.cpu": "2",
							"requests.memory": "2G",
							"requests.storage": "100G",
							"pods": "100",
							"persistentvolumeclaims": "100",
							"limits.cpu": "4",
							"limits.memory": "8G"
						}
					}
				},
				"limitrange": {
					"apiVersion": "v1",
					"kind": "LimitRange",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"limits": [{
							"default": {
								"memory": "512M",
								"cpu": "500m"
							},
							"defaultRequest": {
								"memory": "512M",
								"cpu": "500m"
							},
							"max": {
								"cpu": "4",
								"memory": "4G"
							},
							"type": "Container"
						}]
					}
				}
			}`,
		},
		{
			name: "no limitrange",
			request: `
			{
				"namespace": {
					"apiVersion": "v1",
					"kind": "Namespace",
					"metadata": {
						"name": "system-test-alauda",
						"labels": {
						"alauda.io/project": "system"
					},
						"annotations": {}
					}
				},
				"resourcequota": {
					"apiVersion": "v1",
					"kind": "ResourceQuota",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"hard": {
							"requests.cpu": "3",
							"requests.memory": "2G",
							"requests.storage": "100G",
							"pods": "100",
							"persistentvolumeclaims": "100",
							"limits.cpu": "4",
							"limits.memory": "8G"
						}
					}
				}`,
		},
		{
			name: "no limitrange resourcequota",
			request: `
			{
				"namespace": {
					"apiVersion": "v1",
					"kind": "Namespace",
					"metadata": {
						"name": "system-test-alauda",
						"labels": {
						"alauda.io/project": "system"
					},
						"annotations": {}
					}
				}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := test.FakeRestfulRequestWithJSON(http.MethodPost, "fake", tt.request)
			resp, recorder := test.FakeRestfulResponse()
			appREST.UpdateGeneralNamespace(req, resp)
			httpResp := recorder.Result()
			body := recorder.Body.Bytes()
			if httpResp.StatusCode != http.StatusOK {
				t.Errorf("Expect status %d, got %d, resp: %s", http.StatusOK, httpResp.StatusCode, body)
				return
			}
		})
	}
}

func TestREST_CreateGeneralNamespace(t *testing.T) {
	appREST := NewREST(config.FakeAPIConfig())
	tests := []struct {
		name    string
		request string
	}{
		{
			name: "full",
			request: `
			{
				"namespace": {
					"apiVersion": "v1",
					"kind": "Namespace",
					"metadata": {
						"name": "system-test-alauda",
						"labels": {
						"alauda.io/project": "system"
					},
						"annotations": {}
					}
				},
				"resourcequota": {
					"apiVersion": "v1",
					"kind": "ResourceQuota",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"hard": {
							"requests.cpu": "2",
							"requests.memory": "2G",
							"requests.storage": "100G",
							"pods": "100",
							"persistentvolumeclaims": "100",
							"limits.cpu": "4",
							"limits.memory": "8G"
						}
					}
				},
				"limitrange": {
					"apiVersion": "v1",
					"kind": "LimitRange",
					"metadata": {
						"name": "default",
						"namespace": "system-test-alauda"
					},
					"spec": {
						"limits": [{
							"default": {
								"memory": "512M",
								"cpu": "500m"
							},
							"defaultRequest": {
								"memory": "512M",
								"cpu": "500m"
							},
							"max": {
								"cpu": "4",
								"memory": "4G"
							},
							"type": "Container"
						}]
					}
				}
			}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req := test.FakeRestfulRequestWithJSON(http.MethodPost, "fake", tt.request)
			resp, recorder := test.FakeRestfulResponse()
			appREST.CreateGeneralNamespace(req, resp)
			httpResp := recorder.Result()
			body := recorder.Body.Bytes()
			if httpResp.StatusCode != http.StatusOK {
				t.Errorf("Expect status %d, got %d, resp: %s", http.StatusOK, httpResp.StatusCode, body)
				return
			}
			gn := &GeneralNamespace{}
			err := json.Unmarshal(body, gn)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
				return
			}
		})
	}
}
