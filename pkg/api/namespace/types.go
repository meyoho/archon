package namespace

import (
	"github.com/thoas/go-funk"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

var (
	BuiltInNamespaces = []string{"default", "kube-public", "kube-system"}
)

//GeneralNamespace is Full GeneralNamespace containe Namespace ,ResourceQuota and LimitRange
type GeneralNamespace struct {
	Namespace     *unstructured.Unstructured `json:"namespace"`
	ResourceQuota *unstructured.Unstructured `json:"resourcequota,omitempty"`
	LimitRange    *unstructured.Unstructured `json:"limitrange,omitempty"`
}

// IsBuiltInNamespace will judge the namespace is builtIn Namespace,for example default
func IsBuiltInNamespace(name string) bool {
	return funk.Contains(name, BuiltInNamespaces)
}
