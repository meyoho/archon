package namespace

import (
	"encoding/json"
	"time"

	k8s "alauda.io/archon/pkg/kubernetes"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/klog"
)

//CreateGeneralNamespace will create GeneralNamespace
func CreateGeneralNamespace(gn *GeneralNamespace, cb k8s.ClientBuilderInterface) (newNamespace *GeneralNamespace, err error) {

	newNamespace = &GeneralNamespace{}

	dc, err := cb.ClientFromResource(gn.Namespace)
	if err != nil {
		return
	}

	newnamespace, err := dc.Create(gn.Namespace, v1.CreateOptions{})
	if err != nil {
		return
	}

	newNamespace.Namespace = newnamespace

	backoff := wait.Backoff{
		Duration: time.Millisecond * 200,
		Steps:    5,
	}

	// TODO: check error type
	// time.Sleep(500 * time.Millisecond)
	// Create or update ResQuota
	err = k8s.RetryOnError(backoff, func() error {
		newResQuota, err := createOrUpdateResQuota(gn.ResourceQuota, cb)
		if err != nil {
			klog.Warningf("createOrUpdateResQuota err: %v will retry", err)
			return err
		}
		klog.Warningf("createOrUpdateResQuota success")
		newNamespace.ResourceQuota = newResQuota
		return nil
	})
	if err != nil {
		return nil, err
	}

	// Create or update LmtRange
	err = k8s.RetryOnError(backoff, func() error {
		newLmtRange, err := createOrUpdateLmtRange(gn.LimitRange, cb)
		if err != nil {
			klog.Warningf("createOrUpdateLmtRange err: %v will retry", err)
			return err
		}
		newNamespace.LimitRange = newLmtRange
		klog.Warningf("createOrUpdateLmtRange success")
		return nil
	})
	if err != nil {
		return nil, err
	}

	return
}

//UpdateGeneralNamespace will update GeneralNamespace
func UpdateGeneralNamespace(gn *GeneralNamespace, cb k8s.ClientBuilderInterface) (newNamespace *GeneralNamespace, err error) {
	newNamespace = &GeneralNamespace{}

	dc, err := cb.ClientFromResource(gn.Namespace)
	if err != nil {
		return
	}
	data, err := json.Marshal(gn.Namespace)
	if err != nil {
		return nil, err
	}
	// Retry: because auth2-controller has delay time.And no way to check error type:
	// can't use this function: apierrors.IsForbidden() to check error type.So retry
	// on any Error
	backoff := wait.Backoff{
		Duration: time.Millisecond * 200,
		Steps:    5,
	}
	err = k8s.RetryOnError(backoff, func() error {
		newnamespace, err := dc.Patch(gn.Namespace.GetName(), types.MergePatchType, data, v1.PatchOptions{})
		if err != nil {
			return err
		}
		newNamespace.Namespace = newnamespace
		return nil
	})
	if err != nil {
		return nil, err
	}

	// CreateOrUpdate ResQuota
	// Retry: because auth2-controller has delay time.And no way to check error type:
	// can't use this function: apierrors.IsForbidden() to check error type.So retry
	// on any Error
	err = k8s.RetryOnError(backoff, func() error {
		newResQuota, err := createOrUpdateResQuota(gn.ResourceQuota, cb)
		if err != nil {
			return err
		}
		newNamespace.ResourceQuota = newResQuota
		return nil
	})
	if err != nil {
		return nil, err
	}

	// CreateOrUpdate LmtRange
	// Retry: because auth2-controller has delay time.And no way to check error type:
	// can't use this function: apierrors.IsForbidden() to check error type.So retry
	// on any Error
	err = k8s.RetryOnError(backoff, func() error {
		newLmtRange, err := createOrUpdateLmtRange(gn.LimitRange, cb)
		if err != nil {
			return err
		}
		newNamespace.LimitRange = newLmtRange
		return nil
	})
	if err != nil {
		return nil, err
	}
	return
}

//createOrUpdateResQuota will create resourceQuota
func createOrUpdateResQuota(resourceQuota *unstructured.Unstructured, cb k8s.ClientBuilderInterface) (newResourceQuota *unstructured.Unstructured, err error) {

	// If resourceQuota is nil ,It will ignore create it
	if resourceQuota == nil {
		return nil, nil
	}

	dc, err := cb.ClientFromResource(resourceQuota)
	if err != nil {
		return nil, err
	}
	// Update resourceQuota and return newResourceQuota. If resourceQuota is  not found ,It will create it
	newResourceQuota, err = dc.Update(resourceQuota, v1.UpdateOptions{})
	if err != nil {
		if apierrors.IsNotFound(err) {
			newResourceQuota, err = dc.Create(resourceQuota, v1.CreateOptions{})
			if err != nil {
				klog.Warningf("resource_quota_create_error: %v", err.Error())

				return
			}
			return
		}
		klog.Warningf("resource_quota_update_error: %v", err.Error())
		return
	}
	return
}

//createOrUpdateLmtRange is Create LimitRange of resource
func createOrUpdateLmtRange(limitRange *unstructured.Unstructured, cb k8s.ClientBuilderInterface) (newLmtRange *unstructured.Unstructured, err error) {

	// If limitRange is nil,It will ignore create limitRange
	if limitRange == nil {
		return nil, nil
	}

	dc, err := cb.ClientFromResource(limitRange)
	if err != nil {
		return
	}

	// Update limitRange and return newLmtRange. If limitRange is not found ,it will create it
	newLmtRange, err = dc.Update(limitRange, v1.UpdateOptions{})
	if err != nil {
		if apierrors.IsNotFound(err) {
			newLmtRange, err = dc.Create(limitRange, v1.CreateOptions{})
			if err != nil {
				klog.Warningf("resource_limitRange_create_error: %v", err.Error())
				return
			}
			return
		}
		klog.Warningf("limit_range_update_error: %v", err.Error())
		return
	}
	return
}
