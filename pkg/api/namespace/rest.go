package namespace

import (
	"errors"
	"fmt"
	"net/http"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"

	"github.com/emicklei/go-restful"
	"k8s.io/klog"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.POST("{cluster}/general-namespaces").To(r.WrapError(r.CreateGeneralNamespace)).
			Doc("Create a GeneralNamespaces contain LimitRange and ResourceQuota").
			Param(restful.QueryParameter("project_name", "project name")).
			Param(restful.PathParameter("cluster", "cluster name")).
			Returns(200, "succuss", GeneralNamespace{}),
	).Route(
		ws.PUT("{cluster}/general-namespaces/{name}").To(r.WrapError(r.UpdateGeneralNamespace)).
			Doc("Update a GeneralNamespaces").
			Param(restful.QueryParameter("project_name", "project name")).
			Param(restful.PathParameter("cluster", "cluster name")).
			Param(restful.PathParameter("name", "namespace name")).
			Returns(200, "succuss", GeneralNamespace{}),
	)
}

//CreateGeneralNamespace will create GeneralNamespace
func (r *REST) CreateGeneralNamespace(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	gn := GeneralNamespace{}
	err = req.ReadEntity(&gn)
	if err != nil {
		return
	}
	if gn.Namespace == nil {
		err = errors.New("namespace is nil")
		return
	}

	if gn.ResourceQuota != nil && gn.Namespace.GetName() != gn.ResourceQuota.GetNamespace() {
		klog.Warningf("ResourceQuota needs to comply with the Namespace restrictions")
		klog.Warningf("Set ResourceQuota Namespace: %s", gn.Namespace.GetName())
		//override wrong name
		gn.ResourceQuota.SetNamespace(gn.Namespace.GetName())
	}

	if gn.LimitRange != nil && gn.Namespace.GetName() != gn.LimitRange.GetNamespace() {
		klog.Warningf("LimitRange needs to comply with the Namespace restrictions")
		klog.Warningf("Set LimitRange Namespace: %s", gn.Namespace.GetName())
		//override wrong name
		gn.LimitRange.SetNamespace(gn.Namespace.GetName())
	}

	nsLabels := gn.Namespace.GetLabels()
	nsLabels["openshift.io/cluster-monitoring"] = "true"
	if project_name := req.QueryParameter("project_name"); project_name != "" {
		nsLabels[fmt.Sprintf("%v/project", r.cfg.LabelBaseDomain)] = project_name
	}
	gn.Namespace.SetLabels(nsLabels)

	clientBuilderr, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	newGn, err := CreateGeneralNamespace(&gn, clientBuilderr)
	if err != nil {
		return
	}
	res.WriteAsJson(newGn)
	return
}

//UpdateGeneralNamespace will update GeneralNamespace
func (r *REST) UpdateGeneralNamespace(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	gn := GeneralNamespace{}
	err = req.ReadEntity(&gn)
	if err != nil {
		return
	}

	if gn.Namespace == nil {
		err = errors.New("namespace is nil")
		return
	}

	if gn.ResourceQuota != nil && gn.Namespace.GetName() != gn.ResourceQuota.GetNamespace() {
		klog.Warningf("ResourceQuota needs to comply with the Namespace restrictions")
		klog.Warningf("Set ResourceQuota Namespace: %s", gn.Namespace.GetName())
		//override wrong name
		gn.ResourceQuota.SetNamespace(gn.Namespace.GetName())
	}

	if gn.LimitRange != nil && gn.Namespace.GetName() != gn.LimitRange.GetNamespace() {
		klog.Warningf("LimitRange needs to comply with the Namespace restrictions")
		klog.Warningf("Set LimitRange Namespace: %s", gn.Namespace.GetName())
		//override wrong name
		gn.LimitRange.SetNamespace(gn.Namespace.GetName())
	}

	nsLabels := gn.Namespace.GetLabels()
	nsLabels["openshift.io/cluster-monitoring"] = "true"
	if project_name := req.QueryParameter("project_name"); project_name != "" {
		nsLabels[fmt.Sprintf("%v/project", r.cfg.LabelBaseDomain)] = project_name
	}
	gn.Namespace.SetLabels(nsLabels)

	clientBuilderr, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	newGn, err := UpdateGeneralNamespace(&gn, clientBuilderr)
	if err != nil {
		return
	}
	res.WriteAsJson(newGn)
	return
}
