package core

import (
	"github.com/emicklei/go-restful"
)

// APIVersion is the type of rest API version.
type APIVersion string

const (
	// APIVersionV1 is the version string of v1 API.
	APIVersionV1 = "v1"
)

// APIRoutesInterface installs routes of specific APIVersion on webservice.
type APIRoutesInterface interface {
	InstallRoutes(version APIVersion, ws *restful.WebService)
}
