package application

import (
	"fmt"

	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// Request Attributes
const (
	// ApplicationComponentTemplatesAttribute is the requset attribute key of componentTemplates
	// in application.
	ApplicationComponentTemplatesAttribute = "ApplicationComponentTemplates"
	// ApplicationComponentTemplatesDiffAttribute is the request attribute key of componentTemplate diff
	// with last revision of application.
	ApplicationComponentTemplatesDiffAttribute = "ApplicationComponentTemplatesDiff"
	// ApplicationComponentStorageQueryFunction is the key of the function used to query the application
	// components from storage.
	ApplicationComponentStorageQueryFunction = "ApplicationComponentStorageQueryFunction"
)

func ComponentsFromRequest(req *restful.Request) ([]unstructured.Unstructured, error) {
	componentsAttr := req.Attribute(ApplicationComponentTemplatesAttribute)
	if componentsAttr == nil {
		return nil, errors.NewInternalError(fmt.Errorf("get components from request failed"))
	}

	components, ok := componentsAttr.([]unstructured.Unstructured)
	if !ok {
		return nil, errors.NewInternalError(fmt.Errorf("get components from request failed"))
	}
	return components, nil
}

func ComponentsDiffFromRequest(req *restful.Request) (k8s.ResourceDiffItems, error) {
	componentsDiffAttr := req.Attribute(ApplicationComponentTemplatesDiffAttribute)
	if componentsDiffAttr == nil {
		return nil, errors.NewInternalError(fmt.Errorf("get components diff from request failed"))
	}

	componentsDiff, ok := componentsDiffAttr.(k8s.ResourceDiffItems)
	if !ok {
		return nil, errors.NewInternalError(fmt.Errorf("get components diff from request failed"))
	}
	return componentsDiff, nil
}

func ComponentStorageQueryFunctionFromRequest(req *restful.Request) (ComponentStorageQueryFunc, error) {
	attr := req.Attribute(ApplicationComponentStorageQueryFunction)
	if attr == nil {
		return nil, errors.NewInternalError(fmt.Errorf("get component storage query function from request failed"))
	}

	ret, ok := attr.(ComponentStorageQueryFunc)
	if !ok {
		return nil, errors.NewInternalError(fmt.Errorf("get component storage query function from request failed"))
	}
	return ret, nil
}
