package application

import (
	"context"
	"fmt"

	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/spf13/cast"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// UpdateStrategy implements the UpdateStrategy interface.
type UpdateStrategy struct{}

// PrepareForUpdate prepares the application update.
func (UpdateStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) error {
	app := obj.(*v1beta1.Application)
	oldApp, err := UnstructuredToApplication(old.(*unstructured.Unstructured))
	if err != nil {
		return errors.NewInternalError(err)
	}

	req, ok := rest.RequestFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get Request from context failed"))
	}
	config, ok := rest.APIConfigFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get APIConfig from context failed"))
	}

	client, err := rest.ClientBuilderFromRequest(req)
	if err != nil {
		return err
	}

	log := config.Logger.Named(fmt.Sprintf("application: %s/%s", app.GetNamespace(), app.GetName()))
	domain := config.LabelBaseDomain
	appUID := string(oldApp.GetUID())

	app.ResourceVersion = oldApp.ResourceVersion
	app.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	app.Spec.AssemblyPhase = v1beta1.Pending
	app.Status = oldApp.Status
	SetAppLabel(app, AppUIDKey(domain), appUID)
	AddAppSelector(app, map[string]string{
		AppNameKey(domain): AppSelectorName(app.GetName(), app.GetNamespace()),
	})

	forceUpdatePod := cast.ToBool(req.QueryParameter("forceUpdatePod"))
	computeComponentKinds(app, domain, forceUpdatePod, PrepareComponentTemplate)
	log.Debugf("Resolved application component kinds: %v", app.Spec.ComponentGroupKinds)
	componentsQuery, err := ComponentStorageQueryFunctionFromRequest(req)
	if err != nil {
		log.Warnf("Get components query function error: %v, use default", err)
		componentsQuery = GetApplicationComponentsFromStorage
	}

	oldComponents, err := componentsQuery(client, oldApp)
	if err != nil {
		return err
	}

	diffs, err := k8s.ComputeResourceDiffs(
		oldComponents,
		app.Spec.ComponentTemplates,
		k8s.ObjectKeyWithNamespace)
	if err != nil {
		return err
	}

	log.Debugf("Resource diffs on update application: %+v", diffs)
	req.SetAttribute(ApplicationComponentTemplatesAttribute, app.Spec.ComponentTemplates)
	req.SetAttribute(ApplicationComponentTemplatesDiffAttribute, diffs)
	app.Spec.ComponentTemplates = nil
	SetAppAnnotation(
		app,
		AppHistoryRevisionKey(domain),
		fmt.Sprintf("%d", AppHistoryRevision(domain, oldApp)))
	SetAppAnnotation(
		app,
		AppHistoryLimitKey(domain),
		fmt.Sprintf("%d", ApplicationDefaultHistoryLimit))
	return nil
}

// ValidateUpdate validates the application update request.
func (UpdateStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) field.ErrorList {
	errs := field.ErrorList{}
	app := obj.(*v1beta1.Application)
	oldApp, _ := UnstructuredToApplication(old.(*unstructured.Unstructured))
	if app.GetName() != oldApp.GetName() {
		errs = append(errs,
			field.Forbidden(field.NewPath("metadata", "name"), "name can't be modified"),
		)
	}
	if app.GetNamespace() != oldApp.GetNamespace() {
		errs = append(errs,
			field.Forbidden(field.NewPath("metadata", "namespace"), "namespace can't be modified"),
		)
	}
	return errs
}

// AfterUpdate does some post update work, e.g. update the components, create history.
func (UpdateStrategy) AfterUpdate(ctx context.Context, obj, old runtime.Object) error {
	req, ok := rest.RequestFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get Request from context failed"))
	}
	config, ok := rest.APIConfigFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get APIConfig from context failed"))
	}

	c, err := rest.ClientBuilderFromRequest(req)
	if err != nil {
		return err
	}

	components, err := ComponentsFromRequest(req)
	if err != nil {
		return err
	}

	componentsDiff, err := ComponentsDiffFromRequest(req)
	if err != nil {
		return err
	}

	appObj := obj.(*unstructured.Unstructured)
	var componentTemplates []interface{}
	for _, c := range components {
		componentTemplates = append(componentTemplates, c.Object)
	}
	unstructured.SetNestedSlice(appObj.Object, componentTemplates, "spec", "componentTemplates")
	ri, err := c.NamespacedDynamicClient("applications", "app.k8s.io/v1beta1", appObj.GetNamespace())
	if err != nil {
		return errors.NewInternalError(err)
	}

	user := k8s.CreatorOfObject(config.LabelBaseDomain, appObj)
	var rollbackFrom *int
	rollbackAttr := req.Attribute(RollbackRequestAttr)
	if rollbackAttr != nil {
		rollback := rollbackAttr.(*v1beta1.ApplicationRollback)
		user = rollback.Spec.User
		rollbackFrom = &rollback.Spec.Revision
	}

	err = updateApplicationComponents(
		c, ri, old.(*unstructured.Unstructured),
		appObj, componentsDiff,
		user, rollbackFrom)
	if err != nil {
		return err
	}
	return nil
}
