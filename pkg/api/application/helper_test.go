package application

import (
	"reflect"
	"testing"

	"alauda.io/archon/pkg/api/application/v1beta1"
	his "alauda.io/archon/pkg/api/applicationhistory/v1beta1"
)

func TestArchiveChartPackage(t *testing.T) {
	type args struct {
		cp      *v1beta1.ApplicationChartPackage
		history *his.ApplicationHistory
	}
	tests := []struct {
		name    string
		args    args
		wantPkg []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPkg, err := ArchiveChartPackage(tt.args.cp, tt.args.history)
			if (err != nil) != tt.wantErr {
				t.Errorf("ArchiveChartPackage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotPkg, tt.wantPkg) {
				t.Errorf("ArchiveChartPackage() gotPkg = %v, want %v", gotPkg, tt.wantPkg)
			}
		})
	}
}
