package application

import "alauda.io/archon/pkg/api/config"

type LifecycleManagerFactory interface {
	LifecycleManager(config config.ManagerConfig) LifecycleManager
}

type defaultLifecycleManagerFactory struct{}

func (lf *defaultLifecycleManagerFactory) LifecycleManager(config config.ManagerConfig) LifecycleManager {
	return newDefaultLifecycleManager(config)
}
