package application

import (
	"fmt"
	"sort"
	"time"

	"alauda.io/archon/pkg/api/application/v1beta1"
	ah "alauda.io/archon/pkg/api/applicationhistory"
	his "alauda.io/archon/pkg/api/applicationhistory/v1beta1"
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/ghodss/yaml"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/validation/field"
	"k8s.io/client-go/dynamic"
	"k8s.io/klog"
)

type defaultLifecycleManager struct {
	rest.Manager
}

func newDefaultLifecycleManager(config config.ManagerConfig) *defaultLifecycleManager {
	return &defaultLifecycleManager{Manager: rest.Manager{ManagerConfig: config}}
}

func (m *defaultLifecycleManager) Get(namespace, name string, option GetOptions) (app *v1beta1.Application, err error) {
	client, err := m.Client(namespace)
	if err != nil {
		return
	}
	unstructuredApp, err := client.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}
	return UnstructuredToApplication(unstructuredApp)
}

func (m *defaultLifecycleManager) List(namespace string, option ListOptions) (appList *v1beta1.ApplicationList, err error) {
	client, err := m.Client(namespace)
	if err != nil {
		return
	}
	unstructuredList, err := client.List(option.ListOptions)
	if err != nil {
		return
	}
	appList = &v1beta1.ApplicationList{}
	appList.Kind = "ApplicationList"
	appList.APIVersion = "app.k8s.io/v1beta1"
	for _, app := range unstructuredList.Items {
		uapp, errI := UnstructuredToApplication(&app)
		if errI != nil {
			err = errI
			return
		}
		appList.Items = append(appList.Items, *uapp)
	}
	return
}

func (m *defaultLifecycleManager) Components(namespace, name string) (clist []unstructured.Unstructured, err error) {
	app, err := m.Get(namespace, name, GetOptions{})
	if err != nil {
		return
	}
	clist, err = GetApplicationComponentsFromStorage(m.ClientBuilder, app)
	if err != nil {
		return
	}
	uapp, err := ApplicationToUnstructured(app)
	if err != nil {
		return
	}
	clist = append(RemoveDuplicateAPPResources(clist), *uapp)
	sort.Slice(clist, func(i, j int) bool {
		return clist[i].GetKind() < clist[j].GetKind() ||
			(clist[i].GetKind() == clist[j].GetKind() &&
				clist[i].GetName() < clist[j].GetName())
	})
	return
}

func (m *defaultLifecycleManager) ValidateCreate(app *v1beta1.Application) error {
	errs := field.ErrorList{}
	if len(app.GetName())+len(app.GetNamespace()) > ApplicationNameAndNamespaceMaxLength {
		errs = append(errs,
			field.TooLong(
				field.NewPath("metadata", "name+namespace"),
				fmt.Sprintf("%s.%s", app.GetName(), app.GetNamespace()),
				ApplicationNameAndNamespaceMaxLength))
	}
	if len(errs) > 0 {
		return errors.NewInvalid(
			schema.GroupKind{
				Group: "app.k8s.io",
				Kind:  "Application",
			},
			app.GetName(),
			errs)
	}
	return nil
}

func (m *defaultLifecycleManager) Create(app *v1beta1.Application, option CreateOptions) (appCreated *v1beta1.Application, err error) {
	err = m.ValidateCreate(app)
	if err != nil {
		return
	}

	client, err := m.Client(app.Namespace)
	if err != nil {
		return
	}
	domain := m.LabelBaseDomain
	appUID := NewUUID()
	SetupApplicationFields(domain, app, appUID, nil)

	err = computeComponentKinds(app, domain, false, PrepareComponentTemplate)
	if err != nil {
		return
	}
	klog.Infof("Resolved application component kinds: %v", app.Spec.ComponentGroupKinds)
	compTemplates := app.Spec.ComponentTemplates
	app.Spec.ComponentTemplates = nil
	obj, err := ApplicationToUnstructured(app)
	if err != nil {
		return
	}
	appObjCreated, err := client.Create(obj, metav1.CreateOptions{})
	if err != nil {
		return
	}
	unstructured.SetNestedField(appObjCreated.Object,
		map[string]interface{}{"state": string(v1beta1.WLPending)}, "status")

	// Create application components from component templates after the application object is created.
	klog.Infof("Creating application components for %s/%s.", appObjCreated.GetNamespace(), appObjCreated.GetName())
	err = UnstructuredSetApplicationComponentsTemplate(appObjCreated, compTemplates)
	if err != nil {
		return
	}

	err = CreateApplicationComponents(m.ClientBuilder, appObjCreated, compTemplates)
	if err != nil {
		// Failed
		klog.Errorf("Create application components for %s/%s error: %v",
			appObjCreated.GetNamespace(), appObjCreated.GetName(), err)
		err = m.handleErrorOnComponents(client, appObjCreated, "Create components failed")
		return
	}

	klog.Infof("Create application components for %s/%s succeeded.", appObjCreated.GetNamespace(), appObjCreated.GetName())
	errPhaseUpdate := updateApplicationPhase(client, appObjCreated, v1beta1.Succeeded)
	if errPhaseUpdate != nil {
		err = errPhaseUpdate
		return
	}

	appCreated, err = UnstructuredToApplication(appObjCreated)
	if err != nil {
		return
	}
	appCreated.Spec.ComponentTemplates = compTemplates
	_, err = m.NewVersion(nil, appCreated, NewVersionOptions{
		User: k8s.CreatorOfObject(domain, appObjCreated),
	})
	if err != nil {
		updateConditionWithError(client, appObjCreated, "Create application his version failed", err)
		return
	}

	updateConditionWithReady(client, appObjCreated, "Create application succeeded", "Every component is created")
	return
}

func (m *defaultLifecycleManager) ValidateUpdate(old, new *v1beta1.Application) error {
	errs := field.ErrorList{}
	if new.GetName() != old.GetName() {
		errs = append(errs,
			field.Forbidden(field.NewPath("metadata", "name"), "name can't be modified"),
		)
	}
	if new.GetNamespace() != old.GetNamespace() {
		errs = append(errs,
			field.Forbidden(field.NewPath("metadata", "namespace"), "namespace can't be modified"),
		)
	}
	if len(errs) > 0 {
		return errors.NewInvalid(
			schema.GroupKind{
				Group: "app.k8s.io",
				Kind:  "Application",
			},
			old.GetName(),
			errs)
	}
	return nil
}

func (m *defaultLifecycleManager) Update(app *v1beta1.Application, option UpdateOptions) (appUpdated *v1beta1.Application, err error) {
	client, err := m.Client(app.Namespace)
	if err != nil {
		return
	}

	old, err := client.Get(app.Name, metav1.GetOptions{})
	if err != nil {
		return
	}

	oldApp, err := UnstructuredToApplication(old)
	if err != nil {
		return
	}

	err = m.ValidateUpdate(oldApp, app)
	if err != nil {
		return
	}

	appUID := string(oldApp.GetUID())
	domain := m.LabelBaseDomain

	app.ResourceVersion = oldApp.ResourceVersion
	SetupApplicationFields(domain, app, appUID, &oldApp.Status)

	computeComponentKinds(app, domain, option.ForceUpdatePod, PrepareComponentTemplate)
	klog.V(1).Infof("Resolved application component kinds: %v", app.Spec.ComponentGroupKinds)
	oldComponents, err := GetApplicationComponentsFromStorage(m.ClientBuilder, oldApp)
	if err != nil {
		return
	}

	diffs, err := k8s.ComputeResourceDiffs(
		oldComponents,
		app.Spec.ComponentTemplates,
		k8s.ObjectKeyWithNamespace)
	if err != nil {
		return
	}

	klog.V(6).Infof("Resource diffs on update application: %+v", diffs)
	components := app.Spec.ComponentTemplates
	app.Spec.ComponentTemplates = nil
	SetAppAnnotation(
		app,
		AppHistoryRevisionKey(domain),
		fmt.Sprintf("%d", AppHistoryRevision(domain, oldApp)))
	obj, err := ApplicationToUnstructured(app)
	if err != nil {
		return
	}

	appUpdatedObj, err := client.Update(obj, metav1.UpdateOptions{})
	if err != nil {
		return
	}

	var componentTemplates []interface{}
	for _, c := range components {
		componentTemplates = append(componentTemplates, c.Object)
	}
	unstructured.SetNestedSlice(appUpdatedObj.Object, componentTemplates, "spec", "componentTemplates")
	err = updateApplicationComponentsDiff(m.ClientBuilder, domain, appUpdatedObj, diffs)
	if err != nil {
		// Failed
		errPhaseUpdate := updateApplicationPhase(client, appUpdatedObj, v1beta1.Failed)
		if errPhaseUpdate != nil {
			err = errPhaseUpdate
			return
		}
		updateConditionWithError(client, appUpdatedObj, "Update components failed", err)
		return
	}

	// Succeeded
	errPhaseUpdate := updateApplicationPhase(client, appUpdatedObj, v1beta1.Succeeded)
	if errPhaseUpdate != nil {
		err = errPhaseUpdate
		return
	}
	appUpdated, err = UnstructuredToApplication(appUpdatedObj)
	if err != nil {
		return
	}
	appUpdated.Spec.ComponentTemplates = components

	user := option.User
	if user == "" {
		// Use user from creator as default.
		user = k8s.CreatorOfObject(domain, appUpdatedObj)
	}
	_, err = m.NewVersion(oldApp, appUpdated, NewVersionOptions{
		User:         user,
		RollbackFrom: option.RollbackFrom,
	})
	if err != nil {
		updateConditionWithError(client, appUpdatedObj, "Create application his failed", err)
		return
	}
	updateConditionWithReady(client, appUpdatedObj, "Update application succeeded", "Every component is updated")
	return
}

func (m *defaultLifecycleManager) Delete(namespace, name string, option DeleteOptions) (err error) {
	client, err := m.Client(namespace)
	if err != nil {
		return
	}

	return client.Delete(name, &metav1.DeleteOptions{})
}

func (m *defaultLifecycleManager) Start(namespace, name string, option RunOptions) (err error) {
	return m.startOrStopEachComponent(namespace, name, true)
}

func (m *defaultLifecycleManager) Stop(namespace, name string, option RunOptions) error {
	return m.startOrStopEachComponent(namespace, name, false)
}

func (m *defaultLifecycleManager) startOrStopEachComponent(namespace, name string, isStart bool) (err error) {
	app, err := m.Get(namespace, name, GetOptions{})
	if err != nil {
		return
	}
	components, err := GetApplicationComponentsFromStorage(m.ClientBuilder, app)
	if err != nil {
		return
	}
	domain := m.LabelBaseDomain
	for _, c := range components {
		if k8s.IsPodController(c.GetKind()) && c.GetKind() != "DaemonSet" {
			var ri dynamic.ResourceInterface
			ri, err = m.ClientBuilder.ClientFromResourceKind(c.GetKind(), c.GetNamespace())
			if err != nil {
				return
			}
			targetReplicas := 0
			if isStart {
				targetReplicas = LastReplicas(domain, &c)
				if targetReplicas == 0 {
					// Last replicas is 0, use 1 as default.
					targetReplicas = 1
				}
			} else if Replicas(&c) == 0 {
				// When stopping the workload with 0 replicas, skip it to
				// avoid the last replicas is updated to 0.
				continue
			}
			_, err = ri.Patch(
				c.GetName(), types.MergePatchType,
				WorkloadReplicasPatchJSON(domain, &c, targetReplicas),
				metav1.PatchOptions{})
			if err != nil {
				return
			}
		}
	}
	return
}

func (m *defaultLifecycleManager) NewVersion(old, new *v1beta1.Application, option NewVersionOptions) (history *his.ApplicationHistory, err error) {
	domain := m.LabelBaseDomain
	ahs := NewHistoryStorage(HistoryStorageConfig{LabelBaseDomain: domain}, m.ClientBuilder)
	h, err := ahs.CreateApplicationHistory(&HistoryResource{
		AppUID:            new.GetUID(),
		OldApp:            old,
		NewApp:            new,
		CreationTimestamp: time.Now(),
		User:              option.User,
		ChangeCause:       ChangeCause(domain, new),
		RollbackFrom:      option.RollbackFrom,
	})
	if err != nil {
		return
	}
	history = &his.ApplicationHistory{}
	err = k8s.UnstructuredToObject(h, history)
	if err != nil {
		return
	}

	revision := ah.AppHistoryRevision(h)
	client, err := m.Client(new.Namespace)
	if err != nil {
		return
	}

	newUnObj, err := ApplicationToUnstructured(new)
	if err != nil {
		return
	}
	updateApplicationRevision(client, newUnObj, domain, revision)
	return
}

func (m *defaultLifecycleManager) GetVersion(namespace, name string, version int) (app *v1beta1.Application, err error) {
	ahs := NewHistoryStorage(HistoryStorageConfig{LabelBaseDomain: m.LabelBaseDomain}, m.ClientBuilder)
	history, err := ahs.GetAppHistoryForRevision(namespace,
		AppNameSelectorString(m.LabelBaseDomain, name, namespace), version)
	if err != nil {
		return
	}
	return m.ApplicationFromVersion(history)
}

func (m *defaultLifecycleManager) Versions(namespace, name string) (list *his.ApplicationHistoryList, err error) {
	c, err := m.ClientBuilder.ClientFromResourceKind("ApplicationHistory", namespace)
	if err != nil {
		return
	}
	ulist, err := c.List(metav1.ListOptions{
		LabelSelector: AppNameSelectorString(m.LabelBaseDomain, name, namespace),
	})
	if err != nil {
		return
	}
	list = &his.ApplicationHistoryList{}
	list.Kind = "ApplicationHistory"
	list.APIVersion = "app.k8s.io/v1beta1"
	for _, uh := range ulist.Items {
		h, errI := ah.UnstructuredToApplicationHistory(&uh)
		if errI != nil {
			err = errI
			return
		}
		list.Items = append(list.Items, *h)
	}
	return
}

func (m *defaultLifecycleManager) ApplicationFromVersion(version *his.ApplicationHistory) (app *v1beta1.Application, err error) {
	var objects []unstructured.Unstructured
	err = yaml.Unmarshal([]byte(version.Spec.YAML), &objects)
	if err != nil {
		return nil, err
	}

	var components []unstructured.Unstructured
	for _, o := range objects {
		if o.GetKind() == "Application" {
			app, err = UnstructuredToApplication(&o)
			if err != nil {
				return nil, err
			}
		} else {
			components = append(components, o)
		}
	}

	if app == nil {
		return nil, fmt.Errorf("application not found in his version")
	}

	app.Spec.ComponentTemplates = components
	return
}

func (m *defaultLifecycleManager) RollbackToVersion(namespace, name string, version int, option RollbackOptions) (err error) {
	client, err := m.Client(namespace)
	if err != nil {
		return
	}

	old, err := client.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	domain := m.LabelBaseDomain
	newApp, err := m.GetVersion(namespace, name, version)
	if err != nil {
		return
	}
	newApp.ResourceVersion = old.GetResourceVersion()
	newApp.UID = old.GetUID()
	k8s.AddAnnotations(newApp,
		map[string]string{
			ChangeCauseKey(domain): fmt.Sprintf("Rollback from revision %d", version),
		})
	_, err = m.Update(newApp, UpdateOptions{
		User:         option.User,
		RollbackFrom: &version,
	})
	return
}

func (m *defaultLifecycleManager) handleErrorOnComponents(
	client dynamic.ResourceInterface, app *unstructured.Unstructured, reason string) (err error) {
	err = updateApplicationPhase(client, app, v1beta1.Failed)
	updateConditionWithError(client, app, reason, err)
	return
}

func (m *defaultLifecycleManager) ChartPackage(namespace, name string, cp *v1beta1.ApplicationChartPackage) (pkg []byte, err error) {
	var history *his.ApplicationHistory
	if cp.Spec.Revision == nil {
		// Create chart package from current application state. A new application snapshot will be created
		// first before creating the chart package.
		history, err = m.Snapshot(namespace, name)
		if err != nil {
			return
		}
		cp.Spec.Revision = &history.Spec.Revision
	} else {
		ahs := NewHistoryStorage(HistoryStorageConfig{LabelBaseDomain: m.LabelBaseDomain}, m.ClientBuilder)
		history, err = ahs.GetAppHistoryForRevision(namespace,
			AppNameSelectorString(m.LabelBaseDomain, name, namespace), *cp.Spec.Revision)
		if err != nil {
			return
		}
	}

	pkg, err = ArchiveChartPackage(cp, history)
	return
}

func (m *defaultLifecycleManager) Snapshot(namespace, name string) (history *his.ApplicationHistory, err error) {
	app, err := m.Get(namespace, name, GetOptions{})
	if err != nil {
		return
	}
	components, err := GetApplicationComponentsFromStorage(m.ClientBuilder, app)
	if err != nil {
		return
	}
	app.Spec.ComponentTemplates = components
	ahs := NewHistoryStorage(HistoryStorageConfig{LabelBaseDomain: m.LabelBaseDomain}, m.ClientBuilder)
	lastApp, err := ahs.ApplicationFromRevision(namespace,
		AppNameSelectorString(m.LabelBaseDomain, app.Name, app.Namespace),
		AppHistoryRevision(m.LabelBaseDomain, app),
	)
	// If the application history is not found, ignore the error
	if err != nil && !errors.IsNotFound(err) {
		return
	}
	history, err = m.NewVersion(lastApp, app, NewVersionOptions{User: k8s.CreatorOfObject(m.LabelBaseDomain, app)})
	return
}
