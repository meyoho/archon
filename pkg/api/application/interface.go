package application

import (
	"alauda.io/archon/pkg/api/application/v1beta1"
	history "alauda.io/archon/pkg/api/applicationhistory/v1beta1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type GetOptions struct{}

type ListOptions struct {
	metav1.ListOptions
}

type Lister interface {
	Get(namespace, name string, option GetOptions) (*v1beta1.Application, error)
	List(namespace string, option ListOptions) (*v1beta1.ApplicationList, error)
	Components(namespace, name string) ([]unstructured.Unstructured, error)
}

type CreateOptions struct{}

type Creator interface {
	ValidateCreate(application *v1beta1.Application) error
	Create(application *v1beta1.Application, option CreateOptions) (*v1beta1.Application, error)
}

type UpdateOptions struct {
	User           string
	RollbackFrom   *int
	ForceUpdatePod bool
}

type Updater interface {
	ValidateUpdate(old, new *v1beta1.Application) error
	Update(application *v1beta1.Application, option UpdateOptions) (*v1beta1.Application, error)
}

type DeleteOptions struct{}

type Deleter interface {
	Delete(namespace, name string, option DeleteOptions) error
}

type RunOptions struct{}

type Runner interface {
	Start(namespace, name string, option RunOptions) error
	Stop(namespace, name string, option RunOptions) error
}

type NewVersionOptions struct {
	User         string
	RollbackFrom *int
}

type RollbackOptions struct {
	User string
}

type Snapshoter interface {
	NewVersion(old, new *v1beta1.Application, option NewVersionOptions) (*history.ApplicationHistory, error)
	Snapshot(namespace, name string) (*history.ApplicationHistory, error)
	GetVersion(namespace, name string, version int) (*v1beta1.Application, error)
	Versions(namespace, name string) (*history.ApplicationHistoryList, error)
	ApplicationFromVersion(version *history.ApplicationHistory) (*v1beta1.Application, error)
	RollbackToVersion(namespace, name string, version int, option RollbackOptions) error
}

type Exporter interface {
	ChartPackage(namespace, name string, cp *v1beta1.ApplicationChartPackage) ([]byte, error)
}

type LifecycleManager interface {
	Lister
	Creator
	Updater
	Deleter
	Runner

	Snapshoter

	Exporter
}
