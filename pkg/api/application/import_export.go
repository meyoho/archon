package application

import (
	"net/http"
	"reflect"

	"alauda.io/archon/pkg/api/application/v1beta1"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/klog"
)

// ImportResource imports resources to an application. After imported, the
// resources will be added with the application name selector and owner
// reference. For pod controllers, the pod template labels will be updated
// either.
// The component kinds of the application will be updated if changed.
func (a *REST) ImportResource(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	importResource := &v1beta1.ApplicationImport{}
	statusCode = http.StatusNoContent
	err = req.ReadEntity(importResource)
	if err != nil {
		return
	}

	if len(importResource.Spec.Components) == 0 {
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	klog.V(1).Infof("Import resources to application %s:%s", namespace, name)
	ri, cb, err := a.ClientToolsFromRequest(req, namespace)
	if err != nil {
		return
	}

	appObj, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	app, err := UnstructuredToApplication(appObj)
	if err != nil {
		return
	}

	newCK, err := a.importResources(appObj, app.Spec.ComponentGroupKinds, importResource.Spec.Components, cb)
	if err != nil {
		return
	}

	err = a.updateComponentKinds(newCK, app, ri)
	if err != nil {
		return
	}

	setApplicationReadyCondition(ri, appObj, "Import Resources", "Import resources success.")
	return
}

func (a *REST) importResources(
	app *unstructured.Unstructured,
	ck []metav1.GroupKind,
	components []v1beta1.ApplicationTransportComponent,
	client k8s.ClientBuilderInterface) ([]metav1.GroupKind, error) {
	appObj, err := UnstructuredToApplication(app)
	if err != nil {
		return nil, err
	}
	for _, c := range components {
		klog.V(1).Infof("Start to import resource %q", &c)
		gvk := c.GroupVersionKind()
		cri, err := client.NamespacedDynamicClientByGroupKind(
			metav1.GroupKind{
				Group: gvk.Group,
				Kind:  gvk.Kind,
			}, c.Namespace)
		if err != nil {
			return nil, err
		}
		var o *unstructured.Unstructured
		o, err = cri.Get(c.Name, metav1.GetOptions{})
		if err != nil {
			return nil, err
		}
		k8s.AddOwnerReference(o, app)
		appLabels := appObj.Spec.Selector.MatchLabels
		if len(appLabels) == 0 {
			appLabels = map[string]string{
				AppNameKey(a.config.LabelBaseDomain): AppSelectorName(app.GetName(), app.GetNamespace()),
			}
		}
		k8s.AddLabels(o, appLabels)
		if k8s.IsPodController(o.GetKind()) {
			err := AddPodControllerAppLabels(o, appLabels)
			if err != nil {
				klog.V(2).Infof("AddPodControllerAppLabels resource with %v", err)
			}
		}
		klog.V(10).Infof("Update resource %q with %+v", &c, o)
		_, err = cri.Update(o, metav1.UpdateOptions{})
		if err != nil {
			return nil, err
		}

		ck = AddApplicationComponentKind(ck, metav1.GroupKind{
			Group: gvk.Group,
			Kind:  gvk.Kind,
		})
	}
	return ck, nil
}

// ExportResource exports resources from an application. After exported, the
// application name selector and owner reference will be removed from the
// resources. For pod controllers, the pod template labels will be updated
// either.
// The component kinds of the application will be updated if changed.
func (a *REST) ExportResource(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	exportResource := &v1beta1.ApplicationExport{}
	statusCode = http.StatusNoContent
	err = req.ReadEntity(exportResource)
	if err != nil {
		return
	}

	if len(exportResource.Spec.Components) == 0 {
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	klog.V(1).Infof("Export resources from application %s:%s", namespace, name)
	ri, client, err := a.ClientToolsFromRequest(req, namespace)
	if err != nil {
		return
	}

	appObj, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	app, err := UnstructuredToApplication(appObj)
	if err != nil {
		return
	}

	newCK, err := a.exportResources(appObj, app.Spec.ComponentGroupKinds, exportResource.Spec.Components, client)
	if err != nil {
		return
	}

	err = a.updateComponentKinds(newCK, app, ri)
	if err != nil {
		return
	}

	setApplicationReadyCondition(ri, appObj, "Export Resources", "Export resources success.")
	return
}

func (a *REST) exportResources(
	app *unstructured.Unstructured,
	ck []metav1.GroupKind,
	components []v1beta1.ApplicationTransportComponent,
	client k8s.ClientBuilderInterface) ([]metav1.GroupKind, error) {
	appObj, err := UnstructuredToApplication(app)
	if err != nil {
		return nil, err
	}
	changedCK, err := a.exportEachComponent(components, client, app, appObj)
	if err != nil {
		return nil, err
	}

	v1beta1app, err := UnstructuredToApplication(app)
	if err != nil {
		return nil, err
	}

	for gk := range changedCK {
		res, err := getAppComponentsForGroupKind(gk, v1beta1app, client)
		if err != nil {
			return nil, err
		}
		if len(res) == 0 {
			ck = RemoveApplicationComponentKind(ck, gk)
		}
	}
	return ck, nil
}

func (a *REST) exportEachComponent(
	components []v1beta1.ApplicationTransportComponent,
	client k8s.ClientBuilderInterface,
	app *unstructured.Unstructured,
	appObj *v1beta1.Application) (changedCK map[metav1.GroupKind]bool, err error) {
	changedCK = make(map[metav1.GroupKind]bool)
	for _, c := range components {
		klog.V(1).Infof("Start to export resource %q", &c)
		var cri dynamic.ResourceInterface
		gvk := c.GroupVersionKind()
		cri, err = client.NamespacedDynamicClientByGroupKind(
			metav1.GroupKind{
				Group: gvk.Group,
				Kind:  gvk.Kind,
			}, c.Namespace)
		if err != nil {
			return
		}
		var o *unstructured.Unstructured
		o, err = cri.Get(c.Name, metav1.GetOptions{})
		if err != nil {
			if errors.IsNotFound(err) {
				klog.Warningf("Resource %q not found", &c)
				continue
			}
			return
		}
		k8s.RemoveOwnerReference(o, app)
		appLabels := appObj.Spec.Selector.MatchLabels
		if len(appLabels) == 0 {
			appLabels = map[string]string{
				AppNameKey(a.config.LabelBaseDomain): AppSelectorName(app.GetName(), app.GetNamespace()),
			}
		}
		k8s.RemoveLabels(o, appLabels)
		if k8s.IsPodController(o.GetKind()) {
			err := RemovePodControllerAppLabels(o, appLabels)
			if err != nil {
				klog.V(2).Infof("RemovePodControllerAppLabels resource with %v", err)
			}
		}
		klog.V(10).Infof("Update resource %q with %+v", &c, o)
		_, err = cri.Update(o, metav1.UpdateOptions{})
		if err != nil {
			return
		}

		changedCK[metav1.GroupKind{
			Group: gvk.Group,
			Kind:  gvk.Kind,
		}] = true
	}
	return
}

func (a *REST) updateComponentKinds(
	newCK []metav1.GroupKind,
	app *v1beta1.Application,
	ri dynamic.ResourceInterface) error {
	if !reflect.DeepEqual(app.Spec.ComponentGroupKinds, newCK) {
		// Component kinds changed, to update the application
		klog.V(1).Infof("Component kinds changes to %v, update the application", newCK)
		app.Spec.ComponentGroupKinds = newCK
		_, err := ri.Patch(app.GetName(), types.MergePatchType, AppComponentKindsPatchJSON(newCK), metav1.PatchOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}
