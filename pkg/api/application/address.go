package application

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	albutil "alauda.io/archon/pkg/api/alauda/util"
	alb2v1 "alauda.io/archon/pkg/api/alauda/v1"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"
	"alauda.io/archon/pkg/util"

	"github.com/emicklei/go-restful"
	extv1beta1 "k8s.io/api/extensions/v1beta1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/klog"
)

const (
	featureType              = "ingress-controller"
	featureInstanceTypeALB   = "alb2"
	featureInstanceTypeNginx = "nginx-ingress-controller"
)

// AddressResource describes the addresses of workloads in application.
type AddressResource struct {
	Workloads map[string]map[string][]ServiceAddr `json:"workloads"`
}

// ServiceAddr describes the kubernetes service address.
type ServiceAddr struct {
	Protocol              string `json:"protocol"`
	Host                  string `json:"host"`
	URL                   string `json:"url"`
	Port                  int    `json:"port"`
	IngressControllerName string `json:"ingress_controller_name"`
	IngressControllerType string `json:"ingress_controller_type"`
}

// HandleGetApplicationAddress handles the GET application address request.
func (a *REST) HandleGetApplicationAddress(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	applicationAddressRes := AddressResource{Workloads: make(map[string]map[string][]ServiceAddr)}
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	ri, err := a.ClientFromRequest(req, namespace)
	if err != nil {
		return
	}
	client := req.Attribute(rest.ClientAttribute).(k8s.ClientBuilderInterface)

	appUnstruct, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	app, _ := UnstructuredToApplication(appUnstruct)
	components, err := GetApplicationComponentsFromStorage(client, app)
	if err != nil {
		return
	}
	hasPodController := false
	for _, c := range components {
		if k8s.IsPodController(c.GetKind()) {
			hasPodController = true
			break
		}
	}
	if !hasPodController {
		klog.Warningf("application: %v has no pod controller", app.Name)
		return
	}
	ingressControllers, err := getIngressControllers(client)
	if err != nil {
		// tolerate this error and don't return
		klog.Warningf("%v", err)
		err = nil
		return
	}
	hasIngressNginx := false
	for _, ic := range ingressControllers {
		if ic.Type == featureInstanceTypeNginx {
			hasIngressNginx = true
		}
	}
	var ingressList *unstructured.UnstructuredList
	if hasIngressNginx {
		ic, err1 := client.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
			Group: extv1beta1.GroupName,
			Kind:  "Ingress",
		}, namespace)
		if err1 != nil {
			klog.Warningf("%v", err1)
			return
		}
		ingressList, err1 = ic.List(metav1.ListOptions{})
		if err1 != nil {
			klog.Warningf("%v", err1)
			return
		}
	}
	klog.V(3).Infof("region %s ingress controllers %+v", req.PathParameter("cluster"), ingressControllers)
	alb2s := make(map[string]*AlaudaLoadBalancer)
	for _, ic := range ingressControllers {
		if ic.Type == featureInstanceTypeALB {
			alb2Name := strings.TrimSuffix(ic.Name, fmt.Sprintf("-%s", a.config.AlaudaNamespace))
			alb2, err := loadALBByName(client, a.config.AlaudaNamespace, alb2Name, a.config.LabelBaseDomain)
			klog.V(3).Infof("load alb2: %s", alb2Name)
			// ignore err
			if err != nil {
				// tolerate this error and don't return
				klog.Warningf("%v", err)
				err = nil
				continue
			}
			alb2s[alb2Name] = alb2
		}
	}

	var servicesList *unstructured.UnstructuredList
	sc, err1 := client.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: "",
		Kind:  "Service",
	}, namespace)
	if err1 != nil {
		klog.Warningf("%v", err)
		return
	}
	servicesList, err1 = sc.List(metav1.ListOptions{})
	if err1 != nil {
		klog.Warningf("%v", err)
		return
	}

	for _, c := range components {
		if k8s.IsPodController(c.GetKind()) {
			k := fmt.Sprintf("%s-%s", c.GetKind(), c.GetName())
			workload := make(map[string][]ServiceAddr)
			services, err := getServicesByWorkload(servicesList, &c)
			if err != nil {
				// tolerate this error and don't return
				klog.Warningf("%v", err)
				err = nil
				continue
			}
			klog.V(3).Infof("workload %s services %+v", k, services)
			for _, svc := range services {
				sa := []ServiceAddr{}
				for _, ic := range ingressControllers {
					switch ic.Type {
					case featureInstanceTypeNginx:
						ingresses, err := getIngressesByService(ingressList, &svc)
						if err != nil {
							// tolerate this error and don't return
							klog.Warningf("%v", err)
							err = nil
							continue
						}
						klog.V(5).Infof("extract svc: %s address from ingresses", svc.GetNamespace())
						addresses := extractAddressFromIngresses(ic, ingresses, &svc)
						if len(addresses) > 0 {
							sa = append(sa, addresses...)
						}
					case featureInstanceTypeALB:
						alb2Name := strings.TrimSuffix(ic.Name, fmt.Sprintf("-%s", a.config.AlaudaNamespace))
						// if not set alb2s[alb2Name],ignore it
						if _, ok := alb2s[alb2Name]; !ok {
							klog.Warningf("alb2s[%v] is not set", alb2Name)
							continue
						}
						klog.V(5).Infof("extract svc: %s address from alb: %s", svc.GetNamespace(), alb2Name)
						addresses := extractAddressFromALB2(ic, alb2s[alb2Name], &svc)
						if len(addresses) > 0 {
							sa = append(sa, addresses...)
						}
					default:
						klog.Warningf("unkonwn instance type")
						continue
					}
				}
				workload[svc.GetName()] = sa
				klog.V(5).Infof("service %s address %+v", svc.GetName(), workload[svc.GetName()])
			}
			klog.V(3).Infof("workload %s addresses %+v", k, workload)
			applicationAddressRes.Workloads[k] = workload
		}
	}
	res.WriteAsJson(applicationAddressRes)
	return
}

func getServicesByWorkload(svcListRaw *unstructured.UnstructuredList, obj *unstructured.Unstructured) ([]unstructured.Unstructured, error) {
	wlLabelsObj, ok, err := unstructured.NestedMap(obj.Object, "spec", "template", "metadata", "labels")
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, nil
	}
	wlLabels := util.MapStringObjToStringString(wlLabelsObj)
	svcs := []unstructured.Unstructured{}
	for _, svcRaw := range svcListRaw.Items {
		svcSpec := svcRaw.Object["spec"].(map[string]interface{})
		if svcSpec["selector"] == nil {
			continue
		}
		svcLabelsObj := svcSpec["selector"].(map[string]interface{})
		svcLabels := util.MapStringObjToStringString(svcLabelsObj)
		if IsLabelMatch(svcLabels, wlLabels) {
			svcs = append(svcs, svcRaw)
		}
	}
	return svcs, err
}

func getIngressesByService(ingressList *unstructured.UnstructuredList, obj *unstructured.Unstructured) ([]extv1beta1.Ingress, error) {
	ingresses := []extv1beta1.Ingress{}
	for _, ingressRaw := range ingressList.Items {
		ingress, err := UnstructuredToIngress(&ingressRaw)
		if err != nil {
			return nil, err
		}
		if ingress.Spec.Backend != nil && ingress.Spec.Backend.ServiceName == obj.GetName() {
			ingresses = append(ingresses, *ingress)
			continue
		}
	LOOP:
		for _, rule := range ingress.Spec.Rules {
			if rule.IngressRuleValue.HTTP != nil {
				for _, path := range rule.IngressRuleValue.HTTP.Paths {
					if path.Backend.ServiceName == obj.GetName() {
						ingresses = append(ingresses, *ingress)
						break LOOP
					}
				}
			}
		}
	}
	return ingresses, nil
}

func extractAddressFromIngresses(ic *IngressController, ingresses []extv1beta1.Ingress, service *unstructured.Unstructured) []ServiceAddr {
	serviceAddrs := []ServiceAddr{}
	serviceName := service.GetName()
	for _, ingress := range ingresses {
		if ingress.Spec.Backend != nil && ingress.Spec.Backend.ServiceName == serviceName {
			serviceAddrs = append(serviceAddrs, ServiceAddr{
				Protocol: "http",
				Host:     ic.Host,
				URL:      "",
				Port:     80,
			})
		}
		httpsDomains := make(map[string]bool)
		for _, tls := range ingress.Spec.TLS {
			for _, host := range tls.Hosts {
				httpsDomains[host] = true
			}
		}
		for _, rule := range ingress.Spec.Rules {
			var protocol string
			var port int
			if httpsDomains[rule.Host] {
				protocol = "https"
				port = 443
			} else {
				protocol = "http"
				port = 80
			}
			if rule.IngressRuleValue.HTTP != nil {
				for _, path := range rule.IngressRuleValue.HTTP.Paths {
					if path.Backend.ServiceName == serviceName {
						host := ic.Host
						if rule.Host != "" {
							host = rule.Host
						}
						serviceAddrs = append(serviceAddrs, ServiceAddr{
							Protocol:              protocol,
							Host:                  host,
							URL:                   path.Path,
							Port:                  port,
							IngressControllerName: ic.Name,
							IngressControllerType: ic.Type,
						})
					}
				}
			}
		}
	}
	return serviceAddrs
}

func extractAddressFromALB2(ic *IngressController, alb2 *AlaudaLoadBalancer, service *unstructured.Unstructured) []ServiceAddr {
	serviceAddrs := []ServiceAddr{}
	serviceName := service.GetName()
	serviceNamespace := service.GetNamespace()
	for _, ft := range alb2.Frontends {
		if ft.ServiceGroup != nil {
			for _, svc := range ft.ServiceGroup.Services {
				if serviceName == svc.Name && serviceNamespace == svc.Namespace {
					serviceAddrs = append(serviceAddrs, ServiceAddr{
						Protocol:              ft.Protocol,
						Port:                  ft.Port,
						Host:                  ic.Host,
						IngressControllerName: ic.Name,
						IngressControllerType: ic.Type,
					})
				}
			}
		}
		for _, rl := range ft.Rules {
			if rl.ServiceGroup != nil {
				for _, svc := range rl.ServiceGroup.Services {
					if serviceName == svc.Name && serviceNamespace == svc.Namespace {
						tempHost := ic.Host
						if rl.Domain != "" {
							tempHost = rl.Domain
						}
						serviceAddrs = append(serviceAddrs, ServiceAddr{
							Protocol:              ft.Protocol,
							Port:                  ft.Port,
							Host:                  tempHost,
							URL:                   rl.URL,
							IngressControllerName: ic.Name,
							IngressControllerType: ic.Type,
						})
					}
				}
			}
		}
	}

	return serviceAddrs
}

// IngressController stores a ingress-controller info
type IngressController struct {
	Type string
	Host string
	Name string
}

func getIngressControllers(client k8s.ClientBuilderInterface) ([]*IngressController, error) {
	// http://confluence.alaudatech.com/pages/viewpage.action?pageId=42500276
	// store ingress-controller address in features crd
	fc, err := client.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: "infrastructure.alauda.io",
		Kind:  "Feature",
	}, "")
	if err != nil {
		return nil, err
	}
	ingressControllers := []*IngressController{}
	// kubectl get features -l 'type=ingress-controller'
	objList, err := fc.List(metav1.ListOptions{LabelSelector: fmt.Sprintf("type=%s", featureType)})
	if err != nil {
		return nil, err
	}
	for _, item := range objList.Items {
		cf, err := getIngressControllerFeature(&item)
		if err != nil {
			return nil, err
		}
		ingressControllers = append(ingressControllers, cf)
	}
	if len(ingressControllers) == 0 {
		return nil, errors.New("missing ingress controller feature")
	}

	return ingressControllers, nil
}

func getIngressControllerFeature(obj *unstructured.Unstructured) (*IngressController, error) {
	ic := &IngressController{
		Name: obj.GetName(),
	}
	spec := obj.Object["spec"].(map[string]interface{})
	if spec["accessInfo"] == nil {
		return nil, errors.New("no accessInfo")
	}
	infoObj := spec["accessInfo"].(map[string]interface{})
	info := util.MapStringObjToStringString(infoObj)
	ic.Host = info["host"]
	labels := obj.GetLabels()
	ic.Type = labels["instanceType"]
	return ic, nil
}

// AlaudaLoadBalancer represents an alb instance
type AlaudaLoadBalancer struct {
	Spec      alb2v1.ALB2Spec
	Name      string
	Namespace string
	Frontends []*Frontend
}

// Frontend represents alb frontend
type Frontend struct {
	Name string
	alb2v1.FrontendSpec
	Rules []*Rule
}

// Rule represents alb rule
type Rule struct {
	alb2v1.RuleSpec
	Name string
}

func loadALBByName(client k8s.ClientBuilderInterface, namespace, name, labelBaseDomain string) (*AlaudaLoadBalancer, error) {
	alb2 := AlaudaLoadBalancer{
		Name:      name,
		Namespace: namespace,
		Frontends: []*Frontend{},
	}
	ac, err := client.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: "crd.alauda.io",
		Kind:  "ALB2",
	}, namespace)
	if err != nil {
		return nil, err
	}
	alb2obj, err := ac.Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	alb, err := albutil.UnstructuredToALB2(alb2obj)
	if err != nil {
		return nil, err
	}
	alb2.Spec = alb.Spec
	selector := fmt.Sprintf("%s=%s", fmt.Sprintf("alb2.%s/name", labelBaseDomain), name)
	fc, err := client.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: "crd.alauda.io",
		Kind:  "Frontend",
	}, namespace)
	if err != nil {
		return nil, err
	}
	ftobjs, err := fc.List(metav1.ListOptions{LabelSelector: selector})
	if err != nil {
		return nil, err
	}
	for _, ftobj := range ftobjs.Items {
		ftres, err := albutil.UnstructuredToFrontend(&ftobj)
		if err != nil {
			return nil, err
		}
		ft := &Frontend{
			Name:         ftobj.GetName(),
			FrontendSpec: ftres.Spec,
			Rules:        []*Rule{},
		}
		rc, err := client.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
			Group: "crd.alauda.io",
			Kind:  "Rule",
		}, namespace)
		if err != nil {
			return nil, err
		}
		selector := fmt.Sprintf(
			"%s=%s,%s=%s",
			fmt.Sprintf("alb2.%s/name", labelBaseDomain), name,
			fmt.Sprintf("alb2.%s/frontend", labelBaseDomain), ft.Name,
		)
		rlobjs, err := rc.List(metav1.ListOptions{LabelSelector: selector})
		if err != nil {
			return nil, err
		}
		for _, rlobj := range rlobjs.Items {
			rlres, err := albutil.UnstructuredToRule(&rlobj)
			if err != nil {
				return nil, err
			}
			rule := &Rule{
				RuleSpec: rlres.Spec,
				Name:     rlres.Name,
			}
			ft.Rules = append(ft.Rules, rule)
		}
		alb2.Frontends = append(alb2.Frontends, ft)
	}

	return &alb2, nil
}
