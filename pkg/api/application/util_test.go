package application

import (
	"fmt"
	"reflect"
	"testing"

	"alauda.io/archon/pkg/api/application/v1beta1"

	"go.uber.org/zap"
	extv1beta1 "k8s.io/api/extensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
)

func makeObjFromJSON(objJSON string) (obj *unstructured.Unstructured) {
	obj = &unstructured.Unstructured{}
	obj.UnmarshalJSON([]byte(objJSON))
	return
}

func Test_getAppKey(t *testing.T) {
	type args struct {
		domain string
		attr   string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				attr:   "foo",
			},
			want: "app.alauda.io/foo",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getAppKey(tt.args.domain, tt.args.attr); got != tt.want {
				t.Errorf("getAppKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppNameKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
			},
			want: "app.alauda.io/name",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppNameKey(tt.args.domain); got != tt.want {
				t.Errorf("AppNameKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppUIDKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
			},
			want: "app.alauda.io/uuid",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppUIDKey(tt.args.domain); got != tt.want {
				t.Errorf("AppUIDKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppHistoryLimitKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
			},
			want: "app.alauda.io/history-limit",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppHistoryLimitKey(tt.args.domain); got != tt.want {
				t.Errorf("AppHistoryLimitKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppHistoryRevisionKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
			},
			want: "app.alauda.io/revision",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppHistoryRevisionKey(tt.args.domain); got != tt.want {
				t.Errorf("AppHistoryRevisionKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppRollbackFromKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
			},
			want: "app.alauda.io/rollback-from",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppRollbackFromKey(tt.args.domain); got != tt.want {
				t.Errorf("AppRollbackFromKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLastReplicasKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
			},
			want: "app.alauda.io/last-replicas",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LastReplicasKey(tt.args.domain); got != tt.want {
				t.Errorf("LastReplicasKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLastReplicas(t *testing.T) {
	template := `
{
	"metadata": {
		"annotations": {
			"app.alauda.io/last-replicas": "%d"
		}
	}
}
`
	type args struct {
		domain string
		obj    *unstructured.Unstructured
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				obj:    makeObjFromJSON(fmt.Sprintf(template, 3)),
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LastReplicas(tt.args.domain, tt.args.obj); got != tt.want {
				t.Errorf("LastReplicas() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestChangeCauseKey(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "foo.bar",
			},
			want: "app.foo.bar/change-cause",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ChangeCauseKey(tt.args.domain); got != tt.want {
				t.Errorf("ChangeCauseKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestChangeCause(t *testing.T) {
	template := `
{
	"metadata": {
		"annotations": {
			"app.alauda.io/change-cause": "%s"
		}
	}
}
`
	type args struct {
		domain string
		obj    *unstructured.Unstructured
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				obj:    makeObjFromJSON(fmt.Sprintf(template, "foobar")),
			},
			want: "foobar",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ChangeCause(tt.args.domain, tt.args.obj); got != tt.want {
				t.Errorf("ChangeCause() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewUUID(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "ok",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewUUID(); len(got) != 36 {
				t.Errorf("length of NewUUID() = %v, want 36", len(got))
			}
		})
	}
}

func TestAppUID(t *testing.T) {
	type args struct {
		app    *v1beta1.Application
		domain string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{
				app:    &v1beta1.Application{},
				domain: "alauda.io",
			},
		},
	}
	for _, tt := range tests {
		uuid := NewUUID()
		tt.args.app.Labels = map[string]string{
			AppUIDKey(tt.args.domain): uuid,
		}
		t.Run(tt.name, func(t *testing.T) {
			if got := AppUID(tt.args.app, tt.args.domain); got != uuid {
				t.Errorf("AppUID() = %v, want %v", got, uuid)
			}
		})
	}
}

func TestSetAppLabel(t *testing.T) {
	type args struct {
		app   *v1beta1.Application
		key   string
		value string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{
				app:   &v1beta1.Application{},
				key:   "foo",
				value: "bar",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			SetAppLabel(tt.args.app, tt.args.key, tt.args.value)
			if tt.args.app.Labels[tt.args.key] != tt.args.value {
				t.Errorf("Expect label %q = %q, got %q",
					tt.args.key, tt.args.value, tt.args.app.Labels[tt.args.key])
			}
		})
	}
}

func TestSetAppAnnotation(t *testing.T) {
	type args struct {
		app   *v1beta1.Application
		key   string
		value string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "ok",
			args: args{
				app:   &v1beta1.Application{},
				key:   "foo",
				value: "bar",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			SetAppAnnotation(tt.args.app, tt.args.key, tt.args.value)
			if tt.args.app.Annotations[tt.args.key] != tt.args.value {
				t.Errorf("Expect annotation %q = %q, got %q",
					tt.args.key, tt.args.value, tt.args.app.Annotations[tt.args.key])
			}
		})
	}
}

func TestIsUnsupportedComponentKind(t *testing.T) {
	type args struct {
		kind string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "supported kind",
			args: args{kind: "Deployment"},
			want: false,
		},
		{
			name: "unsupported kind",
			args: args{kind: "Namespace"},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsUnsupportedComponentKind(tt.args.kind); got != tt.want {
				t.Errorf("IsUnsupportedComponentKind() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppSelectorName(t *testing.T) {
	type args struct {
		name      string
		namespace string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				name:      "foo",
				namespace: "default",
			},
			want: "foo.default",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppSelectorName(tt.args.name, tt.args.namespace); got != tt.want {
				t.Errorf("AppSelectorName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppLabelSelectorMap(t *testing.T) {
	type args struct {
		app *v1beta1.Application
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "nil selector",
			args: args{
				app: &v1beta1.Application{},
			},
			want: map[string]string{},
		},
		{
			name: "with selector",
			args: args{
				app: &v1beta1.Application{
					Spec: v1beta1.ApplicationSpec{
						Selector: &metav1.LabelSelector{
							MatchLabels: map[string]string{
								"foo": "bar",
							},
						},
					},
				},
			},
			want: map[string]string{"foo": "bar"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppLabelSelectorMap(tt.args.app); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AppLabelSelectorMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppSelectorWithName(t *testing.T) {
	type args struct {
		domain string
		app    *v1beta1.Application
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				app: &v1beta1.Application{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "foo",
						Namespace: "default",
					},
				},
			},
			want: map[string]string{
				"app.alauda.io/name": "foo.default",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppSelectorWithName(tt.args.domain, tt.args.app); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AppSelectorWithName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppNameSelectorString(t *testing.T) {
	type args struct {
		domain string
		app    *v1beta1.Application
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				app: &v1beta1.Application{
					ObjectMeta: metav1.ObjectMeta{
						Name:      "foo",
						Namespace: "default",
					},
				},
			},
			want: "app.alauda.io/name=foo.default",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppNameSelectorString(tt.args.domain, tt.args.app.Name, tt.args.app.Namespace); got != tt.want {
				t.Errorf("AppNameSelectorString() = %v, want %v", got, tt.want)
			}
		})
	}
}

//func TestAppNameFromSelectorString(t *testing.T) {
//	type args struct {
//		selector string
//	}
//	tests := []struct {
//		name string
//		args args
//		want string
//	}{
//		{
//			name: "ok",
//			args: args{
//				selector: "app.alauda.io/name=foo.default",
//			},
//			want: "foo",
//		},
//		{
//			name: "invalid1",
//			args: args{
//				selector: "app.alauda.io/namefoo.default",
//			},
//			want: "",
//		},
//		{
//			name: "invalid2",
//			args: args{
//				selector: "app.alauda.io/name=foodefault",
//			},
//			want: "",
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			if got := AppNameFromSelectorString(tt.args.selector); got != tt.want {
//				t.Errorf("AppNameFromSelectorString() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}

func TestAppHistoryRevision(t *testing.T) {
	type args struct {
		domain string
		app    *v1beta1.Application
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				app: &v1beta1.Application{
					ObjectMeta: metav1.ObjectMeta{
						Annotations: map[string]string{
							"app.alauda.io/revision": "3",
						},
					},
				},
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppHistoryRevision(tt.args.domain, tt.args.app); got != tt.want {
				t.Errorf("AppHistoryRevision() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLabelString(t *testing.T) {
	type args struct {
		k string
		v string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				k: "foo",
				v: "bar",
			},
			want: "foo=bar",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LabelString(tt.args.k, tt.args.v); got != tt.want {
				t.Errorf("LabelString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppHistoryRevisionLimit(t *testing.T) {
	type args struct {
		domain string
		app    *v1beta1.Application
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppHistoryRevisionLimit(tt.args.domain, tt.args.app); got != tt.want {
				t.Errorf("AppHistoryRevisionLimit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppHistoryName(t *testing.T) {
	type args struct {
		appName  string
		revision int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppHistoryName(tt.args.appName, tt.args.revision); got != tt.want {
				t.Errorf("AppHistoryName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAddApplicationComponentKind(t *testing.T) {
	type args struct {
		ck []metav1.GroupKind
		gk metav1.GroupKind
	}
	tests := []struct {
		name string
		args args
		want []metav1.GroupKind
	}{
		{
			name: "ok",
			args: args{
				ck: []metav1.GroupKind{
					{"app.k8s.io", "Deployment"},
				},
				gk: metav1.GroupKind{"", "Service"},
			},
			want: []metav1.GroupKind{
				{"app.k8s.io", "Deployment"},
				{"", "Service"},
			},
		},
		{
			name: "exist",
			args: args{
				ck: []metav1.GroupKind{
					{"app.k8s.io", "Deployment"},
				},
				gk: metav1.GroupKind{"extensions.k8s.io", "Deployment"},
			},
			want: []metav1.GroupKind{
				{"app.k8s.io", "Deployment"},
				{"extensions.k8s.io", "Deployment"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AddApplicationComponentKind(tt.args.ck, tt.args.gk); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AddApplicationComponentKind() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRemoveApplicationComponentKind(t *testing.T) {
	type args struct {
		ck []metav1.GroupKind
		gk metav1.GroupKind
	}
	tests := []struct {
		name string
		args args
		want []metav1.GroupKind
	}{
		{
			name: "ok",
			args: args{
				ck: []metav1.GroupKind{
					{"app.k8s.io", "Deployment"},
					{"", "Service"},
				},
				gk: metav1.GroupKind{"app.k8s.io", "Deployment"},
			},
			want: []metav1.GroupKind{
				{"", "Service"},
			},
		},
		{
			name: "same kind",
			args: args{
				ck: []metav1.GroupKind{
					{"app.k8s.io", "Deployment"},
					{"", "Service"},
				},
				gk: metav1.GroupKind{"extensions.k8s.io", "Deployment"},
			},
			want: []metav1.GroupKind{
				{"app.k8s.io", "Deployment"},
				{"", "Service"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RemoveApplicationComponentKind(tt.args.ck, tt.args.gk); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveApplicationComponentKind() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPhasePatchJSON(t *testing.T) {
	type args struct {
		phase string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{phase: "Succeeded"},
			want: `{"spec":{"assemblyPhase":"Succeeded"}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PhasePatchJSON(tt.args.phase); string(got) != tt.want {
				t.Errorf("PhasePatchJSON() = %v, want %v", string(got), tt.want)
			}
		})
	}
}

func TestRevisionPatchJSON(t *testing.T) {
	type args struct {
		domain   string
		revision int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{domain: "alauda.io", revision: 3},
			want: `{"metadata":{"creationTimestamp":null,"annotations":{"app.alauda.io/revision":"3"}}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RevisionPatchJSON(tt.args.domain, tt.args.revision); string(got) != tt.want {
				t.Errorf("RevisionPatchJSON() = %v, want %v", string(got), tt.want)
			}
		})
	}
}

func TestAppComponentKindsPatchJSON(t *testing.T) {
	type args struct {
		ck []metav1.GroupKind
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{ck: []metav1.GroupKind{
				{"apps.k8s.io", "Deployment"},
			}},
			want: `{"spec":{"componentKinds":[{"group":"apps.k8s.io","kind":"Deployment"}]}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppComponentKindsPatchJSON(tt.args.ck); string(got) != tt.want {
				t.Errorf("AppComponentKindsPatchJSON() = %v, want %v", string(got), tt.want)
			}
		})
	}
}

func TestWorkloadReplicasPatchJSON(t *testing.T) {
	type args struct {
		domain   string
		obj      *unstructured.Unstructured
		replicas int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok",
			args: args{
				domain: "alauda.io",
				obj: func() *unstructured.Unstructured {
					obj := &unstructured.Unstructured{}
					obj.SetName("foobar")
					obj.SetUID(types.UID(NewUUID()))
					return obj
				}(),
				replicas: 3,
			},
			want: `{"metadata":{"creationTimestamp":null,"annotations":{"app.alauda.io/last-replicas":"0"}},"spec":{"replicas":3}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := WorkloadReplicasPatchJSON(tt.args.domain, tt.args.obj, tt.args.replicas); string(got) != tt.want {
				t.Errorf("WorkloadReplicasPatchJSON() = %v, want %v", string(got), tt.want)
			}
		})
	}
}

func TestApplicationToUnstructured(t *testing.T) {
	type args struct {
		app *v1beta1.Application
	}
	tests := []struct {
		name    string
		args    args
		want    *unstructured.Unstructured
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ApplicationToUnstructured(tt.args.app)
			if (err != nil) != tt.wantErr {
				t.Errorf("ApplicationToUnstructured() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ApplicationToUnstructured() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnstructuredToApplication(t *testing.T) {
	type args struct {
		obj *unstructured.Unstructured
	}
	tests := []struct {
		name    string
		args    args
		want    *v1beta1.Application
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UnstructuredToApplication(tt.args.obj)
			if (err != nil) != tt.wantErr {
				t.Errorf("UnstructuredToApplication() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnstructuredToApplication() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppLogger(t *testing.T) {
	type args struct {
		log *zap.SugaredLogger
		app *v1beta1.Application
	}
	tests := []struct {
		name string
		args args
		want *zap.SugaredLogger
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := AppLogger(tt.args.log, tt.args.app); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AppLogger() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCleanApplicationAutogenerated(t *testing.T) {
	type args struct {
		domain string
		obj    *unstructured.Unstructured
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			CleanApplicationAutogenerated(tt.args.domain, tt.args.obj)
		})
	}
}

func TestUnstructuredToIngress(t *testing.T) {
	type args struct {
		obj *unstructured.Unstructured
	}
	tests := []struct {
		name    string
		args    args
		want    *extv1beta1.Ingress
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UnstructuredToIngress(tt.args.obj)
			if (err != nil) != tt.wantErr {
				t.Errorf("UnstructuredToIngress() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnstructuredToIngress() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsLabelMatch(t *testing.T) {
	type args struct {
		a map[string]string
		b map[string]string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsLabelMatch(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("IsLabelMatch() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAddPodControllerAppLabels(t *testing.T) {
	type args struct {
		o      *unstructured.Unstructured
		labels map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := AddPodControllerAppLabels(tt.args.o, tt.args.labels); (err != nil) != tt.wantErr {
				t.Errorf("AddPodControllerAppLabels() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestRemovePodControllerAppLabels(t *testing.T) {
	type args struct {
		o      *unstructured.Unstructured
		labels map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := RemovePodControllerAppLabels(tt.args.o, tt.args.labels); (err != nil) != tt.wantErr {
				t.Errorf("RemovePodControllerAppLabels() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestLabelsSelectorString(t *testing.T) {
	type args struct {
		labels map[string]string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := LabelsSelectorString(tt.args.labels); got != tt.want {
				t.Errorf("LabelsSelectorString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAddAppSelector(t *testing.T) {
	type args struct {
		app *v1beta1.Application
		sel map[string]string
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			AddAppSelector(tt.args.app, tt.args.sel)
		})
	}
}
