package application

import (
	"encoding/json"
	"fmt"
	"k8s.io/klog"
	"sort"
	"sync"
	"time"

	appv1beta1 "alauda.io/archon/pkg/api/application/v1beta1"
	ah "alauda.io/archon/pkg/api/applicationhistory"
	ahv1beta1 "alauda.io/archon/pkg/api/applicationhistory/v1beta1"
	k8s "alauda.io/archon/pkg/kubernetes"
	"github.com/ghodss/yaml"
	"github.com/spf13/cast"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
)

const (
	// RollbackRequestAttr is the request attribute key of rollback,
	// the attribute stores the ApplicationRollback object.
	RollbackRequestAttr = "Rollback"
)

// HistoryResource stores the data for a application history creation.
type HistoryResource struct {
	// AppUID is the application UID to create the history.
	AppUID types.UID
	// OldApp is the old application object that is updated,
	// should be null on application creation.
	OldApp *appv1beta1.Application
	// NewApp is the new application object that is created.
	NewApp *appv1beta1.Application
	// User is the user who revokes the request.
	User string
	// ChangeCause is the change cause of the application history.
	ChangeCause string
	// CreationTimestamp is the creation timestamp of the application history.
	CreationTimestamp time.Time
	// RollbackFrom is the revision the application history rollbacks from.
	// A 'nil' value indicates that the history is not a rollback one.
	RollbackFrom *int
}

type HistoryStorageConfig struct {
	LabelBaseDomain string
}

// HistoryStorage stores the ApplicationHistory on the backend storage.
type HistoryStorage struct {
	HistoryStorageConfig
	client k8s.ClientBuilderInterface
}

// NewHistoryStorage returns a new HistoryStorage object.
func NewHistoryStorage(config HistoryStorageConfig, client k8s.ClientBuilderInterface) *HistoryStorage {
	return &HistoryStorage{
		HistoryStorageConfig: config,
		client:               client,
	}
}

// NextRevision returns the next revision number by the old application. If no
// revision found, will return revision 1.
func (r *HistoryStorage) NextRevision(app *appv1beta1.Application) (revision int) {
	var history *ahv1beta1.ApplicationHistory
	history, err := r.LatestHistory(app)
	if err != nil {
		return
	}
	revision = 1
	if history != nil {
		revision = history.Spec.Revision + 1
	}
	return
}

func (r *HistoryStorage) cleanAutogenerated(o *unstructured.Unstructured) {
	k8s.CleanAutogenerated(r.LabelBaseDomain, o, k8s.CleanPodControllerAutogenerated, CleanApplicationAutogenerated)
	o.SetOwnerReferences(nil)
}

// GenerateRevisionYAML generates the YAML string of revision for the application.
func (r *HistoryStorage) GenerateRevisionYAML(app *appv1beta1.Application) (string, error) {
	objects := []interface{}{}
	for _, o := range app.Spec.ComponentTemplates {
		r.cleanAutogenerated(&o)
		objects = append(objects, o.Object)
	}
	appCopy := app.DeepCopy()
	appCopy.Spec.ComponentTemplates = nil
	appCopyUnstruct, _ := ApplicationToUnstructured(appCopy)
	r.cleanAutogenerated(appCopyUnstruct)
	objects = append(objects, appCopyUnstruct)
	yamlStr, err := yaml.Marshal(objects)
	if err != nil {
		return "", err
	}
	return string(yamlStr), nil
}

// ApplicationFromRevision returns an Application object from a application revision. The
// "selector" argument specifies the application histories to search the application revision.
func (r *HistoryStorage) ApplicationFromRevision(namespace string, selector string, revision int) (*appv1beta1.Application, error) {
	appHistory, err := r.GetAppHistoryForRevision(namespace, selector, revision)
	if err != nil {
		return nil, err
	}
	objects, err := k8s.YamlToUnstructuredArray([]byte(appHistory.Spec.YAML))
	if err != nil {
		return nil, err
	}

	var app *appv1beta1.Application
	var components []unstructured.Unstructured
	for _, o := range objects {
		if o.GetKind() == "Application" {
			app, err = UnstructuredToApplication(&o)
			if err != nil {
				return nil, err
			}
		} else {
			components = append(components, o)
		}
	}

	if app == nil {
		return nil, fmt.Errorf("application not found in history revison")
	}

	app.Spec.ComponentTemplates = components
	return app, nil
}

// GenerateResourceDiffsWithLastRevision generates resource diffs of the application history
// between the old application and the new application request.
func (r *HistoryStorage) GenerateResourceDiffsWithLastRevision(
	oldApp *appv1beta1.Application,
	newApp *appv1beta1.Application) (map[ahv1beta1.ResourceChangeAction][]ahv1beta1.ResourceDiffItem, error) {
	var oldComponents []unstructured.Unstructured
	domain := r.LabelBaseDomain
	if oldApp != nil {
		lastRevision := cast.ToInt(AppHistoryRevision(r.LabelBaseDomain, oldApp))
		// Compatible with the applications without history revision
		if lastRevision == 0 {
			return nil, nil
		}

		lastAppRev, err := r.GetAppHistoryForRevision(
			oldApp.Namespace,
			AppNameSelectorString(domain, oldApp.Name, oldApp.Namespace),
			cast.ToInt(AppHistoryRevision(domain, oldApp)))
		if err != nil {
			klog.Warningf("Get application history %d of %s:%s error(%v), try to get components from storage.",
				AppHistoryRevision(domain, oldApp), oldApp.Namespace, oldApp.Name, err)
			oldComponents, err = GetApplicationComponentsFromStorage(r.client, oldApp)
		} else {
			oldComponents, err = ah.GetObjectsInAppHistory(lastAppRev)
		}

		if err != nil {
			return nil, err
		}
	}
	newComponents := newApp.Spec.ComponentTemplates
	newAppObj := newApp.DeepCopy()
	newAppObj.Spec.ComponentTemplates = nil
	unstructApp, err := k8s.ObjectToUnstructured(newAppObj)
	if err != nil {
		return nil, err
	}
	newComponents = append(newComponents, *unstructApp)
	for _, o := range newComponents {
		r.cleanAutogenerated(&o)
	}
	return r.ComputeResourceDiffs(oldComponents, newComponents)
}

// ComputeResourceDiffs computes the resource diffs between two KubernetesObject arrays.
// The returned result is a map which has 3 kinds of keys: 'create', 'update' and 'delete'.
func (r *HistoryStorage) ComputeResourceDiffs(oldResources []unstructured.Unstructured,
	newResources []unstructured.Unstructured) (map[ahv1beta1.ResourceChangeAction][]ahv1beta1.ResourceDiffItem, error) {
	diffResources, err := k8s.ComputeResourceDiffs(oldResources, newResources, k8s.ObjectKeyWithoutNamespace)
	if err != nil {
		return nil, err
	}
	if len(diffResources) == 0 {
		return nil, nil
	}
	resourcediffs := make(map[ahv1beta1.ResourceChangeAction][]ahv1beta1.ResourceDiffItem)
	for k, v := range diffResources {
		for _, o := range v {
			resourcediffs[ahv1beta1.ResourceChangeAction(k)] = append(
				resourcediffs[ahv1beta1.ResourceChangeAction(k)],
				ahv1beta1.ResourceDiffItem{
					Kind: o.GetKind(),
					Name: o.GetName(),
				})
		}
	}
	return resourcediffs, nil
}

func (r *HistoryStorage) clientForNamespace(namespace string) (dynamic.ResourceInterface, error) {
	return r.client.NamespacedDynamicClient(
		"applicationhistories",
		"app.k8s.io/v1beta1",
		namespace)
}

// CreateApplicationHistory creates a new application history object from the HistoryResource.
func (r *HistoryStorage) CreateApplicationHistory(history *HistoryResource) (*unstructured.Unstructured, error) {
	domain := r.LabelBaseDomain
	oldApp := history.OldApp
	newApp := history.NewApp

	appHistory := &ahv1beta1.ApplicationHistory{}
	appHistory.Kind = "ApplicationHistory"
	appHistory.APIVersion = "app.k8s.io/v1beta1"
	appHistory.Spec.Revision = r.NextRevision(newApp)
	appHistory.Spec.CreationTimestamp = metav1.Time{Time: history.CreationTimestamp}
	appHistory.Spec.User = history.User
	appHistory.Spec.ChangeCause = history.ChangeCause
	appHistory.Name = AppHistoryName(history.NewApp.GetName(), appHistory.Spec.Revision)
	appHistory.Namespace = newApp.GetNamespace()
	if history.RollbackFrom != nil {
		k8s.AddAnnotations(appHistory.GetObjectMeta(), map[string]string{
			AppRollbackFromKey(domain): fmt.Sprintf("%d", *history.RollbackFrom),
		})
	}

	// Generate YAML
	yamlStr, err := r.GenerateRevisionYAML(newApp)
	if err != nil {
		return nil, err
	}
	appHistory.Spec.YAML = yamlStr

	// Compute the resource diffs
	resourceDiffs, err := r.GenerateResourceDiffsWithLastRevision(oldApp, newApp)
	if err != nil {
		return nil, err
	}
	appHistory.Spec.ResourceDiffs = resourceDiffs

	// Set labels
	appHistory.SetLabels(AppSelectorWithName(domain, newApp))

	// Set ownerReference
	controller := false
	blockOwnerDeletion := true
	appHistory.OwnerReferences = []metav1.OwnerReference{
		{
			APIVersion:         newApp.APIVersion,
			Kind:               newApp.Kind,
			UID:                history.AppUID,
			Name:               newApp.GetName(),
			Controller:         &controller,
			BlockOwnerDeletion: &blockOwnerDeletion,
		},
	}

	unstructHistory, err := k8s.ObjectToUnstructured(appHistory)
	if err != nil {
		return nil, err
	}

	ri, err := r.clientForNamespace(newApp.GetNamespace())
	if err != nil {
		return nil, err
	}

	res, err := ri.Create(unstructHistory, metav1.CreateOptions{})
	if errors.IsAlreadyExists(err) {
		// Application history with the same revision exists, bump to new revision.
		err = r.bumpHistoryRevision(newApp, appHistory)
		if err != nil {
			return nil, err
		}
		unstructHistory, err = k8s.ObjectToUnstructured(appHistory)
		if err != nil {
			return nil, err
		}
		res, err = ri.Create(unstructHistory, metav1.CreateOptions{})

	}
	if err != nil {
		return nil, err
	}

	// GC the histories after create done, and GC error won't
	// cause create error. It will be executed again at next create.
	err = r.TryGCForApp(newApp)
	if err != nil {
		klog.Warningf("GC for application history error: %v", err)
	}
	return res, nil
}

// bumpHistoryRevision bumps the history revision to the actual revision according to the histories in the storage.
func (r *HistoryStorage) bumpHistoryRevision(app *appv1beta1.Application, appHistory *ahv1beta1.ApplicationHistory) error {
	latestRev := r.NextRevision(app)
	appHistory.Name = AppHistoryName(app.GetName(), latestRev)
	appHistory.Spec.Revision = latestRev
	return nil
}

// GetAppHistoryForRevision returns an application history by the application selector and revision.
func (r *HistoryStorage) GetAppHistoryForRevision(namespace string, selector string, revision int) (*ahv1beta1.ApplicationHistory, error) {
	options := metav1.ListOptions{
		LabelSelector: selector,
	}

	ri, err := r.clientForNamespace(namespace)
	if err != nil {
		return nil, err
	}

	objs, err := ri.List(options)
	if err != nil {
		return nil, err
	}

	var revObj *ahv1beta1.ApplicationHistory
	for _, obj := range objs.Items {
		raw, err := obj.MarshalJSON()
		if err != nil {
			return nil, err
		}
		appHistory := &ahv1beta1.ApplicationHistory{}
		err = json.Unmarshal(raw, appHistory)
		if err != nil {
			return nil, err
		}
		if appHistory.Spec.Revision == revision {
			revObj = appHistory
		}
	}

	if revObj == nil {
		return nil, errors.NewNotFound(schema.GroupResource{
			Group:    "app.k8s.io",
			Resource: "applicationhistories",
		}, fmt.Sprintf("%s-%d", AppNameFromSelectorString(selector), revision))
	}

	return revObj, nil
}

// TryGCForApp garbage collects the old application histories that exceed the history limit.
func (r *HistoryStorage) TryGCForApp(app *appv1beta1.Application) error {
	objs, err := r.HistoriesOfApp(app)
	if err != nil {
		return err
	}

	historyLimit := AppHistoryRevisionLimit(r.LabelBaseDomain, app)
	historyCount := len(objs.Items)
	deleteCount := historyCount - historyLimit
	if deleteCount > 0 {
		sort.Slice(objs.Items, func(i, j int) bool {
			hi, _ := ah.UnstructuredToApplicationHistory(&objs.Items[i])
			hj, _ := ah.UnstructuredToApplicationHistory(&objs.Items[j])
			return hi.Spec.Revision < hj.Spec.Revision
		})

		ri, err := r.clientForNamespace(app.GetNamespace())
		if err != nil {
			return err
		}

		var wg sync.WaitGroup
		for i := 0; i < deleteCount; i++ {
			i := i
			wg.Add(1)
			go func() {
				defer wg.Done()
				err := ri.Delete(objs.Items[i].GetName(), &metav1.DeleteOptions{})
				if err != nil {
					klog.Errorf("Delete application history when GC error: %v", err)
				}
			}()
		}
		wg.Wait()
	}

	return nil
}

// HistoriesOfApp returns the histories of an application in the storage.
func (r *HistoryStorage) HistoriesOfApp(app *appv1beta1.Application) (*unstructured.UnstructuredList, error) {
	options := metav1.ListOptions{
		LabelSelector: AppNameSelectorString(r.LabelBaseDomain, app.Name, app.Namespace),
	}

	ri, err := r.clientForNamespace(app.GetNamespace())
	if err != nil {
		return nil, err
	}

	return ri.List(options)
}

// LatestHistory returns the latest history of an application.
func (r *HistoryStorage) LatestHistory(app *appv1beta1.Application) (*ahv1beta1.ApplicationHistory, error) {
	hlist, err := r.HistoriesOfApp(app)
	if err != nil {
		return nil, err
	}

	if len(hlist.Items) == 0 {
		return nil, nil
	}

	sort.Slice(hlist.Items, func(i, j int) bool {
		hi, _ := ah.UnstructuredToApplicationHistory(&hlist.Items[i])
		hj, _ := ah.UnstructuredToApplicationHistory(&hlist.Items[j])
		return hi.Spec.Revision < hj.Spec.Revision
	})

	return ah.UnstructuredToApplicationHistory(&hlist.Items[len(hlist.Items)-1])
}
