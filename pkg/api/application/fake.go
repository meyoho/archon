package application

import (
	"alauda.io/archon/pkg/api/application/v1beta1"
	ah "alauda.io/archon/pkg/api/applicationhistory/v1beta1"
	"alauda.io/archon/pkg/api/config"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type FakeLifecycleManagerFactory struct{}

func (f FakeLifecycleManagerFactory) LifecycleManager(config config.ManagerConfig) LifecycleManager {
	return &FakeLifecycleManager{Config: config}
}

type FakeLifecycleManager struct {
	Config config.ManagerConfig
}

func (f *FakeLifecycleManager) Snapshot(namespace, name string) (*ah.ApplicationHistory, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) ChartPackage(namespace, name string, cp *v1beta1.ApplicationChartPackage) ([]byte, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) Get(namespace, name string, option GetOptions) (*v1beta1.Application, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) List(namespace string, option ListOptions) (*v1beta1.ApplicationList, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) Components(namespace, name string) ([]unstructured.Unstructured, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) ValidateCreate(application *v1beta1.Application) error {
	panic("implement me")
}

func (f *FakeLifecycleManager) Create(application *v1beta1.Application, option CreateOptions) (*v1beta1.Application, error) {
	return application, nil
}

func (f *FakeLifecycleManager) ValidateUpdate(old, new *v1beta1.Application) error {
	panic("implement me")
}

func (f *FakeLifecycleManager) Update(application *v1beta1.Application, option UpdateOptions) (*v1beta1.Application, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) Delete(namespace, name string, option DeleteOptions) error {
	panic("implement me")
}

func (f *FakeLifecycleManager) Start(namespace, name string, option RunOptions) error {
	panic("implement me")
}

func (f *FakeLifecycleManager) Stop(namespace, name string, option RunOptions) error {
	panic("implement me")
}

func (f *FakeLifecycleManager) NewVersion(old, new *v1beta1.Application, option NewVersionOptions) (*ah.ApplicationHistory, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) GetVersion(namespace, name string, version int) (*v1beta1.Application, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) Versions(namespace, name string) (*ah.ApplicationHistoryList, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) ApplicationFromVersion(version *ah.ApplicationHistory) (*v1beta1.Application, error) {
	panic("implement me")
}

func (f *FakeLifecycleManager) RollbackToVersion(namespace, name string, version int, option RollbackOptions) error {
	return nil
}
