package application

const (
	// ApplicationDefaultHistoryLimit is the limt of the application history numbers.
	ApplicationDefaultHistoryLimit = 6
	// ApplicationNameAndNamespaceMaxLength is the max lenght of the applicaiton name
	// contact with namespace, such as "yourname.yournamespace".
	ApplicationNameAndNamespaceMaxLength = 63
	// KindCanary is the kind of the controller for canary deployment.
	KindCanary = "Canary"
)

var (
	// UnsupportedApplicationComponentKind is unsupported component kinds map in application.
	UnsupportedApplicationComponentKind = map[string]bool{
		"Namespace": true,
	}
)

var (
	PodControllerSelectorFieldPath = []string{"spec", "selector", "matchLabels"}
	PodControllerPodLabelFieldPath = []string{"spec", "template", "metadata", "labels"}
)
