package application

import (
	"context"
	"fmt"

	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/spf13/cast"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

// CreateStrategy is the implementation of CreateStrategy for application.
type CreateStrategy struct{}

// PrepareForCreate prepares the application creation.
func (CreateStrategy) PrepareForCreate(ctx context.Context, obj runtime.Object) error {
	app := obj.(*v1beta1.Application)
	req, ok := rest.RequestFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get Request from context failed"))
	}
	config, ok := rest.APIConfigFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get APIConfig from context failed"))
	}
	log := AppLogger(config.Logger, app)
	domain := config.LabelBaseDomain
	appUID := NewUUID()
	app.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	app.Spec.AssemblyPhase = v1beta1.Pending
	SetAppLabel(app, AppUIDKey(domain), appUID)
	AddAppSelector(app, map[string]string{
		AppNameKey(domain): AppSelectorName(app.GetName(), app.GetNamespace()),
	})

	forceUpdatePod := cast.ToBool(req.QueryParameter("forceUpdatePod"))
	err := computeComponentKinds(app, domain, forceUpdatePod, PrepareComponentTemplate)
	if err != nil {
		return err
	}
	log.Debugf("Resolved application component kinds: %v", app.Spec.ComponentGroupKinds)
	req.SetAttribute(ApplicationComponentTemplatesAttribute, app.Spec.ComponentTemplates)
	app.Spec.ComponentTemplates = nil
	SetAppAnnotation(
		app,
		AppHistoryLimitKey(domain),
		fmt.Sprintf("%d", ApplicationDefaultHistoryLimit))
	return nil
}

func (CreateStrategy) CreateFunc() rest.CreateFunc {
	return nil
}

// Validate validates the application creation request.
func (CreateStrategy) Validate(ctx context.Context, obj runtime.Object) field.ErrorList {
	errs := field.ErrorList{}
	app := obj.(*v1beta1.Application)
	if len(app.GetName())+len(app.GetNamespace()) > ApplicationNameAndNamespaceMaxLength {
		errs = append(errs,
			field.TooLong(
				field.NewPath("metadata", "name+namespace"),
				fmt.Sprintf("%s.%s", app.GetName(), app.GetNamespace()),
				ApplicationNameAndNamespaceMaxLength))
	}
	return errs
}

// AfterCreate called after the application created successfully.
func (CreateStrategy) AfterCreate(ctx context.Context, obj runtime.Object) (err error) {
	req, ok := rest.RequestFromContext(ctx)
	if !ok {
		err = errors.NewInternalError(fmt.Errorf("get Request from context failed"))
		return
	}
	config, ok := rest.APIConfigFromContext(ctx)
	if !ok {
		err = errors.NewInternalError(fmt.Errorf("get APIConfig from context failed"))
		return
	}

	c, err := rest.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	components, err := ComponentsFromRequest(req)
	if err != nil {
		return
	}

	log := config.Logger
	appObj := obj.(*unstructured.Unstructured)
	unstructured.SetNestedField(appObj.Object, map[string]interface{}{"state": string(v1beta1.WLPending)}, "status")
	var componentTemplates []interface{}
	for _, c := range components {
		componentTemplates = append(componentTemplates, c.Object)
	}
	unstructured.SetNestedSlice(appObj.Object, componentTemplates, "spec", "componentTemplates")
	ri, err := c.NamespacedDynamicClient("applications", "app.k8s.io/v1beta1", appObj.GetNamespace())
	if err != nil {
		return errors.NewInternalError(err)
	}

	log.Infof("Creating application components for %s/%s.", appObj.GetNamespace(), appObj.GetName())
	err = CreateApplicationComponents(c, obj, components)
	if err != nil {
		// Failed
		log.Errorf("Create application components for %s/%s error: %v", appObj.GetNamespace(), appObj.GetName(), err)
		errPhaseUpdate := updateApplicationPhase(ri, appObj, v1beta1.Failed)
		if errPhaseUpdate != nil {
			return errors.NewInternalError(errPhaseUpdate)
		}
		updateConditionWithError(ri, appObj, "Create components failed", err)
		return err
	}

	// Succeeded
	log.Infof("Create application components for %s/%s succeeded.", appObj.GetNamespace(), appObj.GetName())
	errPhaseUpdate := updateApplicationPhase(ri, appObj, v1beta1.Succeeded)
	if errPhaseUpdate != nil {
		return errors.NewInternalError(errPhaseUpdate)
	}

	err = createApplicationHistory(
		c, ri, nil,
		appObj, k8s.CreatorOfObject(config.LabelBaseDomain, appObj), nil)
	if err != nil {
		updateConditionWithError(ri, appObj, "Create application history failed", err)
		return errors.NewInternalError(err)
	}

	updateConditionWithReady(ri, appObj, "Create application succeeded", "Every component is created")
	return nil
}
