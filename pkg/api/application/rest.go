package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"sort"
	"strings"

	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/features"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"github.com/spf13/cast"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	utilfeature "k8s.io/apiserver/pkg/util/feature"
	"k8s.io/client-go/dynamic"
)

// REST handles the application rest request.
type REST struct {
	rest.Handler
	lifeFactory LifecycleManagerFactory
	config      *config.APIConfig
}

// NewREST returns an application REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		config: cfg,
		Handler: *rest.NewHandler(
			cfg,
			CreateStrategy{},
			UpdateStrategy{},
			func() runtime.Object { return &v1beta1.Application{} },
			func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "app.k8s.io",
					Version:  "v1beta1",
					Resource: "applications",
				}
			},
			func() bool { return true },
		),
		lifeFactory: &defaultLifecycleManagerFactory{},
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (a *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	clusterParam := ws.PathParameter("cluster", "The name of the kubernetes cluster").
		DataType("string")
	nsParam := ws.PathParameter("namespace", "The namespace of the application").
		DataType("string")
	nameParam := ws.PathParameter("name", "The name of the application").
		DataType("string")
	ws.Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications").
			To(a.CreateApplication).
			Doc("Create an application").
			Param(clusterParam).
			Param(nsParam).
			Returns(http.StatusOK, "Create success", v1beta1.Application{}).
			Reads(v1beta1.Application{}).
			Produces(restful.MIME_JSON).Consumes(restful.MIME_JSON),
	).Route(
		ws.PUT("{cluster}/namespaces/{namespace}/applications/{name}").
			To(a.UpdateApplication).
			Doc("Update an application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusOK, "Update success", v1beta1.Application{}).
			Reads(v1beta1.Application{}).
			Produces(restful.MIME_JSON).Consumes(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/rollback").
			To(a.WrapError(a.RollbackApplication)).
			Doc("Rollback an application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusNoContent, "Rollback success", nil).
			Reads(v1beta1.ApplicationRollback{}).
			Produces(restful.MIME_JSON).Consumes(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/start").
			To(a.WrapError(a.StartOrStopApplication)).
			Doc("Start an application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusNoContent, "Start success", nil).
			Consumes("*/*").
			Produces(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/stop").
			To(a.WrapError(a.StartOrStopApplication)).
			Doc("Stop an application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusNoContent, "Stop success", nil).
			Consumes("*/*").
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("{cluster}/namespaces/{namespace}/applications/{name}/address").
			To(a.WrapError(a.HandleGetApplicationAddress)).
			Doc("Get an application addresses").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusOK, "Get success", AddressResource{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("{cluster}/namespaces/{namespace}/applications/{name}").
			To(a.WrapError(a.ListApplicationResources)).
			Doc("Get an application all resources").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusOK, "Get success", []unstructured.Unstructured{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/import").
			To(a.WrapError(a.ImportResource)).
			Doc("Import resources to application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Reads(v1beta1.ApplicationImport{}).
			Returns(http.StatusNoContent, "Import success", nil),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/export").
			To(a.WrapError(a.ExportResource)).
			Doc("Export resources from application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Reads(v1beta1.ApplicationExport{}).
			Returns(http.StatusNoContent, "Export success", nil),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/snapshot").
			To(a.WrapError(a.CreateSnapshot)).
			Doc("Create a snapshot from application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Reads(v1beta1.ApplicationSnapshot{}).
			Returns(http.StatusNoContent, "Snapshot created success", nil),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/chartpackage").
			To(a.WrapError(a.ChartPackage)).
			Doc("Export resources from application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Reads(v1beta1.ApplicationChartPackage{}).
			Returns(http.StatusNoContent, "Export success", nil).
			Consumes(restful.MIME_JSON).
			Produces("application/x-gzip"),
	)
}

// CreateApplication handles the application creation rest request.
func (a *REST) CreateApplication(req *restful.Request, res *restful.Response) {
	if utilfeature.DefaultMutableFeatureGate.Enabled(features.ApplicationNG) {
		var err error
		defer func() {
			if err != nil {
				a.WriteError(res, err)
			}
		}()
		lm, err := a.lifecycleManagerFromRequest(req)
		if err != nil {
			return
		}

		obj := &v1beta1.Application{}
		err = a.ReadEntity(obj, req)
		if err != nil {
			return
		}

		appCreated, err := lm.Create(obj, CreateOptions{})
		if err != nil {
			return
		}
		res.WriteAsJson(appCreated)
		return
	}
	a.Create(a.ContextFromRequest(req), req, res)
}

// UpdateApplication handles the application update rest request.
func (a *REST) UpdateApplication(req *restful.Request, res *restful.Response) {
	if utilfeature.DefaultMutableFeatureGate.Enabled(features.ApplicationNG) {
		var err error
		defer func() {
			if err != nil {
				a.WriteError(res, err)
			}
		}()
		lm, err := a.lifecycleManagerFromRequest(req)
		if err != nil {
			return
		}

		obj := &v1beta1.Application{}
		err = a.ReadEntity(obj, req)
		if err != nil {
			return
		}

		forceUpdatePod := cast.ToBool(req.QueryParameter("forceUpdatePod"))
		appUpdated, err := lm.Update(obj, UpdateOptions{ForceUpdatePod: forceUpdatePod})
		if err != nil {
			return
		}
		res.WriteAsJson(appUpdated)
		return
	}
	rest.SetForceUpdateRetry(req, true)
	a.Update(a.ContextFromRequest(req), req, res)
}

// RollbackApplication handles the application rollback rest request.
func (a *REST) RollbackApplication(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	rollback := &v1beta1.ApplicationRollback{}
	statusCode = http.StatusNoContent
	err = req.ReadEntity(rollback)
	if err != nil {
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	if utilfeature.DefaultMutableFeatureGate.Enabled(features.ApplicationNG) {
		lm, errI := a.lifecycleManagerFromRequest(req)
		if errI != nil {
			err = errI
			return
		}
		err = lm.RollbackToVersion(namespace, name, rollback.Spec.Revision,
			RollbackOptions{
				User: rollback.Spec.User,
			})
		return
	}

	ri, err := a.ClientFromRequest(req, namespace)
	if err != nil {
		return
	}
	client := req.Attribute(rest.ClientAttribute).(k8s.ClientBuilderInterface)

	old, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	ahs := NewHistoryStorage(HistoryStorageConfig{LabelBaseDomain: a.config.LabelBaseDomain}, client)
	oldApp, err := UnstructuredToApplication(old)
	if err != nil {
		return
	}

	domain := a.config.LabelBaseDomain
	newApp, err := ahs.ApplicationFromRevision(
		namespace,
		AppNameSelectorString(domain, oldApp.Name, oldApp.Namespace),
		rollback.Spec.Revision)
	if err != nil {
		return
	}
	newApp.ResourceVersion = old.GetResourceVersion()
	newApp.UID = old.GetUID()
	k8s.AddAnnotations(newApp,
		map[string]string{
			ChangeCauseKey(domain): fmt.Sprintf("Rollback from revision %d", rollback.Spec.Revision),
		})

	// TODO: Use hack request to call Update application method,
	// should refactor the update logic to be independent with REST
	// request.
	body, _ := json.Marshal(newApp)
	req.SetAttribute(rest.RequestBodyHackAttribute, body)

	req.SetAttribute(RollbackRequestAttr, rollback)
	recorder := httptest.NewRecorder()
	fakeRes := restful.NewResponse(recorder)
	a.Update(a.ContextFromRequest(req), req, fakeRes)
	if fakeRes.StatusCode() != http.StatusOK {
		fakeRes.Flush()
		res.WriteHeader(fakeRes.StatusCode())
		res.Write(recorder.Body.Bytes())
		return
	}
	res.WriteHeader(http.StatusNoContent)
	return
}

// StartOrStopApplication handles the start or stop rest request of an application.
func (a *REST) StartOrStopApplication(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusNoContent
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	isStart := strings.HasSuffix(req.Request.URL.Path, "start")

	if utilfeature.DefaultMutableFeatureGate.Enabled(features.ApplicationNG) {
		lm, errI := a.lifecycleManagerFromRequest(req)
		if errI != nil {
			err = errI
			return
		}
		if isStart {
			err = lm.Start(namespace, name, RunOptions{})
		} else {
			err = lm.Stop(namespace, name, RunOptions{})
		}
		return
	}

	ri, err := a.ClientFromRequest(req, namespace)
	if err != nil {
		return
	}
	client := req.Attribute(rest.ClientAttribute).(k8s.ClientBuilderInterface)

	appUnstruct, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	app, _ := UnstructuredToApplication(appUnstruct)
	components, err := GetApplicationComponentsFromStorage(client, app)
	if err != nil {
		return
	}

	err = a.startOrStopEachComponent(components, client, isStart)
	if err != nil {
		errPhaseUpdate := updateApplicationPhase(ri, appUnstruct, v1beta1.Failed)
		if errPhaseUpdate != nil {
			err = errPhaseUpdate
		}
		return
	}
	updateApplicationPhase(ri, appUnstruct, v1beta1.Succeeded)
	return
}

func (a *REST) startOrStopEachComponent(components []unstructured.Unstructured, client k8s.ClientBuilderInterface, isStart bool) (err error) {
	domain := a.config.LabelBaseDomain
	for _, c := range components {
		if k8s.IsPodController(c.GetKind()) && c.GetKind() != "DaemonSet" {
			var ri dynamic.ResourceInterface
			ri, err = client.ClientFromResource(&c)
			if err != nil {
				return
			}
			targetReplicas := 0
			if isStart {
				targetReplicas = LastReplicas(domain, &c)
				if targetReplicas == 0 {
					// Last replicas is 0, use 1 as default.
					targetReplicas = 1
				}
			} else if Replicas(&c) == 0 {
				// When stopping the workload with 0 replicas, skip it to
				// avoid the last replicas is updated to 0.
				continue
			}
			_, err = ri.Patch(
				c.GetName(), types.MergePatchType,
				WorkloadReplicasPatchJSON(domain, &c, targetReplicas),
				metav1.PatchOptions{})
			if err != nil {
				return
			}
		}
	}
	return
}

func (a *REST) ListApplicationResources(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	if utilfeature.DefaultMutableFeatureGate.Enabled(features.ApplicationNG) {
		lm, errI := a.lifecycleManagerFromRequest(req)
		if errI != nil {
			err = errI
			return
		}
		clist, errI := lm.Components(namespace, name)
		if errI != nil {
			err = errI
			return
		}
		res.WriteAsJson(clist)
		return
	}

	ri, err := a.ClientFromRequest(req, namespace)
	if err != nil {
		return
	}
	client := req.Attribute(rest.ClientAttribute).(k8s.ClientBuilderInterface)

	appUnstruct, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	app, err := UnstructuredToApplication(appUnstruct)
	if err != nil {
		return
	}
	allresources, err := GetApplicationComponentsFromStorage(client, app)
	if err != nil {
		return
	}
	// remove duplicate app resources
	allresources = RemoveDuplicateAPPResources(allresources)
	allresources = append(allresources, *appUnstruct)
	//sort by kind and compare name if kind is equal
	sort.Slice(allresources, func(i, j int) bool {
		return allresources[i].GetKind() < allresources[j].GetKind() || (allresources[i].GetKind() == allresources[j].GetKind() && allresources[i].GetName() < allresources[j].GetName())
	})
	res.WriteAsJson(allresources)
	return
}

func (a *REST) lifecycleManagerFromRequest(req *restful.Request) (lm LifecycleManager, err error) {
	c, err := a.config.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	lm = a.lifeFactory.LifecycleManager(config.ManagerConfig{
		GroupKind: metav1.GroupKind{
			Group: "app.k8s.io",
			Kind:  "Application",
		},
		Cluster:         req.PathParameter("cluster"),
		Context:         context.TODO(),
		ClientBuilder:   c,
		LabelBaseDomain: config.GetConfig().LabelBaseDomain,
	})
	return
}

func (a *REST) ChartPackage(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	cp := &v1beta1.ApplicationChartPackage{}
	err = a.ReadEntity(cp, req)
	if err != nil {
		return
	}

	lm, err := a.lifecycleManagerFromRequest(req)
	if err != nil {
		return
	}
	pkgBytes, err := lm.ChartPackage(namespace, name, cp)
	if err != nil {
		return
	}
	res.AddHeader("Content-Disposition",
		fmt.Sprintf("attachment; filename=\"%s-%s.tar.gz\"", cp.Spec.Name, cp.Spec.Version))
	_, err = res.Write(pkgBytes)
	return
}
