package application

import (
	"encoding/json"
	"fmt"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/kubefed/pkg/controller/util"
	"strings"

	"k8s.io/apimachinery/pkg/types"

	"github.com/thoas/go-funk"

	"alauda.io/archon/pkg/api/application/v1beta1"
	k8s "alauda.io/archon/pkg/kubernetes"
	extv1beta1 "k8s.io/api/extensions/v1beta1"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/uuid"
)

func getAppKey(domain, attr string) string {
	return k8s.GetKey(domain, "app", attr)
}

// AppNameKey returns the application name key.
func AppNameKey(domain string) string {
	return getAppKey(domain, "name")
}

// AppUIDKey returns the application UID key.
func AppUIDKey(domain string) string {
	return getAppKey(domain, "uuid")
}

// AppHistoryLimitKey returns the application history limit key.
func AppHistoryLimitKey(domain string) string {
	return getAppKey(domain, "history-limit")
}

// AppHistoryRevisionKey returns the application revision key.
func AppHistoryRevisionKey(domain string) string {
	return getAppKey(domain, "revision")
}

// AppRollbackFromKey returns the application rollback from key.
func AppRollbackFromKey(domain string) string {
	return getAppKey(domain, "rollback-from")
}

// LastReplicasKey returns the last replicas key.
func LastReplicasKey(domain string) string {
	return getAppKey(domain, "last-replicas")
}

// LastReplicas returns the last replicas of an workload.
func LastReplicas(domain string, obj *unstructured.Unstructured) int {
	replicas, _, _ := unstructured.NestedString(
		obj.Object,
		k8s.KubernetesMetadataKey,
		k8s.KubernetesAnnotationsKey,
		LastReplicasKey(domain))
	return cast.ToInt(replicas)
}

// Replicas returns the replicas of an workload.
func Replicas(obj *unstructured.Unstructured) int64 {
	replicas, _, _ := unstructured.NestedInt64(
		obj.Object,
		k8s.KubernetesSpecKey,
		k8s.KubernetesReplicasKey)
	return replicas
}

// ChangeCauseKey returns the change cause key of annotation.
func ChangeCauseKey(domain string) string {
	return getAppKey(domain, "change-cause")
}

// ChangeCause returns the change cause string of an application.
func ChangeCause(domain string, obj runtime.Object) string {
	meta := util.MetaAccessor(obj)
	annos := meta.GetAnnotations()
	if annos != nil {
		return annos[ChangeCauseKey(domain)]
	}
	return ""
}

// NewUUID returns a new UUID.
func NewUUID() string {
	return string(uuid.NewUUID())
}

// AppUID returns a Application UID of an application.
// Note: the AppUID is not the ObjectMeta UID of the application object.
func AppUID(app *v1beta1.Application, domain string) string {
	key := AppUIDKey(domain)
	labels := app.GetLabels()
	if labels != nil {
		return labels[key]
	}
	return ""
}

// SetAppLabel sets a label to an application, override if exist.
func SetAppLabel(app *v1beta1.Application, key, value string) {
	labels := app.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
	}
	labels[key] = value
	app.SetLabels(labels)
}

// SetAppAnnotation sets an annotation to an application, override if exist.
func SetAppAnnotation(app *v1beta1.Application, key, value string) {
	annos := app.GetAnnotations()
	if annos == nil {
		annos = make(map[string]string)
	}
	annos[key] = value
	app.SetAnnotations(annos)
}

// IsUnsupportedComponentKind returns true if the kind is not supported
// in application.
func IsUnsupportedComponentKind(kind string) bool {
	return UnsupportedApplicationComponentKind[kind]
}

// AppSelectorName returns the application name selector.
func AppSelectorName(name, namespace string) string {
	return fmt.Sprintf("%s.%s", name, namespace)
}

// AppLabelSelectorMap returns the selector map of the application.
func AppLabelSelectorMap(app *v1beta1.Application) map[string]string {
	result := make(map[string]string)
	if app.Spec.Selector == nil || app.Spec.Selector.MatchLabels == nil {
		return result
	}
	return app.Spec.Selector.MatchLabels
}

// AppSelectorWithName returns the application name selector map.
func AppSelectorWithName(domain string, app *v1beta1.Application) map[string]string {
	origin := AppLabelSelectorMap(app)
	appSelectorName := AppSelectorName(app.GetName(), app.GetNamespace())
	origin[AppNameKey(domain)] = appSelectorName
	return origin
}

// AppNameSelectorStringFromApp returns the application selector in string format from the
// labels of the application.
func AppNameSelectorStringFromApp(app *v1beta1.Application) string {
	value := AppSelectorName(app.Name, app.Namespace)
	if app.Spec.Selector != nil && app.Spec.Selector.MatchLabels != nil {
		for k, v := range app.Spec.Selector.MatchLabels {
			if v == value {
				return fmt.Sprintf("%s=%s", k, v)
			}
		}
	}
	return ""
}

func AppNameSelectorStringFromAppUnstruct(app *unstructured.Unstructured) string {
	value := AppSelectorName(app.GetName(), app.GetNamespace())
	lbls, ok, err := unstructured.NestedMap(app.Object, "spec", "selector", "matchLabels")
	if err != nil {
		return ""
	}
	if !ok {
		return ""
	}
	for k, v := range lbls {
		if v == value {
			return fmt.Sprintf("%s=%s", k, v)
		}
	}
	return ""
}

// AppNameSelectorString returns the application selector in string format.
func AppNameSelectorString(domain, name, namespace string) string {
	return fmt.Sprintf("%s=%s", AppNameKey(domain), AppSelectorName(name, namespace))
}

// AppNameFromSelectorString returns the application name from an application selector.
// Returns empty string if parse error.
func AppNameFromSelectorString(selector string) string {
	s := strings.Split(selector, "=")
	if len(s) < 2 {
		return ""
	}
	ss := strings.Split(s[1], ".")
	if len(ss) < 2 {
		return ""
	}
	return ss[0]
}

// AppHistoryRevision returns the current revision number of the application.
func AppHistoryRevision(domain string, app *v1beta1.Application) int {
	annos := app.GetAnnotations()
	if annos != nil {
		return cast.ToInt(annos[AppHistoryRevisionKey(domain)])
	}
	return 0
}

// LabelString return labelSelector string format
func LabelString(k, v string) string {
	return fmt.Sprintf("%s=%s", k, v)
}

// AppHistoryRevisionLimit returns the revision limit of the application.
func AppHistoryRevisionLimit(domain string, app *v1beta1.Application) int {
	annos := app.GetAnnotations()
	if annos != nil && annos[AppHistoryLimitKey(domain)] != "" {
		return cast.ToInt(annos[AppHistoryLimitKey(domain)])
	}
	return ApplicationDefaultHistoryLimit
}

// AppHistoryName returns the generated application history name.
func AppHistoryName(appName string, revision int) string {
	return fmt.Sprintf("%s-%d", appName, revision)
}

// AddApplicationComponentKind adds a group kind to the ComponentGroupKinds of the
// application. Note that the same kind with different group will be treated as identical.
func AddApplicationComponentKind(ck []metav1.GroupKind, gk metav1.GroupKind) []metav1.GroupKind {
	for _, k := range ck {
		// kind and  group must be equal
		if k.Kind == gk.Kind && k.Group == gk.Group {
			return ck
		}
	}

	ck = append(ck, gk)
	return ck
}

// RemoveApplicationComponentKind removes a group kind from the ComponentGroupKinds of the
// application. Note that the same kind with different group will be treated as identical.
func RemoveApplicationComponentKind(ck []metav1.GroupKind, gk metav1.GroupKind) []metav1.GroupKind {
	var newCK []metav1.GroupKind
	for _, k := range ck {
		if k.Kind == gk.Kind && k.Group == gk.Group {
			// If kind and group is equal, remove the gk.
			continue
		}
		newCK = append(newCK, k)
	}

	return newCK
}

// PhasePatchJSON returns json bytes of the application phase patch request.
func PhasePatchJSON(phase string) []byte {
	phaseS := struct {
		Spec struct {
			AssemblyPhase string `json:"assemblyPhase"`
		} `json:"spec"`
	}{}
	phaseS.Spec.AssemblyPhase = phase
	raw, _ := json.Marshal(phaseS)
	return raw
}

// RevisionPatchJSON returns json bytes of the application revision patch request.
func RevisionPatchJSON(domain string, revision int) []byte {
	revisionStruct := struct {
		Metadata metav1.ObjectMeta `json:"metadata"`
	}{}
	revisionStruct.Metadata.Annotations = make(map[string]string)
	revisionStruct.Metadata.Annotations[AppHistoryRevisionKey(domain)] = fmt.Sprintf("%d", revision)
	raw, _ := json.Marshal(revisionStruct)
	return raw
}

// AppComponentKindsPatchJSON returns json bytes of the application component kinds patch request.
func AppComponentKindsPatchJSON(ck []metav1.GroupKind) []byte {
	ckStruct := struct {
		Spec struct {
			ComponentKinds []metav1.GroupKind `json:"componentKinds"`
		} `json:"spec"`
	}{}
	ckStruct.Spec.ComponentKinds = ck
	raw, _ := json.Marshal(ckStruct)
	return raw
}

// WorkloadReplicasPatchJSON returns json bytes of the application replicas patch request.
// The current "replicas" will be saved as "LastReplicasKey" in the annotation.
func WorkloadReplicasPatchJSON(domain string, obj *unstructured.Unstructured, replicas int) []byte {
	workload := struct {
		Metadata metav1.ObjectMeta `json:"metadata"`
		Spec     struct {
			Replicas int `json:"replicas"`
		} `json:"spec"`
	}{}
	workload.Metadata.Annotations = make(map[string]string)
	lastRS, _, _ := unstructured.NestedInt64(obj.Object, "spec", "replicas")
	workload.Metadata.Annotations[LastReplicasKey(domain)] = fmt.Sprintf("%d", lastRS)
	workload.Spec.Replicas = replicas
	raw, _ := json.Marshal(workload)
	return raw
}

// ApplicationToUnstructured converts "Application" type object to "Unstructured" type.
func ApplicationToUnstructured(app *v1beta1.Application) (*unstructured.Unstructured, error) {
	obj := &unstructured.Unstructured{}
	raw, err := json.Marshal(app)
	if err != nil {
		return nil, err
	}
	err = obj.UnmarshalJSON(raw)
	if err != nil {
		return nil, err
	}
	return obj, err
}

// UnstructuredToApplication converts "Unstructured" type object to "Application" type.
func UnstructuredToApplication(obj *unstructured.Unstructured) (*v1beta1.Application, error) {
	app := &v1beta1.Application{}
	raw, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(raw, app)
	if err != nil {
		return nil, err
	}
	return app, err
}

// AppLogger returns an application named logger. The name is "application.<namespace>/<name>".
func AppLogger(log *zap.SugaredLogger, app *v1beta1.Application) *zap.SugaredLogger {
	return log.Named(fmt.Sprintf("application.%s/%s", app.GetNamespace(), app.GetName()))
}

// CleanApplicationAutogenerated cleans the auto-generated fields in the application.
func CleanApplicationAutogenerated(domain string, obj *unstructured.Unstructured) {
	// Remove application revision annotation in Application.
	if obj.GetKind() == "Application" {
		unstructured.RemoveNestedField(obj.Object, k8s.KubernetesMetadataKey, k8s.KubernetesAnnotationsKey, AppHistoryRevisionKey(domain))
		unstructured.RemoveNestedField(obj.Object, k8s.KubernetesMetadataKey, k8s.KubernetesAnnotationsKey, AppHistoryLimitKey(domain))
		unstructured.RemoveNestedField(obj.Object, k8s.KubernetesSpecKey, "assemblyPhase")
	}
}

// UnstructuredToIngress converts "Unstructured" type object to "Ingress" type.
func UnstructuredToIngress(obj *unstructured.Unstructured) (*extv1beta1.Ingress, error) {
	o := &extv1beta1.Ingress{}
	raw, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(raw, o)
	if err != nil {
		return nil, err
	}
	return o, err
}

// IsLabelMatch returns true if every item of labels a match b.
func IsLabelMatch(a, b map[string]string) bool {
	for k, v := range a {
		if b[k] != v {
			return false
		}
	}
	return true
}

// AddPodControllerAppLabels adds application labels to selector match labels and pod template
// labels of the pod controller.
func AddPodControllerAppLabels(o *unstructured.Unstructured, labels map[string]string) error {
	podTemplateLabels, ok, err := unstructured.NestedMap(o.Object, PodControllerPodLabelFieldPath...)
	if err != nil {
		return err
	}

	if !ok {
		podTemplateLabels = make(map[string]interface{})
	}

	for k, v := range labels {
		podTemplateLabels[k] = v
	}

	err = unstructured.SetNestedMap(o.Object, podTemplateLabels, PodControllerPodLabelFieldPath...)
	if err != nil {
		return err
	}
	return nil
}

// RemovePodControllerAppLabels removes application labels from selector match labels and pod template
// labels of the pod controller.
func RemovePodControllerAppLabels(o *unstructured.Unstructured, labels map[string]string) error {
	podTemplateLabels, ok, err := unstructured.NestedMap(o.Object, PodControllerPodLabelFieldPath...)
	if err != nil {
		return nil
	}

	if !ok {
		return nil
	}

	for k := range labels {
		delete(podTemplateLabels, k)
	}

	err = unstructured.SetNestedMap(o.Object, podTemplateLabels, PodControllerPodLabelFieldPath...)
	if err != nil {
		return err
	}
	return nil
}

// LabelsSelectorString will create format string of LabelsSelector by labels
func LabelsSelectorString(labels map[string]string) string {
	labelstring := []string{}
	for k, v := range labels {
		labelstring = append(labelstring, LabelString(k, v))
	}
	return strings.Join(labelstring, ",")
}

// AddAppSelector adds selectors to the application's selector. The label with the same name will be
// overridden.
func AddAppSelector(app *v1beta1.Application, sel map[string]string) {
	if app.Spec.Selector == nil {
		app.Spec.Selector = &metav1.LabelSelector{
			MatchLabels: map[string]string{},
		}
	}
	for k, v := range sel {
		app.Spec.Selector.MatchLabels[k] = v
	}
}

// AddAppNameLabel adds a label of application name to the resource's labels.
func AddAppNameLabel(obj *unstructured.Unstructured, domain, name, namespace string) {
	k8s.AddLabels(obj, map[string]string{
		AppNameKey(domain): AppSelectorName(name, namespace),
	})
}

func RemoveDuplicateAPPResources(components []unstructured.Unstructured) []unstructured.Unstructured {
	tidyResources := []unstructured.Unstructured{}
	addedUID := []types.UID{}
	for _, component := range components {
		uid := component.GetUID()
		if !funk.Contains(addedUID, uid) {
			tidyResources = append(tidyResources, component)
			addedUID = append(addedUID, uid)
		}

	}
	return tidyResources
}

func UnstructuredSetApplicationComponentsTemplate(app *unstructured.Unstructured, compTemplates []unstructured.Unstructured) error {
	var comps []interface{}
	for _, c := range compTemplates {
		comps = append(comps, c.Object)
	}
	return unstructured.SetNestedSlice(app.Object, comps, "spec", "componentsTemplates")
}

func SetupApplicationFields(domain string, app *v1beta1.Application, uid string, status *v1beta1.ApplicationStatus) {
	app.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	app.Spec.AssemblyPhase = v1beta1.Pending
	if status != nil {
		app.Status = *status
	}
	SetAppLabel(app, AppUIDKey(domain), uid)
	AddAppSelector(app, map[string]string{
		AppNameKey(domain): AppSelectorName(app.GetName(), app.GetNamespace()),
	})
	SetAppAnnotation(
		app,
		AppHistoryLimitKey(domain),
		fmt.Sprintf("%d", ApplicationDefaultHistoryLimit))
}
