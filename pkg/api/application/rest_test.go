package application

import (
	"encoding/json"
	"fmt"
	"gotest.tools/assert"
	"net/http"
	"testing"

	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/features"
	"alauda.io/archon/pkg/test"

	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	apiruntime "k8s.io/apimachinery/pkg/runtime"
	utilfeature "k8s.io/apiserver/pkg/util/feature"
	"k8s.io/client-go/kubernetes/scheme"
	fgtest "k8s.io/component-base/featuregate/testing"
)

func SetupREST(method, url, file string, objects string, args ...interface{}) (*REST, *test.FakeRestful) {
	v1beta1.AddToScheme(scheme.Scheme)
	var objs []apiruntime.Object
	if objects != "" {
		uos := []*unstructured.Unstructured{}
		test.ObjectFromFixture(objects, &uos)
		for _, uo := range uos {
			objs = append(objs, uo)
		}
	}
	cfg := config.FakeAPIConfig(objs...)
	config.SetConfig(*cfg)
	r := NewREST(cfg)
	r.lifeFactory = &defaultLifecycleManagerFactory{}
	return r, test.FakeRestfulWithJSONFile(method, url, file, args...)
}

func TestNewREST(t *testing.T) {
	type args struct {
		cfg *config.APIConfig
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "generic",
			args: args{
				cfg: &config.APIConfig{
					LabelBaseDomain: "fake",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewREST(tt.args.cfg)
			if got.config.LabelBaseDomain != tt.args.cfg.LabelBaseDomain {
				t.Errorf("Expect LabelBaseDomain %s, got %s", tt.args.cfg.LabelBaseDomain, got.config.LabelBaseDomain)
				return
			}
			if !got.NamespaceScoped() {
				t.Errorf("Unexpected NamespaceScoped %v", got.NamespaceScoped())
				return
			}
			gvr := got.ResourceFunc()
			if gvr.Group != "app.k8s.io" || gvr.Version != "v1beta1" || gvr.Resource != "applications" {
				t.Errorf("Unexpected gvr %+v", gvr)
				return
			}
			newObj := got.NewFunc()
			_, ok := newObj.(*v1beta1.Application)
			if !ok {
				t.Errorf("Unexpected object %T NewFunc returned", newObj)
				return
			}
		})
	}
}

func TestRESTInstallRoutes(t *testing.T) {
	type fields struct {
		Handler rest.Handler
		config  *config.APIConfig
	}
	type args struct {
		version core.APIVersion
		ws      *restful.WebService
	}
	rest := NewREST(config.FakeAPIConfig())
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "generic",
			fields: fields{
				Handler: rest.Handler,
				config:  rest.config,
			},
			args: args{
				version: core.APIVersionV1,
				ws:      new(restful.WebService),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &REST{
				Handler: tt.fields.Handler,
				config:  tt.fields.config,
			}
			a.InstallRoutes(tt.args.version, tt.args.ws)
			if len(tt.args.ws.Routes()) == 0 {
				t.Errorf("Unexpected installed routes is empty")
				return
			}
		})
	}
}

func TestRESTContextFromRequest(t *testing.T) {
	type fields struct {
		Handler rest.Handler
		config  *config.APIConfig
	}
	type args struct {
		req *restful.Request
	}
	appREST := NewREST(config.FakeAPIConfig())
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "generic",
			fields: fields{
				Handler: appREST.Handler,
				config:  appREST.config,
			},
			args: args{
				req: test.FakeRestfulRequest(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &REST{
				Handler: tt.fields.Handler,
				config:  tt.fields.config,
			}
			ctx := a.ContextFromRequest(tt.args.req)
			if cfg, ok := rest.APIConfigFromContext(ctx); !ok || cfg != appREST.config {
				t.Errorf("Unexpected APIConfig object")
				return
			}
			if req, ok := rest.RequestFromContext(ctx); !ok || req != tt.args.req {
				t.Errorf("Unexpected Request object")
				return
			}
		})
	}
}

func TestRESTCreateApplication(t *testing.T) {
	tests := []struct {
		name            string
		requestJson     string
		enableNGFeature bool
		initObjects     []apiruntime.Object
		wantErr         bool
	}{
		{
			name:        "old",
			requestJson: "create_application.json",
		},
		{
			name:            "new",
			requestJson:     "create_application.json",
			enableNGFeature: true,
		},
		{
			name:            "new",
			requestJson:     "create_application.json",
			enableNGFeature: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cfg := config.FakeAPIConfig(tt.initObjects...)
			config.SetConfig(*cfg)
			appREST := NewREST(cfg)
			appREST.lifeFactory = FakeLifecycleManagerFactory{}
			if tt.enableNGFeature {
				defer fgtest.SetFeatureGateDuringTest(t, utilfeature.DefaultFeatureGate, features.ApplicationNG, true)()
			}
			req := test.FakeRestfulWithJSONFile(http.MethodPost, "fake", tt.requestJson)
			appREST.CreateApplication(req.Request, req.Response)
			httpResp := req.ReturnedResponse()
			body := req.ReturnedBody()
			if httpResp.StatusCode != http.StatusOK {
				t.Errorf("Expect status %d, got %d, resp: %s", http.StatusOK, httpResp.StatusCode, body)
				return
			}
			app := &v1beta1.Application{}
			err := json.Unmarshal(body, app)
			if err != nil {
				t.Errorf("Unexpected error: %v", err)
				return
			}
		})
	}
}

func TestREST_UpdateApplication(t *testing.T) {
	tests := []struct {
		name            string
		appName         string
		requestJson     string
		enableNGFeature bool
		initObjects     string
		expectStatus    int
		expectCK        []metav1.GroupKind
		wantErr         bool
	}{
		{
			name:         "delete",
			appName:      "wordpress-01",
			requestJson:  "update_application_del.json",
			initObjects:  "init_applications.json",
			expectStatus: http.StatusOK,
			expectCK: []metav1.GroupKind{
				{
					Group: "",
					Kind:  "Service",
				},
				{
					Group: "apps",
					Kind:  "Deployment",
				},
			},
		},
		{
			name:         "add",
			appName:      "wordpress-01",
			requestJson:  "update_application_add.json",
			initObjects:  "init_applications.json",
			expectStatus: http.StatusOK,
			expectCK: []metav1.GroupKind{
				{
					Group: "",
					Kind:  "Service",
				},
				{
					Group: "apps",
					Kind:  "Deployment",
				},
				{
					Group: "apps",
					Kind:  "DaemonSet",
				},
			},
		},
		{
			name:            "new delete",
			appName:         "wordpress-01",
			requestJson:     "update_application_del.json",
			initObjects:     "init_applications.json",
			enableNGFeature: true,
			expectStatus:    http.StatusOK,
			expectCK: []metav1.GroupKind{
				{
					Group: "",
					Kind:  "Service",
				},
				{
					Group: "apps",
					Kind:  "Deployment",
				},
			},
		},
		{
			name:            "new add",
			appName:         "wordpress-01",
			requestJson:     "update_application_add.json",
			initObjects:     "init_applications.json",
			enableNGFeature: true,
			expectStatus:    http.StatusOK,
			expectCK: []metav1.GroupKind{
				{
					Group: "",
					Kind:  "Service",
				},
				{
					Group: "apps",
					Kind:  "Deployment",
				},
				{
					Group: "apps",
					Kind:  "DaemonSet",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodPut, "fake", tt.requestJson, tt.initObjects)
			if tt.enableNGFeature {
				defer fgtest.SetFeatureGateDuringTest(t, utilfeature.DefaultFeatureGate, features.ApplicationNG, true)()
			}
			fr.Request.PathParameters()["namespace"] = "default"
			fr.Request.PathParameters()["name"] = tt.appName
			r.UpdateApplication(fr.Request, fr.Response)
			status := fr.ReturnedResponse().StatusCode
			if status != tt.expectStatus {
				t.Errorf("Expect status %d, got %d, resp: %s", tt.expectStatus, status, fr.ReturnedBody())
				return
			}
			if status == http.StatusOK {
				updatedApp := &v1beta1.Application{}
				err := json.Unmarshal(fr.ReturnedBody(), updatedApp)
				if err != nil {
					t.Errorf("Unexpected error")
					return
				}
				assert.DeepEqual(t, tt.expectCK, updatedApp.Spec.ComponentGroupKinds)
			}
		})
	}
}

func TestREST_RollbackApplication(t *testing.T) {
	tests := []struct {
		name            string
		appName         string
		requestJson     string
		initObjects     string
		enableNGFeature bool
		expectStatus    int
	}{
		{
			name:         "rollback ok",
			appName:      "wordpress-01",
			requestJson:  "rollback_application.json",
			initObjects:  "init_applications.json",
			expectStatus: http.StatusNoContent,
		},
		{
			name:            "new rollback ok",
			appName:         "wordpress-01",
			requestJson:     "rollback_application.json",
			initObjects:     "init_applications.json",
			enableNGFeature: true,
			expectStatus:    http.StatusNoContent,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodPut, "fake", tt.requestJson, tt.initObjects)
			if tt.enableNGFeature {
				defer fgtest.SetFeatureGateDuringTest(t, utilfeature.DefaultFeatureGate, features.ApplicationNG, true)()
			}
			fr.Request.PathParameters()["namespace"] = "default"
			fr.Request.PathParameters()["name"] = tt.appName
			status, err := r.RollbackApplication(fr.Request, fr.Response)
			assert.NilError(t, err)
			if status != tt.expectStatus {
				t.Errorf("Expect status %d, got %d, resp: %s", tt.expectStatus, status, fr.ReturnedBody())
				return
			}
		})
	}
}

func TestREST_StartOrStopApplication(t *testing.T) {
	type fields struct {
		Handler rest.Handler
		config  *config.APIConfig
	}
	type args struct {
		req *restful.Request
		res *restful.Response
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &REST{
				Handler: tt.fields.Handler,
				config:  tt.fields.config,
			}
			a.StartOrStopApplication(tt.args.req, tt.args.res)
		})
	}
}

func TestREST_ChartPackage(t *testing.T) {
	type args struct {
		Name     string
		Version  string
		Revision *int
	}
	rev := 1
	tests := []struct {
		name         string
		appName      string
		args         args
		requestJson  string
		initObjects  string
		expectStatus int
		wantErr      bool
	}{
		{
			name:    "revision specified",
			appName: "wordpress-01",
			args: args{
				Name:     "wordpress",
				Version:  "v1.0.1",
				Revision: &rev,
			},
			requestJson:  "chart_package_application.json",
			initObjects:  "init_applications.json",
			expectStatus: http.StatusOK,
		},
		{
			name:    "no revision",
			appName: "wordpress-01",
			args: args{
				Name:    "wordpress",
				Version: "v1.0.1",
			},
			requestJson:  "chart_package_application.json",
			initObjects:  "init_applications.json",
			expectStatus: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r, fr := SetupREST(http.MethodPost, "fake", tt.requestJson, tt.initObjects, tt.args)
			fr.Request.PathParameters()["namespace"] = "default"
			fr.Request.PathParameters()["name"] = tt.appName
			status, err := r.ChartPackage(fr.Request, fr.Response)
			assert.NilError(t, err)
			if status != tt.expectStatus {
				t.Errorf("Expect status %d, got %d, resp: %s", tt.expectStatus, status, fr.ReturnedBody())
				return
			}
			if !tt.wantErr {
				resHeader := fr.Response.Header()
				assert.Equal(t, resHeader.Get("Content-Type"), "application/x-gzip")
				assert.Equal(t, resHeader.Get("Content-Disposition"),
					fmt.Sprintf("attachment; filename=\"%s-%s.tar.gz\"", tt.args.Name, tt.args.Version))
			}
		})
	}
}
