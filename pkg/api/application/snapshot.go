package application

import (
	"net/http"
	"strconv"
	"time"

	"alauda.io/archon/pkg/api/application/v1beta1"
	ah "alauda.io/archon/pkg/api/applicationhistory"
	"alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog"
)

// CreateSnapshot creates a snapshot of the current application as an application
// history. The state of current application and it's components will be fetched
// to generate the snapshot.
func (a *REST) CreateSnapshot(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	snapshot := &v1beta1.ApplicationSnapshot{}
	statusCode = http.StatusNoContent
	err = req.ReadEntity(snapshot)
	if err != nil {
		return
	}

	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	klog.V(1).Infof("Create a snapshot for application %s:%s", namespace, name)
	ri, client, err := a.ClientToolsFromRequest(req, namespace)
	if err != nil {
		return
	}

	appObj, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	app, err := UnstructuredToApplication(appObj)
	if err != nil {
		return
	}

	components, err := GetApplicationComponentsFromStorage(client, app)
	if err != nil {
		return
	}
	app.Spec.ComponentTemplates = components

	config := a.config
	domain := config.LabelBaseDomain

	ahs := NewHistoryStorage(HistoryStorageConfig{LabelBaseDomain: domain}, client)
	oldApp, err := ahs.ApplicationFromRevision(
		namespace,
		AppNameSelectorString(domain, app.Name, app.Namespace),
		AppHistoryRevision(domain, app))
	if err != nil && !errors.IsNotFound(err) {
		return
	}

	if oldApp != nil {
		// Since the old app from revision won't have an annotation of history revision,
		// we should add the annotation to the old app from the current app.
		kubernetes.AddAnnotations(oldApp,
			map[string]string{
				AppHistoryRevisionKey(domain): strconv.Itoa(AppHistoryRevision(domain, app)),
			})
	}

	history, err := ahs.CreateApplicationHistory(&HistoryResource{
		AppUID:            app.GetUID(),
		OldApp:            oldApp,
		NewApp:            app,
		CreationTimestamp: time.Now(),
		User:              kubernetes.CreatorOfObject(domain, appObj),
		ChangeCause:       snapshot.Spec.ChangeCause,
		RollbackFrom:      nil,
	})
	if err != nil {
		return
	}

	revision := ah.AppHistoryRevision(history)
	err = updateApplicationRevision(ri, appObj, domain, revision)
	return
}
