package api

import (
	"alauda.io/archon/pkg/api/application"
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/cronjob"
	"alauda.io/archon/pkg/api/feature_gate"
	fgv1 "alauda.io/archon/pkg/api/feature_gate/v1"
	fedapp "alauda.io/archon/pkg/api/federated/application"
	fedns "alauda.io/archon/pkg/api/federated/namespace"
	"alauda.io/archon/pkg/api/hpa"
	"alauda.io/archon/pkg/api/license"
	"alauda.io/archon/pkg/api/log"
	"alauda.io/archon/pkg/api/namespace"
	"alauda.io/archon/pkg/api/psp"
	"alauda.io/archon/pkg/api/resource"
	"alauda.io/archon/pkg/api/topology"
	k8s "alauda.io/archon/pkg/kubernetes"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/runtime"

	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
)

type ServiceRegisterFunc func(srv server.Server, config *config.APIConfig) *restful.WebService

// Server defines the API Server to run the archon-api.
type Server struct {
	Config config.APIConfig
}

// AddToSchemes may be used to add all resources defined in the project to a Scheme
var AddToSchemes runtime.SchemeBuilder

// AddToScheme adds all Resources to the Scheme
func AddToScheme(s *runtime.Scheme) error {
	return AddToSchemes.AddToScheme(s)
}

func init() {
	// Register the types with the Scheme so the components can map objects to GroupVersionKinds and back
	AddToSchemes = append(AddToSchemes, fgv1.SchemeBuilder.AddToScheme)
}

// NewServer returns an Server.
func NewServer() *Server {
	return &Server{
		Config: config.APIConfig{
			NewClientBuilder: k8s.DefaultClientBuilder,
		},
	}
}

// ApplyToServer applies web services to server.
func (a *Server) ApplyToServer(srv server.Server) error {
	logger := zap.L().Named("archon-api")
	a.Config.Server = srv
	a.Config.Logger = logger.Sugar()
	a.Config.RESTConfig = a.Config.RESTConfigFromServerManager

	cors := restful.CrossOriginResourceSharing{
		AllowedDomains: []string{".*"},
		AllowedMethods: []string{"GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders: []string{"DNT", "X-CustomHeader", "Keep-Alive", "User-Agent", "X-Requested-With", "If-Modified-Since", "Cache-Control", "Content-Type", "Authorization"},
		CookiesAllowed: true,
		Container:      srv.Container(),
	}
	services := []ServiceRegisterFunc{
		a.HealthService,
		a.V1Service,
		a.V1Resources,
		a.V1FeatureGate,
		a.V1License,
	}
	container := srv.Container()
	for _, svc := range services {
		container.Add(svc(srv, &a.Config))
	}
	container.Filter(cors.Filter)
	return nil
}

// V1Service returns the api v1 web services, with URL prefix
// "/acp/v1/kubernetes/".
func (a *Server) V1Service(srv server.Server, config *config.APIConfig) *restful.WebService {
	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	ws.Doc("APIs for archon")
	ws.Path("/acp/v1/kubernetes/")

	var routes []core.APIRoutesInterface
	routes = append(
		routes,
		application.NewREST(config),
		topology.NewREST(config),
		log.NewREST(config),
		cronjob.NewREST(config),
		namespace.NewREST(config),
		fedns.NewREST(config),
		fedapp.NewREST(config),
		hpa.NewREST(config),
		psp.NewREST(config),
	)

	for _, r := range routes {
		r.InstallRoutes(core.APIVersionV1, ws)
	}

	return ws
}

// V1Resources returns the api v1 resources web service,
// with URL prefix "/acp/v1/resources/".
func (a *Server) V1Resources(srv server.Server, config *config.APIConfig) *restful.WebService {
	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	ws.Doc("APIs for resources")
	ws.Path("/acp/v1/resources/")

	var routes []core.APIRoutesInterface
	routes = append(routes, resource.NewREST(config))
	for _, r := range routes {
		r.InstallRoutes(core.APIVersionV1, ws)
	}

	return ws
}

// V1FeatureGate returns the api v1 feature gate web service,
// with URL prefix "/fg/v1/".
func (a *Server) V1FeatureGate(srv server.Server, config *config.APIConfig) *restful.WebService {
	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	ws.Doc("APIs for feature gate")
	ws.Path("/fg/v1/")

	var routes []core.APIRoutesInterface
	routes = append(
		routes,
		feature_gate.NewFeatureGateREST(config),
		feature_gate.NewClusterFeatureGateREST(config))
	for _, r := range routes {
		r.InstallRoutes(core.APIVersionV1, ws)
	}

	return ws
}

// V1License returns the api v1 license web service,
// with URL prefix "/lic/v1/".
func (a *Server) V1License(srv server.Server, config *config.APIConfig) *restful.WebService {
	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	ws.Doc("APIs for license")
	ws.Path("/lic/v1/")

	var routes []core.APIRoutesInterface
	routes = append(
		routes,
		license.NewREST(config))
	for _, r := range routes {
		r.InstallRoutes(core.APIVersionV1, ws)
	}

	return ws
}

// HealthService returns the health check web service.
func (a *Server) HealthService(srv server.Server, config *config.APIConfig) *restful.WebService {
	gen := decorator.NewWSGenerator()
	ws := gen.New(srv)
	ws.Doc("APIs for archon")
	ws.Path("/")

	ws.Route(
		ws.GET("").To(func(_ *restful.Request, res *restful.Response) {
			res.Write([]byte("Thorasoh'cahp!"))
		}),
	)

	return ws
}
