package license

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

type LicenseSubjectMetadata struct {
	Name string `json:"name"`
	CFC  string `json:"cfc"`
}

type LicenseValidity struct {
	NotBefore time.Time `json:"notBefore"`
	NotAfter  time.Time `json:"notAfter"`
}

type LicenseSpec struct {
	Products []AlaudaProduct `json:"products"`
	Validity LicenseValidity `json:"validity"`
	Type     LicenseType     `json:"type"`
	Enabled  bool            `json:"enabled"`
}

type LicenseObject struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec LicenseSpec `json:"spec,omitempty"`
}

type LicenseObjectList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []LicenseObject `json:"items"`
}
