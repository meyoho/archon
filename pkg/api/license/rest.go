package license

import (
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"github.com/spf13/cast"
	"io/ioutil"
	"k8s.io/klog"
	"net/http"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.GET("/metadata").To(r.WrapError(r.GetMetadata)).
			Doc("Get the metadata of the platform including company name and CFC etc.").
			Returns(http.StatusOK, "success", LicenseSubjectMetadata{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.POST("/licenses").To(r.WrapError(r.ImportLicense)).
			Doc("Import the license into the platform.").
			Returns(http.StatusOK, "success", LicenseObject{}).
			Consumes(restful.MIME_OCTET).
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("/licenses").To(r.WrapError(r.ListLicenses)).
			Doc("List licenses in the platform.").
			Returns(http.StatusOK, "success", LicenseObjectList{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.DELETE("/licenses/{name}").To(r.WrapError(r.DeleteLicense)).
			Doc("Delete a license in the platform.").
			Returns(http.StatusNoContent, "success", nil),
	)
}

// GetMetadata returns the metadata of the platform, including company name and the CFC.
func (r *REST) GetMetadata(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	cb, err := r.cfg.ClientBuilderInCluster(req)
	if err != nil {
		return
	}

	cfc, err := getCFCFromCluster(cb)
	if err != nil {
		return
	}
	subject, err := getSubjectNameFromStorage(cb)
	if err != nil {
		return
	}
	res.WriteEntity(LicenseSubjectMetadata{
		Name: subject,
		CFC:  string(cfc),
	})
	return
}

func getCFCFromCluster(cb kubernetes.ClientBuilderInterface) (cfc []byte, err error) {
	c, err := cb.ClientFromResourceKind("Namespace", "")
	if err != nil {
		return
	}

	cfcns, err := c.Get(CFCNamespace, metav1.GetOptions{})
	if err != nil {
		return
	}

	sum := sha256.Sum256([]byte(cfcns.GetUID()))
	cfc = []byte(base64.StdEncoding.EncodeToString(sum[:]))
	return
}

func (r *REST) ImportLicense(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	licPEM, err := ioutil.ReadAll(req.Request.Body)
	if err != nil {
		return
	}

	cb, err := r.cfg.ClientBuilderInCluster(req)
	if err != nil {
		return
	}

	alic, err := AlaudaLicenseFromPEM(licPEM)
	if err != nil {
		err = NewBadLicenseError(BadLicenseReasonInvalid, err.Error())
		return
	}

	err = ValidateLicense(alic, cb)
	if err != nil {
		return
	}

	c, err := cb.ClientFromResourceKind("Secret", licNamespace)
	if err != nil {
		return
	}

	secret := NewLicenseSecret(alic)
	o, err := kubernetes.ObjectToUnstructured(secret)
	if err != nil {
		return
	}

	sObj, err := c.Create(o, metav1.CreateOptions{})
	if err != nil {
		return
	}

	res.WriteAsJson(SecretToLicenseObject(sObj, alic, nil))
	return
}

func getSubjectNameFromStorage(cb kubernetes.ClientBuilderInterface) (name string, err error) {
	c, err := cb.ClientFromResourceKind("Secret", licNamespace)
	if err != nil {
		return
	}
	secrets, err := c.List(metav1.ListOptions{
		LabelSelector: fmt.Sprintf("%s=", LabelLicense()),
	})
	if err != nil {
		return
	}
	if len(secrets.Items) == 0 {
		return
	}

	alic, err := AlaudaLicenseFromSecret(&secrets.Items[0])
	return alic.License.Subject.CommonName, nil
}

func ValidateLicense(alic *AlaudaLicense, cb kubernetes.ClientBuilderInterface) (err error) {
	if !alic.IgnoreCFC {
		cfc, errC := getCFCFromCluster(cb)
		if errC != nil {
			err = errC
			return
		}
		if alic.CFC != string(cfc) {
			err = NewBadLicenseError(BadLicenseReasonInvalid, "CFC verify failed")
			return
		}
	}

	if len(alic.Products) <= 0 {
		err = NewBadLicenseError(BadLicenseReasonInvalid, "Products is empty")
		return
	}

	if alic.License.SerialNumber == nil {
		err = NewBadLicenseError(BadLicenseReasonInvalid, "Serial number is null")
		return
	}

	errT := validateLicenseType(alic.Type)
	if errT != nil {
		err = NewBadLicenseError(BadLicenseReasonInvalid, "Invalid license type")
		return
	}

	err = verifyLicense(alic.License)
	if err != nil {
		if e, ok := err.(x509.CertificateInvalidError); ok {
			if e.Reason == x509.Expired {
				err = NewBadLicenseError(BadLicenseReasonExpired, e.Detail)
			} else {
				err = NewBadLicenseError(BadLicenseReasonInvalid, e.Detail)
			}
			return
		}
		err = NewBadLicenseError(BadLicenseReasonInvalid, err.Error())
		return
	}
	return
}

func (r *REST) ListLicenses(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	limit := req.QueryParameter("limit")
	continueV := req.QueryParameter("continue")

	cb, err := r.cfg.ClientBuilderInCluster(req)
	if err != nil {
		return
	}

	c, err := cb.ClientFromResourceKind("Secret", licNamespace)
	if err != nil {
		return
	}
	secrets, err := c.List(metav1.ListOptions{
		LabelSelector: fmt.Sprintf("%s=", LabelLicense()),
		Limit:         cast.ToInt64(limit),
		Continue:      continueV,
	})
	if err != nil {
		return
	}

	lics := &LicenseObjectList{}
	lics.APIVersion = "alauda.io/v1"
	lics.Kind = "LicenseList"
	lics.Continue = secrets.GetContinue()
	lics.ResourceVersion = secrets.GetResourceVersion()
	lics.RemainingItemCount = secrets.GetRemainingItemCount()
	for _, s := range secrets.Items {
		alic, errs := AlaudaLicenseFromSecret(&s)
		if alic == nil && errs != nil {
			klog.Errorf("License parse with %q error: %v", s.GetName(), errs)
			continue
		}
		errs = ValidateLicense(alic, cb)
		l := SecretToLicenseObject(&s, alic, errs)
		lics.Items = append(lics.Items, *l)
	}
	res.WriteAsJson(lics)
	return
}

func (r *REST) DeleteLicense(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusNoContent
	name := req.PathParameter("name")

	cb, err := r.cfg.ClientBuilderInCluster(req)
	if err != nil {
		return
	}

	c, err := cb.ClientFromResourceKind("Secret", licNamespace)
	if err != nil {
		return
	}
	err = c.Delete(name, &metav1.DeleteOptions{})
	if err != nil {
		return
	}
	return
}
