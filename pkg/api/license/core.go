package license

import (
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"net/http"

	"alauda.io/archon/pkg/kubernetes"

	"github.com/spf13/cast"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

var (
	CFCExtensionOID         = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 2}
	IgnoreCFCExtensionOID   = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 3}
	ProductsExtensionOID    = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 4}
	LicenseTypeExtensionOID = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 5}
)

type LicenseType string

const (
	OfficialLicense LicenseType = "Official"
	DemoLicense                 = "Demo"
	InternalLicense             = "Internal"
)

const (
	licNamespace = "kube-public"
	certPEM      = `-----BEGIN CERTIFICATE-----
MIICXzCCAcgCCQCtFXcR0lUbXjANBgkqhkiG9w0BAQUFADBzMQswCQYDVQQGEwJD
TjEQMA4GA1UECAwHQmVpamluZzEQMA4GA1UEBwwHQmVpamluZzEPMA0GA1UECgwG
QWxhdWRhMQ8wDQYDVQQDDAZBbGF1ZGExHjAcBgkqhkiG9w0BCQEWD2FkbWluQGFs
YXVkYS5pbzAgFw0yMDAyMDMwODAwNTBaGA8yMTIwMDExMDA4MDA1MFowczELMAkG
A1UEBhMCQ04xEDAOBgNVBAgMB0JlaWppbmcxEDAOBgNVBAcMB0JlaWppbmcxDzAN
BgNVBAoMBkFsYXVkYTEPMA0GA1UEAwwGQWxhdWRhMR4wHAYJKoZIhvcNAQkBFg9h
ZG1pbkBhbGF1ZGEuaW8wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMauPQpE
DkK56IR57Yl3J3iZFPe8IR3bx1pguRrBhuzKBDBzAF6IbIjiFpooRgdWqE+C4870
8EOiHPNMzF2o0WJ+FNhB7Sw0q8CZMZdUNu+xbWhkQIw1f1eZnhKVgkM0F9RwJU74
4JmwMLZ7zrLzyCrPsBI+e6Cujj6++4ag78tFAgMBAAEwDQYJKoZIhvcNAQEFBQAD
gYEAvQYkJl9ycXxRC6+YfkxeUSGNGDEId97UyLYpT3FSK4RGZk01cSLvBWFAqyn3
DYkjRUaYY6A06Ie/aw085mGymCtV71dweZpVgpDKw4gPDblWdaP5X3XfMjl9zWM7
Jd+R/19Yg7oef/JcdtHrsM0g8PUeR6p+RL41jyWG+jDuq/o=
-----END CERTIFICATE-----`
)

var rootLicense *x509.Certificate

var (
	RootLicenseNotFoundError = fmt.Errorf("root license not found")
	DecodePEMError           = fmt.Errorf("decode pem error")
	ParseLicenseError        = fmt.Errorf("failed to parse license")
	DecodeProductsError      = fmt.Errorf("decode products data error")
	InvalidLicenseTypeError  = fmt.Errorf("invalid license type")
)

type AlaudaProduct struct {
	Name string `json:"name"`
}

type AlaudaLicense struct {
	License   *x509.Certificate
	Type      LicenseType
	CFC       string
	Products  []AlaudaProduct
	IgnoreCFC bool
}

func init() {
	rootLicense, _ = parseLicensePEM([]byte(certPEM))
}

func parseLicensePEM(pemStr []byte) (cert *x509.Certificate, err error) {
	block, _ := pem.Decode(pemStr)
	if block == nil {
		return nil, DecodePEMError
	}

	return x509.ParseCertificate(block.Bytes)
}

func verifyLicense(lic *x509.Certificate) (err error) {
	if rootLicense == nil {
		return RootLicenseNotFoundError
	}

	roots := x509.NewCertPool()
	roots.AddCert(rootLicense)
	opts := x509.VerifyOptions{
		Roots: roots,
	}

	_, err = lic.Verify(opts)
	return err
}

func validateLicenseType(lt LicenseType) (err error) {
	switch lt {
	case OfficialLicense, DemoLicense, InternalLicense:
		return
	default:
		return InvalidLicenseTypeError
	}
	return
}

func NewAlaudaLicense(lic *x509.Certificate) (alic *AlaudaLicense, err error) {
	alic = &AlaudaLicense{
		License: lic,
		Type:    OfficialLicense,
	}

	for _, ext := range lic.Extensions {
		switch {
		case ext.Id.Equal(CFCExtensionOID):
			var cfc []byte
			_, err = asn1.Unmarshal(ext.Value, &cfc)
			alic.CFC = string(cfc)
		case ext.Id.Equal(IgnoreCFCExtensionOID):
			var ignore []byte
			_, err = asn1.Unmarshal(ext.Value, &ignore)
			if err == nil {
				alic.IgnoreCFC = cast.ToBool(string(ignore))
				continue
			}
			_, err = asn1.Unmarshal(ext.Value, &alic.IgnoreCFC)
		case ext.Id.Equal(ProductsExtensionOID):
			var p []byte
			_, err = asn1.Unmarshal(ext.Value, &p)
			products := struct {
				Products []AlaudaProduct `json:"products"`
			}{}
			err = json.Unmarshal(p, &products)
			if err != nil {
				alic = nil
				return
			} else {
				alic.Products = products.Products
			}
		case ext.Id.Equal(LicenseTypeExtensionOID):
			var t []byte
			_, err = asn1.Unmarshal(ext.Value, &t)
			if err != nil {
				alic = nil
			} else if err = validateLicenseType(LicenseType(t)); err == nil {
				alic.Type = LicenseType(t)
			}
		}
		if err != nil {
			alic = nil
			return
		}
	}
	return
}

func AlaudaLicenseFromPEM(licPEM []byte) (alic *AlaudaLicense, err error) {
	lic, err := parseLicensePEM(licPEM)
	if err != nil {
		return
	}

	return NewAlaudaLicense(lic)
}

func (alic *AlaudaLicense) SerialNumberString() string {
	return alic.License.SerialNumber.Text(16)
}

func NewBadLicenseError(reason string, message string) *errors.StatusError {
	return &errors.StatusError{
		ErrStatus: metav1.Status{
			Status:  metav1.StatusFailure,
			Code:    http.StatusBadRequest,
			Reason:  metav1.StatusReason(reason),
			Message: message,
		}}
}

func NewLicenseSecret(alic *AlaudaLicense) (secret *v1.Secret) {
	secret = &v1.Secret{}
	secret.Kind = "Secret"
	secret.APIVersion = "v1"
	sn := alic.SerialNumberString()
	secret.SetName(fmt.Sprintf("license-%s", sn))
	secret.SetNamespace(licNamespace)
	secret.SetAnnotations(map[string]string{
		SerialNumberAnnotation(): sn,
	})
	secret.SetLabels(map[string]string{
		LabelLicense(): "",
	})
	secret.Data = map[string][]byte{
		"license": alic.License.Raw,
	}
	return
}

func AlaudaLicenseFromSecret(secret *unstructured.Unstructured) (alic *AlaudaLicense, err error) {
	rawLic, ok, err := unstructured.NestedFieldCopy(secret.Object, "data", "license")
	if err != nil {
		return
	}
	if !ok {
		return
	}

	rawLicByte, err := base64.StdEncoding.DecodeString(rawLic.(string))
	if err != nil {
		return
	}

	lic, err := x509.ParseCertificate(rawLicByte)
	if err != nil {
		return
	}

	return NewAlaudaLicense(lic)
}

func QueryValidLicenseForProduct(cb kubernetes.ClientBuilderInterface, product string) (alic *AlaudaLicense, err error) {
	c, err := cb.ClientFromResourceKind("Secret", licNamespace)
	if err != nil {
		return
	}
	secrets, err := c.List(metav1.ListOptions{
		LabelSelector: fmt.Sprintf("%s=", LabelLicense()),
	})
	if err != nil {
		return
	}

	// Default error
	err = NewBadLicenseError(BadLicenseReasonInvalid, "License not found")
	for _, s := range secrets.Items {
		al, errs := AlaudaLicenseFromSecret(&s)
		if errs != nil {
			err = errs
			continue
		}
		found := false
		for _, p := range al.Products {
			if p.Name == product {
				found = true
				break
			}
		}
		if !found {
			continue
		}
		errs = ValidateLicense(al, cb)
		if errs != nil {
			err = errs
			continue
		}
		alic = al
		err = nil
		break
	}
	return
}

func SecretToLicenseObject(secret *unstructured.Unstructured, alic *AlaudaLicense, err error) (lo *LicenseObject) {
	lo = &LicenseObject{}
	lo.APIVersion = "alauda.io/v1"
	lo.Kind = "License"
	lo.Name = secret.GetName()
	if err != nil {
		lo.Annotations = map[string]string{
			ReasonLicenseInvalidAnnotation(): ReasonFromError(err),
		}
	}
	lo.CreationTimestamp = secret.GetCreationTimestamp()
	lo.Spec.Enabled = err == nil
	lo.Spec.Products = alic.Products
	lo.Spec.Type = alic.Type
	lo.Spec.Validity.NotBefore = alic.License.NotBefore
	lo.Spec.Validity.NotAfter = alic.License.NotAfter
	return
}

func ReasonFromError(err error) (reason string) {
	if err == nil {
		return
	}
	if errStatus, ok := err.(errors.APIStatus); ok {
		reason = string(errStatus.Status().Reason)
	} else {
		reason = BadLicenseReasonInvalid
	}
	return
}
