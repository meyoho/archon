package license

import (
	"fmt"

	"alauda.io/archon/pkg/api/config"
)

const (
	CFCNamespace = "kube-system"
)

const (
	BadLicenseReasonInvalid = "Invalid"
	BadLicenseReasonExpired = "Expired"
)

func SerialNumberAnnotation() string {
	return fmt.Sprintf("license.%s/serial-number", config.GetConfig().LabelBaseDomain)
}

func LabelLicense() string {
	return fmt.Sprintf("%s/license", config.GetConfig().LabelBaseDomain)
}

func ReasonLicenseInvalidAnnotation() string {
	return fmt.Sprintf("license.%s/reason", config.GetConfig().LabelBaseDomain)
}

func LicenseProductAnnotation() string {
	return fmt.Sprintf("license.%s/product", config.GetConfig().LabelBaseDomain)
}

func LicenseTypeAnnotation() string {
	return fmt.Sprintf("license.%s/type", config.GetConfig().LabelBaseDomain)
}
