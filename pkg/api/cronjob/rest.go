package cronjob

import (
	"net/http"

	batchv1 "k8s.io/api/batch/v1"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.POST("{cluster}/cronjobs/{namespace}/{name}/exec").To(r.WrapError(r.CreateJobFromCronJob)).
			Doc("Create Job from CronJob to run").
			Param(restful.PathParameter("namespace", "namespace").DataType("string")).
			Param(restful.PathParameter("name", "cronjob name").DataType("string")).
			Param(restful.PathParameter("cluster", "cluster name").DataType("string")).
			Returns(200, "OK", batchv1.Job{}).
			Produces(restful.MIME_JSON),
	)
}

// CreateJobFromCronJob create a job from cronjob template and to run
func (r *REST) CreateJobFromCronJob(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	c, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	cronjob, err := queryCronJob(c, name, namespace)
	if err != nil {
		return
	}

	kc, err := c.KubeClient()
	if err != nil {
		return
	}

	jobVersion, err := kc.GetApiResourceByKindInsensitive("job")
	if err != nil {
		return
	}

	cronjobVersion, err := kc.GetApiResourceByKindInsensitive("cronjob")
	if err != nil {
		return
	}

	job, err := kubernetes.CreateJobFromCronJob(cronjob, jobVersion, cronjobVersion)
	if err != nil {
		return
	}

	runjob, err := executeJob(c, job, namespace)
	if err != nil {
		return
	}
	res.WriteAsJson(runjob)
	return
}

//executeJob will create job and run
func executeJob(c kubernetes.ClientBuilderInterface, job *unstructured.Unstructured, namespace string) (runjob *unstructured.Unstructured, err error) {
	dynamicClient, err := c.ClientFromResourceKind("job", namespace)
	if err != nil {
		return
	}
	runjob, err = dynamicClient.Create(job, v1.CreateOptions{})
	return
}

//queryCronJob query CronJob and return unstructured
func queryCronJob(c kubernetes.ClientBuilderInterface, name, namespace string) (cronjob *unstructured.Unstructured, err error) {
	dynamicClient, err := c.ClientFromResourceKind("cronjob", namespace)
	if err != nil {
		return
	}
	cronjob, err = dynamicClient.Get(name, v1.GetOptions{})
	return
}
