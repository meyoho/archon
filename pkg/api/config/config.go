package config

import (
	"context"
	"net"
	"os"

	k8s "alauda.io/archon/pkg/kubernetes"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/rest"
	restclient "k8s.io/client-go/rest"
)

var config *APIConfig

func SetConfig(cfg APIConfig) {
	config = &cfg
}

func GetConfig() APIConfig {
	return *config
}

type RESTConfigFunc func(req *restful.Request) (*rest.Config, error)

// APIConfig is the configuration of the APIServer.
type APIConfig struct {
	LabelBaseDomain  string
	AlaudaNamespace  string
	Server           server.Server
	RESTConfig       RESTConfigFunc
	Logger           *zap.SugaredLogger
	NewClientBuilder k8s.ClientBuilderNewFunc
}

// RESTConfigFromServerManager returns rest GetConfig from server manager.
func (ac *APIConfig) RESTConfigFromServerManager(req *restful.Request) (*rest.Config, error) {
	return ac.Server.GetManager().Config(req)
}

// RESTConfigFromInternalCluster
func (ac *APIConfig) RESTConfigFromInternalCluster(req *restful.Request) (*rest.Config, error) {
	return restclient.InClusterConfig()
}

// ClientBuilderFromRequest creates a dynamic KubeClient from a Request.
func (ac *APIConfig) ClientBuilderFromRequest(req *restful.Request) (k8s.ClientBuilderInterface, error) {
	return ac.ClientBuilderFromRequestAndCluster(req, req.PathParameter("cluster"))
}

// ClientBuilderFromRequestAndCluster creates a dynamic KubeClient from a Request and cluster.
func (ac *APIConfig) ClientBuilderFromRequestAndCluster(req *restful.Request, cluster string) (k8s.ClientBuilderInterface, error) {
	oldCluster := req.PathParameter("cluster")
	req.PathParameters()["cluster"] = cluster
	cfg, err := ac.RESTConfig(req)
	if err != nil {
		return nil, errors.NewInternalError(err)
	}

	req.PathParameters()["cluster"] = oldCluster
	cfg.TLSClientConfig.Insecure = true
	// If the insecure flag is true, the root ca should be nil.
	cfg.CAFile = ""
	cfg.CAData = nil
	return ac.NewClientBuilder(cfg, cluster), nil
}

// ClientBuilderInCluster returns client builder of the cluster which the pod is running on.
func (ac *APIConfig) ClientBuilderInCluster(req *restful.Request) (k8s.ClientBuilderInterface, error) {
	cfg, err := ac.RESTConfig(req)
	if err != nil {
		return nil, errors.NewInternalError(err)
	}
	host, port := os.Getenv("KUBERNETES_SERVICE_HOST"), os.Getenv("KUBERNETES_SERVICE_PORT")
	if len(host) == 0 || len(port) == 0 {
		return nil, restclient.ErrNotInCluster
	}
	cfg.Host = "https://" + net.JoinHostPort(host, port)
	cfg.TLSClientConfig.Insecure = true
	// If the insecure flag is true, the root ca should be nil.
	cfg.CAFile = ""
	cfg.CAData = nil

	return ac.NewClientBuilder(cfg, ""), nil
}

func FakeAPIConfig(objects ...runtime.Object) *APIConfig {
	fb := k8s.NewFakeKubeClientBuilder(objects...)
	logger, _ := zap.NewDevelopment()
	config := &APIConfig{
		LabelBaseDomain:  "alauda.io",
		RESTConfig:       k8s.FakeRESTConfig,
		Logger:           logger.Sugar(),
		NewClientBuilder: fb.FakeClientBuilder,
	}
	return config
}

type ManagerConfig struct {
	GroupKind       metav1.GroupKind
	Cluster         string
	LabelBaseDomain string
	Context         context.Context
	ClientBuilder   k8s.ClientBuilderInterface
}
