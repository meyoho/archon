package config

import (
	"fmt"
	"testing"

	"alauda.io/archon/pkg/test"

	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	clientrest "k8s.io/client-go/rest"
)

func TestAPIConfigRESTConfigFromServerManager(t *testing.T) {
	fakeServer := test.NewFakeServer()
	options := &test.FakeClientOptions{}
	options.ApplyToServer(fakeServer)
	tests := []struct {
		name    string
		Server  server.Server
		req     *restful.Request
		wantErr bool
	}{
		{
			name:   "normal",
			Server: fakeServer,
			req:    test.FakeRestfulRequest(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ac := &APIConfig{
				Server: tt.Server,
			}
			got, err := ac.RESTConfigFromServerManager(tt.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("APIConfig.RESTConfigFromServerManager() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil && got == nil {
				t.Errorf("Expect not nil rest.GetConfig")
				return
			}
		})
	}
}

func TestAPIConfigClientBuilderFromRequest(t *testing.T) {
	config := FakeAPIConfig()
	tests := []struct {
		name    string
		req     *restful.Request
		wantErr bool
	}{
		{
			name: "normal",
			req:  test.FakeRestfulRequest(),
		},
		{
			name:    "error config",
			req:     test.FakeRestfulRequest(),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.wantErr {
				config.RESTConfig = func(req *restful.Request) (*clientrest.Config, error) {
					return nil, fmt.Errorf("error")
				}
			}
			got, err := config.ClientBuilderFromRequest(tt.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("APIConfig.ClientBuilderFromRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil && got == nil {
				t.Errorf("Unexpected nil ClientBuilder")
				return
			}
		})
	}
}
