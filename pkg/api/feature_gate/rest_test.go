package feature_gate

import (
	"alauda.io/archon/pkg/kubernetes"
	"testing"

	v1 "alauda.io/archon/pkg/api/feature_gate/v1"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func TestResolveFeatureGateDependency(t *testing.T) {
	type args struct {
		feature              *v1.AlaudaFeatureGate
		features             map[string]*unstructured.Unstructured
		disabledFeatureStage v1.AlaudaFeatureGateStage
	}
	features := make(map[string]*v1.AlaudaFeatureGate)
	features["a"] = &v1.AlaudaFeatureGate{}
	features["a"].Name = "a"
	features["a"].Spec.Enabled = true
	features["a"].Spec.Description = "feature a"
	features["a"].Spec.Stage = v1.Alpha
	features["a"].Spec.Dependency.FeatureGates = []string{"b", "c"}
	features["a"].Spec.Dependency.Type = v1.AllDenpendencies
	features["b"] = &v1.AlaudaFeatureGate{}
	features["b"].Name = "b"
	features["b"].Spec.Enabled = true
	features["b"].Spec.Dependency.Type = v1.AnyDenpendency
	features["b"].Spec.Dependency.FeatureGates = []string{"c", "d", "nil"}
	features["b"].Spec.Description = "feature b"
	features["b"].Spec.Stage = v1.Beta
	features["c"] = &v1.AlaudaFeatureGate{}
	features["c"].Name = "c"
	features["c"].Spec.Enabled = false
	features["c"].Spec.Description = "feature c"
	features["c"].Spec.Stage = v1.Beta
	features["d"] = &v1.AlaudaFeatureGate{}
	features["d"].Name = "d"
	features["d"].Spec.Enabled = true
	features["d"].Spec.Description = "feature d"
	features["d"].Spec.Stage = v1.Alpha
	features["e"] = &v1.AlaudaFeatureGate{}
	features["e"].Name = "e"
	features["e"].Spec.Enabled = true
	features["e"].Spec.Dependency.Type = v1.AllDenpendencies
	features["e"].Spec.Dependency.FeatureGates = []string{"f"}
	features["e"].Spec.Description = "feature e"
	features["e"].Spec.Stage = v1.Alpha
	features["f"] = &v1.AlaudaFeatureGate{}
	features["f"].Name = "f"
	features["f"].Spec.Enabled = true
	features["f"].Spec.Dependency.Type = v1.AllDenpendencies
	features["f"].Spec.Dependency.FeatureGates = []string{"g"}
	features["f"].Spec.Description = "feature f"
	features["f"].Spec.Stage = v1.Alpha
	features["g"] = &v1.AlaudaFeatureGate{}
	features["g"].Name = "g"
	features["g"].Spec.Enabled = true
	features["g"].Spec.Dependency.Type = v1.AllDenpendencies
	features["g"].Spec.Dependency.FeatureGates = []string{"e"}
	features["g"].Spec.Description = "feature g"
	features["g"].Spec.Stage = v1.Alpha
	features["h"] = &v1.AlaudaFeatureGate{}
	features["h"].Name = "h"
	features["h"].Spec.Enabled = true
	features["h"].Spec.Dependency.Type = v1.AllDenpendencies
	features["h"].Spec.Dependency.FeatureGates = []string{"nil"}
	features["h"].Spec.Description = "feature h"
	features["h"].Spec.Stage = v1.Alpha
	features["i"] = &v1.AlaudaFeatureGate{}
	features["i"].Name = "i"
	features["i"].Spec.Enabled = true
	features["i"].Spec.Dependency.Type = "invalid"
	features["i"].Spec.Dependency.FeatureGates = []string{"a"}
	features["i"].Spec.Description = "feature i"
	features["i"].Spec.Stage = v1.Alpha
	features["beta"] = &v1.AlaudaFeatureGate{}
	features["beta"].Name = v1.BetaFeaturesGateName
	features["beta"].Spec.Enabled = true
	features["beta"].Spec.Description = "global feature Beta"
	features["beta"].Spec.Stage = v1.Beta
	featuresUnstruct := make(map[string]*unstructured.Unstructured)
	for k, v := range features {
		v.Kind = "AlaudaFeatureGate"
		v.APIVersion = "alauda.io/v1"
		f, _ := kubernetes.ObjectToUnstructured(v)
		featuresUnstruct[k] = f
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "all dependency",
			args: args{
				feature:  features["a"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "any dependency",
			args: args{
				feature:  features["b"],
				features: featuresUnstruct,
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "loop dependency",
			args: args{
				feature:  features["e"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "Beta feature stage enabled",
			args: args{
				feature:              features["c"],
				disabledFeatureStage: v1.BetaFeaturesGateName,
				features:             featuresUnstruct,
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "feature not found with all dep",
			args: args{
				feature:  features["h"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: true,
		},
		{
			name: "invalid dep type",
			args: args{
				feature:  features["i"],
				features: featuresUnstruct,
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.args.disabledFeatureStage != "" {
				unstructured.SetNestedField(
					tt.args.features[string(tt.args.disabledFeatureStage)].Object,
					false, "spec", "enabled")
			}
			got, err := ResolveFeatureGateState(tt.args.feature, tt.args.features, make(map[string]bool), make(map[string]bool))
			if (err != nil) != tt.wantErr {
				t.Errorf("ResolveFeatureGateState() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ResolveFeatureGateState() = %v, want %v", got, tt.want)
			}
		})
	}
}
