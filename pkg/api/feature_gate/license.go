package feature_gate

import (
	"k8s.io/klog"
	"net/http"

	v1 "alauda.io/archon/pkg/api/feature_gate/v1"
	"alauda.io/archon/pkg/api/license"

	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var DisableLicense = "false"

func (r *FeatureGateREST) GetLicenseFeatureGate(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	product := req.QueryParameter("product")

	cb, err := r.config.ClientBuilderInCluster(req)
	if err != nil {
		return
	}

	alic, err := license.QueryValidLicenseForProduct(cb, product)
	invalidReason := ""
	enabled := true
	if err != nil {
		klog.Errorf("Query valid license for product %q error: %v", product, err)
		invalidReason = license.ReasonFromError(err)
		err = nil
		enabled = false
	}
	licFG := &v1.AlaudaFeatureGate{
		TypeMeta: metav1.TypeMeta{
			APIVersion: v1.AlaudaFeatureGateAPIVersion,
			Kind:       v1.AlaudaFeatureGateKind,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "License",
			Annotations: map[string]string{
				license.LicenseProductAnnotation(): product,
			},
		},
		Spec: v1.AlaudaFeatureGateSpec{
			Description: "License",
			Stage:       "GA",
			Enabled:     enabled,
		},
		Status: v1.AlaudaFeatureGateStatus{
			Enabled: enabled,
		},
	}
	if alic != nil {
		licFG.Annotations[license.LicenseTypeAnnotation()] = string(alic.Type)
	}
	if invalidReason != "" {
		licFG.Annotations[license.ReasonLicenseInvalidAnnotation()] = invalidReason
	}
	res.WriteAsJson(licFG)
	return
}
