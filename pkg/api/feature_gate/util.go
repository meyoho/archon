package feature_gate

import (
	"alauda.io/archon/pkg/api/config"
	v1 "alauda.io/archon/pkg/api/feature_gate/v1"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/klog"
)

// UnstructuredToAlaudaFeatureGate converts "Unstructured" type object to "AlaudaFeatureGate" type.
func UnstructuredToAlaudaFeatureGate(obj *unstructured.Unstructured) (*v1.AlaudaFeatureGate, error) {
	o := &v1.AlaudaFeatureGate{}
	err := k8s.UnstructuredToObject(obj, o)
	if err != nil {
		return nil, err
	}
	o.APIVersion = v1.AlaudaFeatureGateAPIVersion
	o.Kind = v1.AlaudaFeatureGateKind
	return o, nil
}

// UnstructuredToClusterAlaudaFeatureGate converts "Unstructured" type object to "ClusterAlaudaFeatureGate" type.
func UnstructuredToClusterAlaudaFeatureGate(obj *unstructured.Unstructured) (*v1.ClusterAlaudaFeatureGate, error) {
	o := &v1.ClusterAlaudaFeatureGate{}
	err := k8s.UnstructuredToObject(obj, o)
	if err != nil {
		return nil, err
	}
	o.APIVersion = v1.ClusterAlaudaFeatureGateAPIVersion
	o.Kind = v1.ClusterAlaudaFeatureGateKind
	return o, nil
}

func FeatureGateOverrideKey(domain string) string {
	return "feature-gate." + domain + "/override"
}

//GetClusterFeatureGateAPI returns the cluster scope feature gate with the runtime state.
//It's method that get cluster scope feature  in archon
func GetClusterFeatureGateAPI(cluster, name string, req *restful.Request, cfg *config.APIConfig) (*v1.ClusterAlaudaFeatureGate, error) {
	klog.V(1).Infof("Get cluster feature gate %q in cluster %q", name, cluster)

	ftMap, err := getClusterFeatureGatesMap(cluster, req, cfg)
	if err != nil {
		return nil, err
	}

	f, ok := ftMap[name]
	if !ok {
		err = errors.NewNotFound(schema.GroupResource{
			Group:    v1.AlaudaFeatureGateGroup,
			Resource: v1.ClusterAlaudaFeatureGateResource,
		}, name)
		return nil, err
	}

	fObj, err := UnstructuredToClusterAlaudaFeatureGate(f)
	if err != nil {
		return nil, err
	}

	enabled, err := ResolveFeatureGateState((*v1.AlaudaFeatureGate)(fObj), ftMap, make(map[string]bool), make(map[string]bool))
	if err != nil {
		klog.Warningf("Resolve feature gate error: %v, assume the feature gate is disabled", err)
		err = nil
	}

	fObj.Status.Enabled = enabled
	return fObj, nil
}

func getClusterFeatureGatesMap(cluster string, req *restful.Request, cfg *config.APIConfig) (map[string]*unstructured.Unstructured, error) {

	cb, err := cfg.ClientBuilderFromRequestAndCluster(req, cluster)
	if err != nil {
		return nil, err
	}

	ri, err := cb.NamespacedDynamicClient(v1.ClusterAlaudaFeatureGateResource, v1.ClusterAlaudaFeatureGateAPIVersion, "")
	if err != nil {
		return nil, err
	}

	clusterFeatures, err := ri.List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	ftMap, err := getGlobalFeatureGatesMap(req, cfg)
	if err != nil {
		return nil, err
	}

	for _, f := range clusterFeatures.Items {
		f := f
		// Add 'alauda.io.feature-gate/override: true' label to the overridden feature gate by cluster.
		k8s.AddLabels(&f, map[string]string{
			FeatureGateOverrideKey(cfg.LabelBaseDomain): "true",
		})
		ftMap[f.GetName()] = &f
	}
	return ftMap, nil
}

func getGlobalFeatureGatesMap(req *restful.Request, cfg *config.APIConfig) (map[string]*unstructured.Unstructured, error) {
	cb, err := cfg.ClientBuilderInCluster(req)
	if err != nil {
		return nil, err
	}

	ri, err := cb.NamespacedDynamicClient(
		v1.AlaudaFeatureGateResource,
		v1.AlaudaFeatureGateAPIVersion,
		cfg.AlaudaNamespace)
	if err != nil {
		return nil, err
	}

	features, err := ri.List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	ftMap := make(map[string]*unstructured.Unstructured)
	for _, f := range features.Items {
		f := f
		ftMap[f.GetName()] = &f
	}
	return ftMap, nil
}
