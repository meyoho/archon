package v1

import (
	"k8s.io/apimachinery/pkg/runtime"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

const (
	ClusterAlaudaFeatureGateAPIVersion = "alauda.io/v1"
	ClusterAlaudaFeatureGateKind       = "ClusterAlaudaFeatureGate"
	ClusterAlaudaFeatureGateListKind   = "ClusterAlaudaFeatureGateList"
	ClusterAlaudaFeatureGateResource   = "clusteralaudafeaturegates"
)

// ClusterAlaudaFeatureGate
// +k8s:openapi-gen=true
type ClusterAlaudaFeatureGate AlaudaFeatureGate

func (in *ClusterAlaudaFeatureGate) DeepCopyObject() runtime.Object {
	return (*AlaudaFeatureGate)(in).DeepCopyObject()
}

type ClusterAlaudaFeatureGateList AlaudaFeatureGateList
