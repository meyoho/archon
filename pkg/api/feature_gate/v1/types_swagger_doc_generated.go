package v1

var map_AlaudaFeatureGate = map[string]string{
	"": "AlaudaFeatureGate describes a feature gate of the alauda product.",
}

func (AlaudaFeatureGate) SwaggerDoc() map[string]string {
	return map_AlaudaFeatureGate
}

var map_AlaudaFeatureGateDependency = map[string]string{
	"featureGates": "FeatureGates is the names of feature gates that depends on.",
	"type":         "Type is the type of dependency, should be one of \"all\" or \"any\".",
}

func (AlaudaFeatureGateDependency) SwaggerDoc() map[string]string {
	return map_AlaudaFeatureGateDependency
}

var map_AlaudaFeatureGateSpec = map[string]string{
	"":            "AlaudaFeatureGateSpec defines the desired state of AlaudaFeatureGate.",
	"description": "Description is a human readable text of the feature gate.",
	"stage":       "Stage is the stage of the feature gate, should be one of \"Alpha\", \"Beta\", \"GA\" or \"EOF\".",
	"dependency":  "Dependency is the denpendencies of the feature gate.",
	"enabled":     "Enabled indicates if the feature gate is enabled manually.",
}

func (AlaudaFeatureGateSpec) SwaggerDoc() map[string]string {
	return map_AlaudaFeatureGateSpec
}

var map_AlaudaFeatureGateStatus = map[string]string{
	"":        "AlaudaFeatureGateStatus defines the observed state of AlaudaFeatureGate",
	"enabled": "Enabled indicates if the feature gate is enabled at runtime.",
}

func (AlaudaFeatureGateStatus) SwaggerDoc() map[string]string {
	return map_AlaudaFeatureGateStatus
}
