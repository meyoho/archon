package feature_gate

import (
	"fmt"
	"net/http"
	"strings"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	v1 "alauda.io/archon/pkg/api/feature_gate/v1"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/klog"
)

// FeatureGateREST handles the feature gate rest request.
type FeatureGateREST struct {
	rest.Handler
	config *config.APIConfig
}

// ClusterFeatureGateREST handles the cluster feature gate rest request.
type ClusterFeatureGateREST struct {
	FeatureGateREST
}

// NewFeatureGateREST returns an feature gate FeatureGateREST object.
func NewFeatureGateREST(cfg *config.APIConfig) *FeatureGateREST {
	return &FeatureGateREST{
		config: cfg,
		Handler: *rest.NewHandler(
			cfg,
			nil,
			nil,
			func() runtime.Object { return &v1.AlaudaFeatureGate{} },
			func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    "alaudafeaturegates.alauda.io",
					Version:  "v1",
					Resource: "alaudafeaturegates",
				}
			},
			func() bool { return true },
		),
	}
}

// NewClusterFeatureGateREST returns an feature gate FeatureGateREST object.
func NewClusterFeatureGateREST(cfg *config.APIConfig) *ClusterFeatureGateREST {
	return &ClusterFeatureGateREST{
		FeatureGateREST: FeatureGateREST{
			config: cfg,
			Handler: *rest.NewHandler(
				cfg,
				nil,
				nil,
				func() runtime.Object { return &v1.ClusterAlaudaFeatureGate{} },
				func() schema.GroupVersionResource {
					return schema.GroupVersionResource{
						Group:    "clusteralaudafeaturegates.alauda.io",
						Version:  "v1",
						Resource: "clusteralaudafeaturegates",
					}
				},
				func() bool { return false },
			),
		},
	}
}

// InstallRoutes installs feature gates REST APIs.
func (r *FeatureGateREST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	nameParam := ws.PathParameter("name", "The name of the feature gate.").
		DataType("string").Required(true)
	ws.Route(
		ws.GET("featuregates").To(r.WrapError(r.ListFeatureGate)).
			Doc("List global feature gates.").
			Returns(http.StatusOK,
				"List feature gates success.",
				v1.AlaudaFeatureGateList{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("featuregates/license").To(r.WrapError(r.GetLicenseFeatureGate)).
			Doc("Get the license feature gate.").
			Returns(http.StatusOK,
				"Get the license feature gate success.",
				v1.AlaudaFeatureGate{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("featuregates/{name}").To(r.WrapError(r.GetFeatureGate)).
			Doc("Get a global feature gate.").
			Param(nameParam).
			Returns(http.StatusOK,
				"Get feature gate success.",
				v1.AlaudaFeatureGate{}).
			Produces(restful.MIME_JSON),
	)
}

// InstallRoutes installs cluster feature gates REST APIs.
func (r *ClusterFeatureGateREST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	clusterParam := ws.PathParameter("cluster", "The cluster of the feature gate.").
		DataType("string").Required(true)
	nameParam := ws.PathParameter("name", "The name of the feature gate.").
		DataType("string").Required(true)
	ws.Route(
		ws.GET("{cluster}/featuregates").To(r.WrapError(r.ListClusterFeatureGate)).
			Doc("List cluster scope feature gates.").
			Param(clusterParam).
			Returns(http.StatusOK,
				"List feature gates success.",
				v1.ClusterAlaudaFeatureGate{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.GET("{cluster}/featuregates/{name}").To(r.WrapError(r.GetClusterFeatureGate)).
			Doc("Get a cluster scope feature gate.").
			Param(clusterParam).
			Param(nameParam).
			Returns(http.StatusOK,
				"Get feature gate success.",
				v1.ClusterAlaudaFeatureGate{}).
			Produces(restful.MIME_JSON),
	)
}

func (r *FeatureGateREST) getGlobalFeatureGatesMap(req *restful.Request) (map[string]*unstructured.Unstructured, error) {
	cb, err := r.config.ClientBuilderInCluster(req)
	if err != nil {
		return nil, err
	}

	ri, err := cb.NamespacedDynamicClient(
		v1.AlaudaFeatureGateResource,
		v1.AlaudaFeatureGateAPIVersion,
		r.config.AlaudaNamespace)
	if err != nil {
		return nil, err
	}

	features, err := ri.List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	ftMap := make(map[string]*unstructured.Unstructured)
	for _, f := range features.Items {
		f := f
		ftMap[f.GetName()] = &f
	}
	return ftMap, nil
}

func (r *FeatureGateREST) getClusterFeatureGatesMap(domain string, req *restful.Request, cb k8s.ClientBuilderInterface) (map[string]*unstructured.Unstructured, error) {
	ri, err := cb.NamespacedDynamicClient(v1.ClusterAlaudaFeatureGateResource, v1.ClusterAlaudaFeatureGateAPIVersion, "")
	clusterFeatures, err := ri.List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	ftMap, err := r.getGlobalFeatureGatesMap(req)
	if err != nil {
		return nil, err
	}

	for _, f := range clusterFeatures.Items {
		f := f
		// Add 'alauda.io.feature-gate/override: true' label to the overridden feature gate by cluster.
		k8s.AddLabels(&f, map[string]string{
			FeatureGateOverrideKey(domain): "true",
		})
		ftMap[f.GetName()] = &f
	}
	return ftMap, nil
}

// ListFeatureGate returns the list of all registered global feature gates with the runtime state.
func (r *FeatureGateREST) ListFeatureGate(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK

	klog.V(1).Infof("List global feature gate")

	ftMap, err := r.getGlobalFeatureGatesMap(req)
	if err != nil {
		return
	}

	resolvedFeatures := &v1.AlaudaFeatureGateList{
		Items: []v1.AlaudaFeatureGate{},
	}
	resolvedFeatures.APIVersion = v1.AlaudaFeatureGateAPIVersion
	resolvedFeatures.Kind = v1.AlaudaFeatureGateListKind

	resolved := make(map[string]bool)
	for name, f := range ftMap {
		fObj, errI := UnstructuredToAlaudaFeatureGate(f)
		if errI != nil {
			err = errI
			return
		}

		_, ok := resolved[name]
		if !ok {
			var enabled bool
			enabled, err = ResolveFeatureGateState(fObj, ftMap, make(map[string]bool), resolved)
			if err != nil {
				klog.Warningf("Resolve feature gate error: %v, assume the feature gate is disabled", err)
				err = nil
			}
			resolved[name] = enabled
		}

		fObj.Status.Enabled = resolved[name]
		resolvedFeatures.Items = append(resolvedFeatures.Items, *fObj)
	}

	res.WriteAsJson(resolvedFeatures)
	return
}

// GetFeatureGate returns the feature gate with the resolved runtime state.
func (r *FeatureGateREST) GetFeatureGate(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	name := req.PathParameter("name")

	klog.V(1).Infof("Get global feature gate %q %s", name, DisableLicense)

	ftMap, err := r.getGlobalFeatureGatesMap(req)
	if err != nil {
		return
	}

	f, ok := ftMap[name]
	if !ok {
		err = errors.NewNotFound(r.ResourceFunc().GroupResource(), name)
		return
	}

	fObj, err := UnstructuredToAlaudaFeatureGate(f)
	if err != nil {
		return
	}

	enabled, err := ResolveFeatureGateState(fObj, ftMap, make(map[string]bool), make(map[string]bool))
	if err != nil {
		klog.Warningf("Resolve feature gate error: %v, assume the feature gate is disabled", err)
		err = nil
	}

	fObj.Status.Enabled = enabled
	res.WriteAsJson(fObj)
	return
}

// ListClusterFeatureGate returns the list of all registered global feature gates with the runtime state.
func (r *ClusterFeatureGateREST) ListClusterFeatureGate(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	cluster := req.PathParameter("cluster")

	klog.V(1).Infof("List cluster feature gates in cluster %q", cluster)
	_, cb, err := r.ClientToolsFromRequest(req, "")
	if err != nil {
		return
	}

	ftMap, err := r.getClusterFeatureGatesMap(r.config.LabelBaseDomain, req, cb)
	if err != nil {
		return
	}

	resolvedFeatures := &v1.ClusterAlaudaFeatureGateList{
		Items: []v1.AlaudaFeatureGate{},
	}
	resolvedFeatures.APIVersion = v1.ClusterAlaudaFeatureGateAPIVersion
	resolvedFeatures.Kind = v1.ClusterAlaudaFeatureGateListKind

	resolved := make(map[string]bool)
	for name, f := range ftMap {
		fObj, errI := UnstructuredToClusterAlaudaFeatureGate(f)
		if errI != nil {
			err = errI
			return
		}

		_, ok := resolved[name]
		if !ok {
			var enabled bool
			enabled, err = ResolveFeatureGateState((*v1.AlaudaFeatureGate)(fObj), ftMap, make(map[string]bool), resolved)
			if err != nil {
				klog.Warningf("Resolve feature gate error: %v, assume the feature gate is disabled", err)
				err = nil
			}
			resolved[name] = enabled
		}

		fObj.Status.Enabled = resolved[name]
		resolvedFeatures.Items = append(resolvedFeatures.Items, *(*v1.AlaudaFeatureGate)(fObj))
	}

	res.WriteAsJson(resolvedFeatures)
	return
}

// GetClusterFeatureGate returns the cluster scope feature gate with the runtime state.
func (r *ClusterFeatureGateREST) GetClusterFeatureGate(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	cluster := req.PathParameter("cluster")
	name := req.PathParameter("name")

	klog.V(1).Infof("Get cluster feature gate %q in cluster %q", name, cluster)
	_, cb, err := r.ClientToolsFromRequest(req, "")
	if err != nil {
		return
	}

	ftMap, err := r.getClusterFeatureGatesMap(r.config.LabelBaseDomain, req, cb)
	if err != nil {
		return
	}

	f, ok := ftMap[name]
	if !ok {
		err = errors.NewNotFound(schema.GroupResource{
			Group:    v1.AlaudaFeatureGateGroup,
			Resource: v1.ClusterAlaudaFeatureGateResource,
		}, name)
		return
	}

	fObj, err := UnstructuredToClusterAlaudaFeatureGate(f)
	if err != nil {
		return
	}

	enabled, err := ResolveFeatureGateState((*v1.AlaudaFeatureGate)(fObj), ftMap, make(map[string]bool), make(map[string]bool))
	if err != nil {
		klog.Warningf("Resolve feature gate error: %v, assume the feature gate is disabled", err)
		err = nil
	}

	fObj.Status.Enabled = enabled
	res.WriteAsJson(fObj)
	return
}

// ResolveFeatureGateState resolves the feature gate's enabled state by
// resolving its dependencies and global feature stage gate recursively.
//
// The dependency graph should not have circles, an error will return if it
// has one.
//
// Parameters:
//
// - feature: The feature gate to be resolved.
//
// - features: The registered feature list.
//
// - visited: The visited feature map used to detect the dependency circle. An empty map should be passed on the initial method call.
//
// - resolved: A map stores the feature name as the key, and it's resolved enabled state as value.
//
// It returns feature's enabled state and error encountered.
func ResolveFeatureGateState(
	feature *v1.AlaudaFeatureGate,
	features map[string]*unstructured.Unstructured,
	visited map[string]bool,
	resolved map[string]bool) (enabled bool, err error) {
	defer func() {
		if err == nil && enabled {
			resolved[feature.Name] = true
		} else {
			resolved[feature.Name] = false
		}
	}()

	if visited[feature.Name] {
		return false, errors.NewInternalError(fmt.Errorf("feature dependency circle detected at %q", feature.Name))
	}

	if _, ok := resolved[feature.Name]; ok {
		return resolved[feature.Name], nil
	}

	// If the stage feature gate is disabled, the feature gate is disabled
	// in spite of its 'enabled' value.
	featureStage := features[strings.ToLower(string(feature.Spec.Stage))]
	if featureStage != nil {
		fgObj, err := UnstructuredToAlaudaFeatureGate(featureStage)
		if err != nil {
			return false, err
		}
		if !fgObj.Spec.Enabled {
			return false, nil
		}
	}

	if !feature.Spec.Enabled {
		return false, nil
	}

	if len(feature.Spec.Dependency.FeatureGates) == 0 {
		return feature.Spec.Enabled, nil
	}

	visited[feature.Name] = true
	enabled = true

	switch feature.Spec.Dependency.Type {
	case v1.AllDenpendencies:
		enabled, err = resolveAllDependency(feature, features, visited, resolved)
	case v1.AnyDenpendency:
		enabled, err = resolveAnyDependency(feature, features, visited, resolved)
	default:
		enabled = false
		err = errors.NewInternalError(fmt.Errorf("invalid feature dependency type %q", feature.Spec.Dependency.Type))
	}
	visited[feature.Name] = false
	return
}

func resolveAllDependency(
	feature *v1.AlaudaFeatureGate,
	features map[string]*unstructured.Unstructured,
	visited map[string]bool,
	resolved map[string]bool) (enabled bool, err error) {
	for _, f := range feature.Spec.Dependency.FeatureGates {
		fg := features[f]
		if fg != nil {
			fgObj, errInternal := UnstructuredToAlaudaFeatureGate(fg)
			if errInternal != nil {
				enabled = false
				err = errInternal
				break
			}
			enabled, err = ResolveFeatureGateState(fgObj, features, visited, resolved)
			if err != nil || !enabled {
				break
			}
		} else {
			enabled = false
			err = errors.NewInternalError(fmt.Errorf("feature %q not found", f))
			break
		}
	}
	return enabled, err
}

func resolveAnyDependency(
	feature *v1.AlaudaFeatureGate,
	features map[string]*unstructured.Unstructured,
	visited map[string]bool,
	resolved map[string]bool) (enabled bool, err error) {
	for _, f := range feature.Spec.Dependency.FeatureGates {
		fg := features[f]
		if fg != nil {
			fgObj, errInternal := UnstructuredToAlaudaFeatureGate(fg)
			if errInternal != nil {
				enabled = false
				err = errInternal
				break
			}
			enabled, err = ResolveFeatureGateState(fgObj, features, visited, resolved)
			if err == nil && enabled {
				break
			}
		} else {
			continue
		}
	}
	return enabled, err
}
