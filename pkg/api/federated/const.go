package federated

import (
	"fmt"
)

const (
	FederatedTypePrefix        = "federated"
	FederatedTypesGroup        = "types.kubefed.io"
	FederatedTypesVersion      = "v1beta1"
	FederatedTypesGroupVersion = "types.kubefed.io/v1beta1"

	ClusterfedKind     = "Clusterfed"
	ClusterfedResource = "clusterfeds"
	ClusterfedGroup    = "infrastructure.alauda.io"
	ClusterfedVersion  = "v1alpha1"
)

func FederatedResourceType(resource string) string {
	return fmt.Sprintf("%s%s", FederatedTypePrefix, resource)
}
