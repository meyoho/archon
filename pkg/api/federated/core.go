package federated

import (
	"encoding/json"
	"fmt"
	"strings"

	"alauda.io/archon/pkg/api/config"
	k8s "alauda.io/archon/pkg/kubernetes"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/klog"
	ctlutil "sigs.k8s.io/kubefed/pkg/controller/util"
	"sigs.k8s.io/kubefed/pkg/kubefedctl/enable"
	"sigs.k8s.io/kubefed/pkg/kubefedctl/federate"
)

type FederatedResourceRESTConfig struct {
	Client            dynamic.ResourceInterface
	FederatedResource *unstructured.Unstructured
}

type Options struct {
	APIConfig   config.APIConfig
	Owner       *unstructured.Unstructured
	Labels      map[string]string
	Annotations map[string]string
	Placement   *FederatedTypePlacement
	Overrides   []FederatedTypeOverride
}

func NewFederatedResourceRESTConfig(cb k8s.ClientBuilderInterface, resource string, obj *unstructured.Unstructured, option *Options) (cfg *FederatedResourceRESTConfig, err error) {
	cfg = &FederatedResourceRESTConfig{}
	kc, err := cb.KubeClient()
	if err != nil {
		return
	}
	ar, err := kc.GetApiResourceByKind(obj.GetKind())
	if err != nil {
		return
	}

	k8s.AddLabels(obj, map[string]string{
		IsFederatedKey(option.APIConfig.LabelBaseDomain): "true",
	})
	typeConfig := enable.GenerateTypeConfigForTarget(*ar, enable.NewEnableTypeDirective())
	federatedResource, err := federate.FederatedResourceFromTargetResource(typeConfig, obj)
	if err != nil {
		return
	}

	c, err := cb.NamespacedDynamicClient(
		FederatedResourceType(resource), FederatedTypesGroupVersion, federatedResource.GetNamespace())
	if err != nil {
		return
	}

	cfg.Client = c

	var placement = FederatedTypePlacement{
		ClusterSelector: &metav1.LabelSelector{},
	}
	if option != nil && option.Placement != nil {
		placement = *option.Placement
	}
	b, _ := json.Marshal(placement)
	var pi interface{}
	json.Unmarshal(b, &pi)
	err = unstructured.SetNestedField(federatedResource.Object, pi, ctlutil.SpecField, ctlutil.PlacementField)
	if err != nil {
		return
	}
	if option != nil && option.Overrides != nil {
		var ovr []interface{}
		b, _ := json.Marshal(option.Overrides)
		json.Unmarshal(b, &ovr)
		err = unstructured.SetNestedField(federatedResource.Object, ovr, ctlutil.SpecField, ctlutil.OverridesField)
		if err != nil {
			return
		}
	}

	cfg.FederatedResource = federatedResource
	return
}

func GetFederatedResource(cb k8s.ClientBuilderInterface, kind string, name, namespace string) (obj *unstructured.Unstructured, err error) {
	resource, err := k8s.ResourceTypeForKind(cb, kind)
	if err != nil {
		return
	}
	c, err := cb.NamespacedDynamicClient(
		FederatedResourceType(resource), FederatedTypesGroupVersion, namespace)
	if err != nil {
		return
	}
	return c.Get(name, metav1.GetOptions{})
}

func ApplyOptions(o *unstructured.Unstructured, options *Options) {
	if options != nil {
		if options.Owner != nil {
			k8s.AddOwnerReference(o, options.Owner)
		}
		if options.Labels != nil {
			k8s.AddLabels(o, options.Labels)
		}
		if options.Annotations != nil {
			k8s.AddAnnotations(o, options.Annotations)
		}
	}
}

func CreateFederatedResource(
	cb k8s.ClientBuilderInterface, obj *unstructured.Unstructured, options *Options) (fObj *unstructured.Unstructured, err error) {
	resource, err := k8s.ResourceTypeForKind(cb, obj.GetKind())
	if err != nil {
		return
	}
	cfg, err := NewFederatedResourceRESTConfig(cb, resource, obj, options)
	if err != nil {
		return
	}
	ApplyOptions(cfg.FederatedResource, options)

	return cfg.Client.Create(cfg.FederatedResource, metav1.CreateOptions{})
}

func UpdateFederatedResource(
	cb k8s.ClientBuilderInterface, obj *unstructured.Unstructured, options *Options) (fObj *unstructured.Unstructured, err error) {
	resource, err := k8s.ResourceTypeForKind(cb, obj.GetKind())
	if err != nil {
		return
	}
	cfg, err := NewFederatedResourceRESTConfig(cb, resource, obj, options)
	if err != nil {
		return
	}
	old, err := GetFederatedResource(cb, obj.GetKind(), obj.GetName(), cfg.FederatedResource.GetNamespace())
	if err != nil {
		return
	}
	cfg.FederatedResource.SetResourceVersion(old.GetResourceVersion())
	ApplyOptions(cfg.FederatedResource, options)

	return cfg.Client.Update(cfg.FederatedResource, metav1.UpdateOptions{})
}

func DeleteFederatedResource(
	cb k8s.ClientBuilderInterface, kind string, name, namespace string) (err error) {
	resource, err := k8s.ResourceTypeForKind(cb, kind)
	if err != nil {
		return
	}
	c, err := cb.NamespacedDynamicClient(
		FederatedResourceType(resource), FederatedTypesGroupVersion, namespace)
	if err != nil {
		return
	}
	propagationPolicy := metav1.DeletePropagationBackground
	return c.Delete(name, &metav1.DeleteOptions{
		PropagationPolicy: &propagationPolicy,
	})
}

func ListFederatedResource(cb k8s.ClientBuilderInterface, kind, namespace string, options metav1.ListOptions) (obj *unstructured.UnstructuredList, err error) {
	resource, err := k8s.ResourceTypeForKind(cb, kind)
	if err != nil {
		return
	}
	c, err := cb.NamespacedDynamicClient(
		FederatedResourceType(resource), FederatedTypesGroupVersion, namespace)
	if err != nil {
		return
	}
	return c.List(options)
}

func KindFromFederatedKind(federatedKind string) string {
	return strings.Replace(federatedKind, "Federated", "", -1)
}

// CreateOrUpdateFederatedResource updates the resource as a federated resource and create a new one if it's not existed.
func CreateOrUpdateFederatedResource(obj *unstructured.Unstructured, cb k8s.ClientBuilderInterface, options *Options) (nobj *unstructured.Unstructured, err error) {
	// If obj is nil, do nothing.
	if obj == nil {
		return nil, nil
	}

	// Updates the resource object and returns the created or updated one. If it's not found, will create it.
	nobj, err = UpdateFederatedResource(cb, obj, options)
	if err != nil {
		if errors.IsNotFound(err) {
			nobj, err = CreateFederatedResource(cb, obj, options)
			if err != nil {
				klog.Errorf("create or update resource error: %v", err)
				return
			}
			return
		}
		klog.Errorf("create or update resource error: %v", err)
		return
	}
	return
}

func GetFederatedHostCluster(cluster string) (fed *unstructured.Unstructured, host string, err error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return
	}

	di, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return
	}
	client := di.Resource(schema.GroupVersionResource{
		Group:    ClusterfedGroup,
		Version:  ClusterfedVersion,
		Resource: ClusterfedResource,
	})
	feds, err := client.List(metav1.ListOptions{})
	if err != nil {
		return
	}
	for _, f := range feds.Items {
		f := f
		clusters, _, _ := unstructured.NestedSlice(f.Object, "spec", "clusters")
		host = ""
		found := false
		for _, c := range clusters {
			c := c.(map[string]interface{})
			if c["name"] == cluster {
				found = true
			}
			if c["type"] == "Host" {
				host = c["name"].(string)
			}
		}
		if found {
			if host == "" {
				return nil, "", errors.NewInternalError(fmt.Errorf("host cluster not found for %q", cluster))
			} else {
				fed = &f
				return
			}
		}
	}
	return nil, "", errors.NewBadRequest(fmt.Sprintf("the cluster %q is not a member of federate cluster", cluster))
}

func FederationLabels(hostname, fedname string) map[string]string {
	domain := config.GetConfig().LabelBaseDomain
	return map[string]string{
		fmt.Sprintf("federation.%s/hostname", domain): hostname,
		fmt.Sprintf("federation.%s/name", domain):     fedname,
		fmt.Sprintf("federation.%s/override", domain): "false",
	}
}
