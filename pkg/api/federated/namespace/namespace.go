package namespace

import (
	"errors"
	"fmt"
	"time"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/federated"
	"alauda.io/archon/pkg/api/namespace"
	k8s "alauda.io/archon/pkg/kubernetes"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/klog"
)

func ValidateFederatedNamespaceRequest(fn *AlaudaFederatedNamespace) (err error) {
	spec := fn.Spec

	if spec.Namespace == nil || spec.Namespace.Resource == nil {
		err = errors.New("FederatedNamespace is nil")
		return
	}

	if spec.ResourceQuota != nil && spec.ResourceQuota.Resource != nil && spec.Namespace.Resource.GetName() != spec.ResourceQuota.Resource.GetNamespace() {
		err = errors.New("ResourceQuota needs to comply with the Namespace restrictions")
		return
	}

	if spec.LimitRange != nil && spec.LimitRange.Resource != nil && spec.Namespace.Resource.GetName() != spec.LimitRange.Resource.GetNamespace() {
		err = errors.New("LimitRange needs to comply with the Namespace restrictions")
		return
	}
	return
}

//CreateFederatedNamespace will create AlaudaFederatedNamespace
func CreateFederatedNamespace(fn *AlaudaFederatedNamespace, cluster string, cb k8s.ClientBuilderInterface, cfg *config.APIConfig) (
	cns *AlaudaFederatedNamespace, err error) {
	cns = &AlaudaFederatedNamespace{}
	spec := fn.Spec

	err = ValidateFederatedNamespaceRequest(fn)
	if err != nil {
		return
	}

	if namespace.IsBuiltInNamespace(spec.Namespace.Resource.GetName()) {
		err = errors.New(fmt.Sprintf("this is a builtin namespace %s, skip", spec.Namespace.Resource.GetName()))
		return
	}

	ns, err := federated.CreateFederatedResource(
		cb, fn.Spec.Namespace.Resource, &federated.Options{
			APIConfig:   *cfg,
			Overrides:   fn.Spec.Namespace.Overrides,
			Labels:      fn.Labels,
			Annotations: fn.Annotations,
		})

	if err != nil {
		return
	}

	cns.Spec.Namespace = &FederatedNamespaceResource{}
	cns.Spec.Namespace.Resource = ns

	backoff := wait.Backoff{
		Duration: time.Millisecond * 200,
		Steps:    5,
	}
	err = k8s.RetryOnError(backoff, func() error {
		retryErr := CreateOrUpdateNamespaceSubResource(fn, cns, cluster, cb, cfg)
		if retryErr != nil {
			klog.Warningf("CreateOrUpdateNamespaceSubResource err: %v will retry", err)
			return retryErr
		}
		klog.Warningf("CreateOrUpdateNamespaceSubResource success")
		return nil
	})
	return
}

//UpdateFederatedNamespace will update AlaudaFederatedNamespace
func UpdateFederatedNamespace(fn *AlaudaFederatedNamespace, cluster string, cb k8s.ClientBuilderInterface, cfg *config.APIConfig) (cns *AlaudaFederatedNamespace, err error) {
	cns = &AlaudaFederatedNamespace{}

	err = ValidateFederatedNamespaceRequest(fn)
	if err != nil {
		return
	}

	ns, err := federated.UpdateFederatedResource(
		cb, fn.Spec.Namespace.Resource, &federated.Options{
			APIConfig:   *cfg,
			Overrides:   fn.Spec.Namespace.Overrides,
			Labels:      fn.Labels,
			Annotations: fn.Annotations,
		})
	if err != nil {
		return
	}
	cns.Spec.Namespace = &FederatedNamespaceResource{}
	cns.Spec.Namespace.Resource = ns

	backoff := wait.Backoff{
		Duration: time.Millisecond * 200,
		Steps:    5,
	}
	err = k8s.RetryOnError(backoff, func() error {
		retryErr := CreateOrUpdateNamespaceSubResource(fn, cns, cluster, cb, cfg)
		if retryErr != nil {
			klog.Warningf("CreateOrUpdateNamespaceSubResource err: %v will retry", err)
			return retryErr
		}
		klog.Warningf("CreateOrUpdateNamespaceSubResource success")
		return nil
	})
	return
}

func CreateOrUpdateNamespaceSubResource(fn, ofn *AlaudaFederatedNamespace, cluster string, cb k8s.ClientBuilderInterface, cfg *config.APIConfig) (err error) {
	// CreateOrUpdate resource quota
	if fn.Spec.ResourceQuota != nil {
		t := templateFromOverride(cluster, fn.Spec.ResourceQuota)
		if t != nil {
			err := unstructured.SetNestedField(fn.Spec.ResourceQuota.Resource.Object, t, "spec", "hard")
			if err != nil {
				klog.Errorf("Set the template from override error: %v", err)
			}
		}
		var nrs *unstructured.Unstructured
		nrs, err = federated.CreateOrUpdateFederatedResource(
			fn.Spec.ResourceQuota.Resource, cb, &federated.Options{
				APIConfig: *cfg,
				Overrides: fn.Spec.ResourceQuota.Overrides,
			})
		if err != nil {
			return
		}
		ofn.Spec.ResourceQuota = &FederatedNamespaceResource{}
		ofn.Spec.ResourceQuota.Resource = nrs
	}

	// CreateOrUpdate limit range
	if fn.Spec.LimitRange != nil {
		t := templateFromOverride(cluster, fn.Spec.LimitRange)
		if t != nil {
			err := unstructured.SetNestedField(fn.Spec.LimitRange.Resource.Object, []interface{}{t}, "spec", "limits")
			if err != nil {
				klog.Errorf("Set the template from override error: %v", err)
			}
		}
		var nlr *unstructured.Unstructured
		nlr, err = federated.CreateOrUpdateFederatedResource(
			fn.Spec.LimitRange.Resource, cb, &federated.Options{
				APIConfig: *cfg,
				Overrides: fn.Spec.LimitRange.Overrides,
			})
		if err != nil {
			return
		}
		ofn.Spec.LimitRange = &FederatedNamespaceResource{}
		ofn.Spec.LimitRange.Resource = nlr
	}
	return
}

func templateFromOverride(cluster string, fnr *FederatedNamespaceResource) map[string]interface{} {
	// Use overrides from host cluster as the template in federated resource.
	for _, ovr := range fnr.Overrides {
		if ovr.ClusterName == cluster {
			if len(ovr.ClusterOverrides) == 0 {
				klog.Warningf("No cluster overrides found in %q", ovr.ClusterName)
				break
			}
			// The value of override must be a map[string]interface{} type.
			return ovr.ClusterOverrides[0].Value.(map[string]interface{})
		}
	}
	return nil
}
