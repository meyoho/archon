package namespace

import (
	"fmt"
	"net/http"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/federated"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.POST("{cluster}/federatednamespaces").To(r.WrapError(r.CreateAlaudaFederatedNamespace)).
			Doc("Create a federated namespace with LimitRange and ResourceQuota").
			Param(restful.QueryParameter("project_name", "project name")).
			Param(restful.PathParameter("cluster", "cluster name")).
			Returns(201, "success", AlaudaFederatedNamespace{}),
	).Route(
		ws.PUT("{cluster}/federatednamespaces/{name}").To(r.WrapError(r.UpdateAlaudaFederatedNamespace)).
			Doc("Update a federated namespace with LimitRange and ResourceQuota").
			Param(restful.QueryParameter("project_name", "project name")).
			Param(restful.PathParameter("cluster", "cluster name")).
			Param(restful.PathParameter("name", "namespace name")).
			Returns(200, "success", AlaudaFederatedNamespace{}),
	).Route(
		ws.POST("{cluster}/namespaces/{name}/federate").To(r.WrapError(r.FederateNamespace)).
			Doc("Federate a namespace with it's LimitRange and ResourceQuota").
			Param(restful.PathParameter("cluster", "cluster name")).
			Param(restful.PathParameter("name", "namespace name")).
			Consumes("*/*").
			Returns(201, "success", AlaudaFederatedNamespace{}),
	)
}

// CreateAlaudaFederatedNamespace will create a federated namespace with federated limit range and resource quota.
func (r *REST) CreateAlaudaFederatedNamespace(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	cluster := req.PathParameter("cluster")
	fn := AlaudaFederatedNamespace{}
	err = req.ReadEntity(&fn)
	if err != nil {
		return
	}

	if project_name := req.QueryParameter("project_name"); project_name != "" {
		nsLabels := map[string]string{
			fmt.Sprintf("%v/project", r.cfg.LabelBaseDomain): project_name,
		}
		fn.Spec.Namespace.Resource.SetLabels(nsLabels)
		kubernetes.AddLabels(&fn, nsLabels)
	}

	cb, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	c, err := cb.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: "",
		Kind:  "Namespace",
	}, "")
	if err != nil {
		return
	}

	_, err = c.Create(fn.Spec.Namespace.Resource, metav1.CreateOptions{})
	if err != nil {
		return
	}

	cfn, err := CreateFederatedNamespace(&fn, cluster, cb, r.cfg)
	if err != nil {
		return
	}
	res.WriteAsJson(cfn)
	return
}

// UpdateAlaudaFederatedNamespace will update GeneralNamespace
func (r *REST) UpdateAlaudaFederatedNamespace(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	cluster := req.PathParameter("cluster")
	fn := AlaudaFederatedNamespace{}
	err = req.ReadEntity(&fn)
	if err != nil {
		return
	}

	if project_name := req.QueryParameter("project_name"); project_name != "" {
		nsLabels := map[string]string{
			fmt.Sprintf("%v/project", r.cfg.LabelBaseDomain): project_name,
		}
		fn.Spec.Namespace.Resource.SetLabels(nsLabels)
		kubernetes.AddLabels(&fn, nsLabels)
	}

	cb, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	newGn, err := UpdateFederatedNamespace(&fn, cluster, cb, r.cfg)
	if err != nil {
		return
	}
	res.WriteAsJson(newGn)
	return
}

// FederateNamespace federate a namespace.
func (r *REST) FederateNamespace(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusCreated
	cluster := req.PathParameter("cluster")
	name := req.PathParameter("name")

	fed, host, err := federated.GetFederatedHostCluster(cluster)
	if err != nil {
		return
	}

	cb, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	fn, err := r.getAlaudaFederatedNamespace(req, cb, fed, name, host)
	if err != nil {
		return
	}

	hostcb := cb
	if host != cluster {
		// The source namespace is not located in host cluster, should
		// create or update a namespace on the host cluster.
		hostcb, err = r.cfg.ClientBuilderFromRequestAndCluster(req, host)
		if err != nil {
			return
		}
		err = r.ensureNamespaceForFederated(hostcb, fn.Spec.Namespace.Resource)
		if err != nil {
			return
		}
	}

	newGn, err := CreateFederatedNamespace(fn, cluster, hostcb, r.cfg)
	if err != nil {
		return
	}
	res.WriteAsJson(newGn)
	return
}

func (r *REST) getAlaudaFederatedNamespace(
	req *restful.Request,
	cb kubernetes.ClientBuilderInterface,
	fed *unstructured.Unstructured, name, host string) (fn *AlaudaFederatedNamespace, err error) {
	fn = &AlaudaFederatedNamespace{}
	c, err := cb.ClientFromResourceKind("Namespace", "")
	if err != nil {
		return
	}

	ns, err := c.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}
	fn.Name = name
	fn.Namespace = name
	fn.Spec.Namespace = &FederatedNamespaceResource{}
	fn.Spec.Namespace.Resource = ns
	labels := ns.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	for k, v := range federated.FederationLabels(host, fed.GetName()) {
		labels[k] = v
	}
	fn.SetLabels(labels)
	err = r.reserveNamespaceAnnotations(req, fn.Spec.Namespace, fed)
	if err != nil {
		return
	}

	c, err = cb.ClientFromResourceKind("ResourceQuota", name)
	if err != nil {
		return
	}

	rq, err := c.Get("default", metav1.GetOptions{})
	if err == nil {
		fn.Spec.ResourceQuota = &FederatedNamespaceResource{}
		fn.Spec.ResourceQuota.Resource = rq
	} else if !errors.IsNotFound(err) {
		return
	}

	c, err = cb.ClientFromResourceKind("LimitRange", name)
	if err != nil {
		return
	}

	lr, err := c.Get("default", metav1.GetOptions{})
	if err == nil {
		fn.Spec.LimitRange = &FederatedNamespaceResource{}
		fn.Spec.LimitRange.Resource = lr
		return
	} else if !errors.IsNotFound(err) {
		return
	}

	err = nil
	return
}

func (r *REST) ensureNamespaceForFederated(
	cb kubernetes.ClientBuilderInterface,
	srcNS *unstructured.Unstructured) (err error) {
	hostc, errC := cb.ClientFromResourceKind("Namespace", "")
	if errC != nil {
		err = errC
		return
	}
	destNS, errC := hostc.Get(srcNS.GetName(), metav1.GetOptions{})
	if errC != nil {
		if errors.IsNotFound(errC) {
			srcNS.SetResourceVersion("")
			_, err = hostc.Create(srcNS, metav1.CreateOptions{})
			if err != nil {
				return
			}
		} else if errors.IsAlreadyExists(errC) {
			srcNS.SetResourceVersion(destNS.GetResourceVersion())
			_, err = hostc.Update(srcNS, metav1.UpdateOptions{})
			if err != nil {
				return
			}
		} else {
			return
		}
	}
	return
}

// reserveNamespaceAnnotations reserves the annotations of the already existed namespace
// on the cluster members of the federation as the overrides.
func (r *REST) reserveNamespaceAnnotations(
	req *restful.Request,
	ns *FederatedNamespaceResource, fed *unstructured.Unstructured) (err error) {
	nsName := ns.Resource.GetName()
	clusters, _, _ := unstructured.NestedSlice(fed.Object, "spec", "clusters")
	for _, c := range clusters {
		c := c.(map[string]interface{})
		clusterName := c["name"].(string)
		cb, errI := r.cfg.ClientBuilderFromRequestAndCluster(req, clusterName)
		if errI != nil {
			err = errI
			return
		}
		ri, errI := cb.ClientFromResourceKind("Namespace", "")
		if errI != nil {
			err = errI
			return
		}
		nsc, errI := ri.Get(nsName, metav1.GetOptions{})
		if errI != nil {
			if errors.IsNotFound(errI) {
				// No the named namespace found on the cluster, continue.
				continue
			}
			err = errI
			return
		}

		ns.Overrides = append(ns.Overrides,
			federated.FederatedTypeOverride{
				ClusterName: clusterName,
				ClusterOverrides: []federated.FederatedTypeClusterOverride{
					{
						Path:  "/metadata/annotations",
						Value: nsc.GetAnnotations(),
					},
				},
			},
		)
	}
	return
}
