package namespace

import (
	"alauda.io/archon/pkg/api/federated"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

const (
	AlaudaFederatedNamespaceKind = "FederatedNamespace"
)

type AlaudaFederatedNamespace struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              FederatedNamespaceSpec `json:"spec,omitempty"`
}

type FederatedNamespaceResource struct {
	Resource  *unstructured.Unstructured        `json:"resource"`
	Overrides []federated.FederatedTypeOverride `json:"overrides"`
}

type FederatedNamespaceSpec struct {
	Namespace     *FederatedNamespaceResource `json:"namespace"`
	ResourceQuota *FederatedNamespaceResource `json:"resourcequota,omitempty"`
	LimitRange    *FederatedNamespaceResource `json:"limitrange,omitempty"`
}
