package federated

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type FederatedTypeClusterOverride struct {
	Op    string      `json:"op,omitempty"`
	Path  string      `json:"path,omitempty"`
	Value interface{} `json:"value,omitempty"`
}

// +k8s:deepcopy-gen=true
type FederatedTypeOverride struct {
	ClusterName      string                         `json:"clusterName"`
	ClusterOverrides []FederatedTypeClusterOverride `json:"clusterOverrides"`
}

// +k8s:deepcopy-gen=true
type FederatedTypeCluster struct {
	Name string `json:"name"`
}

// +k8s:deepcopy-gen=true
type FederatedTypePlacement struct {
	ClusterSelector *metav1.LabelSelector  `json:"clusterSelector,omitempty"`
	Clusters        []FederatedTypeCluster `json:"clusters,omitempty"`
}

// +k8s:deepcopy-gen=true
type FederatedTypeSpec struct {
	Placement FederatedTypePlacement    `json:"placement"`
	Overrides []FederatedTypeOverride   `json:"overrides"`
	Template  unstructured.Unstructured `json:"template"`
}

// +k8s:deepcopy-gen=true
type FederatedType struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              FederatedTypeSpec `json:"spec"`
}

func (in *FederatedTypeClusterOverride) DeepCopyInto(out *FederatedTypeClusterOverride) {
	*out = *in
	out.Op = in.Op
	out.Path = in.Path
	if in.Value != nil {
		switch v := in.Value.(type) {
		case map[string]interface{}:
			outValue := make(map[string]interface{})
			for k, vv := range v {
				outValue[k] = vv
			}
			out.Value = outValue
		default:
			out.Value = in.Value
		}
	}
	return
}

func (in *FederatedTypeClusterOverride) DeepCopy() *FederatedTypeClusterOverride {
	if in == nil {
		return nil
	}
	out := new(FederatedTypeClusterOverride)
	in.DeepCopyInto(out)
	return out
}

func IsFederatedKey(domain string) string {
	return domain + "/federated"
}
