package application

import (
	"alauda.io/archon/pkg/api/federated"
	"context"
	"fmt"
	"k8s.io/klog"

	"alauda.io/archon/pkg/api/application"
	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

type UpdateStrategy struct {
	application.UpdateStrategy
}

// PrepareForUpdate prepares the application update.
func (us UpdateStrategy) PrepareForUpdate(ctx context.Context, obj, old runtime.Object) error {
	newObj := obj.(*unstructured.Unstructured)
	oldObj := old.(*unstructured.Unstructured)
	newObj.SetResourceVersion(oldObj.GetResourceVersion())
	err := VisitApplicationTemplate(ctx, obj, func(ctx context.Context, appObj runtime.Object) interface{} {
		oldAppMap, _, _ := unstructured.NestedMap(oldObj.Object, "spec", "template")
		app := appObj.(*v1beta1.Application)
		app.SetName(oldObj.GetName())
		app.SetNamespace(oldObj.GetNamespace())
		return us.UpdateStrategy.PrepareForUpdate(ctx, appObj, &unstructured.Unstructured{Object: oldAppMap})
	})
	if err != nil {
		return err.(error)
	}
	return nil
}

// ValidateUpdate validates the application update request.
func (us UpdateStrategy) ValidateUpdate(ctx context.Context, obj, old runtime.Object) field.ErrorList {
	oldObj := old.(*unstructured.Unstructured)
	fApp := obj.(*unstructured.Unstructured)
	oldAppMap, _, _ := unstructured.NestedMap(oldObj.Object, "spec", "template")
	transformComponentsTemplate(fApp, ctx)
	err := VisitApplicationTemplate(ctx, obj, func(ctx context.Context, obj runtime.Object) interface{} {
		return us.UpdateStrategy.ValidateUpdate(ctx, obj, &unstructured.Unstructured{Object: oldAppMap})
	})
	if err != nil {
		return err.(field.ErrorList)
	}
	return nil
}

// AfterUpdate does some post update work, e.g. update the components, create history.
func (UpdateStrategy) AfterUpdate(ctx context.Context, obj, old runtime.Object) error {
	req, ok := rest.RequestFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get Request from context failed"))
	}
	config, ok := rest.APIConfigFromContext(ctx)
	if !ok {
		return errors.NewInternalError(fmt.Errorf("get APIConfig from context failed"))
	}

	cb, err := rest.ClientBuilderFromRequest(req)
	if err != nil {
		return err
	}

	federatedComponents, err := ComponentsFromRequest(req)
	if err != nil {
		return err
	}

	components, err := application.ComponentsFromRequest(req)
	if err != nil {
		return err
	}

	componentsDiff, err := application.ComponentsDiffFromRequest(req)
	if err != nil {
		return err
	}

	fedAppObj := obj.(*unstructured.Unstructured)
	ri, err := cb.NamespacedDynamicClient(federated.FederatedResourceType("applications"), federated.FederatedTypesGroupVersion, fedAppObj.GetNamespace())
	if err != nil {
		return errors.NewInternalError(err)
	}
	overrides, err := waitForAppliationPropagation(config, req, ri, fedAppObj)
	if err != nil {
		return err
	}

	nfc := mergeFederatedComponents(components, federatedComponents, overrides)
	unstructured.SetNestedSlice(fedAppObj.Object, nfc, "spec", "template", "spec", "componentTemplates")
	err = UpdateFederatedApplicationComponents(
		config, cb, ri, fedAppObj, componentsDiff, nfc)
	if err != nil {
		return err
	}
	return nil
}

func getFederatedApplicationComponents(cb k8s.ClientBuilderInterface, app *unstructured.Unstructured) (components []unstructured.Unstructured, err error) {
	cks, ok, err := unstructured.NestedSlice(app.Object, "spec", "template", "spec", "componentKinds")
	if err != nil {
		return
	}
	if !ok {
		return
	}
	for _, o := range cks {
		o := o.(map[string]interface{})
		var res []unstructured.Unstructured
		res, err = getFederatedAppComponentsForGroupKind(metav1.GroupKind{
			Group: o["group"].(string),
			Kind:  o["kind"].(string),
		}, app, cb)
		if err != nil {
			return
		}
		components = append(components, res...)
	}
	return
}

func getFederatedAppComponentsForGroupKind(o metav1.GroupKind, app *unstructured.Unstructured, cb k8s.ClientBuilderInterface) (components []unstructured.Unstructured, err error) {
	kc, err := cb.KubeClient()
	if err != nil {
		return
	}
	resource, err := kc.GetResourceTypeByKind(o.Kind)
	if err != nil {
		return
	}
	var namespaced bool
	namespaced, err = kc.IsNamespaceScoped(resource)
	if err != nil {
		return
	}
	namespace := app.GetNamespace()
	if !namespaced {
		namespace = ""
	}

	var res *unstructured.UnstructuredList
	appTpl, ok, err := unstructured.NestedMap(app.Object, "spec", "template")
	if err != nil {
		return
	}
	if !ok {
		return
	}

	res, err = federated.ListFederatedResource(cb, o.Kind, namespace, metav1.ListOptions{
		LabelSelector: application.AppNameSelectorStringFromAppUnstruct(&unstructured.Unstructured{Object: appTpl}),
	})
	if err != nil {
		return
	}
	for _, o := range res.Items {
		components = append(components, o)
	}

	return
}

func getApplicationComponentsFromStorage(cb k8s.ClientBuilderInterface, app *v1beta1.Application) (components []unstructured.Unstructured, err error) {
	defer func() {
		if err != nil {
			err = errors.NewInternalError(err)
		}
	}()
	for _, o := range app.Spec.ComponentGroupKinds {
		var res []unstructured.Unstructured
		res, err = getAppComponentsForGroupKind(o, app, cb)
		if err != nil {
			return
		}
		components = append(components, res...)
	}
	return
}

func getAppComponentsForGroupKind(o metav1.GroupKind, app *v1beta1.Application, cb k8s.ClientBuilderInterface) (components []unstructured.Unstructured, err error) {
	kc, err := cb.KubeClient()
	if err != nil {
		return
	}
	resource, err := kc.GetResourceTypeByKind(o.Kind)
	if err != nil {
		return
	}
	var namespaced bool
	namespaced, err = kc.IsNamespaceScoped(resource)
	if err != nil {
		return
	}
	namespace := app.GetNamespace()
	if !namespaced {
		namespace = ""
	}

	var res *unstructured.UnstructuredList
	res, err = federated.ListFederatedResource(cb, o.Kind, namespace, metav1.ListOptions{
		LabelSelector: application.AppNameSelectorStringFromApp(app),
	})
	if err != nil {
		return
	}
	for _, o := range res.Items {
		t, ok, errO := unstructured.NestedMap(o.Object, "spec", "template")
		if errO != nil {
			err = errO
			return
		}
		if !ok {
			klog.Errorf("No template found in federated resource: %+v", o)
			err = errors.NewInternalError(fmt.Errorf("no template found"))
			return
		}
		tplO := &unstructured.Unstructured{Object: t}
		tplO.SetKind(federated.KindFromFederatedKind(o.GetKind()))
		tplO.SetNamespace(o.GetNamespace())
		tplO.SetName(o.GetName())
		components = append(components, *tplO)
	}

	return
}
