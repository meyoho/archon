package application

import (
	"fmt"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
)

// Request Attributes
const (
	// FederatedApplicationComponentsAttribute is the requset attribute key of overrides
	// in application componentTemplates.
	FederatedApplicationComponentsAttribute = "FederatedApplicationComponents"
)

func ComponentsFromRequest(req *restful.Request) ([]interface{}, error) {
	componentsAttr := req.Attribute(FederatedApplicationComponentsAttribute)
	if componentsAttr == nil {
		return nil, errors.NewInternalError(fmt.Errorf("get components from request failed"))
	}

	components, ok := componentsAttr.([]interface{})
	if !ok {
		return nil, errors.NewInternalError(fmt.Errorf("get components from request failed"))
	}
	return components, nil
}
