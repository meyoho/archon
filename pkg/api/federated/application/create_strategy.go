package application

import (
	"alauda.io/archon/pkg/api/application"
	"alauda.io/archon/pkg/api/application/v1beta1"
	"alauda.io/archon/pkg/api/federated"
	"alauda.io/archon/pkg/api/rest"
	"alauda.io/archon/pkg/kubernetes"
	"context"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/validation/field"
)

type CreateStrategy struct {
	application.CreateStrategy
}

// PrepareForCreate prepares the application creation.
func (cs CreateStrategy) PrepareForCreate(ctx context.Context, obj runtime.Object) error {
	config, _ := rest.APIConfigFromContext(ctx)
	fApp := obj.(*unstructured.Unstructured)
	application.AddAppNameLabel(fApp, config.LabelBaseDomain, fApp.GetName(), fApp.GetNamespace())
	err := VisitApplicationTemplate(ctx, obj, func(ctx context.Context, appObj runtime.Object) interface{} {
		app := appObj.(*v1beta1.Application)
		app.SetName(fApp.GetName())
		app.SetNamespace(fApp.GetNamespace())
		kubernetes.AddLabels(app, map[string]string{
			federated.IsFederatedKey(config.LabelBaseDomain): "true",
		})
		return cs.CreateStrategy.PrepareForCreate(ctx, appObj)
	})
	if err != nil {
		return err.(error)
	}
	return nil
}

// Validate validates the application creation request.
func (cs CreateStrategy) Validate(ctx context.Context, obj runtime.Object) field.ErrorList {
	fApp := obj.(*unstructured.Unstructured)
	transformComponentsTemplate(fApp, ctx)
	err := VisitApplicationTemplate(ctx, obj, func(ctx context.Context, obj runtime.Object) interface{} {
		return cs.CreateStrategy.Validate(ctx, obj)
	})
	if err != nil {
		return err.(field.ErrorList)
	}
	return nil
}

// AfterCreate called after the application created successfully.
func (CreateStrategy) AfterCreate(ctx context.Context, obj runtime.Object) (err error) {
	req, _ := rest.RequestFromContext(ctx)
	config, _ := rest.APIConfigFromContext(ctx)
	log := config.Logger

	cb, err := rest.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	federatedComponents, err := ComponentsFromRequest(req)
	if err != nil {
		return
	}
	components, err := application.ComponentsFromRequest(req)
	if err != nil {
		return
	}

	fedAppObj := obj.(*unstructured.Unstructured)
	ri, err := cb.NamespacedDynamicClient(federated.FederatedResourceType("applications"), federated.FederatedTypesGroupVersion, fedAppObj.GetNamespace())
	if err != nil {
		return errors.NewInternalError(err)
	}
	overrides, err := waitForAppliationPropagation(config, req, ri, fedAppObj)
	if err != nil {
		return
	}

	// Set the overrideMap of the components
	nfc := mergeFederatedComponents(components, federatedComponents, overrides)
	unstructured.SetNestedSlice(fedAppObj.Object, nfc, "spec", "template", "spec", "componentTemplates")

	log.Infof("Creating application components for %s/%s.", fedAppObj.GetNamespace(), fedAppObj.GetName())
	err = CreateFederatedApplicationComponents(config, cb, fedAppObj, nfc)
	if err != nil {
		// Failed
		log.Errorf("Create application components for %s/%s error: %v", fedAppObj.GetNamespace(), fedAppObj.GetName(), err)
		errPhaseUpdate := updateApplicationPhase(ri, fedAppObj, v1beta1.Failed)
		if errPhaseUpdate != nil {
			return errors.NewInternalError(errPhaseUpdate)
		}
		return err
	}

	// Succeeded
	log.Infof("Create application components for %s/%s succeeded.", fedAppObj.GetNamespace(), fedAppObj.GetName())
	errPhaseUpdate := updateApplicationPhase(ri, fedAppObj, v1beta1.Succeeded)
	if errPhaseUpdate != nil {
		return errors.NewInternalError(errPhaseUpdate)
	}

	return nil
}
