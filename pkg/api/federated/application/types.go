package application

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type FederatedApplicationComponentsSpec struct {
	Components []unstructured.Unstructured `json:"components"`
}

type FederatedApplicationComponents struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec FederatedApplicationComponentsSpec `json:"spec,omitempty"`
}
