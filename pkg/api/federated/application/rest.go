package application

import (
	"encoding/json"
	"k8s.io/client-go/dynamic"
	"net/http"
	"sort"

	"alauda.io/archon/pkg/api/application"
	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/federated"
	"alauda.io/archon/pkg/api/rest"
	k8s "alauda.io/archon/pkg/kubernetes"

	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	fedv1b1 "sigs.k8s.io/kubefed/pkg/apis/core/v1beta1"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
		Handler: *rest.NewHandler(
			cfg,
			CreateStrategy{},
			UpdateStrategy{},
			func() runtime.Object { return &unstructured.Unstructured{} },
			func() schema.GroupVersionResource {
				return schema.GroupVersionResource{
					Group:    federated.FederatedTypesGroup,
					Version:  federated.FederatedTypesVersion,
					Resource: federated.FederatedResourceType("applications"),
				}
			},
			func() bool { return true },
		),
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	clusterParam := ws.PathParameter("cluster", "The name of the kubernetes cluster").
		DataType("string")
	nsParam := ws.PathParameter("namespace", "The namespace of the federated application").
		DataType("string")
	nameParam := ws.PathParameter("name", "The name of the federated application").
		DataType("string")
	ws.Route(
		ws.POST("{cluster}/namespaces/{namespace}/federatedapplications").
			To(r.CreateApplication).
			Doc("Create a federated application").
			Param(clusterParam).
			Param(nsParam).
			Returns(http.StatusOK, "Create success", federated.FederatedType{}).
			Reads(fedv1b1.FederatedTypeConfig{}).
			Produces(restful.MIME_JSON).Consumes(restful.MIME_JSON),
	).Route(
		ws.PUT("{cluster}/namespaces/{namespace}/federatedapplications/{name}").
			To(r.UpdateApplication).
			Doc("Update a federated application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusOK, "Update success", federated.FederatedType{}).
			Reads(fedv1b1.FederatedTypeConfig{}).
			Produces(restful.MIME_JSON).Consumes(restful.MIME_JSON),
	).Route(
		ws.GET("{cluster}/namespaces/{namespace}/federatedapplications/{name}/components").
			To(r.WrapError(r.GetApplicationComponents)).
			Doc("Get components of a federated application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusOK, "Get success", FederatedApplicationComponents{}).
			Produces(restful.MIME_JSON),
	).Route(
		ws.POST("{cluster}/namespaces/{namespace}/applications/{name}/federate").
			To(r.WrapError(r.FederateApplication)).
			Doc("Federate an application").
			Param(clusterParam).
			Param(nsParam).
			Param(nameParam).
			Returns(http.StatusCreated, "Federate success", federated.FederatedType{}).
			Produces(restful.MIME_JSON).Consumes("*/*"),
	)
}

// CreateApplication handles the federated application creation rest request.
func (r *REST) CreateApplication(req *restful.Request, res *restful.Response) {
	r.Create(r.ContextFromRequest(req), req, res)
}

// UpdateApplication handles the federated application update rest request.
func (r *REST) UpdateApplication(req *restful.Request, res *restful.Response) {
	req.SetAttribute(application.ApplicationComponentStorageQueryFunction, application.ComponentStorageQueryFunc(getApplicationComponentsFromStorage))
	r.Update(r.ContextFromRequest(req), req, res)
}

// GetApplicationComponents returns components under the federated applications.
func (r *REST) GetApplicationComponents(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	ri, err := r.ClientFromRequest(req, namespace)
	if err != nil {
		return
	}
	client := req.Attribute(rest.ClientAttribute).(k8s.ClientBuilderInterface)

	appUnstruct, err := ri.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	allresources, err := getFederatedApplicationComponents(client, appUnstruct)
	if err != nil {
		return
	}

	allresources = append(allresources, *appUnstruct)
	//sort by kind and compare name if kind is equal
	sort.Slice(allresources, func(i, j int) bool {
		return allresources[i].GetKind() < allresources[j].GetKind() || (allresources[i].GetKind() == allresources[j].GetKind() && allresources[i].GetName() < allresources[j].GetName())
	})
	res.WriteAsJson(&FederatedApplicationComponents{
		TypeMeta: metav1.TypeMeta{
			APIVersion: federated.FederatedTypesGroupVersion,
			Kind:       "FederatedApplicationComponents",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: FederatedApplicationComponentsSpec{
			Components: allresources,
		},
	})
	return
}

// FederateApplication returns components under the federated applications.
func (r *REST) FederateApplication(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusCreated
	cluster := req.PathParameter("cluster")
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	_, host, err := federated.GetFederatedHostCluster(cluster)
	if err != nil {
		return
	}

	cb, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}

	c, err := cb.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: "app.k8s.io",
		Kind:  "Application",
	}, namespace)
	if err != nil {
		return
	}

	appu, err := c.Get(name, metav1.GetOptions{})
	if err != nil {
		return
	}

	err = r.makeApplicationTemplate(cb, appu)
	if err != nil {
		return
	}

	fapp := &federated.FederatedType{}
	fapp.Kind = "FederatedApplication"
	fapp.APIVersion = federated.FederatedTypesGroupVersion
	fapp.Name = appu.GetName()
	fapp.Namespace = appu.GetNamespace()
	fapp.Spec.Placement.ClusterSelector = &metav1.LabelSelector{}
	fapp.Spec.Template = *appu
	fapp.Spec.Overrides = []federated.FederatedTypeOverride{}

	raw, err := json.Marshal(fapp)
	if err != nil {
		return
	}

	req.SetAttribute(rest.RequestBodyHackAttribute, raw)
	req.PathParameters()["cluster"] = host
	r.CreateApplication(req, res)
	return
}

func (r *REST) makeApplicationTemplate(cb k8s.ClientBuilderInterface, app *unstructured.Unstructured) (err error) {
	appO, err := application.UnstructuredToApplication(app)
	if err != nil {
		return
	}

	components, err := application.GetApplicationComponentsFromStorage(cb, appO)
	if err != nil {
		return
	}
	fedcomps := []interface{}{}
	for _, c := range components {
		if !IsFederateEnabledForResourceKind(c.GetKind()) {
			err = removeComponentOwner(cb, &c)
			if err != nil {
				return
			}
			continue
		}
		c := c
		k8s.CleanAutogenerated(config.GetConfig().LabelBaseDomain, &c, k8s.CleanPodControllerAutogenerated)
		fedcomps = append(fedcomps, map[string]interface{}{
			"resource": c.Object,
		})
	}
	k8s.CleanAutogenerated(config.GetConfig().LabelBaseDomain, app)
	err = unstructured.SetNestedField(app.Object, fedcomps, "spec", "componentTemplates")
	return
}

func removeComponentOwner(cb k8s.ClientBuilderInterface, c *unstructured.Unstructured) (err error) {
	// Remove the owner reference with the application
	var gv schema.GroupVersion
	gv, err = schema.ParseGroupVersion(c.GetAPIVersion())
	if err != nil {
		return
	}
	var client dynamic.ResourceInterface
	client, err = cb.NamespacedDynamicClientByGroupKind(metav1.GroupKind{
		Group: gv.Group,
		Kind:  c.GetKind(),
	}, c.GetNamespace())
	if err != nil {
		return
	}
	err = unstructured.SetNestedSlice(c.Object, []interface{}{}, "metadata", "ownerReferences")
	if err != nil {
		return
	}
	_, err = client.Update(c, metav1.UpdateOptions{})
	return
}
