package application

var federateComponentResourceWhitelist = map[string]bool{
	"Deployment":  true,
	"StatefulSet": true,
	"ConfigMap":   true,
	"Secret":      true,
	"Service":     true,
}

// IsFederateEnabledForResourceKind returns true if the resource kind is enabled for federating.
func IsFederateEnabledForResourceKind(kind string) bool {
	return federateComponentResourceWhitelist[kind]
}
