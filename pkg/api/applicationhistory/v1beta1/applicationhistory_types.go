package v1beta1

import (
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ResourceChangeAction string

const (
	ResourceCreate ResourceChangeAction = "create"
	ResourceUpdate ResourceChangeAction = "update"
	ResourceDelete ResourceChangeAction = "delete"
)

// ApplicationHistorySpec defines the desired state of ApplicationHistory
// +k8s:openapi-gen=true
type ApplicationHistorySpec struct {
	// Revision is the version of the application history
	Revision int `json:"revision,omitempty"`

	// YAML is the yaml content of the change on application
	YAML string `json:"yaml,omitempty"`

	// User is the user name who invoke the change request on application
	User string `json:"user,omitempty"`

	// CreationTimestamp is the creation time of the application history
	CreationTimestamp metav1.Time `json:"creationTimestamp,omitempty"`

	// ChangeCause is the cause description on change of the application
	ChangeCause string `json:"changeCause,omitempty"`

	// ResourceDiffs is the diffs of resource changes with last revision
	// the key of the map is the action of change, include:
	//   - create
	//   - update
	//   - delete
	ResourceDiffs map[ResourceChangeAction][]ResourceDiffItem `json:"resourceDiffs,omitempty"`
}

type ResourceDiffItem struct {
	// Kind is the kind of resource
	Kind string `json:"kind,omitempty"`

	// Name is the name of resource
	Name string `json:"name,omitempty"`
}

// ApplicationHistoryStatus defines the observed state of ApplicationHistory
// +k8s:openapi-gen=true
type ApplicationHistoryStatus struct {
	ObservedGeneration int64 `json:"observedGeneration,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ApplicationHistory is the Schema for the applicationhistories API
// +k8s:openapi-gen=true
type ApplicationHistory struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApplicationHistorySpec   `json:"spec,omitempty"`
	Status ApplicationHistoryStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ApplicationHistoryList contains a list of ApplicationHistory
type ApplicationHistoryList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ApplicationHistory `json:"items"`
}

var ApplicationHistoryCRD = v1beta1.CustomResourceDefinition{
	TypeMeta: metav1.TypeMeta{
		Kind: "CustomResourceDefinition",
	},
	ObjectMeta: metav1.ObjectMeta{
		Name: "applicationhistories.app.k8s.io",
	},
	Spec: v1beta1.CustomResourceDefinitionSpec{
		Group:   "app.k8s.io",
		Version: "v1beta1",
		Names: v1beta1.CustomResourceDefinitionNames{
			Kind:   "ApplicationHistory",
			Plural: "applicationhistories",
		},
		Scope: "Namespaced",
		Validation: &v1beta1.CustomResourceValidation{
			OpenAPIV3Schema: &v1beta1.JSONSchemaProps{
				Type: "object",
				Properties: map[string]v1beta1.JSONSchemaProps{
					"apiVersion": {
						Type: "string",
					},
					"kind": {
						Type: "string",
					},
					"metadata": {
						Type: "object",
					},
					"spec": {
						Type: "object",
						Properties: map[string]v1beta1.JSONSchemaProps{
							"revision": {
								Type: "integer",
							},
							"yaml": {
								Type: "string",
							},
							"user": {
								Type: "string",
							},
							"creationTimestamp": {
								Type:   "string",
								Format: "date-time",
							},
							"changeCause": {
								Type: "string",
							},
							"resourceDiffs": {
								Type: "object",
								Properties: map[string]v1beta1.JSONSchemaProps{
									string(ResourceCreate): {
										Type: "array",
										Items: &v1beta1.JSONSchemaPropsOrArray{
											Schema: &v1beta1.JSONSchemaProps{
												Type: "object",
												Properties: map[string]v1beta1.JSONSchemaProps{
													"kind": {
														Type: "string",
													},
													"name": {
														Type: "string",
													},
												},
											},
										},
									},
									string(ResourceUpdate): {
										Type: "array",
										Items: &v1beta1.JSONSchemaPropsOrArray{
											Schema: &v1beta1.JSONSchemaProps{
												Type: "object",
												Properties: map[string]v1beta1.JSONSchemaProps{
													"kind": {
														Type: "string",
													},
													"name": {
														Type: "string",
													},
												},
											},
										},
									},
									string(ResourceDelete): {
										Type: "array",
										Items: &v1beta1.JSONSchemaPropsOrArray{
											Schema: &v1beta1.JSONSchemaProps{
												Type: "object",
												Properties: map[string]v1beta1.JSONSchemaProps{
													"kind": {
														Type: "string",
													},
													"name": {
														Type: "string",
													},
												},
											},
										},
									},
								},
							},
						},
					},
					"status": {
						Type: "object",
						Properties: map[string]v1beta1.JSONSchemaProps{
							"observedGeneration": {
								Type:   "integer",
								Format: "int64",
							},
						},
					},
				},
			},
		},
	},
}
