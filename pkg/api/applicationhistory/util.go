package applicationhistory

import (
	"encoding/json"

	"alauda.io/archon/pkg/api/applicationhistory/v1beta1"

	"github.com/ghodss/yaml"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// GetObjectsInAppHistory returns the KubernetesObject array in the application history YAML.
func GetObjectsInAppHistory(obj *v1beta1.ApplicationHistory) ([]unstructured.Unstructured, error) {
	var res []unstructured.Unstructured
	err := yaml.Unmarshal([]byte(obj.Spec.YAML), &res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// UnstructuredToApplicationHistory converts an Unstructured object to ApplicationHistory object.
func UnstructuredToApplicationHistory(obj *unstructured.Unstructured) (*v1beta1.ApplicationHistory, error) {
	res := &v1beta1.ApplicationHistory{}
	bytes, err := obj.MarshalJSON()
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

// AppHistoryRevision returns application revision of an application.
func AppHistoryRevision(obj *unstructured.Unstructured) int {
	revision, ok, err := unstructured.NestedInt64(obj.Object, "spec", "revision")
	if err != nil || !ok {
		return 0
	}
	return int(revision)
}
