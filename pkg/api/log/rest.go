package log

import (
	"net/http"
	"strconv"

	"alauda.io/archon/pkg/api/config"
	"alauda.io/archon/pkg/api/core"
	"alauda.io/archon/pkg/api/rest"
	"github.com/emicklei/go-restful"
)

// REST handles the resource rest request.
type REST struct {
	rest.Handler
	cfg *config.APIConfig
}

// NewREST returns an resource REST object.
func NewREST(cfg *config.APIConfig) *REST {
	return &REST{
		cfg: cfg,
	}
}

// InstallRoutes implements the APIRoutesInterface.
func (r *REST) InstallRoutes(version core.APIVersion, ws *restful.WebService) {
	ws.Route(
		ws.GET("{cluster}/namespaces/{namespace}/pods/{name}/{container}/log").To(r.WrapError(r.QueryPodLogs)).
			Doc("Get designative Pod's container log").
			Param(restful.PathParameter("cluster", "cluster").DataType("string")).
			Param(restful.PathParameter("namespace", "namespace").DataType("string")).
			Param(restful.PathParameter("name", "pod name").DataType("string")).
			Param(restful.PathParameter("container", "container name").DataType("string")).
			Param(restful.QueryParameter("logFilePosition", "logFilePosition is log file position ").DataType("integer")).
			Param(restful.QueryParameter("referenceTimestamp", "referenceTimestamp options[newest,oldest]").DataType("string")).
			Param(restful.QueryParameter("referenceLineNum", "reference Line Number").DataType("integer")).
			Param(restful.QueryParameter("offsetFrom", "offsetFrom").DataType("integer")).
			Param(restful.QueryParameter("offsetTo", "offsetTo").DataType("integer")).
			Param(restful.QueryParameter("previous", "previous").DataType("bool")).
			Returns(200, "OK", LogDetails{}).
			Produces(restful.MIME_JSON),
	)
}

//QueryPodLogs is query pod's container  logs
func (r *REST) QueryPodLogs(req *restful.Request, res *restful.Response) (statusCode int, err error) {
	statusCode = http.StatusOK
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	containerName := req.PathParameter("container")

	//QueryParams
	logFilePosition := req.QueryParameter("logFilePosition")
	usePreviousLogs := req.QueryParameter("previous") == "true"
	referenceTimestamp := req.QueryParameter("referenceTimestamp")
	if referenceTimestamp == "" {
		referenceTimestamp = NewestTimestamp
	}

	referenceLineNum, err := strconv.Atoi(req.QueryParameter("referenceLineNum"))
	if err != nil {
		referenceLineNum = 0
		err = nil
	}
	offsetFrom, err1 := strconv.Atoi(req.QueryParameter("offsetFrom"))

	offsetTo, err2 := strconv.Atoi(req.QueryParameter("offsetTo"))
	logSelector := DefaultSelection
	if err1 == nil && err2 == nil {
		logSelector = &Selection{
			ReferencePoint: LogLineId{
				LogTimestamp: LogTimestamp(referenceTimestamp),
				LineNum:      referenceLineNum,
			},
			OffsetFrom:      offsetFrom,
			OffsetTo:        offsetTo,
			LogFilePosition: logFilePosition,
		}
	}

	// Get Clientset and Get logs
	clientBuilder, err := r.cfg.ClientBuilderFromRequest(req)
	if err != nil {
		return
	}
	client, err := clientBuilder.ClientSet()
	if err != nil {
		return
	}
	result, err := GetLogDetails(client, namespace, name, containerName, logSelector, usePreviousLogs)
	if err != nil {
		return
	}
	res.WriteAsJson(result)
	return
}
