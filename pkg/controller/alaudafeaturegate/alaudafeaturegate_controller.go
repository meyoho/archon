/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package alaudafeaturegate

import (
	fgv1 "alauda.io/archon/pkg/api/feature_gate/v1"
	"alauda.io/archon/pkg/kubernetes"
	"alauda.io/archon/pkg/util"
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"sync"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/klog"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

type SyncState int

const (
	FeatureGateTargetNamespace = "kube-public"
	ErebusEndpointTemplate     = "https://erebus/kubernetes/%s"
)

// Add creates a new AlaudaFeatureGate Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileAlaudaFeatureGate{
		Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("alaudafeaturegate-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to AlaudaFeatureGate
	err = c.Watch(&source.Kind{Type: &fgv1.AlaudaFeatureGate{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	mgr.AddHealthzCheck("controller", func(req *http.Request) error {
		return nil
	})

	mgr.AddReadyzCheck("controller", func(req *http.Request) error {
		return nil
	})

	return nil
}

var _ reconcile.Reconciler = &ReconcileAlaudaFeatureGate{}

// ReconcileAlaudaFeatureGate reconciles a AlaudaFeatureGate object
type ReconcileAlaudaFeatureGate struct {
	client.Client
	scheme *runtime.Scheme
	config rest.Config
}

func (r *ReconcileAlaudaFeatureGate) InjectConfig(cfg *rest.Config) error {
	r.config = *cfg
	return nil
}

// Reconcile reads that state of the cluster for a AlaudaFeatureGate object and makes changes based on the state read
// and what is in the AlaudaFeatureGate.Spec
func (r *ReconcileAlaudaFeatureGate) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Fetch the AlaudaFeatureGate instance
	instance := &fgv1.AlaudaFeatureGate{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	klog.V(1).Infof("AlaudaFeatureGate %q changed, try to sync to the business clusters.", request.NamespacedName.String())
	err = r.syncFeatureGate(instance)
	if err != nil {
		return reconcile.Result{
			Requeue: true,
		}, err
	}

	return reconcile.Result{}, nil
}

func (r *ReconcileAlaudaFeatureGate) getClustersInfo() (cinfo map[string]string, err error) {
	di, err := dynamic.NewForConfig(&r.config)
	if err != nil {
		return
	}

	ri := di.Resource(schema.GroupVersionResource{
		Group:    "clusterregistry.k8s.io",
		Version:  "v1alpha1",
		Resource: "clusters",
	}).Namespace(util.PodNamespace())
	sri := di.Resource(schema.GroupVersionResource{
		Version:  "v1",
		Resource: "secrets",
	})

	clusters, err := ri.List(metav1.ListOptions{})
	if err != nil {
		return
	}
	cinfo = map[string]string{}
	for _, c := range clusters.Items {
		ts, ok, err := unstructured.NestedMap(c.Object, "spec", "authInfo", "controller")
		if err != nil {
			klog.Errorf("Get secret from cluster %q error: %v", c.GetName(), err)
			continue
		}
		if !ok {
			klog.Errorf("Secret not found in cluster %q", c.GetName())
			continue
		}
		s, err := sri.Namespace(ts["namespace"].(string)).Get(ts["name"].(string), metav1.GetOptions{})
		if err != nil {
			klog.Errorf("Get secret %q in %q error: %v", ts["name"], ts["namespace"], err)
			continue
		}
		token64, ok, err := unstructured.NestedString(s.Object, "data", "token")
		if !ok {
			klog.Errorf("Token not found in secret %q", s.GetName())
			continue
		}
		token, err := base64.StdEncoding.DecodeString(token64)
		if err != nil {
			klog.Errorf("Token decode error: %v", err)
			continue
		}
		cinfo[c.GetName()] = string(token)
	}
	return
}

func (r *ReconcileAlaudaFeatureGate) syncFeatureGate(fg *fgv1.AlaudaFeatureGate) (err error) {
	cinfo, err := r.getClustersInfo()
	if err != nil {
		return
	}

	wg := sync.WaitGroup{}
	for c, t := range cinfo {
		wg.Add(1)
		c := c
		t := t
		go func() {
			defer wg.Done()
			propagateFeatureGates(&r.config, c, t, fg)
		}()
	}
	wg.Wait()
	return
}

func propagateFeatureGates(config *rest.Config, c, t string, fg *fgv1.AlaudaFeatureGate) {
	ccfg := *config
	ccfg.Host = fmt.Sprintf(ErebusEndpointTemplate, c)
	ccfg.BearerToken = t
	ccfg.BearerTokenFile = ""
	ccfg.CAData = nil
	ccfg.CAFile = ""
	ccfg.Insecure = true
	cdi, err := dynamic.NewForConfig(&ccfg)
	if err != nil {
		klog.Errorf("Create dynamic interface for %q error: %v", c, err)
		return
	}
	ufg, err := kubernetes.ObjectToUnstructured(fg)
	if err != nil {
		klog.Errorf("Convert to unstructured error: %v", err)
		return
	}
	klog.V(1).Infof("Creating feature gate %q on %q", ufg.GetName(), c)
	client := cdi.Resource(schema.GroupVersionResource{
		Group:    fgv1.AlaudaFeatureGateGroup,
		Version:  "v1",
		Resource: fgv1.AlaudaFeatureGateResource,
	}).Namespace(FeatureGateTargetNamespace)
	ufg.SetUID("")
	ufg.SetResourceVersion("")
	ufg.SetNamespace(FeatureGateTargetNamespace)
	_, err = client.Create(ufg, metav1.CreateOptions{})
	if err == nil {
		return
	}
	if errors.IsAlreadyExists(err) {
		klog.V(1).Infof("Already existed! Updating feature gate %q on %q", ufg.GetName(), c)
		cfg, err := client.Get(ufg.GetName(), metav1.GetOptions{})
		if err != nil {
			klog.Errorf("Get feature gate %q on %q error: %v", ufg.GetName(), c, err)
			return
		}
		ufg.SetUID(cfg.GetUID())
		ufg.SetResourceVersion(cfg.GetResourceVersion())
		_, err = client.Update(ufg, metav1.UpdateOptions{})
		if err != nil {
			klog.Errorf("Update feature gate %q on %q error: %v", ufg.GetName(), c, err)
			return
		}
	} else {
		klog.Errorf("Create feature gate %q on %q error: %v", ufg.GetName(), c, err)
	}
}
