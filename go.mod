module alauda.io/archon

go 1.12

require (
	bitbucket.org/mathildetech/alauda-backend v0.1.24
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/log v1.0.5
	github.com/alauda/cyborg v0.5.4 // indirect
	github.com/emicklei/go-restful v2.9.6+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/gsamokovarov/assert v0.0.0-20180414063448-8cd8ab63a335
	github.com/juju/errors v0.0.0-20190207033735-e65537c515d7
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20190429233213-dfc56b8c09fc // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/magiconair/properties v1.8.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/prometheus/client_golang v1.0.0
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/spf13/cast v1.3.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.3.2
	github.com/thoas/go-funk v0.4.0
	github.com/yudai/gojsondiff v1.0.0
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	go.uber.org/zap v1.10.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
	gotest.tools v2.2.0+incompatible
	k8s.io/api v0.0.0-20191115135540-bbc9463b57e5
	k8s.io/apiextensions-apiserver v0.0.0-20191117020858-b615a37f53e7
	k8s.io/apimachinery v0.0.0-20191116203941-08e4eafd6d11
	k8s.io/apiserver v0.0.0-20191116100221-f2537b84c964
	k8s.io/client-go v11.0.0+incompatible
	k8s.io/component-base v0.0.0-20191115220130-ea09a2678486
	k8s.io/klog v1.0.0
	k8s.io/kubectl v0.0.0-20190312160839-d28510b1b750 // indirect
	sigs.k8s.io/controller-runtime v0.4.0
	sigs.k8s.io/kubefed v0.1.0-rc6.0.20191109062243-8eec788a86fb
)

replace k8s.io/client-go v11.0.0+incompatible => k8s.io/client-go v0.0.0-20190918160344-1fbdaa4c8d90

replace sigs.k8s.io/controller-runtime v0.4.0 => sigs.k8s.io/controller-runtime v0.3.1-0.20191011155846-b2bc3490f2e3

replace github.com/alauda/cyborg v0.5.4 => github.com/alauda/cyborg v0.5.5-0.20200102173037-19bf217bf8f3
