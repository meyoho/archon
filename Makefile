# Copyright 2019 THL A29 Limited, a Tencent company.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.PHONY: all
all: lint test

# ==============================================================================
# Build Options

ROOT_PACKAGE=alauda.io/archon
VERSION_PACKAGE=bitbucket.org/mathildetech/app/version

# set the shell to bash in case some environments use sh
SHELL := /bin/bash
GO ?= $(shell which go)
PACKAGES ?= $(shell $(GO) list ./...)
GOFILES := $(shell find . -name "*.go" -type f -not -path "./vendor/*")
GOFMT ?= gofmt "-s"


# ==============================================================================
# Includes

include build/lib/common.mk
include build/lib/help.mk
include build/lib/golang.mk
include build/lib/image.mk
include build/lib/deploy.mk

# ==============================================================================
# Tasks

.PHONY: build
build:
	@$(MAKE) go.build

.PHONY: build.all
build.all:
	@$(MAKE) go.build.all

.PHONY: image
image:
	@$(MAKE) image.build

.PHONY: deploy
deploy:
	@$(MAKE) deploy.run

.PHONY: deploy.all
deploy.all:
	@$(MAKE) deploy.run.all

.PHONY: fmt
fmt:
	@$(GOFMT) -w $(GOFILES)

.PHONY: fmt-check
fmt-check:
	@diff=$$($(GOFMT) -d $(GOFILES)); \
	if [ -n "$$diff" ]; then \
		echo "Please run 'make fmt' and commit the result:"; \
		echo "$${diff}"; \
		exit 1; \
	fi;

.PHONY: vet
vet:
	$(GO) vet $(PACKAGES)

.PHONY: lint
lint:
	@hash revive > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		$(GO) get -u github.com/mgechev/revive; \
	fi
	@revive -config ./build/linter/revive.toml -exclude ./vendor/... -exclude ./pkg/api/application/v1beta1/... -exclude ./pkg/api/applicationhistory/v1beta1/... ./... || exit 1

.PHONY: test
test: fmt-check
	@mkdir -p ./output
	$(GO) test -v -cover -coverprofile ./output/coverage.out $(PACKAGES) && $(GO) tool cover -func ./output/coverage.out || exit 1

.PHONY: clean
clean:
	$(GO) clean -x -i ./...
	@rm -rf ./output/
